ok#!/bin/bash
pdfnup Main.pdf --nup 1x1 --pages 27-29  --outfile pdfs/capitulo01.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo1.pdf pdfs/white.pdf pdfs/capitulo01.pdf

pdfnup Main.pdf --nup 1x1 --pages 31-35  --outfile pdfs/capitulo02.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo2.pdf pdfs/white.pdf pdfs/capitulo02.pdf

pdfnup Main.pdf --nup 1x1 --pages 35-116  --outfile pdfs/capitulo03.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo3.pdf pdfs/white.pdf pdfs/capitulo03.pdf

pdfnup Main.pdf --nup 1x1 --pages 117-204  --outfile pdfs/capitulo04.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo4.pdf pdfs/white.pdf pdfs/capitulo04.pdf

pdfnup Main.pdf --nup 1x1 --pages 205-208  --outfile pdfs/capitulo05.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo5.pdf pdfs/white.pdf pdfs/capitulo05.pdf

pdfnup Main.pdf --nup 1x1 --pages 209-211  --outfile pdfs/capitulo06.pdf
pdfjoin --fitpaper true --outfile pdfs/capitulo6.pdf pdfs/white.pdf pdfs/capitulo06.pdf

pdfnup Main.pdf --nup 1x1 --pages 212-217  --outfile pdfs/apendiceAA.pdf
pdfjoin --fitpaper true --outfile pdfs/apendiceA.pdf pdfs/white.pdf pdfs/apendiceAA.pdf

pdfnup Main.pdf --nup 1x1 --pages 219-236  --outfile pdfs/apendiceBB.pdf
pdfjoin --fitpaper true --outfile pdfs/apendiceB.pdf pdfs/white.pdf pdfs/apendiceBB.pdf

pdfnup Main.pdf --nup 1x1 --pages 237-254  --outfile pdfs/apendiceCC.pdf
pdfjoin --fitpaper true --outfile pdfs/apendiceC.pdf pdfs/white.pdf pdfs/apendiceCC.pdf

pdfnup Main.pdf --nup 1x1 --pages 255-259  --outfile pdfs/apendiceDD.pdf
pdfjoin --fitpaper true --outfile pdfs/apendiceD.pdf pdfs/white.pdf pdfs/apendiceDD.pdf

rm pdfs/capitulo01.pdf
rm pdfs/capitulo02.pdf
rm pdfs/capitulo03.pdf
rm pdfs/capitulo04.pdf
rm pdfs/capitulo05.pdf
rm pdfs/capitulo06.pdf
rm pdfs/apendiceAA.pdf
rm pdfs/apendiceBB.pdf
rm pdfs/apendiceCC.pdf
rm pdfs/apendiceDD.pdf
