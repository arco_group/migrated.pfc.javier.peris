# -*- coding: iso-8859-15; tab-width:4 -*-

#  This file defines the current version of the core SDocente code being used.

__version__ = "$Revision: 282 $"
__date_lastVersion = "$1/08/2009"
