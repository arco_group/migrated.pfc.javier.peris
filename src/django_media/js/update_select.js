var ajax = {
   init: function() {
	// Grab the elements we'll need.
	ajax.exams = document.getElementById('exams');
	ajax.questions = document.getElementById('questionslist');
	ajax.form = document.getElementsByTagName('form');
	ajax.items = ajax.questions.getElementsByTagName('option');
	ajax.qprp = document.getElementById('ques');
	YAHOO.util.Event.addListener(ajax.exams, 'change', ajax.onchange_func);
	YAHOO.util.Event.addListener(ajax.form, 'submit', ajax.submit_func);
	YAHOO.util.Event.addListener(ajax.qprp, 'click', ajax.click_func);
   },

//    click_func: function(e){
//       YAHOO.util.Event.preventDefault(e);
//       var page = document.getElementById('page');
//       var result_fade_out = new YAHOO.util.Anim(page, {
// 	      opacity: {to: 0.2}}, 1.5,
// 	  YAHOO.util.Easing.easeOut);
//       var result_fade_in = new YAHOO.util.Anim(examPage, {
// 	      opacity: {to: 1 }
// 	  }, 1, YAHOO.util.Easing.easeIn);
//       result_fade_out.animate();
//       var examPage = document.getElementById('examPage');
//       var Overlay = new YAHOO.widget.Overlay("examPage", {effect:{effect:YAHOO.widget.ContainerEffect.FADE, duration:1.5}});
//       Overlay.center();
//       Overlay.render();
//     },

   submit_func: function(e) {
	var ind_exam = ajax.exams.selectedIndex;
	var ind_ques = ajax.questions.selectedIndex;
	if(ind_exam==-1 && ind_ques==-1){
	    YAHOO.util.Event.preventDefault(e);
	    alert("Debe seleccionar al menos un examen o una pregunta");
	}
	else if(ind_exam==-1 && ind_ques==0){
	    YAHOO.util.Event.preventDefault(e);
	    alert("Debe seleccionar un examen");
	}
	else if(ind_exam!=-1 && ind_ques==-1){
	    YAHOO.util.Event.preventDefault(e);
	    alert("Debe seleccionar todas o alguna pregunta");
	}
    },

   onchange_func: function(e) {
	YAHOO.util.Event.preventDefault(e);
      // Remove any item in the list
      //      var items = document.getElementById("page").childNodes[0].nodeValue;
      //      var it = items.getElementById("wrapper");
      //      alert(ajax.items.length);
      var questions = document.getElementById('questionslist');
      var items = questions.getElementsByTagName('option');

      questions.innerHTML = "";
//       for(var i=0; i<ajax.items.length; i++) {
// 	  ajax.questions.removeChild(ajax.items[i]);
//       }

      for(var i=0; i<items.length; i++){
	  questions.removeChild(items[i]);
      }


      var ind = ajax.exams.selectedIndex;
      var id = ajax.exams.options[ind].value;
      var exams = id + "=" + ajax.exams.options[ind].value;
      var cObj = YAHOO.util.Connect.asyncRequest('POST', "../exam_statistics/",
						 ajax.ajax_callback, exams);
   },

   ajax_callback: {
      success: function(o) {
// 	    var examPage = document.getElementById('examPage');
// 	    var page = document.getElementById('page');
// 	    var result_fade_out = new YAHOO.util.Anim(page, {
// 		    opacity: {to: 0.2}}, 1.5,
// 		YAHOO.util.Easing.easeOut);
// 	    var result_fade_in = new YAHOO.util.Anim(examPage, {
// 		    opacity: {to: 1 }
// 		}, 1, YAHOO.util.Easing.easeIn);
// 	    result_fade_out.animate();
// 	    var Overlay = new YAHOO.widget.Overlay("examPage", {effect:{effect:YAHOO.widget.ContainerEffect.FADE,
// 									duration:1.5}});
// 	    Overlay.center();
// 	    Overlay.render();

	    // This turns the JSON string into a JavaScript object.

	    var response_obj = eval('(' + o.responseText + ')');

	    var questions = document.getElementById('questionslist');
	    var option = document.createElement('option');
	    option.setAttribute("value", 0);
	    option.innerHTML = "Todas";
	    questions.appendChild(option);

	    for (property in response_obj) {
		if(property!=""){
		    var option = document.createElement('option');
		    option.setAttribute("value", response_obj[property]);
		    option.innerHTML = property;
		    questions.appendChild(option);
		}
	    }

	},

      failure: function(o) { // In this example, we shouldn't ever go down this path.
	    alert('An error has occurred');
	}
   },

};

YAHOO.util.Event.addListener(window, 'load', ajax.init);
