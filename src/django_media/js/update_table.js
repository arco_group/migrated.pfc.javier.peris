var ajax = {
   init: function() {
	ajax.eprp = document.getElementById('eprp');
	YAHOO.util.Event.addListener(ajax.eprp, 'change', ajax.onchange_func);
   },

   onchange_func: function(e) {
      YAHOO.util.Event.preventDefault(e);
      ajax.ind = ajax.eprp.selectedIndex;
      var id = ajax.eprp.options[ajax.ind].value;
      var exam = id + "=" + ajax.eprp.options[ajax.ind].value;
      var cObj = YAHOO.util.Connect.asyncRequest('POST',
      						 "../update_paper_exam/",
      						 ajax.ajax_callback, exam);
   },

   submit_func: function(e){
	// alert("Hola");
	for (i in ajax.inputs){
		alert(ajax.inputs[i]);
		if (ajax.inputs[i].value == ""){
		    YAHOO.util.Event.preventDefault(e);
		    alert("Se deben calificar todas las preguntas.");
		}
		else{
		    alert("Esto va a tope");
		}
	}

    },

   ajax_callback: {
      success: function(o) {
// 	    var response_obj = eval('(' + o.responseText + ')');
	    try {
		var response_obj = YAHOO.lang.JSON.parse(o.responseText);
	    }
	    catch (e) {
		alert("Invalid json data");
	    }

	    var candidates = document.getElementById('candidates');


	    for(i=candidates.options.length-1;i>=0;i--){
		candidates.remove(i);
	    }


	    for (key in response_obj){
		val = response_obj[key];
		if(key != "nm"){
		    var opt = document.createElement('option');
		    opt.setAttribute("value", key);
		    opt.text = val;
		    candidates.options.add(opt);
		}
		else{
		    ajax.numques = val
		}
	    }


	    ajax.results = document.getElementsByName('cuerpo')[0];

	    var ult = document.getElementsByName('ult')[0];

	    var tds = document.getElementsByName('options');

	    var tds2 = document.getElementsByName('options2');

	    var trs = document.getElementsByName('newrow');


	    for(var i=0; i<tds.length; i++){
		ult.removeChild(tds[i]);
	    }

	    for(var i=0; i<tds2.length; i++){
		ult.removeChild(tds2[i]);
	    }

	    for(var i=0; i<trs.length; i++){
		ajax.results.removeChild(trs[i]);
	    }

	    ajax.inputs = new Array();

// 	    alert("Numero de preguntas " + response_obj.nm);

	    ajax.numques = response_obj.nm

	    if(ajax.numques > 0){
	        var td = document.createElement('td');
	        td.setAttribute("name", "options");
	        var s = "Pregunta 1";
	        td.setAttribute("value", s);
	        td.setAttribute("align", "center");
	        td.innerHTML = s;
	        ult.appendChild(td);
	        var td2 = document.createElement('td');
	        td2.setAttribute("name", "options2");
	        var input = document.createElement('input');
	        input.setAttribute("name", s);
		input.setAttribute("size", "8px");
	        td2.appendChild(input);
	        ult.appendChild(td2);
	    }

	    i = 2;

	    while (i<=ajax.numques){
	        var tr = document.createElement('tr');
	        if (i>5){
	    	var td1 = document.createElement('td');
	    	var td2 = document.createElement('td');
	    	tr.appendChild(td1);
	    	tr.appendChild(td2);
	        }
	        var td = document.createElement('td');
	        td.setAttribute("align","center")
	        var s = "Pregunta " + i;
	        td.setAttribute("value", s);
	        td.innerHTML = s;
	        ajax.results.appendChild(tr);
	        tr.appendChild(td);
	        var td2 = document.createElement('td');
	        var input = document.createElement('input');
	        input.setAttribute("name", s);
		input.setAttribute("size", "8px");
	        ajax.inputs[i-1] = input;
	        td2.appendChild(input);
	        tr.appendChild(td2);
	        i=i+1;
	    }


	    if(ajax.numques > 0){
		var tr = document.createElement('tr');
		tr.setAttribute("name", "newrow");
		j=0
		if (i<6){
			j=2;
		}

		while(j<3){
			j=j+1
			var td = document.createElement('td');
			tr.appendChild(td);
		}

		var td = document.createElement('td');
		td.setAttribute('align','right');
		ajax.input = document.createElement('input');
		ajax.input.setAttribute("type", "submit");
		ajax.input.setAttribute("value", "Calificar");
		td.appendChild(ajax.input);
		tr.appendChild(td);
		ajax.results.appendChild(tr);
	    }

	    var b = YAHOO.util.Event.addListener(ajax.input, 'submit',
						 ajax.submit_func);

	},

      failure: function(o) {
	    alert('An error has occurred');
	}
   },

};

YAHOO.util.Event.addListener(window, 'load', ajax.init);

