"""This module contains the logic of the page"""
from __future__ import with_statement

# -*- mode: python; coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.contrib import auth
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.utils import simplejson
from django.template import Context#, Template
from django.db.models import *
from django.utils.translation import ugettext as _
from candidateTool.assessmentSystem.models import *

import datetime
import os
import sys
sys.path.append('/home/peris/Desktop/csl2-sdocente' + '/src/Persistencia/')
sys.path.append('/home/peris/Desktop/csl2-sdocente' + '/src/Editor/')
sys.path.append('/home/peris/Desktop/csl2-sdocente' + '/src/AuthoringTool/')

import cairo
import amara
import CairoPlot
import corestats
#import odict

# import editor
import persistencia

AGENT = persistencia.Agente()

TEST = {"1": "Test A", "2": "Test B", "3": "Test C", "4": "Test D",
        "5": "Test E", "6": "Test F", "7": "Test G", "8": "Test H"
        }

QUES = {"1": "Question A", "2": "Question B", "3": "Question C",
        "4": "Question D", "5": "Question E", "6": "Question F",
        "7": "Question G", "8": "Question H"
        }

TEST_QUES = {"1": ["1", "2", "3", "4"],
             "2": ["4", "5", "6", "7"],
             "3": ["1", "2", "7", "8"],
             "4": ["2", "3", "4", "5"],
             "5": ["5", "6", "7", "8"],
             "6": ["3", "4", "5", "6"],
             "7": ["1", "6", "7", "8"],
             "8": ["1", "2", "3", "8"]
             }

A = [0] * 5 + [1] * 15
B = [0] * 34 + [1] * 23
C = [0] * 45 + [1] * 12
D = [0] * 45 + [1] * 34
E = [0] * 43 + [1] * 34
F = [0] * 23 + [1] * 35
G = [0] * 43 + [1] * 76
H = [0] * 34 + [1] * 3

# [Test_id] = {[Ques_id] = [Notas]}
OUTCOME = {"1": {"1": A, "2":H, "3":B, "4": C},
           "2": {"4": B, "5":G, "6":C, "7": D},
           "3": {"1": C, "2":F, "7":D, "8": E},
           "4": {"2": D, "3":E, "4":E, "5": F},
           "5": {"5": E, "6":D, "7":F, "8": G},
           "6": {"3": F, "4":C, "5":G, "6": H},
           "7": {"1": G, "6":B, "7":H, "8": A},
           "8": {"1": H, "2":A, "3":A, "8": B},
           }

FICTITIOUS_DATA = True

PREFIX = "/home/peris/Desktop/csl2-sdocente/"

POST_PER_PAGE = 5

def learner_can_access(user):
    can_access = False
    for group in user.groups.all():
        if group.name == "Alumnos":
            can_access = True
            break
    return user.is_authenticated() and can_access

def lecturer_can_access(user):
    can_access = False
    for group in user.groups.all():
        if group.name == "Profesores":
            can_access = True
            break
    return user.is_authenticated() and can_access

def select_language(request):
    return render_to_response('candidateTool/i18n.html')

def index(request):
    """Show the login page"""
    # if "django_language" in request.COOKIES:
    #     return HttpResponse("The selected language  is %s" % \
    #                             request.COOKIES["django_language"])
    # else:
    #     return HttpResponse("There is not a selected language.")

#    return HttpResponse(request.LANGUAGE_CODE)
    # if request.LANGUAGE_CODE:
    #     return render_to_response('candidateTool/i18n.html',
    #                               {"lang": request.LANGUAGE_CODE})
    # else:
    #     return HttpResponse("Todo falafel")
    # return HttpResponse(request["django_language"])
    return render_to_response('candidateTool/login.html')

def enter(request):
    """If the user and pass are correct log in the user"""
    uid = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=uid, password=password)
    if user is not None and user.is_active:
        auth.login(request, user)
        return render_pages(request, 1)
    else:
        return render_to_response("candidateTool/invalid.html")

def main(request):
    """Show the first page"""
    return render_pages(request, 1)

def render_pages(request, page_id):
    """The exams are divide up in differents 'pages'"""
    if request.user.is_authenticated():
        candidate = Identification.objects.get(user=request.user)

        tests = []
        for sub in candidate.subject.all():
            web_ts = sub.test_set.filter(Q(representation_type="Both") |
                                         Q(representation_type="Web"))
            order_ts = web_ts.order_by("start_date")
            for test in order_ts:
                tests.append(test)

#        numpages = len(tests) / POST_PER_PAGE + 1
        min_page = (int(page_id) - 1) * POST_PER_PAGE
        max_page = int(page_id) * POST_PER_PAGE
#        has_next = False
        pre = None
        next = None
        main = False
        if len(tests) < min_page:
            t = None
        elif len(tests) > max_page:
            t = tests[min_page:max_page]
            # has_next = True
            next = int(page_id) + 1
            if int(page_id) != 1:
                pre = int(page_id) - 1
            else:
                main = True
        elif len(tests) <= max_page:
            t = tests[min_page:]
            if int(page_id) != 1:
                pre = int(page_id) - 1
            else:
                main = True

        ts = {}

        if t:
            s = t[0].subject.name
            ed = is_exam_done(t[0].id, candidate)
            ts[s] = [{t[0]: [ed, get_mark(candidate, str(t[0].id))]}]

        # path = "src/django_templates/candidateTool/exams/hechos.txt"
        # with open(PREFIX + path, "w") as fd:
        #     for e in t[1:]:
        #         exam_done = is_exam_done(e.id, candidate).__str__()
        #         fd.write(str(e.id) + ":" + exam_done + "\n")
        #         if e.subject.name == s:
        #             ts[s].append({e: [is_exam_done(e.id, candidate),
        #                               get_mark(candidate, str(e.id))]})
        #         else:
        #             s = e.subject.name
        #             ts[s] = [{e: [is_exam_done(e.id, candidate),
        #                           get_mark(candidate, str(e.id))]}]


        # with open(PREFIX + "/src/django_templates/pages.txt", "w") as fd:
        #     fd.write("page_id:")
        #     fd.write(str(page_id))
        #     fd.write("\n")
        #     fd.write("min_page:")
        #     fd.write(str(min_page))
        #     fd.write("\n")
        #     fd.write("max_page:")
        #     fd.write(str(max_page))
        #     fd.write("\n")
        #     fd.write("pre:")
        #     fd.write(str(pre))
        #     fd.write("\n")
        #     fd.write("next:")
        #     fd.write(str(next))
        #     fd.write("\n")
        #     fd.write("main:")
        #     fd.write(main.__str__())

        dem = get_demographics(candidate)
        return render_to_response('candidateTool/new_main.html',
                                  {'candidate': candidate, 'dem': dem,
                                   'tests': ts,'pre': pre,
                                   'next': next, 'main': main})
    else:
        return render_to_response('candidateTool/invalid.html')

@user_passes_test(learner_can_access)
def part(request, part_id):
    """Show the second part and next of the questions"""
    # If there were two equal questions, it remains the latest one
    # for k, val in request.POST.items():
    #     request.session[k] = val
#        with open(PREFIX + "src/django_templates/reporte.txt", "w") as fd:
            #         fd.write(" HAY REQUEST \n")
            #         fd.write("=========")
            #         fd.write("\n")
            #     for k, v in request.POST.items():
            #         fd.write(k)
            #         fd.write(":")
            #         fd.write(v)
            #         fd.write("\n")
            # #     fd.write(" QUESTION  \n")
            # #     fd.write("==========")
            # #     fd.write("\n")
            # #     for k, v in question.items():
            # #         fd.write(str(k))
            # #         fd.write(":")
            # #         fd.write(v[0])
            # #         fd.write(",")
            # #         fd.write(v[1])
            # #         fd.write("\n")
    exam_id = request.session.get("exam_id")
    if exam_id:
        for k, val in request.POST.items():
            request.session[k] = val
        id = Identification.objects.get(user=request.user)
        try:
            name = id.name_set.get(partname="f").text
            surname = id.name_set.get(partname="s").text
        except:
            name = ""
            surname = ""
        test = Test.objects.get(id=request.session["exam_id"])
        num_part = int(part_id) + 1
        fname = test.filename.split(".xml")[0].split("/")[-1] + \
            str(num_part) + ".html"
        html = 'candidateTool/exams/' + fname
        return render_to_response(html, {'name': name, 'surname': surname})
    else:
        return render_to_response('candidateTool/invalid.html')

@user_passes_test(learner_can_access)
def report(request, exam_id):
    """Create a report for the exam_id just did the candidate"""
    for k, val in request.POST.items():
        request.session[k] = val
    response = HttpResponse()
    test = AGENT.get_exam_by_id(exam_id)
    fn = test.filename.split(".xml")[0].split("/")[-1]
    path = get_psd()
    if not path == '/home/peris/Desktop/csl2-sdocente/src/':
        path = '/home/peris/Desktop/csl2-sdocente/src/'
    path = path + "django_templates/candidateTool/"
    path_exams = path + "exams/"
    question = {}
    #question[questionNumber] = [questionName(id), questionType]
    for n in range(0, len(test.parts)):
        fname = fn + str(n + 1) + ".html"
        doc = amara.parse(path_exams + fname)
        for q in doc.xml_xpath(u'//td[contains(@class,"questionNumber")]/p'):
            #Taking the grandparent inmediately above (that's why td[1] is used)
            t = q.parentNode.parentNode.td[1]
            if hasattr(t, "input"):
                num = int(q.xml_child_text)
                question[num] = []
                for i in range(0, 2):
                    if t.input[i].name == "nombre":
                        question[num].append(t.input[i].value)
                    elif t.input[i].name == "type":
                        question[num].append(t.input[i].value)

    path = "src/django_templates/candidateTool/"
    docu = amara.parse(PREFIX + path + "templates/report_template.xml")

    iden = Identification.objects.get(user=request.user)
    context = Context(identification=iden)
    context.save()
    sessionId = SessionIdentifier(sourceId="draper.mine.nu",
                                  identifier="SDocente",
                                  context=context)
    sessionId.save()
    now = datetime.datetime.now()
    testResult = TestResult(identifier=exam_id, dateStamp=now)
    testResult.save()
    assessmentResult = AssessmentResult(context=context, testResult=testResult)
    assessmentResult.save()

    # with open(PREFIX + "src/django_templates/reporte-partes.txt", "w") as fd:
    #     fd.write(" SESSION \n")
    #     fd.write("=========")
    #     fd.write("\n")
    #     for k, val in request.session.items():
    #         fd.write(k)
    #         fd.write(":")
    #         if isinstance(val, long):
    #             fd.write(str(val))
    #         else:
    #             fd.write(val)
    #         fd.write("\n")
    #     fd.write(" QUESTION  \n")
    #     fd.write("==========")
    #     fd.write("\n")
    #     for k, val in question.items():
    #         fd.write(str(k))
    #         fd.write(":")
    #         fd.write(val[0])
    #         fd.write(",")
    #         fd.write(val[1])
    #         fd.write("\n")

    for k in question.keys():
        if request.session.has_key(question[k][0]):
            crValue = request.session[question[k][0]]
            cand_response = CandidateResponse(value=crValue)
            cand_response.save()
            q = ["choiceMultiple", "inlineChoice"]
            id = "RESPONSE"
            if question[k][1] in q:
                card = "single"
                base_type = "identifier"
                rVariable = ResponseVariable(identifier=id, cardinality=card,
                                             baseType=base_type,
                                             candidateResponse=cand_response)
            else:
                card = "single"
                base_type = "string"
                rVariable = ResponseVariable(identifier=id, cardinality=card,
                                             baseType=base_type,
                                             candidateResponse=cand_response)
        else:
            # QUESTION WITH NO ANSWER
            cand_response = CandidateResponse(value="NULL")
            id = "RESPONSE"
            card = "single"
            base_type = "identifier"
            rVariable = ResponseVariable(identifier=id, cardinality=card,
                                         baseType=base_type,
                                         candidateResponse=cand_response)

        if question[k][1] == "extendedText":
            sessionStatus = u"prp"
        else:
            sessionStatus = u"f"
        now = datetime.datetime.now()
        itemResult = ItemResult(identifier=unicode(question[k][0]),
                                sequenceIndex=k, datestamp=now,
                                sessionStatus=sessionStatus)
        itemResult.assessmentResult = assessmentResult
        itemResult.save()
#         load_agents()
        q = AGENT.get_question_by_id(question[k][0])
        if not AGENT.is_extended_text(q) and \
                request.session.get(question[k][0]):
            if question[k][1] == "textEntry":
                cal = AGENT.get_calification(request.session[question[k][0]],
                                             question[k][0])
                calification = unicode(cal)
            else:
                answer = AGENT.get_answer(request.session[question[k][0]],
                                          question[k][0])
                calification = unicode(answer.calification)
            outcome = OutComeVariable(identifier="SCORE", cardinality='s',
                                      baseType='float', value=calification)
            outcome.itemResult = itemResult
            outcome.save()
        rVariable.itemResult = itemResult
        rVariable.save()


    udate = unicode(datetime.datetime.now().__str__())
    docu.assessmentResult.xml_append(
        docu.xml_create_element(u'testResult',
                                attributes={u'identifier': u'exam',
                                            u'datestamp': udate }
                                )
        )

    for k in question.keys():
        response.write("<p>Pregunta " + str(k) + question[k][0] + "</p>")
        candidateR = docu.xml_create_element(u'cand_response')
        if request.session.has_key(question[k][0]):
            candidateR.xml_append(docu.xml_create_element(u'value',
                                                          content=request.session[question[k][0]]))
            response.write("TENGO ESA CLAVE")
            q = ["choiceMultiple", "inlineChoice"]
            id = u"RESPONSE"
            if question[k][1] in q:
                response.write("PREGUNTA CHOICE O INLINE")
                card = u"single"
                base_type = u"identifier"
                rVariable = docu.xml_create_element(u'responseVariable',
                                                    attributes={"identifier": id,
                                                                "cardinality": card,
                                                                "baseType": base_type
                                                                }
                                                    )
                rVariable.xml_append(candidateR)
            else:
                response.write("PREGUNTA ENTRY O EXTENDEDTEXT")
                card = u"single"
                base_type = u"string"
                rVariable = docu.xml_create_element(u'responseVariable',
                                                    attributes={"identifier": id,
                                                                "cardinality": card,
                                                                "baseType": base_type
                                                                }
                                                    )
                rVariable.xml_append(candidateR)
        else:
            response.write("NO TENGO ESA CLAVE (PREGUNTA SIN CONTESTAR)")
            value = docu.xml_create_element(u'value', content="NULL")
            candidateR.xml_append(value)
            id = u"RESPONSE"
            card = u"single"
            base_type = u"string"
            rVariable = docu.xml_create_element(u'responseVariable',
                                                attributes={"identifier": id,
                                                            "cardinality": card,
                                                            "baseType": base_type
                                                            }
                                                )
            # rVariable.xml_append(candidateR) #FIXME
            # q = AGENT.get_question_by_id(question[k][0])
            # if not AGENT.is_extended_text(q) and \
            #    request.session.get(question[k][0]):
            #     if question[k][1] == "textEntry":
            #         cal = AGENT.get_calification(request.session[question[k][0]],
            #                                      question[k][0])
            #         u_cal = unicode(cal)
            #     else:
            #         answer = AGENT.get_answer(request.session[question[k][0]],
            #                                   question[k][0])
            #         u_cal = unicode(answer.calification)

            # outcome = doc.xml_create_element(u'outcomeVariable',
            #                                  attributes={u'identifier': u'SCORE',
            #                                              u'cardinality': u'single',
            #                                              u'baseType': u'integer'})
             # outcome.xml_append(docu.xml_create_element(u'value',
             #                                            content=u_cal)

        if question[k][1] == "extendedText":
            sessionStatus = u"pendingResponseProcessing"
        else:
            sessionStatus = u"final"
        now = unicode(datetime.datetime.now().__str__())
        itemResult = docu.xml_create_element(u'itemResult',
                                             attributes={u'identifier': unicode(question[k][0]),
                                                         u'sequenceIndex': unicode(str(k)),
                                                         u'datestamp': now,
                                                         u'sessionStatus': sessionStatus,
                                                         }
                                             )
        itemResult.xml_append(rVariable)
        #itemResult.xml_append(outcome)
        docu.assessmentResult.xml_append(itemResult)

    with open(path + "reports/reportExam.xml", 'w') as fd:
        fd.write(docu.xml())

# #     for k in question.keys():
# #         if request.POST.has_key(question[k][0]):
# #             if question[k][1] == "extendedText":
# #                 results[question[k][0]] = None
# #             else:
# #                 q = Question.objects.get(filename=question[k][0])[0]
# #                 answer = q.answer_set.all().get(ide=request[question[k][0]])
# #                 results[question[k][0]] = answer.calification
# #         else:
# #             results[question[k][0]] = 0

# #     for k, val in results.items():
# #         response.write("<p> Pregunta: " + k )
# #         response.write(" Valoracion " + str(val) + "</p>")

    return render_to_response('candidateTool/exam_completed.html')

@user_passes_test(learner_can_access)
def exam(request, exam_id):
    """Display the exam which was created by the lecturer"""
    request.session["exam_id"] = exam_id
    id = Identification.objects.get(user=request.user)
    try:
        name = id.name_set.get(partname="f").text
        surname = id.name_set.get(partname="s").text
    except:
        name = ""
        surname = ""
    test = Test.objects.get(id=exam_id)
    fname = test.filename.split(".xml")[0].split("/")[-1] + "1.html"
    html = 'candidateTool/exams/' + fname
    return render_to_response(html, {'name': name, 'surname': surname})

@user_passes_test(learner_can_access)
def my_exam(request, exam_id):
    """Show the requested exam by exam_id the candidate did"""
    if request.user.is_authenticated():
        candidate = Identification.objects.get(user=request.user)
        path = get_psd()
        tr = TestResult.objects.filter(identifier=exam_id)

        if tr:
            testResult = None
            for t in tr:
                if t.assessmentresult.context.identification.id == candidate.id:
                    testResult = t

            if testResult:
                irslts = testResult.assessmentresult.itemresult_set.all()
                iresults = irslts.order_by('sequenceIndex')

                test = Test.objects.get(id=exam_id)
                fname = test.filename.split(".xml")[0].split("/")[-1] + "1.html"
                exams_path = "django_templates/candidateTool/exams/"
                path_templates = PREFIX + path + exams_path

                doc = amara.parse(path_templates + fname)

                name = surname = ""
                for n in candidate.name_set.all():
                    if n.partname == "F":
                        name = n.text
                    else:
                        surname = n.text
                input_name = doc.xml_xpath(u'//input[@id="name"]')
                input_name[0].xml_set_attribute(u'value', name)
                input_surname = doc.xml_xpath(u'//input[@id="surname"]')
                input_surname[0].xml_set_attribute(u'value', surname)
                ques = doc.xml_xpath(u'//td[contains(@class,"questionNumber")]')

                for i in range(len(iresults)):
                    rVar = iresults[i].responsevariable_set.get(identifier="RESPONSE")
#                     for rVar in rVariables:
                    ind = iresults[i].sequenceIndex - 1
                    cresponse = rVar.candidateResponse
                    # Mark the answer if it was given
                    if cresponse:
                        if ques[ind].parentNode.td[1].xml_xpath(u'input[contains(@value,"inlineChoice")]'):
                            ques[ind].parentNode.td[1].select.xml_xpath(u'option[contains(@value,"'+ \
                                                                            cresponse.value + u'")]')[0].xml_set_attribute(
                                u'selected', u'selected')
                        elif ques[ind].parentNode.td[1].xml_xpath(u'input[contains(@value,"choiceMultiple")]'):
                            doc.xml_xpath(u'//input[@name='+ ques[1].parentNode.td[1].input.value +']')
                            if doc.xml_xpath(u'//input[@value="' + cresponse.value + \
                                                 '" and @name="' + ques[ind].parentNode.td[1].input.value + '"]'):
                                doc.xml_xpath(u'//input[@value="' + cresponse.value + '" and @name="' + \
                                                  ques[ind].parentNode.td[1].input.value + '"]')[0].xml_set_attribute(u'checked', u'checked')
                        elif ques[ind].parentNode.td[1].xml_xpath(u'input[contains(@value,"textEntry")]'):
                            ques[ind].parentNode.td[1].input = unicode(cresponse.value)
                        elif ques[ind].parentNode.td[1].xml_xpath(u'input[contains(@value,"extendedText")]'):
                            ques[ind].parentNode.td[1].textarea = unicode(cresponse.value)
                        else:
                            pass

                with open(path_templates + "myexam.html", 'w') as fd:
                    fd.write(doc.xml())
                return render_to_response("candidateTool/exams/myexam.html")
            else:
                return render_to_response("candidateTool/noresults.html")

        else:
            return render_to_response("candidateTool/noresults.html")
    else:
        return render_to_response("candidateTool/invalid.html")

    return render_to_response("candidateTool/myexam.html")

@user_passes_test(learner_can_access)
def solved_exam(request, exam_id):
    #Created through parsing the doc (in the 287 revision)
    try:
        can_be_shown = Test.objects.get(id=exam_id).is_finished()
    except:
        return render_to_response("candidateTool/invalid.html")
    else:
        if can_be_shown:
            load_agents()
	    sys.path.append(PREFIX + '/src/AuthoringTool/Renders/')
	    import render
	    import dochtmlrender
	    dochtmlrender.load()
	    load_html_renders()
	    ren = render.Render.create("HTML")
	    t = AGENT.get_exam_by_id(id=exam_id)
	    date = [t.start_date.year, t.start_date.month - 1, t.start_date.day]
	    for i in range(0, len(t.parts)):
	        exam_solved = "src/django_templates/candidateTool/exams/exam_solved"
	        if i == 0:
	            name = PREFIX + exam_solved
	        else:
	            name = PREFIX + exam_solved + str(i + 1)
	        ren.create_header(True, False, name, t.subject.name,
	                          t.parts[0].title,
	                          t.parts[0].sections[0].title,
	                          date, t.id, "/continue_exam/" + str(i + 1))
	        for sec in t.parts[i].sections:
	            for qr in sec.questionref:
	                ques = AGENT.get_question_by_id(qr.questionID)
	                q = AGENT.create_question(ques)
	                q.accept(ren)
	        last = i == len(t.parts) - 1
	        ren.close_part(i, last)
	    ren.close_document()
	    return render_to_response("candidateTool/exams/exam_solved.html")
        else:
            return render_to_response("candidateTool/invalid.html")

@user_passes_test(learner_can_access)
def continue_solved_exam(request, part_id):
    """Show the second and next parts of a solved exam"""
    return render_to_response("candidateTool/exam_solved" + part_id + ".html")

#Now this view does not exist
@user_passes_test(learner_can_access)
def exams(request):
    """Show the exams"""
    candidate = Identification.objects.get(user=request.user)
    dem = get_demographics(candidate)
    subjects = candidate.subject.all()
    date = datetime.datetime.now()
    #     tests = {}
    # for s in subjects:
    #     ts = s.test_set.filter(Q(representation_type="Both") |
    #                            Q(representation_type="Web"))
    #     order_ts = ts.order_by("start_date")

    return render_to_response("candidateTool/exams.html",
                              {'candidate': candidate,
                               'subjects': subjects, 'dem': dem})

# Now this view does not exist
@user_passes_test(learner_can_access)
def marks(request, exam_id):
    """Return the marks for each question of the give exam id"""
    outcomes = {}
    mark = 0
    candidate = Identification.objects.get(user=request.user)
    if candidate.demographics_set.all():
        dem = candidate.demographics_set.all()[0]
    else:
        dem = None
    assessmentResult = is_mark_available(candidate, exam_id)
    test = Test.objects.get(id=exam_id)
    subject = test.subject.name
    if assessmentResult:
        iresults = assessmentResult.itemresult_set
        for item in iresults.order_by("sequenceIndex"):
            if item.outcomevariable_set.all():
                outcome = item.outcomevariable_set.get(identifier="SCORE")
                outcomes[item.sequenceIndex] = outcome.value
                mark += float(outcome.value)
            else:
                outcomes[item.sequenceIndex] = "Pendiente de calificar"
        return render_to_response("candidateTool/marks.html",
                                  {'dem': dem, 'outcomes': outcomes,
                                   'subject': subject, 'mark': mark})
    else:
        return render_to_response("candidateTool/marks.html",
                                  {'dem': dem, 'outcomes': outcomes})

def is_mark_available(candidate, exam_id):
    """Return whether a mark for the given exam is available. If
    a candidate didn't do the exam, the result is also available"""
    # available = True
    assessmentResult = None
    for c in candidate.context_set.all():
        if c.assessmentresult.testResult.identifier == exam_id:
            assessmentResult = c.assessmentresult
            # Right now is allowed do an exam several times by a candidate,
            # to test mark method, the last exam done is taken to be marked
            #            break
            # for item in assessmentResult.itemresult_set.all():
            #     if not item.sessionStatus == 'f':
            #         available = False

    return assessmentResult

#Methods to test some funcionalities of the webpage
def open_exam(request):
    AGENT.open_exams()
    response = exams(request)
    return response

def close_exam(request):
    AGENT.close_exams()
    response = exams(request)
    return response

def extend_exam(request):
    AGENT.extend_exams()
    response = exams(request)
    return response

#Quizas con pwd
def get_psd():
    import os
    pwd = os.getcwd()
    path = pwd.split("src/")[0] + "src/"
    return path

@login_required
def logout(request):
    """Log out the user"""
    auth.logout(request)
    return render_to_response('candidateTool/logout.html')

def download(request):
    """Display the download template"""
    return render_to_response('candidateTool/download.html')

@user_passes_test(lecturer_can_access)
def statistics(request):
    """Show all the available questions and exams to display their
    statistics"""
    lecturer = Identification.objects.get(user=request.user)
    dem = get_demographics(lecturer)
    subjects = lecturer.subject.all()

    list_s = []
    list_t = []
    for s in subjects:
        list_s.append(s.id)
    ts = Test.objects.filter(subject__in=list_s)
    for t in ts:
        list_t.append(t.id)
    trs = TestResult.objects.filter(identifier__in=list_t)
    ques_set = set()
    test_set = set()
    for tr in trs:
        irs = tr.assessmentresult.itemresult_set.filter(sessionStatus='f')
        if irs:
            test_set.add(tr.identifier)
        for ir in irs:
            ques_set.add(str(ir.identifier))

    test = {}
    ques = {}
    for test_id in test_set:
        test[test_id] = get_exam_name(test_id)
    for ques_id in ques_set:
        ques[ques_id] = AGENT.get_question_name(ques_id)

    if FICTITIOUS_DATA:
        print "LECTURER", lecturer
        return render_to_response("candidateTool/statistics.html",
                                  {'ques':QUES, 'test':TEST,
                                   'subjects':subjects, 'dem':dem,
                                   'candidate': lecturer})
    else:
        return render_to_response("candidateTool/statistics.html",
                                  {'ques':ques, 'test':test,
                                   'subjects':subjects, 'dem':dem,
                                   'candidate': lecturer})


@user_passes_test(lecturer_can_access)
def show_statistics(request):
    """Show the statistics for the selected exam/s and questions"""
    candidate = Identification.objects.get(user=request.user)
    dem = get_demographics(candidate)

    ques_id = request.POST["ques"]
    sequence = []

    if not FICTITIOUS_DATA:
        # ABOUT ONE EXAM
        if request.POST.has_key("ex"):
            exam_id = request.POST["ex"]
            # ALL QUESTIONS
            if ques_id == "0":
                results = {}
                for k in AGENT.get_questions_from_exam(exam_id):
                    seq = corestats.Stats(get_marks(k, exam_id))
                    results[k] = seq.avg()
                    sequence = sequence + get_marks(k, exam_id)
                seq = corestats.Stats(sequence)
                create_barchart(results)

            # ONE QUESTION
            else:
                sequence = get_marks(ques_id, exam_id)
                seq = corestats.Stats(sequence)
                title = AGENT.get_question_name(ques_id)
                create_piechart(sequence)
        # ABOUT ALL EXAMS
        else:
            # ONE QUESTION
            if ques_id != "0":
                sequence = get_marks_for_question(ques_id)
                seq = corestats.Stats(sequence)
                create_piechart(sequence)
                part_title = " - Todos los examenes"
                title = AGENT.get_question_name(ques_id) + part_title
    else:
        # ABOUT ONE EXAM
        if request.POST.has_key("ex"):
            exam_id = request.POST["ex"]
            # ALL QUESTIONS
            if ques_id == "0":
                results = {}
                for k in OUTCOME[exam_id].keys():
                    seq = corestats.Stats(OUTCOME[exam_id][k])
                    results[k] = seq.avg()
                    sequence = sequence + OUTCOME[exam_id][k]
                seq = corestats.Stats(sequence)
                create_barchart(results)
                title = TEST[request.POST["ex"]]
            # ONE QUESTION
            else:
                exam = OUTCOME[request.POST["ex"]]
                sequence = exam[request.POST["ques"]]
                seq = corestats.Stats(sequence)
                title = QUES[request.POST["ques"]]
                create_piechart(sequence)
        # ABOUT ALL EXAMS
        else:
            # ONE QUESTION
            if ques_id != "0":
                exams = get_all_exams_with_question(ques_id)
                for k in exams:
                    sequence = sequence + OUTCOME[k][ques_id]
                seq = corestats.Stats(sequence)
                create_piechart(sequence)
                title = TEST[request.POST["ques"]] + " - Todos los examenes"
    avg = str(seq.avg())[0:6]
    ftl = seq.percentile(5)
    ftsl = seq.percentile(10)
    ftm = seq.percentile(50)
    ftsh = seq.percentile(90)
    fth = seq.percentile(95)

#    ud = UsageData()
#    os = OrdinaryStatistic(name=, context=, usageData=ud)
#    to = TargetObject(identifier=, itemStatistic=os)
#    title = qs[0].title
    return render_to_response("candidateTool/show_statistics.html",
                              {'candidate': candidate, 'item': title,
                               'avg': avg, 'ftl':ftl, 'ftsl': ftsl,
                               'ftm': ftm,'ftsh': ftsh, 'fth': fth,
                               'dem': dem})

def get_all_exams_with_question(question):
    """Return all the exams which have the given question"""
    exams = []
    for k, val in TEST_QUES.items():
        if val.count(question):
            exams.append(k)
    return exams

@user_passes_test(lecturer_can_access)
def exam_statistics(request):
    """Show the question of which are saved results associated to
    the selected exam"""
    response_dict = {}
    if not FICTITIOUS_DATA:
        if request.POST.values()[0] == "0":
            lecturer = Identification.objects.get(user=request.user)
            dem = get_demographics(lecturer)
            subjects = lecturer.subject.all()

            list_s = []
            list_t = []
            for s in subjects:
                list_s.append(s.id)
            ts = Test.objects.filter(subject__in=list_s)
            for t in ts:
                list_t.append(t.id)
            trs = TestResult.objects.filter(identifier__in=list_t)
            ques_set = set()
            for tr in trs:
                iresults = tr.assessmentresult.itemresult_set
                irs = iresults.filter(sessionStatus='f')
                for ir in irs:
                    ques_set.add(str(ir.identifier))

            for ques_id in ques_set:
                response_dict[ques_id] = AGENT.get_question_name(ques_id)
        else:
            trs = TestResult.objects.filter(identifier=request.POST.values()[0])
            ques_set = set()
            for tr in trs:
                iresults = tr.assessmentresult.itemresult_set
                irs = iresults.filter(sessionStatus='f')
                for ir in irs:
                    ques_set.add(str(ir.identifier))
            for ques_id in ques_set:
                response_dict[ques_id] = AGENT.get_question_name(ques_id)
    else:
        if request.POST.values()[0] == "0":
            for k, val in QUES.items():
                response_dict[val] = k
        else:
            for item in TEST_QUES[request.POST.values()[0]]:
                response_dict[QUES[item]] = item
    return HttpResponse(simplejson.dumps(response_dict),
                        mimetype='application/javascript')

def create_piechart(sequence):
    """Create a pie chart from the given sequence"""
    background = cairo.LinearGradient(300, 0, 300, 400)
    background.add_color_stop_rgb(0, 0.7, 0, 0)
    background.add_color_stop_rgb(1.0, 0.3, 0, 0)
    data = {}
    i = 0
    sequence.sort()
    while(i<len(sequence)):
        percent = str((sequence.count(sequence[i])/float(len(sequence))*100))[0:6]
        key = str(sequence[i]) + " puntos (" + percent + "%)"
        data[key] = sequence.count(sequence[i])
        i = i + sequence.count(sequence[i])

#     colors = [ (232.0/255, 118.0/255, 107.0/255),
#                (255.0/255, 150.0/255, 117.0/255),
#                (255.0/255, 130.0/255, 154.0/255),
#                (232.0/255, 107.0/255, 194.0/255),
#                (240.0/255, 117.0/255, 255.0/255) ]

    # FIFXME: Cambiar los paths absolutos
    CairoPlot.pie_plot(PREFIX + "src/django_media/chart.png",
                       data, 504, 336, # background = background,
                       gradient = True, shadow = True#,  colors = colors
                       )

def create_barchart(results):
    """Create a bar chart from the given results"""
    data = []
    h_labels = []
    for k, val in results.items():
        data.append(val)
        h_labels.append(QUES[k])

#     data = [[1.4, 3, 11], [8, 9, 21], [13, 10, 9], [2, 30, 8]]
#     h_labels = ["group1", "group2", "group3", "group4"]
#    colors = [(1,0.2,0), (1,0.7,0), (1,1,0)]

    # FIFXME: Cambiar los paths absolutos
    CairoPlot.bar_plot(PREFIX + "/src/django_media/chart.png",
                       data, 504, 336, border=20, grid=True,
                       three_dimension=True, h_labels=h_labels)

@user_passes_test(lecturer_can_access)
def questions_prp(request):
    """Show a list of the questions pending of the response processing"""
    lecturer = Identification.objects.get(user=request.user)
    dem = get_demographics(lecturer)
    questions = get_questionsprp(lecturer)
    template = 'candidateTool/base_qprp.html'
    return render_to_response(template, {'questions': questions,
                                         'dem': dem, 'candidate':lecturer})

@user_passes_test(lecturer_can_access)
def process_questions_prp(request, item_result):
    """Update the questions pending of the response processing, changing
    its sessionStatus to final and associating it the outcomevariable"""

    PREFIX = "/home/peris/Desktop/csl2-sdocente/"
    path = PREFIX + "src/django_templates/candidateTool/exams/prueba.txt"
    fde = open(path, "w")
    fde.write("pruebaaaaaa")
    fde.close()

    lecturer = Identification.objects.get(user=request.user)
    dem = get_demographics(lecturer)

    # with open(PREFIX + "src/django_templates/reporte.txt", "w") as fd:
    #     for k, val in request.POST.items():
    #         fd.write(k)
    #         fd.write(":")
    #         fd.write(val)
    #         fd.write("\n")

    ir = ItemResult.objects.filter(id=item_result)
    if len(ir):
        ir = ir[0]
        ir.sessionStatus = u"f"
        ir.save()
        calification = request.POST["mark"]
        outcome = OutComeVariable(identifier="SCORE", cardinality='s',
                                  baseType='float', value=calification)
        outcome.itemResult = ir
        outcome.save()

    questions = get_questionsprp(lecturer)

    # import os
    # cwd = os.getcwd()
    # os.chdir("/home/peris/Desktop/csl2-sdocente/src/django_templates/candidateTool/exams/")
    # fde = open("equestion.html", "r")
    # fdq = open("question.html", "w")
    # content = fde.read()
    # fdq.write(content)
    # fde.close()
    # fdq.close()
    # os.chdir(cwd)

    # exams_path = "/home/peris/Desktop/csl2-sdocente/src/django_templates/candidateTool/exams/"
    # os.system("cp " + exams_path + "empty_question.html " + exams_path + "question.html")
    template = 'candidateTool/base_qprp.html'
    return render_to_response(template, {'dem': dem, 'candidate':lecturer,
                                         'questions':questions})

@user_passes_test(lecturer_can_access)
def questions_prp_view(request):
    """Create the question view to mark"""
    dict = {}
#     fd = open(PREFIX + "src/django_templates/pqpqpq.txt", 'w')
# #     fd.write(request.POST["qprp"])
#     for k, v in request.POST.items():
#         fd.write(k)
#         fd.write("\n")
#         fd.write(val)
#         fd.write("\n")


    ir = ItemResult.objects.filter(id=request.POST.get("qprp"))[0]
    id = ir.assessmentResult.testResult.identifier

#     fd.write("ID DEL IR: ")
#     fd.write(str(ir.id))
#     fd.write("\n ID DEL TEST: ")
#     fd.write(str(id))
#     fd.close()

    load_html_renders()

    load_agents()

    sys.path.append(PREFIX + '/src/AuthoringTool/Renders/')
    import render
    import dochtmlrender
    dochtmlrender.load()

    load_html_renders()

    ren = render.Render.create("HTML")

    names = ir.assessmentResult.context.identification.name_set

    name = ""
    surname = ""
    for nam in list(names.all()):
        if nam.partname == "F":
            name = str(nam.text)
        else:
            surname = str(nam.text)

    ques = AGENT.get_question_by_id(ir.identifier)
    t = AGENT.get_exam_by_id(id)

#    qwer = open(PREFIX + "src/django_templates/candidateTool/paths.txt", "w")
    for qr in ques.questionref:
#         qwer.write(str(type(qr.section.part.testID)))
#         qwer.write(":")
#         qwer.write(str(qr.section.part.testID))
#         qwer.write("\n")
#         qwer.write(str(type(id)))
#         qwer.write(":")
#         qwer.write(str(id))
#         qwer.write("\n")
        if str(qr.section.part.testID) == id:
            part = qr.section.part
            section = qr.section
            qref = qr
            break

    exams_path = "src/django_templates/candidateTool/exams/"
    path = PREFIX + exams_path + "question1"
#     path = get_psd()[0] + exams_path + "question"

    subject = t.subject.name
    subject = subject
    date = [t.start_date.year, t.start_date.month-1, t.start_date.day]

    # qwer.write(str(type(path)))
    # qwer.write(":")
    # qwer.write(path)
#     qwer.write("\n")
#     qwer.write(str(type(subject)))
#     qwer.write(":")
#     qwer.write(subject)
#     qwer.write("\n")
#     qwer.write(str(type(part.title)))
#     qwer.write(":")
#     qwer.write(part.title)
#     qwer.write("\n")
#     qwer.write(str(type(section.title)))
#     qwer.write(":")
#     qwer.write(section.title)
#     qwer.write("\n")
#     qwer.write(str(type(date.__str__())))
#     qwer.write(":")
#     qwer.write(date.__str__())
#     qwer.write("\n")
#     qwer.write(str(type(t.id)))
#     qwer.write(":")
#     qwer.write(str(t.id))
#     qwer.write("\n")
#     qwer.write(str(type(name)))
#     qwer.write(":")
#     qwer.write(name)
#     qwer.write("\n")
#     qwer.write(str(type(surname)))
#     qwer.write(":")
#     qwer.write(surname)

    ren.create_header(False, False, path, subject, part.title, section.title,
                      date, request.POST.get("qprp"),
                      "/process_questionsprp/", name, surname)
    q = AGENT.create_question(ques)
    q.accept(ren)

    rv = ir.responsevariable_set.filter(identifier="RESPONSE")[0]

    # qwer = open(PREFIX + "src/django_templates/candidateTool/paths.txt", "w")
    # qwer.write("IEEEEEEEEE\n")
    # # qwer.write(str(rv.candidateResponse.value))
    # qwer.close()

    ren.close_document()
    doc = amara.parse("/home/peris/Desktop/csl2-sdocente/src/django_templates/candidateTool/exams/question1.html")

    fd = open("/home/peris/Desktop/csl2-sdocente/src/django_templates/candidateTool/exams/question.html", "w")
    td = doc.xml_xpath(u'//td[contains(@class,"invisible")]')
    ans = unicode("""Es un mecanismo TCP para el control de congestion de la red. TCP asume que la perdida de paquetes es debida al congestionamiento de los enrutadores de la subred. Controlando el modo en el que se inyecta trafico en la red se puede prevenir la congestion.""")

#     En la fase inicial, el arranque lento envia un segmento, y por cada
# reconocimiento envia un segmento adicional; la ventana de congestion
# crece de este modo hasta alcanzar un umbral (la mitad de la ventana de
# envio maxima). A partir de ahi, el crecimiento es lineal (aumento
# aditivo). Si expira algun timeout, el umbral se fija a la mitad del
# tamano de la ventana de congestion actual y el proceso empieza desde
# el principio (disminucion multiplicativa).
    td[0].parentNode.td[1].textarea.xml_children = [ans]
#    td.textarea.xml_children = [u' vengaaa']
    # td[0].parentNode.td[1].textarea = "prueba" + str(rv.candidateResponse.value)
    fd.write(doc.xml())
    fd.close()

#    qwer.close()

    return HttpResponse(simplejson.dumps(dict),
                        mimetype='application/javascript')

@user_passes_test(lecturer_can_access)
def exam_in_paper(request):
    """Show the exams created in paper and the pupils who are registered
    in the related subject to the first test"""
    lecturer = Identification.objects.get(user=request.user)
    dem = get_demographics(lecturer)
    subjects = lecturer.subject.all()
    tests = []
    for s in subjects:
        t = Test.objects.filter(subject__id=s.id,
                                representation_type="Paper")
        tests.extend(t)

    pupils = {}
    if tests:
        ids = Identification.objects.filter(subject__id=tests[0].subject_id)
        for id in ids:
            for group in id.user.groups.all():
                if group.name == "Alumnos":
                    pupils[id.id] = id.formName

    return render_to_response('candidateTool/exam_in_paper.html',
                              {'tests': tests, 'pupils': pupils,
                               'dem':dem, 'candidate':lecturer})

@user_passes_test(lecturer_can_access)
def update_paper_exam(request):
    """Update the number of entrys to put the marks and the candidates
    related to selected test"""

    exam_id = request.POST.values()[0]

    ques_path = "src/django_templates/candidateTool/exams/questions.txt"
    name = PREFIX + ques_path

    numques = 0
    if exam_id != 0:
        update = {}
        t = Test.objects.filter(id=exam_id)
        ids = Identification.objects.filter(subject__id=t[0].subject_id)

        for id in ids:
            for group in id.user.groups.all():
                if group.name == "Alumnos":
                    update[id.id] = id.formName

        try:
            t = AGENT.get_exam_by_id(exam_id)
            ques = []
            for par in t.parts:
                for sec in par.sections:
                    for qref in sec.questionref:
                        numques += 1
                        ques.append(qref.questionID)

        except:
            print "El id de examen no existe"
            ques_path = "src/django_templates/candidateTool/exams/questions.txt"
            name = PREFIX + ques_path
            with open(name, 'w') as fd:
                fd.write("Numero de preguntas: ")
                fd.write(str(numques))
                fd.write("\nId del examen: ")
                fd.write(str(exam_id) + "\n")

#     import random
#     if id:
#         numques = random.randint(1,10)
    template = ""
    for i in range(1, numques+1):
        if i < 6:
            template += """
    <td widht="100%" align="center" valign="center" bgcolor="#161410">
      Pregunta """ + str(i) + """
    </td>
    <td id="ques" align="right" bgcolor="#161410">
      <input type="text" name='question """ + str(i) + """'/>
    </td>
  </tr>"""
            if i < numques:
                template += "\n<tr bgcolor='#161410'>"
        if i > 5:
            template +=   """
<tr bgcolor="#161410">
<td>
</td>
<td>
</td>
    <td widht="100%" align="center" valign="center" bgcolor="#161410">
      Pregunta """ + str(i) + """
    </td>
    <td id="ques" align="right" bgcolor="#161410">
      <input type="text" name='question """ + str(i) + """'/>
    </td>
  </tr>
"""
    if numques > 4:
        template += """
    <tr>
      <td>
      </td>
      <td>
      </td>
      <td>
      </td>
      <td align="right">
        <input type="submit" value="Calificar"/>
      </td>
    </tr>"""
    else:
        template += """
    <tr>
      <td>
      </td>
      <td align="right">
        <input type="submit" value="Calificar"/>
      </td>
    </tr>
"""

    path = PREFIX + "src/django_templates/candidateTool/table.html"
    with open(path, 'w') as fd:
        fd.write(template)

    # with open(name, 'w') as fd:
    #     fd.write("\nId del examen: ")
    #     fd.write(str(exam_id) + "\n")

    update["nm"] = numques
    return HttpResponse(simplejson.dumps(update),
                        mimetype='application/javascript')

@user_passes_test(lecturer_can_access)
def update_results_paper(request):
    """Gather the marks inserted by the lecturer and creates a report"""
    # with open(PREFIX + "src/django_templates/variables.txt", 'w') as fd:
    #     for k, val in request.POST.items():
    #         fd.write("Clave: ")
    #         fd.write(k)
    #         fd.write("\n")
    #         fd.write("Valor: ")
    #         fd.write(val)
    #         fd.write("\n")

    lecturer = Identification.objects.get(user=request.user)
    dem = get_demographics(lecturer)

    subjects = lecturer.subject.all()
    list_s = []
    for s in subjects:
        list_s.append(s.id)

    pexam = request.POST.get("paperexam")
    ts = AGENT.get_exams_by_id(list_s)
    initial = AGENT.get_exam_by_id(pexam)

    # REVISAR: CUIDADO CON LAS CODIFICACIONES
    test = Test.objects.get(id=request.POST.get("paperexam"))
    name = test.filename.split(".xml")[0].split("/")[-1]

    path = PREFIX + "src/" + "django_templates/candidateTool/"
    questions = {}
    for k, val in request.POST.items():
        if not k.split("question ")[0]:
            questions[k.split("question")[1]] = val

    if request.POST.get("candidate") == "0":
        iden = Identification.objects.get(formName="Anonimo")
    else:
        iden = Identification.objects.get(user=request.POST.get("candidate"))
    context = Context(identification=iden)
    context.save()
    sessionId = SessionIdentifier(sourceId="draper.mine.nu", identifier="SGAE",
                                  context=context)
    sessionId.save()
    testResult = TestResult(identifier=request.POST.get("paperexam"),
                            dateStamp=datetime.datetime.now())
    testResult.save()
    assessmentResult = AssessmentResult(context=context, testResult=testResult)
    assessmentResult.save()
    for k, val in questions.items():
        itemResult = ItemResult(identifier=unicode(k),
                                datestamp=datetime.datetime.now(),
                                sequenceIndex=None,
                                sessionStatus=u"f")

        itemResult.assessmentResult = assessmentResult
        itemResult.save()
        outcome = OutComeVariable(identifier="SCORE", cardinality='s',
                                  baseType='float', value=unicode(val))
        outcome.itemResult = itemResult
        outcome.save()

    docu = amara.parse(path + "templates/report_template.xml")

    dstamp = unicode(datetime.datetime.now().__str__())
    docu.assessmentResult.xml_append(
        docu.xml_create_element(u'testResult', attributes={u'identifier': u'exam',
                                                           u'datestamp': dstamp
                                                           }
                                )
        )

    for k, val in questions.items():
        outcome = docu.xml_create_element(u'outcomeVariable',
                                          attributes={u'identifier': u'SCORE',
                                                      u'cardinality': u'single',
                                                      u'baseType': u'integer'})
        outcome.xml_append(docu.xml_create_element(u'value',
                                                   content=unicode(val)))

        sessionStatus = u'f'
        udate = unicode(datetime.datetime.now().__str__())
        itemResult = docu.xml_create_element(u'itemResult',
                                             attributes={u'identifier': unicode(k),
                                                         u'datestamp': udate,
                                                         u'sessionStatus': sessionStatus,
                                                         }
                                             )
        itemResult.xml_append(outcome)
        docu.assessmentResult.xml_append(itemResult)

#     fd.write("Questions \n")

#     for k, val in questions.items():
#         fd.write("Clave: ")
#         fd.write(k)
#         fd.write("\n")
#         fd.write("Valor: ")
#         fd.write(val)
#         fd.write("\n")

#     fd.close()
    candidate = request.POST.get("candidate")
    fname = path + "reports/" + candidate + "-" + name + ".xml"
    with open(fname, 'w') as fd:
        fd.write(docu.xml())

    tests, pupils = get_tests_pupils(lecturer)
    # pupils = {0:"Anonimo"}

    # try:
    #     t = ts[0]
    #     initial = t.id
    #     candidates = Identification.objects.get(id=t.subject.id)
    #     for can in candidates:
    #         pupils[can.id] = can.formName
    # except:
    #     pass

    # tests = [17, 24, 25, 26, 27, 28, 29, 30, 31]

    return render_to_response('candidateTool/exam_in_paper.html',
                              {'tests': tests, 'candidate':lecturer,
                               'initial':initial, 'pupils': pupils,
                               'dem': dem})

def is_exam_done(test_id, iden):
    """Return whether a candidate did an exam"""
    exam_done = False
    for con in iden.context_set.all():
        if str(test_id) == con.assessmentresult.testResult.identifier:
            exam_done = True
    return exam_done


def load_agents():
    """Load the available question agents"""
    # FIXME: Rutas absolutas
    path = PREFIX + "src/AuthoringTool/Questions/"
    for c_dir in os.listdir(path):
        if not c_dir.startswith(".") and not c_dir.startswith("__init__"):
            sys.path.append(path + c_dir)
            __import__(os.path.splitext(c_dir.lower())[0]).load_agent()

def load_html_renders():
    """Load the available html renders"""
    #FIXME: Rutas absolutas
    path = PREFIX + "src/AuthoringTool/Renders/"
    sys.path.append(path)
    path_ques = PREFIX + "src/AuthoringTool/Questions/"
    for c_dir in os.listdir(path_ques):
        if not c_dir.startswith(".") and not c_dir.startswith("__init__"):
            sys.path.append(path_ques + c_dir)
            __import__(os.path.splitext(c_dir.lower())[0]).load_html_render()

def get_mark(candidate, exam_id):
    """Get the mark of the given exam"""
    assessment_result = is_mark_available(candidate, exam_id)
    mark = 0
    if assessment_result:
        for item in assessment_result.itemresult_set.order_by("sequenceIndex"):
            if item.outcomevariable_set.all():
                outcome = item.outcomevariable_set.get(identifier="SCORE")
                mark += float(outcome.value)
            else:
                mark = "?"
                break
    return mark

def get_demographics(user):
    """Return the user photo if it's available"""
    if user.demographics_set.all():
        dem = user.demographics_set.all()[0]
    else:
        dem = None
    return dem

def get_marks(ques_id, exam_id):
    """Return the mark associated to the given question id in the
    exam specified for the given exam_id """
    tresults = TestResult.objects.get(identifier=exam_id)
    marks = []
    for tresult in tresults:
        irs = tresult.assessmentresult.itemresult_set
        iresult = irs.filter(identifier=ques_id)[0]
        outcome = iresult.outcomevariable_set.filter(identifier="SCORE")[0]
        marks.append(outcome.value)
    return marks

def get_marks_for_question(ques_id):
    """Return the marks associated to the given question id in all
    the exams"""
    marks = []
    iresults = ItemResult.objects.get(identifier=ques_id)
    for iresult in iresults:
        if len(iresult.outcomevariable_set.filter(identifier="SCORE")):
            outcome = iresult.outcomevariable_set.filter(identifier="SCORE")[0]
            marks.append(outcome.value)
    return marks

def get_exam_name(exam_id):
    """Return the name of the exam associated to the given exam id"""
    return Test.objects.get(id=exam_id).name

def get_questionsprp(lecturer):
    """Return the questions pending of the response processing"""
    subjects = lecturer.subject.all()
    list_s = []
    list_t = []
    test = {}
    for s in subjects:
        list_s.append(s.id)
    ts = Test.objects.filter(subject__in=list_s)
    for t in ts:
        list_t.append(t.id)
        test[t.id] = t.name
    trs = TestResult.objects.filter(identifier__in=list_t)
    s = set()
    dict_irs = {}
    for tr in trs:
        irs = tr.assessmentresult.itemresult_set.filter(sessionStatus='prp')
        for ir in irs:
            name = AGENT.get_question_by_id([ir.identifier]).title
            if len(name) > 12:
                name = name[0:12] + "..."
            dict_irs[ir.id] = name
            s.add(str(ir.identifier))

    if not dict_irs:
        ques_path = "src/django_templates/candidateTool/exams/question.html"
        fd = open(PREFIX + ques_path, 'w')
        fd.close()
    return dict_irs

def get_tests_pupils(lecturer):
    """Return the associated tests to the lecturer and the registered
    pupils in the subject of the first test"""
    subjects = lecturer.subject.all()
    tests = []
    for s in subjects:
        t = Test.objects.filter(subject__id=s.id,
                                representation_type="Paper")
        tests.extend(t)

    ids = Identification.objects.filter(subject__id=tests[0].subject_id)
    pupils = {}
    for id in ids:
        for group in id.user.groups.all():
            if group.name == "Alumnos":
                pupils[id.id] = id.formName
    return tests, pupils
