import django
from django.test.client import Client
from django.contrib.auth.models import Group
from models import Test, Subject, User, Identification, Name, TestResult
import datetime

class ExamTestCase(django.test.TestCase):

    def setUp(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        dtaweekago = dtyesterday - datetime.timedelta(6)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        self.exam2 = Test.objects.create(name="prueba",
                                         filename="../QuizsBank/prueba2.xml",
                                         start_date=dtaweekago,
                                         end_date=dtyesterday,
                                         subject=subject,
                                         description="",
                                         representation_type="Web")

    def testIsFinished(self):
        self.assertEquals(self.exam.is_finished(), False)
        self.assertEquals(self.exam2.is_finished(), True)

    def testIsFeasible(self):
        self.assertEquals(self.exam.is_feasible(), True)
        self.assertEquals(self.exam2.is_feasible(), False)

    def testIsStarted(self):
        self.assertEquals(self.exam.is_started(), True)
        self.assertEquals(self.exam2.is_started(), True)

# class IdentificationTestCase(unittest.TestCase):
#     def setUp(self):


#     def testGetAssessmentResult(self):
#         self.assertEquals(self.pupil.get_assessmentResult(), None)
#         self.assertEquals(self.pupil.get_assessmentResult(), self.aresult)

class PermissionLearnerTestCase(django.test.TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="fakeuser",
                                             email="test@fakemail",
                                             password="fakepassword")
        Identification.objects.create(formName="dummy", user=self.user)
        Group.objects.create(name="Alumnos")
        self.user.groups.add(1)

    def testMain(self):
        response = self.client.post("/main/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual("candidateTool/invalid.html",
                             response.template[0].name)

    def testMainPage(self):
        response = self.client.post("/main/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual("candidateTool/invalid.html",
                             response.template[0].name)

    def testExam(self):
        response = self.client.post("/exam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testContinueExam(self):
        response = self.client.post("/continue_exam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testMyExam(self):
        response = self.client.post("/myexam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testSolvedExam(self):
        response = self.client.post("/solved/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testReport(self):
        response = self.client.post("/report/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testLogOut(self):
        response = self.client.post("/salir/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testStatistics(self):
        response = self.client.post("/statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testShowStatistics(self):
        response = self.client.post("/show_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testExamStatistics(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testQuestionsPrp(self):
        response = self.client.post("/questionsprp/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testQuestionsPrpView(self):
        response = self.client.post("/questionsprpview/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testExamInPaper(self):
        response = self.client.post("/examinpaper/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testUpdatePaperExam(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testUpdateResultsPaper(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testProcessQuestionsPrp(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

class PermissionLecturerTestCase(django.test.TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="fakeuser",
                                             email="test@fakemail",
                                             password="fakepassword")
        Identification.objects.create(formName="dummy", user=self.user)
        Group.objects.create(name="Profesores")
        self.user.groups.add(1)

    def testMain(self):
        response = self.client.post("/main/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual("candidateTool/invalid.html",
                             response.template[0].name)

    def testMainPage(self):
        response = self.client.post("/main/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual("candidateTool/invalid.html",
                             response.template[0].name)

    def testExam(self):
        response = self.client.post("/exam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testContinueExam(self):
        response = self.client.post("/continue_exam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testMyExam(self):
        response = self.client.post("/myexam/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testSolvedExam(self):
        response = self.client.post("/solved/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testReport(self):
        response = self.client.post("/report/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testLogOut(self):
        response = self.client.post("/salir/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testStatistics(self):
        response = self.client.post("/statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testShowStatistics(self):
        response = self.client.post("/show_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testExamStatistics(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testQuestionsPrp(self):
        response = self.client.post("/questionsprp/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testQuestionsPrpView(self):
        response = self.client.post("/questionsprpview/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testExamInPaper(self):
        response = self.client.post("/examinpaper/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testUpdatePaperExam(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testUpdateResultsPaper(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)

    def testProcessQuestionsPrp(self):
        response = self.client.post("/exam_statistics/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 302)


class ViewsNoDataLecturerTestCase(django.test.TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="fakeuser",
                                             email="test@fakemail",
                                             password="fakepassword")
        self.iden = Identification.objects.create(formName="dummy",
                                                  user=self.user)
        Group.objects.create(name="Profesores")
        self.user.groups.add(1)
        Name.objects.create(partname="F", text="fakename",
                            identification=self.iden)
        Name.objects.create(partname="S", text="fakesurname",
                            identification=self.iden)
        self.client.login(username="fakeuser", password="fakepassword")

    def testStatistics(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        self.iden.subject.add(subject)

        response = self.client.post("/statistics/")

        self.failUnlessEqual(response.context['candidate'], self.iden)
        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['subjects'][0], subject)

        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual(response.template[0].name,
                             "candidateTool/statistics.html")

    def testQuestionsPrp(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        self.iden.subject.add(subject)

        response = self.client.post("/questionsprp/")

        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['questions'], {})

        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual(response.template[0].name,
                             "candidateTool/base_qprp.html")

    def testProcessQuestionsPrp(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        self.iden.subject.add(subject)

        response = self.client.post("/process_questionsprp/5/")

        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['questions'], {})

        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual(response.template[0].name,
                             "candidateTool/questions_prp.html")

    def testExamInPaper(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        self.iden.subject.add(subject)

        response = self.client.post("/examinpaper/")

        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['tests'], [])
        self.failUnlessEqual(response.context['pupils'], {})

        self.failUnlessEqual(response.status_code, 200)
        self.failUnlessEqual(response.template[0].name,
                             "candidateTool/exam_in_paper.html")


class ViewsNoDataLearnerTestCase(django.test.TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="fakeuser",
                                             email="test@fakemail",
                                             password="fakepassword")
        self.iden = Identification.objects.create(formName="dummy",
                                                  user=self.user)

        Group.objects.create(name="Alumnos")
        self.user.groups.add(1)
        Name.objects.create(partname="F", text="fakename",
                            identification=self.iden)
        Name.objects.create(partname="S", text="fakesurname",
                            identification=self.iden)
        self.client.login(username="fakeuser", password="fakepassword")

    def testMain(self):
        response = self.client.post("/main/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        tems = ["candidateTool/new_main.html",
                "candidateTool/base_main.html",
                "candidateTool/base.html"]
        for i in range(len(response.template)):
            self.failUnlessEqual(tems[i], response.template[i].name)
        con = response.context[0]
        self.failUnlessEqual(response.context['pre'], None)
        self.failUnlessEqual(response.context['tests'], {})
        self.failUnlessEqual(response.context['candidate'], iden)
        self.failUnlessEqual(response.context['next'], None)
        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['main'], True)

    def testMainPage(self):
        response = self.client.post("/main/2/")
        iden = Identification.objects.get(user=self.user)
        self.failUnlessEqual(response.status_code, 200)
        tems = ["candidateTool/new_main.html",
                "candidateTool/base_main.html",
                "candidateTool/base.html"]
        for i in range(len(response.template)):
            self.failUnlessEqual(tems[i], response.template[i].name)
        con = response.context[0]
        self.failUnlessEqual(response.context['pre'], None)
        self.failUnlessEqual(response.context['tests'], {})
        self.failUnlessEqual(response.context['candidate'], iden)
        self.failUnlessEqual(response.context['next'], None)
        self.failUnlessEqual(response.context['dem'], None)
        self.failUnlessEqual(response.context['main'], False)

    def testExam(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.iden.subject.add(subject)
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")
        name = self.iden.name_set.get(partname="f").text
        surname = self.iden.name_set.get(partname="s").text
        import os
        path = "../django_templates/candidateTool/exams/prueba1.html"
        has_been_created = False
        if not os.path.exists(path):
            fd = open(path, "w")
            fd.write("prueba")
            fd.close()
            has_been_created = True
            fname = exam.filename.split(".xml")[0].split("/")[-1] + "1.html"
            template = 'candidateTool/exams/' + fname
        response = self.client.post("/exam/1/")
        self.failUnlessEqual(response.context['name'], name)
        self.failUnlessEqual(response.context['surname'], surname)
        if has_been_created:
            self.failUnlessEqual(response.template.name, template)
            os.remove(path)

    def testContinueExam(self):
        response = self.client.post("/continue_exam/1/")
        self.failUnlessEqual(response.status_code, 200)

    def testMyExam(self):
        tr = TestResult.objects.filter(identifier=2)
        response = self.client.post("/myexam/1/")
        self.failUnlessEqual(response.status_code, 200)

    def testSolvedNotAvailableExam(self):
        response = self.client.post("/solved/2/")
        self.failUnlessEqual(response.status_code, 200)

    def testSolvedNotFinishedExam(self):
        dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
        dtinaweek = dtyesterday + datetime.timedelta(8)
        subject = Subject.objects.create(name="Prueba",
                                         uri="http://arco.esi.uclm.es/")
        self.exam = Test.objects.create(name="prueba",
                                        filename="../QuizsBank/prueba.xml",
                                        start_date=dtyesterday,
                                        end_date=dtinaweek,
                                        subject=subject,
                                        description="",
                                        representation_type="Web")

        response = self.client.post("/solved/1/")
        self.failUnlessEqual(response.status_code, 200)

# class ViewsDumpDBTestCase(django.test.TestCase):

#     def testSolvedAvailableExam(self):
#         dtyesterday = datetime.datetime.now() - datetime.timedelta(1)
#         dtaweekago = dtyesterday - datetime.timedelta(6)
#         subject = Subject.objects.create(name="Prueba",
#                                          uri="http://arco.esi.uclm.es/")
#         self.exam = Test.objects.create(name="prueba",
#                                         filename="../QuizsBank/prueba.xml",
#                                         start_date=dtaweekago,
#                                         end_date=dtyesterday,
#                                         subject=subject,
#                                         description="",
#                                         representation_type="Web")

#         response = self.client.post("/solved/1/")
#         self.failUnlessEqual(response.status_code, 200)
#         self.failUnlessEqual(response.template[0].name,
#                              "candidateTool/invalid.html")


