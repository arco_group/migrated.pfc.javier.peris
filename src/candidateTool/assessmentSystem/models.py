"""Descriptions of the database tables as Python classes"""

from django.db import models
from django.contrib.auth.models import User
import datetime

# class PersonInformation(models.Model):
#     langtype = models.CharField(max_length=16)

#     class Admin:
#         ordering = ('uid')
#         search_fields = ('uid')


class Identification(models.Model):
    formName = models.CharField(blank=True, max_length=128)
    user = models.OneToOneField(User)
    subject = models.ManyToManyField('Subject')

    def __unicode__(self):
        return self.formName

    def get_assessmentResult(self, exam):
        for c in id.context_set.all():
            if c.assessmentresult.testResult.identifier == exam:
                return c.assessmentresult
        return None

    class Admin:
        list_filter = ('locality', 'city', 'country',)
        search_fields = ('formName',)


# class ContactInfo(models.Model):
#     telephoneAreaCode = models.CharField(blank=True, max_length=10)
#     telephoneIndNumber = models.CharField(blank=True, max_length=10)
#     faxAreaCode = models.CharField(blank=True, max_length=10)
#     faxIndNumber = models.CharField(blank=True, max_length=10)
#     mobileAreaCode = models.CharField(blank=True, max_length=10)
#     mobileIndNumber = models.CharField(blank=True, max_length=10)
#     email = models.EmailField(blank=True)
#     web = models.URLField(blank=True, max_length=128, verify_exists=False)
#     identification = models.ForeignKey('Identification')


GENDER = (
    ('M','Male'),
    ('F','Female'),
)
class Demographics(models.Model):
    representation = models.ImageField(upload_to='learner_images')
    gender = models.CharField(choices=GENDER, max_length=6)
    date = models.DateTimeField(blank=True)
    placeofbirth = models.CharField(blank=True, max_length=128)
    uid = models.CharField(blank=True, max_length=32)
    identification = models.ForeignKey('Identification')

    def __unicode__(self):
        return self.representation.__str__()

    class Admin:
        list_display = ('uid', 'date', 'placeofbirth',)
        ordering = ('uid',)
        search_fields = ('uid',)


# ADDRESS = (
#     ('w', 'work'),
#     ('pe', 'permanent'),
#     ('pr', 'private'),
#     ('t', 'temporary'),
#     ('m', 'mailing'),
#     ('c', 'campus'),
#     ('b', 'billing'),
# )
# class Address(models.Model):
#     type = models.CharField(choices=ADDRESS, max_length=9)
#     complex = models.CharField(blank=True, max_length=128)
#     streetnumber = models.CharField(blank=True, max_length=8)
#     streetname = models.CharField(blank=True, max_length=128)
#     streettype = models.CharField(blank=True, max_length=8)
#     aptnumber = models.CharField(blank=True, max_length=8)
#     aptsuffix = models.CharField(blank=True, max_length=2)
#     locality = models.CharField(blank=True, max_length=128)
#     city = models.CharField(blank=True, max_length=128)
#     country = models.CharField(blank=True, max_length=128)
#     statepr = models.CharField(blank=True, max_length=128)
#     region = models.CharField(blank=True, max_length=128)
#     postcode = models.CharField(blank=True, max_length=16)
#     timezone = models.CharField(blank=True, max_length=8)
#     identification = models.ForeignKey('Identification')



PARTNAME = (
    ('F','First'),
    ('S','Surname'),
)
class Name(models.Model):
    partname = models.CharField(choices=PARTNAME, max_length=6)
    text = models.CharField(max_length=128)
    identification = models.ForeignKey('Identification')

    def __unicode__(self):
        return self.text

    class Admin:
        list_display = ('partname', 'text',)
        ordering = ('text',)
        search_fields = ('text',)


class Test(models.Model):

    name = models.TextField(blank=True)
    filename = models.CharField(unique=True, max_length=255, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    time_limit = models.IntegerField(null=True, blank=True)
    subject = models.ForeignKey('Subject')
    description = models.TextField(blank=True)
    representation_type = models.CharField(max_length=15)

    def is_finished(self):
        return self.end_date < datetime.datetime.now()

    def is_feasible(self):
        feasible = False
        if self.start_date <= datetime.datetime.now() < self.end_date:
            feasible = True
        return feasible

    def is_started(self):
        return self.start_date <= datetime.datetime.now()

    @property
    def descripcion(self):
        return self.description

    def __str__(self):
        return self.name

    class Meta:
        db_table = u'test'

    class Admin:
        list_display = ('name', )
        ordering = ('start_date', 'end_date', )
        search_fields = ('name', 'start_date', 'end_date', )


class AssessmentResult(models.Model):
    context = models.OneToOneField('Context')
    testResult = models.OneToOneField('TestResult')
#    itemResults = models.ManyToManyField('ItemResult')

    def is_scored(self):
        scored = True
        for i in self.itemresult_set.all():
            if i.sessionStatus != "f":
                scored = False
        return scored


class Context(models.Model):
    identification = models.ForeignKey('Identification')
#     identification = models.OneToOneField('Identification')
#     sessionIdentifier = models.OneToOneField('SessionIdentifier')


SESSION_STATUS = (
    ('i', 'initial'),
    ('ps', 'pendingSubmission',),
    ('prp', 'pendingResponseProcessing',),
    ('f', 'final'),
)
class ItemResult(models.Model):
    assessmentResult = models.ForeignKey(AssessmentResult)
    identifier = models.CharField(max_length=128)
    #Position within the test, first postion is numbered with 1
    sequenceIndex = models.PositiveIntegerField(null=True, blank=True)
    datestamp = models.DateField()
    sessionStatus = models.CharField(choices=SESSION_STATUS, max_length=32)
    candidateComment = models.TextField(blank=True)

class SessionIdentifier(models.Model):
    sourceId = models.URLField(verify_exists=False)
    identifier = models.CharField(max_length=128)
    context = models.ForeignKey('Context')


class TestResult(models.Model):
    identifier = models.CharField(max_length=128)
    dateStamp = models.DateField()

BASE_TYPE = (
    ('id', 'identifier'),
    ('b', 'boolean'),
    ('i', 'integer'),
    ('f', 'float'),
    ('s', 'string'),
    ('po', 'point'),
    ('pa', 'pair'),
    ('dp', 'directedPair'),
    ('du', 'duration'),
    ('f', 'file'),
    ('u', 'uri'),
)
CARDINALITY = (
    ('s', 'single'),
    ('m', 'multiple'),
    ('o', 'ordered'),
    ('r', 'record'),
)
class ItemVariable(models.Model):
    testResult = models.ForeignKey(TestResult, null=True)
    itemResult = models.ForeignKey(ItemResult, null=True)
    identifier = models.CharField(max_length=128)
    cardinality = models.CharField(choices=CARDINALITY, max_length=8)
    baseType = models.CharField(choices=BASE_TYPE, max_length=12, blank=True)

    class Meta:
        abstract = True


class CandidateResponse(models.Model):
    value = models.CharField(max_length=128)


class CorrectResponse(models.Model):
    value = models.CharField(max_length=128)


class ResponseVariable(ItemVariable):
#    itemVariable = models.OneToOneField('ItemVariable')
    correctResponse = models.OneToOneField('CorrectResponse', null=True)
    candidateResponse = models.OneToOneField('CandidateResponse', null=True)


VIEW = (
    ('a', 'author'),
    ('c', 'candidate',),
    ('p', 'proctor'),
    ('s', 'scorer'),
    ('tc', 'testConstructor'),
    ('t', 'tutor'),
)
class OutComeVariable(ItemVariable):
    view = models.CharField(choices=VIEW, max_length=16)
    interpretation = models.CharField(max_length=128)
    longInterpretation = models.URLField(verify_exists=False)
    normalMaximum = models.FloatField(null=True)
    normalMinimum = models.FloatField(null=True)
    masteryValue = models.FloatField(null=True)
    value = models.CharField(max_length=128)
#    itemVariable = models.OneToOneField('ItemVariable')

# class Subject(models.Model):
#     name = models.CharField(max_length=128)
#     uri = models.URLField(blank=True, max_length=128, verify_exists=False)
# #    identification = models.ForeignKey('Identification')

#     def __unicode__(self):
#         return self.name

#     class Admin:
#         ordering = ('name',)
#         search_fields = ('name',)

class Question(models.Model):
    filename = models.CharField(unique=True, max_length=255, blank=True)
    title = models.TextField(blank=True)
    default_mark = models.FloatField(null=True, blank=True)
    child_name = models.CharField(max_length=765, blank=True)
    class Meta:
        db_table = u'question'

class Answer(models.Model):
    value = models.TextField(blank=True)
    calification = models.FloatField(null=True, blank=True)
    ide = models.TextField(blank=True)
    question = models.ForeignKey('Question')
    class Meta:
        db_table = u'answer'

class Subject(models.Model):
    name = models.CharField(max_length=384, blank=True)
    uri = models.CharField(max_length=384, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        db_table = u'subject'

    class Admin:
        ordering = ('name',)
        search_fields = ('name',)

# class UsageData(models.Model):
#     glossary = models.CharField(unique=True, max_length=128, blank=True)

# class ItemStatistic(models.Model):
#     name = models.CharField(unique=True, max_length=128)
#     glossary = models.CharField(unique=True, max_length=128, blank=True)
#     context = models.CharField(max_length=128)
#     caseCount = models.CharField(max_length=128, blank=True)
#     stdError = models.FloatField(blank=True)
#     stdDeviation = models.FloatField(blank=True)
#     lastUpdated = models.DateTimeField(blank=True)
#usageData = models.ForeignKey('UsageData')

#     class Meta:
#         abstract = True

# class TargetObject(models.Model):
#     identifier = models.CharField(unique=True, max_length=128)
#     identifier = models.CharField(unique=True, max_length=128, blank=True)
#     itemStatistic = models.ForeignKey('OrdinaryStatistic')

# class OrdinaryStatistic(ItemStatistic):
#     value = models.IntegerField()

# class CategorizedStatistic(ItemStatistic):
#     mapping =
