from candidateTool.assessmentSystem.models import Identification, Subject, Demographics, Name, Test, Context
from django.contrib import admin

admin.site.register(Identification)
admin.site.register(Subject)
admin.site.register(Demographics)
admin.site.register(Name)
admin.site.register(Test)
admin.site.register(Context)
