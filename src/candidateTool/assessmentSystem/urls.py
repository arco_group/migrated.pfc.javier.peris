from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('candidateTool.assessmentSystem.views',
    (r'^$', 'index'),
    (r'^enter/$', 'enter'),
    (r'^main/(?P<page_id>\d+)/$', 'render_pages'),
    (r'^main/$', 'main'),
    (r'^exam/(?P<exam_id>\d+)/$', 'exam'),
    (r'^continue_exam/(?P<part_id>\d+)/$', 'part'),
    (r'^continue_solved_exam/(?P<part_id>\d+)/$', 'continue_solved_exam'),
    (r'^myexam/(?P<exam_id>\d+)/$', 'my_exam'),
    (r'^solved/(?P<exam_id>\d+)/$', 'solved_exam'),
    (r'^report/(?P<exam_id>\d+)/$', 'report'),
    (r'^salir/$', 'logout'),
    (r'^open/$', 'open_exam'),
    (r'^close/$', 'close_exam'),
    (r'^extend/$', 'extend_exam'),
    (r'^exams/$', 'exams'),
    (r'^mark/(?P<exam_id>\d+)/$', 'marks'),
    (r'^download/$', 'download'),
    (r'^statistics/$', 'statistics'),
    (r'^show_statistics/$', 'show_statistics'),
    (r'^exam_statistics/$', 'exam_statistics'),
    (r'^questionsprp/$', 'questions_prp'),
    (r'^questionsprpview/$', 'questions_prp_view'),
    (r'^examinpaper/$', 'exam_in_paper'),
    (r'^update_paper_exam/$', 'update_paper_exam'),
    (r'^update_results_paper/$', 'update_results_paper'),
    (r'^process_questionsprp/(?P<item_result>\d+)/$', 'process_questions_prp'),
    (r'^language/', 'select_language'),
    (r'^i18n/', include('django.conf.urls.i18n')),

#    (r'^$', 'login', template_name='login.html'),
#    (r'^enter/invalid/$', 'invalid'),
#      (r'^static/(?P<path>.*)$', 'django.views.static.serve',
#      {'document_root': '/home/peris/Desktop/csl2-sdocente/src/django_media',
#       'show_indexes': True }),
    # Example:
    # (r'^candidateTool/', include('candidateTool.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
#    (r'^admin/', admin.site.root),
    (r'^admin/(.*)', admin.site.root),
)
