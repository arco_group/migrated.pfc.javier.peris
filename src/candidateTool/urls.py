from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
     (r'', include('candidateTool.assessmentSystem.urls')),
     (r'^static/(?P<path>.*)$', 'django.views.static.serve',
      {'document_root': '/home/peris/Desktop/csl2-sdocente/src/django_media',
       'show_indexes': True}),
    # Example:
    # (r'^candidateTool/', include('candidateTool.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

     # Uncomment the next line to enable the admin:
#     (r'^admin/(.*)', admin.site.root),
)
