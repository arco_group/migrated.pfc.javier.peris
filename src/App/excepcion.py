class DataException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)

class TitleException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)

class DefaultMarkException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)

class WordingException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)

class ExpectedLengthException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)

class SolutionsException(Error):

    def __init__(self, msg):
        self.msg = msg
        
    def __str__(self):
        return "Error " + str(self.msg)
