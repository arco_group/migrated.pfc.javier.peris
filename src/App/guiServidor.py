#!/usr/bin/python
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
except:
	sys.exit(1)

import controlador	

class GuiServidor:

	def __init__(self,file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)

		self.dialogConectar = self.widgets.get_widget("dialogConectar")
		self.dialogConectar.show()
		
	def wMain_delete_event(self, widget, event, data=None):
		gtk.main_quit()

	#Boton dialog
	def buttonEjecutar_clicked(self,widget,data=None):
		co = controlador.Controlador()
		co.arrancar(self.widgets.get_widget("entryPuerto").get_text())

	def main(self):
		gtk.main()
		return 0

if __name__ == "__main__":
	app = GuiServidor("servidor.glade")
	app.main()
