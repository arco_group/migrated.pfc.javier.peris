#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys, traceback, Ice
import Comunicaciones

class Proxy:
	def conectar(self,ip,puerto):
		status = 0
		ic = None
		try:
			ic = Ice.initialize(sys.argv)
			base = ic.stringToProxy("SimpleConector: "+ip+" -p "+puerto)
			servidor = Comunicaciones.ServidorPrx.checkedCast(base)
			if not servidor:
				raise RuntimeError("Invalid proxy")
			servidor.conectar("Estoy conectado")
		except:
			traceback.print_exc()
			status = 1
		if ic:
			# Clean up
			try:
				ic.destroy()
			except:
				traceback.print_exc()
				status = 1
		#sys.exit(status)

	def registrarse(self,nombre,passw):
		status = 0
		ic = None
		try:
			ic = Ice.initialize(sys.argv)
			base = ic.stringToProxy("SimpleConector: default  -p 10000")
			servidor = Comunicaciones.ServidorPrx.checkedCast(base)
			if not servidor:
				raise RuntimeError("Invalid proxy")
			servidor.registrarCliente(nombre,passw)
		except:
			traceback.print_exc()
			status = 1
		if ic:
			# Clean up
			try:
				ic.destroy()
			except:
				traceback.print_exc()
				status = 1
				
	def identificarse(self,nombre,passw):
		status = 0
		ic = None
		try:
			ic = Ice.initialize(sys.argv)
			base = ic.stringToProxy("SimpleConector: default  -p 10000")
			servidor = Comunicaciones.ServidorPrx.checkedCast(base)
			if not servidor:
				raise RuntimeError("Invalid proxy")
			servidor.identificarCliente(nombre,passw)
		except:
			traceback.print_exc()
			status = 1
		if ic:
			# Clean up
			try:
				ic.destroy()
			except:
				traceback.print_exc()
				status = 1
