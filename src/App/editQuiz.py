#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import os
import sys
sys.path.append('../AuthoringTool')
sys.path.append('../App')
sys.path.append('../Editor')
# import libxml2
# import libxslt
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
except:
	sys.exit(1)
import gobject
import amara
import time

import choiceMultipleController
import extended_textController
import text_entryController
import inLineChoiceController
#import match
import alert
import persistencia
import vocabulary
import term
import collection
import dochtmlRender
import doctexRender
import editor
import db

import logging


class EditQuiz:

    targets = [
        ('MY_TREE_MODEL_ROW', gtk.TARGET_SAME_WIDGET, 0),
        ('text/plain', 0, 1),
        ('TEXT', 0, 2),
        ('STRING', 0, 3),
    ]
    target_type_vbox = 1
    target_type_cbutton = 2

    categoryImageTooltip = {"match": ["../Icons/matchImage.png",
									  "Conectar"],
							"choiceMultiple": ["../Icons/choice.png",
											   "Eleccion multiple"],
							"extendedText": ["../Icons/extendedText.png",
											 "Ensayo"],
							"textEntry": ["../Icons/textEntry.png",
										  "Entrada de texto"],
							"inLineChoice": ["../Icons/inLineChoice.png",
											 "Eleccion en linea"]}


    def __init__(self, fileGlade, fileQuiz="../QuizsBank/prueba.xml"):
		self.widgets = gtk.glade.XML(fileGlade)
		self.widgets.signal_autoconnect(self)
		self.window = self.widgets.get_widget("wEditQuiz")
		self.fileQuiz = fileQuiz
		self.numParts = 1
		self.numSections = 0
		self.questions = []
		self.categories = []
		self.filenames = []
		self.hayElementos = False
		self.tQuestions = self.widgets.get_widget("tableQuestion")
		self.cbQuestion = self.widgets.get_widget("checkbuttonQuestion")
		self.dCreateVocabulary = self.widgets.get_widget("dialogCreateVocabulary")
		self.dCreateTerm = self.widgets.get_widget("dialogCreateTerm")
		self.bufferInfo = self.widgets.get_widget("textviewInfo").get_buffer()
		self.agente = persistencia.Agente()

		self.widgets.get_widget("toolbutton6").connect('clicked',
 														self.preview_question,
 														"HTML")
		self.widgets.get_widget("toolbutton5").connect('clicked',
	 													self.preview_question,
 														"LaTeX")

		# ELEMENTOS DE PRUEBA
		l = ["label32", "label33", "checkbuttonQuestion"]
		self.questions.append(l)
		for e in l:
			self.widgets.get_widget(e).set_data("row", 0)

		self.cbtarget = [("text/plain", 0, EditQuiz.target_type_vbox)]
		self.vbtarget = [("text/plain", 0, EditQuiz.target_type_cbutton)]

		# DRAG 'N DROP
		checkbuttonTest = self.widgets.get_widget("checkbuttonTest")
		checkbuttonTest.connect("drag_data_get", self.sendCallback)
		checkbuttonTest.set_data("filepath", "../QuestionBank/Ensayo.xml")
		checkbuttonTest.drag_source_set(gtk.gdk.BUTTON1_MASK, self.cbtarget,
										gtk.gdk.ACTION_COPY)

		self.vbox = self.widgets.get_widget("vboxQuestionsQuiz")
		self.vbox.connect("drag_data_received", self.receiveCallback)
		self.vbox.drag_dest_set(gtk.DEST_DEFAULT_MOTION |
								gtk.DEST_DEFAULT_HIGHLIGHT |
								gtk.DEST_DEFAULT_DROP,
								self.vbtarget , gtk.gdk.ACTION_COPY)

		# TREEVIEW
        self.treestore = gtk.TreeStore(str, str, str)
        self.treeview = gtk.TreeView(self.treestore)
        self.tvcolumn = gtk.TreeViewColumn('Examen')
		self.tvcolumn2 = gtk.TreeViewColumn(u'Puntuacin')
        self.treeview.append_column(self.tvcolumn)
		self.treeview.append_column(self.tvcolumn2)
        self.cell = gtk.CellRendererText()
		self.cell.set_property('editable', True)
        self.cellrenderertext = gtk.CellRendererText()
		self.cellrenderertext.set_property('editable', True)
		self.tvcolumn2.pack_start(self.cellrenderertext, True)
		self.cellrenderertext.connect('edited', self.edited_mark,
 										self.treestore)
		self.tvcolumn2.pack_start(self.cellrenderertext, True)
        self.tvcolumn2.add_attribute(self.cellrenderertext, 'text', 2)
        self.tvcolumn.pack_start(self.cell, True)
        self.tvcolumn.add_attribute(self.cell, 'text', 0)
        self.treeview.set_search_column(0)
        self.tvcolumn.set_sort_column_id(0)
        self.treeview.set_reorderable(True)
		piter = self.treestore.append(None, ["Parte 1", "PARTE", ""])
		txt = "Seccin 1"
		self.piter = self.treestore.append(piter, [txt.decode('latin-').encode('utf-8'), "SECCION",""])

        # Allow enable drag and drop of rows including row move
        self.treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK,
											   self.targets,
											   gtk.gdk.ACTION_DEFAULT|
											   gtk.gdk.ACTION_MOVE |
											   gtk.gdk.ACTION_COPY)

        self.treeview.enable_model_drag_dest(self.targets,
                                             gtk.gdk.ACTION_DEFAULT)

        self.treeview.connect("drag_data_get", self.drag_data_get_data)
        self.treeview.connect("drag_data_received",
                              self.drag_data_received_data)

        #self.treeview.add_events(gtk.gdk.BUTTON2_MASK)
		self.treeview.connect("button-press-event", self.rightClick)
		# self.treeview.set_hover_expand(True)
# 		self.treeview.set_show_expanders(True)

		# POP UP MENU
		self.menu = gtk.Menu()
		self.menuItem_add_part = gtk.ImageMenuItem(gtk.STOCK_ADD)
		self.menuItem_add_part.get_children()[0].set_label('Agregar parte')
		self.menuItem_add_section = gtk.ImageMenuItem(gtk.STOCK_ADD)
		self.menuItem_add_section.get_children()[0].set_label('Agregar seccion')
		self.menu.append(self.menuItem_add_part)
		self.menuItem_add_part.connect("activate", self.menuitem_add_part)
		self.menu.append(self.menuItem_add_section)
		self.menuItem_add_section.connect("activate", self.menuitem_add_section)
		self.menuItem_remove = gtk.ImageMenuItem(gtk.STOCK_REMOVE)
		self.menu.append(self.menuItem_remove)
		self.menuItem_remove.connect("activate", self.menuitem_remove)

		# LOAD CATEGORIES TAB
        treeviews = [gtk.TreeView(), gtk.TreeView()]
        scrolledwindows = ["scrolledwindowVocabularies", "scrolledwindowTerms"]
        func_cell_name = [self.cell_name_vocabulary_edited,
						  self.cell_name_term_edited]
        func_cell_description = [self.cell_description_vocabulary_edited,
                                 self.cell_description_term_edited]
        for i in range(2):
            liststore = gtk.ListStore(str, str)
            treeviews[i].set_model(liststore)
            cell_name = gtk.CellRendererText()
            cell_name.set_property("editable", True)
            cell_name.connect('edited', func_cell_name[i], liststore)
            tvcolumn_name = gtk.TreeViewColumn("Nombre", cell_name, text=0)
            cell_description = gtk.CellRendererText()
            cell_description.set_property("editable", True)
            cell_description.connect('edited', func_cell_description[i],
                                     liststore)
			txt = "Descripcin"
            tvcolumn_description = gtk.TreeViewColumn(txt.decode('latin-').encode('utf-8'),
                                                      cell_description, text=1)
            treeviews[i].append_column(tvcolumn_name)
            treeviews[i].append_column(tvcolumn_description)
            treeviews[i].set_search_column(0)
            tvcolumn_name.set_sort_column_id(0)
            self.widgets.get_widget(scrolledwindows[i]).add(treeviews[i])
        self.tvVocabularies = treeviews[0]
        self.tvVocabularies.get_selection().connect("changed",
													self.changed_selection)
        self.tvTerms = treeviews[1]

		# LOAD VOCABULARIES AND TERMS IN THE QUESTION_MANAGER
        self.dict_vocabularies = {}
		self.treeviews = []
        self.load_vocabularies()
		self.widgets.get_widget("comboboxQuestions").set_active(0)

		self.window.show_all()

	def window_delete_event(self, widget, event, data=None):
		#self.window.hide()
		gtk.main_quit()
		return False

	# TREEVIEW METHODS
	def edited_mark(self, cell, path, new_text, user_data):
		print "EDITANDO: Path", path
		print "EDITANDO: NEW_TEXT", new_text
# 		self.treestore[path][0] = new_text
# 		self.treestore[path][1] = new_text
		self.treestore[path][2] = new_text
		iter = self.treestore.get_iter(path)
		self.treestore.set_value(iter, 1, new_text)
		self.treestore.set_value(iter, 2, new_text)
		return

	def drag_data_get_data(self, treeview, context, selection, target_id,
                           etime):
        treeselection = treeview.get_selection()
        model, iter = treeselection.get_selected()
        data = model.get_value(iter, 0)
        selection.set(selection.target, 8, data)

    def drag_data_received_data(self, treeview, context, x, y, selection,
                                info, etime):
        model = treeview.get_model()
		limitador = selection.data.find("##")
		label = selection.data[0:limitador]
		filepath = selection.data[limitador + 2: len(selection.data)]

        data = selection.data
		print "DATAAA TREEVIEW", data
        drop_info = treeview.get_dest_row_at_pos(x, y)
		logging.debug("Drop_Info: %s" % str(drop_info))
		isvalid = False
        if drop_info:
            path, position = drop_info
			logging.info("PATH" + str(path))
            itera = model.get_iter(path)
			if len(path) == 2 or len(path) == 3:
				isvalid = True
				if (position == gtk.TREE_VIEW_DROP_BEFORE):
					model.insert_before(None, itera, [label, filepath, ""])
				elif (position == gtk.TREE_VIEW_DROP_INTO_OR_BEFORE):
					model.append(itera, [label, filepath, ""])
				else:
					model.insert_after(None, itera, [label, filepath, ""])
			else:
				txt = "Una pregunta debe situarse dentro de una seccin"
				self.bufferInfo.set_text(txt.decode('latin-').encode('utf-8'))
        #else:
           # model.append(None, [data])
        if context.action == gtk.gdk.ACTION_MOVE and isvalid:
            context.finish(True, True, etime)
        return


	def rightClick(self, widget, event):
		if event.button == 3:
		    info_at_pos = self.treeview.get_path_at_pos(event.x, event.y)
			logging.info("Info At Pos: %" % str(info_at_pos))
			if info_at_pos == None:
				self.treeview.get_selection().unselect_all()
				self.menuItem_add_section.hide()
				self.menuItem_add_part.show()
# 				self.menuItem_remove.show()
# 				self.menu.popup(None, None, None, event.button, event.time)
			elif len(info_at_pos[0]) == 1:
			    self.menuItem_add_part.hide()
				self.menuItem_add_section.show()
# 				self.menuItem_remove.show()
# 				self.menu.popup(None, None, None, event.button, event.time)
				self.menuItem_remove.show()
			else:
				self.menuItem_remove.show()
			self.menu.popup(None, None, None, event.button, event.time)

	def menuitem_add_part(self, widget):
		self.numParts += 1
		piter = self.treestore.append(None,
									  ['Parte ' + str(self.numParts), 'PARTE' +
										str(self.numParts), ""])
		txt = 'Seccin '
		self.treestore.append(piter, [txt.decode('latin-').encode('utf-8') + str(self.numSections), 'SECCION' +
							str(self.numSections), ""])
# 		self.treestore.append(piter, ['(Vacio)'])

	def menuitem_add_section(self, widget):
		self.numSections += 1
		selection = self.treeview.get_selection()
		model, iterator = selection.get_selected()
		piter = self.treestore.insert_after(iterator, None,
											['Seccion' + str(self.numSections), 'SECCION' +
											 str(self.numSections), ""])
		self.treeview.expand_to_path(model.get_path(iterator))
		# self.treestore.append(piter, ['(Vacio)'])

	def menuitem_remove(self, widget):
		remove = True
		selection = self.treeview.get_selection()
		model, iterator = selection.get_selected()
		print "PATH:", model.get_path(iterator)
		len_path = len(model.get_path(iterator))
		if len_path == 1:
			try:
				model.get_iter(1,)
			except:
				remove = False
				self.bufferInfo.set_text("El examen tiene que tener al menos una parte")
		if len_path == 2:
			parent = model.iter_parent(iterator)
			n_children = model.iter_n_children(parent)
			if not n_children:
				remove = False
				txt = "Cada parte tiene que tener al menos una seccion"
				self.bufferInfo.set_text(txt.decode('latin-').encode('utf-8'))
			print "CREO QUE ELIMINO PARTE"
			self.numSections = 	self.numSections - 1
		print "PASE POR AQUI"
		if remove:
			self.treestore.remove(iterator)

	def sendCallback(self, widget, context, selection, targetType, eventTime):
 		selection.set(selection.target, 8, widget.get_label() + "##" + 
						widget.get_data('filepath'))

	def receiveCallback(self, widget, context, x, y, selection, targetType,
		 						time):
		if not self.hayElementos:
				self.update_quizView()
		limitador = selection.data.find("##")
		label = selection.data[0:limitador]
		path = selection.data[limitador + 2: len(selection.data)]
		print "vbox label", label
		print "vbox path", path
		# wig_sent = self.get_widget(selection.data)
		# print "WIDGET", wig_sent
# 		print "ETIQUETA", wig_sent.get_label()
# 		print "PATH:", wig_sent.get_data('filepath')
 		iterator = self.treestore.append(self.piter, [label, path, ""])
# 		iterator = self.treestore.append(self.piter, [wig_sent.get_label(),
# 											wig_sent.get_data('filepath')])

    def get_widget(self, name):
		for widget in self.tQuestions.get_children():
			print "NOMBRES DE LOS WIDGETS:", widget.name
			if widget.name == name:
			    return widget

# 	def comboboxCategories_changed(self, widget, data=None):
# 		self.update_elements_by_category()

	# QUESTION TAB
	def buttonEditCategory_clicked(self, widget, data=None):
		self.widgets.get_widget("notebook").next_page()

	def comboboxNewQuestion_changed(self, widget, data=None):
		wa = widget.get_active()
        if wa == 1:
            cm = choiceMultipleController.ChoiceMultipleController("../AuthoringTool/choiceMultiple.glade",
																   self.dict_vocabularies, True)
            cm.run()
        elif wa == 2:
            cm = choiceMultipleController.ChoiceMultipleController("../AuthoringTool/choiceMultiple.glade",
																   self.dict_vocabularies, False)
            cm.run()
        elif wa == 3:
            et = extended_textController.ExtendedTextController("../AuthoringTool/extended_text.glade",
																self.dict_vocabularies)
            et.run()
        elif wa == 4:
            te = text_entryController.TextEntryController("../AuthoringTool/text_entry.glade",
														  self.dict_vocabularies)
            te.run()
        elif wa == 5:
            ic = inLineChoiceController.InLineChoiceController("../AuthoringTool/inLineChoice.glade",
															   self.dict_vocabularies)
            ic.run()
        # elif wa == 6:
        # 				match.Match("match.glade", self.dict_vocabularies)
		terms = []
        if self.treeviews != None:
            for treeview in self.treeviews:
                liststore = treeview.get_model()
                for element in liststore:
                    if element[1]:
                        terms.append(element[0])
            self.update_elements_by_terms(terms)

    def toolbuttonDeleteAll_clicked(self, widget, data=None):
		if self.widgets.get_widget("checkbuttonQuestion").get_active():
			self.deleteAll_elements()
		else:
			seleccionados = []
			for c in self.tQuestions.get_children():
			    if isinstance(c, gtk.CheckButton) and c.get_active() and c.get_data("row") != 0:
				    seleccionados.append(c.get_data("row"))
			for c in self.tQuestions.get_children():
				if c.get_data("row") in seleccionados:
				    self.tQuestions.remove(c)
		    self.tQuestions.resize(self.tQuestions.get_property("n-rows") - 
									len(seleccionados), 3)
			self.tQuestions.show_all()

	def _deleteAll_elements(self):
		for c in self.tQuestions.get_children():
		    if c.get_data("row")!=0:
			    self.tQuestions.remove(c)
		self.tQuestions.resize(1, 4)

    def update_elements_by_terms(self, term):
        self._deleteAll_elements()
		if term != None:
            # try:
                questions = self.agente.get_questions_by_term(term)
                print "PREGUNTAS!", questions
                for q in questions:
                    self._create_element(q)
#             except:
#                 logging.info("There are not questions for this terms")

    def checkbuttonselectAll_toggled(self, widget, data=None):
		active = widget.get_active()
		for w in self.widgets.get_widget("tableQuestion").get_children():
			if isinstance(w, gtk.CheckButton):
				w.set_active(active)

    def preview_question(self, widget, data):
        print "DATAA", data
        # row = widget.get_data("row")
#         logging.info("Row question widget" + str(row))
        filenames = []
        # if self.widgets.get_widget("checkbuttonQuestion").get_active():
        for e in self.tQuestions:
            if isinstance(e, gtk.CheckButton) and e.get_data("row") != 0 and \
                e.get_active():
                    filepath = e.get_data("filepath")
                    print "PATH:", filepath
                    logging.info("Filepath: %s" % filepath)
                    filenames.append(filepath)
#         else:
#             for e in self.tQuestions:
#                 if isinstance(e, gtk.CheckButton) and e.get_data("row") == row:
#                     filepath = e.get_data("filepath")
#                     print "PATHS:", filepath
#                     logging.info("Filepath:" +  filepath)
#                     filenames.append(filepath)
        sequence = collection.Collection(filenames)
        questions = sequence.create()
        if data == "HTML":
            render = dochtmlRender.DocHTMLRender()
        else:
            render = doctexRender.DocTeXRender()
        print "PREGUNTAS", questions
        for question in questions:
            print "PREGUNTA", question
            question.accept(render)
        render._close_document()
#         if data == "HTML":
#             os.system('iceweasel ../Previews/tmp.html')
#         else:
#             for i in range(2):
#                 os.system('pdflatex ../Previews/tmp.tex')
#             os.system('kpdf ../Previews/tmp.pdf')


	def edit_question(self, widget, data=None):
		row = widget.get_data("row")
        filenames = []
        for e in self.tQuestions.children():
            if isinstance(e, gtk.CheckButton) and e.get_data("row") == row:
                filepath = e.get_data("filepath")
                print "PATH:", filepath
                logging.info("Filepath: %s" % filepath)
                filenames.append(filepath)
                sequence = collection.Collection(filenames)
        questions = sequence.create()
        render = gtkRender.GtkRender()
        print "PREGUNTAS", questions
        for question in questions:
            print "PREGUNTA", question
            question.accept(render)

	def remove_question(self, widget, data=None):
		tableQ = self.widgets.get_widget("tableQuestion")
		nRows = tableQ.get_property("n-rows")
		logging.debug("Row %s" % str(widget.get_data('row')))
		for q in tableQ.get_children():
				if q.get_data("row") == widget.get_data("row"):
						if isinstance(q, gtk.CheckButton):
						    filename = q.get_data("filepath")
						tableQ.remove(q)
		tableQ.resize(nRows - 1, 4)
		tableQ.show_all()
		self.agente.delete_question(filename)

    def _create_element(self, question):
		tableQ = self.tQuestions
		nRows = tableQ.get_property("n-rows")
		lrow = []

		toolBar = gtk.Toolbar()
		toolBar.set_style(gtk.TOOLBAR_ICONS)
		toolBar.set_icon_size(gtk.ICON_SIZE_MENU)
		toolBar.set_property('width-request', 138)
		toolBar.set_data('row', nRows)

		icons = ["../Icons/iceweasel.png", "../Icons/kpdf.png",
                 gtk.STOCK_EDIT, gtk.STOCK_DELETE]
		tb_func = [self.preview_question, self.preview_question,
					self.edit_question, self.remove_question]
        data = ["HTML", "LaTeX"]

		for i in range(4):
			if i < 2:
                toolButton = gtk.ToolButton()
                image = gtk.image_new_from_file(icons[i])
                toolButton.set_icon_widget(image)
                toolButton.connect("clicked", tb_func[i], data[i])
            else:
                toolButton = gtk.ToolButton(icons[i])
                toolButton.connect("clicked", tb_func[i])
			toolBar.add(toolButton)
			toolButton.set_data('row', nRows)
			lrow.append(toolButton)

		checkButton = gtk.CheckButton(question.title, None)
		checkButton.set_data('row', nRows)
		checkButton.set_data('filepath', question.filename)
		checkButton.set_property("tooltip-markup", self.create_tooltip(question))
		checkButton.connect("drag_data_get", self.sendCallback)
		checkButton.drag_source_set(gtk.gdk.BUTTON1_MASK, self.cbtarget,
							   		gtk.gdk.ACTION_COPY)
		lrow.append(checkButton)

		fileimage = EditQuiz.categoryImageTooltip[question.qtype][0]
		tooltip = EditQuiz.categoryImageTooltip[question.qtype][1]
		imageTypeQuestion = gtk.image_new_from_file(fileimage)
		imageTypeQuestion.set_property("tooltip-text", tooltip)
		imageTypeQuestion.set_data("row", nRows)
		lrow.append(imageTypeQuestion)
		self.questions.append(lrow)

		tableQ.resize(nRows + 1, 3)
		tableQ.attach(toolBar, 0, 1,
					  nRows, nRows + 1, gtk.FILL, gtk.FILL, 0, 0)
		tableQ.attach(checkButton, 1, 2,
					  nRows, nRows + 1,  gtk.EXPAND|gtk.FILL,
					  gtk.FILL, 0, 0)
		tableQ.attach(imageTypeQuestion, 2, 3,
					  nRows, nRows + 1, gtk.FILL,
					  gtk.FILL, 0, 0)
		tableQ.show_all()

	def create_tooltip(self, question):
		for part in question.wording:
			if isinstance(part, db.PangoBuffer):
				if len(part.text.split(" ")) > 8:
					tooltip = part.text[0:8]
					break
				else:
					tooltip = part.text
					break
		return tooltip

	def create_plain_tooltip(self, question):
		for part in question.wording:
			if isinstance(part, db.PangoBuffer):
				if len(part.text.split(" ")) > 8:
					tooltip = self._clear_text(part.text, 8)
					break
				else:
					tooltip = self._clear_text(part.text)
					break
		print "IEJAAAA toolTIP", tooltip
		return tooltip

	def _clear_text(self, text, fin=-1):
		ini = 0
		tooltip = ""
		print text
		if fin == -1:
			fin = len(text.split(" "))
		tag = False
		for t in text.split(" ")[ini:fin]:
			pos = t.find("<span")
			if not tag:
				if pos == -1:
					tooltip = tooltip + " " + t
				else:
					tag = True
			if tag:
				pos = t.find(">")
				if pos != -1:
					ini_tag = t[pos + 1:].find("<")
					if 	fin != -1 :
						tooltip = tooltip + " " + t[pos + 1:ini_tag]
					else:
						tooltip = tooltip + " " + t[pos + 1:]
			print "TOOLTIP HASTA EL MOMENTO", tooltip
		if text.split(" ")[fin - 1].find("span") != -1:
			tooltip = tooltip + text.split(" ")[fin - 1] + "..."
		return tooltip

	# CATEGORIES TAB
	def buttonCreateVocabulary_clicked(self, widget, data=None):
        name_vocabulary = self.widgets.get_widget("entryVocabulary").get_text()
        des_vocabulary = self.widgets.get_widget("entryDesVocabulary").get_text()
        self.dCreateVocabulary.hide()
        self.widgets.get_widget("entryVocabulary").set_text("")
        self.widgets.get_widget("entryDesVocabulary").set_text("")
        new_vocabulary = vocabulary.Vocabulary(name_vocabulary, des_vocabulary)
		try:
            self.agente.create_vocabulary(new_vocabulary)
            model = self.tvVocabularies.get_model()
            model.append([name_vocabulary, des_vocabulary])
        except:
            logging.info("El vocabulario ya existe")
            ed = alert.ErrorData()
			ed.label_info_set_text("<b>Error insertando</b> \n" +
                                   "El vocabulario ya existe")
		self.refresh_vocabularies()

    def buttonCreateVocabularyCancel_clicked(self, widget, data=None):
        self.dCreateVocabulary.hide()
        self.widgets.get_widget("entryVocabulary").set_text("")
        self.widgets.get_widget("entryDesVocabulary").set_text("")

    def cell_name_vocabulary_edited(self, cell, path, new_text, model):
        name = model[path][0]
        new_vocabulary = vocabulary.Vocabulary(new_text, model[path][1])
		try:
            self.agente.update_vocabulary(name, new_vocabulary)
            model[path][0] = new_text
        except:
            ed = alert.ErrorData()
			ed.label_info_set_text("<b>Error editando vocabularios</b> \n" +
                                   "El vocabulario ya existe")

    def cell_description_vocabulary_edited(self, cell, path, new_text, model):
        name = model[path][0]
        new_vocabulary = vocabulary.Vocabulary(model[path][0], new_text)
        try:
            self.agente.update_vocabulary(name, new_vocabulary)
            model[path][1] = new_text
        except:
            ed = alert.ErrorData()
			ed.label_info_set_text("<b>Error insertando</b> \n")

    def buttonCreateTerm_clicked(self, widget, data=None):
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            name_term = self.widgets.get_widget("entryTerm").get_text()
            des_term = self.widgets.get_widget("entryDesTerm").get_text()
            selected_vocabulary = self.agente.get_vocabulary(model[iter][0])
            new_term = term.Term(name_term, des_term, vocabulary.Vocabulary(selected_vocabulary.name,
																			selected_vocabulary.description))
            try:
				self.agente.add_term_vocabulary(new_term, selected_vocabulary)
            # selected_vocabulary.terms.append(term_id=1, name=name_term,
# 						description=des_term, vocabulary=selected_vocabulary)
                self.dCreateTerm.hide()
				model = self.tvTerms.get_model()
				model.append([name_term, des_term])
		    except:
                ed = alert.ErrorData()
                ed.label_info_set_text("<b>Error insertando</b> \n" +
                                       "Ya existe ese trmino en este vocabulario.")
		self.refresh_vocabularies()
#         name_term = self.widgets.get_widget("entryTerm").get_text()
#         des_term = self.widgets.get_widget("entryDesTerm").get_text()
#         self.dCreateTerm.hide()
#         new_term = term.Term(name_term, des_term)
#         model = self.tvTerms.get_model()
#         model.append([name_term, des_term])
#         self.agente.update_vocabulary(new_vocabulary)

    def buttonCreateTermCancel_clicked(self, widget, data=None):
        self.dCreateTerm.hide()

    def cell_name_term_edited(self, cell, path, new_text, model):
        treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
            #selected_vocabulary = self.agente.get_vocabulary(model_vocabularies[iter][0])
            name_vocabulary = model_vocabularies[iter][0]
            des_vocabulary = model_vocabularies[iter][1]
            name = model[path][0]
			try:
                update_term = term.Term(model[path][0], model[path][1],
										vocabulary.Vocabulary(name_vocabulary,
															  des_vocabulary))
                self.agente.update_term(name_vocabulary, name, update_term)
                model[path][0] = new_text
            except:
                ed = alert.ErrorData()
                ed.label_info_set_text("<b>Error insertando</b> \n" + e.msg)

    def cell_description_term_edited(self, cell, path, new_text, model):
		treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
#             selected_vocabulary = self.agente.get_vocabulary(model_vocabularies[iter][0])
            name_vocabulary = model_vocabularies[path][0]
            des_vocabulary = model_vocabularies[iter][1]
            name = model[path][0]
            try:
                update_term = term.Term(model[path][0], new_text,
                                        vocabulary.Vocabulary(name_vocabulary,
                                                              des_vocabulary))
                self.agente.update_term(name_vocabulary, name, update_term)
                print "TEXTO A PONER", new_text
                print "PATH", path
                model[path][1] = new_text
            except:
                ed = alert.ErrorData()
                ed.label_info_set_text("<b>Error insertando</b> \n" + ed.msg)

    def changed_selection(self, w, *args):
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            self.tvTerms.get_model().clear()
            print "HA CAMBIANDO Y HE SELECCIONADO:", model[iter][0]
            terms = self.agente.get_terms_vocabulary(model[iter][0])
            model = self.tvTerms.get_model()
            if terms != None:
                for term in terms:
                    model.append([term.name, term.description])

    def buttonRemoveVocabulary_clicked(self, widget, data=None):
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            old_vocabulary = vocabulary.Vocabulary(model[iter][0],
													model[iter][1])
            self.agente.delete_vocabulary(old_vocabulary)
            model.remove(iter)

    def buttonAddVocabulary_clicked(self, widget, data=None):
        response = self.dCreateVocabulary.run()
        if response == gtk.RESPONSE_DELETE_EVENT:
            self.dCreateVocabulary.hide()
            self.widgets.get_widget("entryVocabulary").set_text("")
            self.widgets.get_widget("entryDesVocabulary").set_text("")
#         start_editing(gtk.gdk.BUTTON_PRESS, widget, path,
#                       self.tvVocabularies.get_background_area(path, self.tvVocabularies.get_column(0)),
#                       self.tvVocabularies.get_cell_area(path, self.tvVocabularies.get_column(0)),
#                       gtk.CELL_RENDERER_SELECTED)
#         self.agente.create_vocabulary(vocabulary)

    def buttonRemoveTerm_clicked(self, widget, data=None):
        treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
            selected_vocabulary = self.agente.get_vocabulary(model_vocabularies[iter][0])
            treeselection = self.tvTerms.get_selection()
            model, iter = treeselection.get_selected()
            if iter != None:
                self.agente.delete_term(selected_vocabulary, model[iter][0])
                model.remove(iter)

    def buttonAddTerm_clicked(self, widget, data=None):
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            self.dCreateTerm.run()
            self.widgets.get_widget("entryTerm").set_text("")
            self.widgets.get_widget("entryDesTerm").set_text("")

	def load_vocabularies(self):
        vocabularies = self.refresh_vocabularies()
        for vocab in vocabularies:
            model = self.tvVocabularies.get_model()
            model.append([vocab.name, vocab.description])

    def refresh_vocabularies(self):
        vocabularies = self.agente.get_all_vocabularies()
        vbox = self.widgets.get_widget('vboxCategories')
        children = vbox.get_children()
        for child in children:
            vbox.remove(child)
        for vocab in vocabularies:
            logging.info(str(vocab))
            terms = self.agente.get_terms_vocabulary(vocab.name)
            self.create_vocabulary_view(vocab.name, terms)
            self.dict_vocabularies[vocab.name] = terms
        return vocabularies

    def create_vocabulary_view(self, vocabulary, terms):
        scrolledwindow = gtk.ScrolledWindow()

        liststore = gtk.ListStore(str, gobject.TYPE_BOOLEAN)
        treeview = gtk.TreeView(liststore)
        self.treeviews.append(treeview)
        cell = gtk.CellRendererText()
        cellToggle = gtk.CellRendererToggle()
        cellToggle.set_property('activatable', True)
        cellToggle.connect('toggled', self.col1_toggled_cb,
                           liststore)
        tvcolumn = gtk.TreeViewColumn(vocabulary, cell, text=0)
        tvcolumn1 = gtk.TreeViewColumn("Filtro", cellToggle )
        tvcolumn1.add_attribute(cellToggle, "active", 1)
        treeview.append_column(tvcolumn)
        treeview.append_column(tvcolumn1)
        treeview.set_search_column(0)
        tvcolumn.set_sort_column_id(0)
        for term in terms:
            liststore.append([term.name, False])

        scrolledwindow.add(treeview)
        self.widgets.get_widget("vboxCategories").add(scrolledwindow)
        scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrolledwindow.show_all()

    def col1_toggled_cb(self, cell, path, model):
        model[path][1] = not model[path][1]
        if model[path][1]:
            self.update_elements_by_terms(model[path][0])
        else:
            self.update_elements_by_terms(None)
        return

	def update_quizView(self):
		# label = self.widgets.get_widget("labelNoQuestions")
		vboxQ = self.widgets.get_widget("vboxQuestionsQuiz")
# 		vboxQ.remove(label)
		for child in vboxQ.get_children():
			vboxQ.remove(child)
		vboxQ.add(self.treeview)
		hbox = gtk.HBox()
		txt = "Calficacion\nMnima"
		labelMin = gtk.Label(txt.decode('latin-').encode('utf-8'))
		self.entryMin = gtk.Entry()
		txt = "Calficacion\nMxima"
		labelMax = gtk.Label(txt.decode('latin-').encode('utf-8'))
		self.entryMax = gtk.Entry()
		hbox.add(labelMin)
		hbox.add(self.entryMin)
		hbox.add(labelMax)
		hbox.add(self.entryMax)
		hbox.set_spacing(10)
		vboxQ.add(hbox)
		hbox2 = gtk.HBox()
		button = gtk.Button(stock=gtk.STOCK_SAVE)
		# button.get_children()[0].set_label('Agregar parte')
		# button = gtk.Button("Guardar cambios")
		button.connect("clicked", self.save_exam)
		hbox2.add(button)
		hbox2.set_child_packing(button, False, False, 0, gtk.PACK_END)
		vboxQ.add(hbox2)
		vboxQ.set_child_packing(hbox2, False, False, 0, gtk.PACK_START)
		vboxQ.set_child_packing(hbox, False, False, 0, gtk.PACK_START)
		self.vbox.show_all()
		self.hayElementos = True

	def checkbutton_pdf(self, widget):
		if 	self.widgets.get_widget("checkbutton_pdf").get_active():
			if isinstance(widget, gtk.CheckButton):
				self.widgets.get_widget("menuitempdf").set_active(True)
			else:
				self.widgets.get_widget("checkbutton_pdf").set_active(False)
		else:
			if isinstance(widget, gtk.CheckButton):
				self.widgets.get_widget("menuitempdf").set_active(False)
			else:
				self.widgets.get_widget("checkbutton_pdf").set_active(True)


	def checkbutton_html(self, widget):
		if 	self.widgets.get_widget("checkbutton_html").get_active():
			if isinstance(widget, gtk.CheckButton):
				self.widgets.get_widget("menuitemhtml").set_active(True)
			else:
				self.widgets.get_widget("checkbutton_html").set_active(False)
		else:
			if isinstance(widget, gtk.CheckButton):
				self.widgets.get_widget("menuitemhtml").set_active(False)
			else:
				self.widgets.get_widget("checkbutton_html").set_active(True)

	def imagemenuitemAbout_activate(self, widget):
		self.widgets.get_widget("aboutdialog").run()
		self.widgets.get_widget("aboutdialog").hide()
# 	def save_exam(self, widget):
# 		self.filenames = []
# 		pdf = self.widgets.get_widget("checkbutton_pdf").get_active()
# 		html = self.widgets.get_widget("checkbutton_html").get_active()
# 		doc = amara.parse(self.fileQuiz)
# 		if self.entryMin.get_text()!="":
# 		    doc.assessmentTest.normalMaximum = unicode(self.entryMax.get_text(),
# 													   'utf-8')
# 		if self.entryMax.get_text()!="":
# 		    doc.assessmentTest.normalMinimum = unicode(self.entryMin.get_text(),
# 													   'utf-8')

# 		iterador = self.treestore.get_iter_first()
# # 		print "PRUEBAAAAAAA",
# # 		treemodelrow = self.treestore[(0,0)]
# # 		print "PRIMER VALOR", treemodelrow[1]
# # 		treemodelrow = self.treestore[(0,1)]
# # 		print "SEGUNDO VALOR", treemodelrow[1]
# 		while iterador != None :
# 			path = self.treestore.get_path(iterador)
# 			treemodelrow = self.treestore[path]
# 			value = treemodelrow[1]
# 			if len(path) == 1:
# 			    doc.assessmentTest.xml_append(
# 						doc.xml_create_element(u'testPart',
# 						attributes={ u'identifier': unicode(self.treestore[path][0], 'utf-8'),
# 						u'navigationMode': u'linear', u'submissionMode': u'individual'}))
# 			self.create_exam(doc, iterador)
#  			iterador = self.treestore.iter_next(iterador)

# 		print doc.xml()
# 		current_time = time.gmtime()
# 		ide = ""
# 		for i in range(6):
# 		    ide += str(current_time[i])
# 		ext = self.fileQuiz.find(".xml")
# 		filename = self.fileQuiz[:ext] + ide
# 		self.generate_exam(filename, html, pdf)
# 		filename = filename + ".xml"
# 		# fd = open(filename, 'w')
# # 		fd.write(doc.xml())
# # 		fd.close()
# # 		logging.info(doc.xml())

# 	def create_exam(self, doc, iterador):
# 		print "DESPUES DE BUSCAR", self.treestore.iter_n_children(iterador)
# 		for i in range(self.treestore.iter_n_children(iterador)):
# 		    iter_n_child = self.treestore.iter_nth_child(iterador, i)
# 			path = self.treestore.get_path(iter_n_child)
# 			treemodelrow = self.treestore[path]
# 			value = treemodelrow[1]
# 			self.filenames.append(value)
# 			print "PRIMER VALOR", value
# 			if value =="":
# 				doc.assessmentTest.testPart.xml_append(
# 					doc.xml_create_element(u'assessmentSection',
# 						attributes={ u'identifier': unicode(self.treestore[path][0], 'utf-8')
# 						}))
# 			elif len(path) == 2:
# 				print "ESTOY AQUI valor", value
# 				doc.assessmentTest.testPart.xml_append(
# 				    doc.xml_create_element(u'assessmentItemRef', attributes={
# 					    u'href': unicode(value,'utf-8')}))
# 				print "PATH", path, "COLUMNA HIJO", value
# 			else:
# 				doc.assessmentTest.testPart.assessmentSection.xml_append(
# 				    doc.xml_create_element(u'assessmentItemRef', attributes={
# 					    u'href': unicode(value,'utf-8')}))
# 				print "PATH", path, "COLUMNA HIJO", value
# 			print "BUSCANDO HIJOS DE HIJOS"
# 			self.create_exam(doc, iter_n_child)


	def generate_exam(self, filename, html, pdf):
		sequence = collection.Collection(self.filenames)
        questions = sequence.create()
		print "HTML", html
		print "PDF", pdf
        if html:
            render = dochtmlRender.DocHTMLRender()
			render.exam_header(filename+".html")
			for question in questions:
				print "PREGUNTA", question
				question.accept(render)
			render._close_document()
        if pdf:
            render = doctexRender.DocTeXRender()
			render.exam_header(filename+".tex")
			for question in questions:
				print "PREGUNTA", question
				question.accept(render)
			render._close_document()

### NEW VERSION ###
	def create_exam(self, widget):
		self.filenames = []
		pdf = self.widgets.get_widget("checkbutton_pdf").get_active()
		html = self.widgets.get_widget("checkbutton_html").get_active()
		if pdf or html:
			doc = amara.parse(self.fileQuiz)
			if self.entryMin.get_text()!="":
				doc.assessmentTest.normalMaximum = unicode(self.entryMax.get_text(),
														   'utf-8')
			if self.entryMax.get_text()!="":
				doc.assessmentTest.normalMinimum = unicode(self.entryMin.get_text(),
													   'utf-8')
			exm = exam.Exam()
			iterador = self.treestore.get_iter_first()
			while iterador != None :
				path = self.treestore.get_path(iterador)
				treemodelrow = self.treestore[path]
				value = treemodelrow[1]
				if len(path) == 1:
					tp = tespart.TestPart(id=self.treestore[path][0])
					print tp.id
				print "PASANDO"
				self.create_sections_exam(doc, iterador)
				iterador = self.treestore.iter_next(iterador)

			print doc.xml()
			current_time = time.gmtime()
			ide = ""
			for i in range(6):
				ide += str(current_time[i])
			ext = self.fileQuiz.find(".xml")
			filename = self.fileQuiz[:ext] + ide
			self.generate_exam(filename, html, pdf)
			filename = filename + ".xml"
		else:
			self.bufferInfo.set_text("Debe seleccionar un formato de salida")
		# fd = open(filename, 'w')
# 		fd.write(doc.xml())
# 		fd.close()
# 		logging.info(doc.xml())

	def create_sections_exam(self, doc, iterador):
		print "DESPUES DE BUSCAR", self.treestore.iter_n_children(iterador)
		for i in range(self.treestore.iter_n_children(iterador)):
		    iter_n_child = self.treestore.iter_nth_child(iterador, i)
			path = self.treestore.get_path(iter_n_child)
			treemodelrow = self.treestore[path]
			value = treemodelrow[1]
			print value
			self.filenames.append(value)
			print "PRIMER VALOR", value
			if value =="":
				section.Section(title=self.treestore[path][0])
			elif len(path) > 2:
				qref = questionref.QuestionRef(href=value, weight=self.treesotre[path][2])
			else:
				# qref = questionref.QuestionRef(href=value, weight=self.treesotre[path][2])
				print "PATH", path, "COLUMNA HIJO", value
			print "BUSCANDO HIJOS DE HIJOS"
			self.create_exam(doc, iter_n_child)


	def save_exam(self, exam):
		num_part = 0
		num_section = 0
		num_question = 0
		doc = amara.parse(self.fileQuiz)
		for part in exam.testpart:
			doc.assessmentTest.xml_append(
					doc.xml_create_element(u'testPart',
										   attributes={u'identifier': unicode(part.id, 'utf-8'),
													   u'navigationMode': u'linear', u'submissionMode': u'individual'}))
			for section in part.sections:
				doc.assessmentTest.testPart[num_part].xml_append(
						doc.xml_create_element(u'assessmentSection',
											   attributes={ u'identifier': unicode(section.title, 'utf-8')
															}))
				for qref in section.questionrefs:
					doc.assessmentTest.testPart[num_part].assessmentSection[num_section].xml_append(
							doc.xml_create_element(u'assessmentItemRef', attributes={
											u'href': unicode(qref.href,'utf-8')}))
					doc.assessmentTest.testPart[num_part].assessmentSection[num_section].assessmentItemRef[num_question].xml_append(
							doc.xml_create_element(u'weight', attributes={
											u'value': unicode(qref.weight, 'utf-8')}))
					num_question = num_question + 1
				num_section = num_section + 1
			num_part = num_part + 1
# fd = open(filename, 'w')
# 		fd.write(doc.xml())
# 		fd.close()

	def menuitemConfig_toggled(self, widget):
		config = configquiz.ConfigQuiz("ConfQuiz.glade")
		response = self.widgets.get_widget("dialogConfig").run()
		if response == 0:
			print ""
		elif response == 1:
			print ""
		elif response == gtk.RESPONSE_DELETE_EVENT:
			print ""

	def menuitemNew_activate(self, widget):
		vbox = self.widgets.get_widget("vboxQuestionsQuiz")
		for child in vbox.get_children():
			vbox.remove(child)
		text = "An no se han aadido preguntas"
		label = gtk.Label(text.decode('latin-').encode('utf-8'))
		vbox.pack_start(label, False, False, 14)
		vbox.show_all()

	def main(self):
		gtk.main()
		return 0


if __name__ == "__main__":
	app = EditQuiz("EditQuiz.glade", "../QuizsBank/test.xml")
	app.main()
