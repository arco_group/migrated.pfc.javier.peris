#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys, traceback, Ice
import Comunicaciones
import DB

class ServidorI(Comunicaciones.Servidor):

     __sesiones = []
     
     def conectar(self, s, current=None):
          print s

     def registrarCliente(self, user, passw):
         #pass
         usuario = Alumno.select(Alumno.q.usuario==user)
         if usuario != None:
              raise RegistroException("El usuario ya existe")
         Alumno(usuario = user, password = passw, respuestas = None)

     def identificarCliente(self, user, passw):
         #pass
         usuario = Alumno.select( AND (Alumno.q.nick==user, 
										Alumno.q.password==passw))
         if usuario == None:
              usuario = Alumno.select( AND (Profesor.q.usuario==user,
		 									Profesor.q.password==passw))
              if usuario == None:
                   raise IdentificacionException
         else: 
              est = False
              for se in __sesiones:
                   est = se == user
                   if est == True:
                        break
              if est == True:
                   raise IdentificacionException("Ese usuario ya est� " +
 													"identificado")
              __sesiones.add(user)
          
class Controlador:
     
     def arrancar(self,puerto):
          status = 0
          ic = None
          try:
               ic = Ice.initialize(sys.argv)
               adapter = ic.createObjectAdapterWithEndpoints(
                    "SimpleConectorAdapter", "default -p "+puerto)
               object = ServidorI()
               adapter.add(object, Ice.stringToIdentity("SimpleConector"))
               adapter.activate()
               ic.waitForShutdown()
	  except:
               traceback.print_exc()
               status = 1
          if ic:
               # Clean up
               try:
                    ic.destroy()
               except:
                    traceback.print_exc()
                    status = 1
	  sys.exit(status)
