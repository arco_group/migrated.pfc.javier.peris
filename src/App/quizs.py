#!/usr/bin/env python

import os
import pygtk
pygtk.require('2.0')
import gtk.glade
import gtk

import db
import configQuiz
from sqlobject import *

class Quizs:

    QUESTIONSBANK = "../QuizsBank/"
    # close the window and quit
    def delete_event(self, widget, event, data=None):
        gtk.main_quit()
        False

    def __init__(self):
        self.widgets = gtk.glade.XML("ConfQuiz.glade")
        self.widgets.signal_autoconnect(self)
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title("Cuestionarios - SDocente")
        self.window.connect("delete_event", self.delete_event)

        # self.liststore = gtk.ListStore(str)
        self.listore = self.make_list()
        self.treeview = gtk.TreeView()
        self.tvcolumn = gtk.TreeViewColumn('Titulo del Cuestionario')

        # self.liststore.append(['Cuestionario A'])
#         self.liststore.append(['Cuestionario B'])
#         self.liststore.append(['Cuestionario C'])

        self.treeview.append_column(self.tvcolumn)
        self.cell = gtk.CellRendererText()
        self.tvcolumn.pack_start(self.cell, True)
        self.tvcolumn.set_cell_data_func(self.cell, self.file_name)
        self.tvcolumn.set_attributes(self.cell, text=0,
                                      cell_background_set=3)

        self.treeview.set_search_column(0)
        self.tvcolumn.set_sort_column_id(0)
        self.treeview.set_model(self.listore)

        self.treeview.connect("button-press-event", self.rightClick)

        # POP UP MENU
        image = [gtk.STOCK_DELETE, gtk.STOCK_EDIT, None]
        mItem_func = [self.mItem_delete, self.mItem_edit, self.mItem_statistics]
        self.menu = gtk.Menu()
        for i in range(3):
            menuItem = gtk.ImageMenuItem(image[i])
            menuItem.show()
            if i == 2:
                label = gtk.Label('Ver estadisticas')
                menuItem.add(label)
                label.show()
                menuItem.set_image(gtk.image_new_from_file("statistics.png"))
            self.menu.append(menuItem)
            menuItem.connect("activate", mItem_func[i])

        self.window.add(self.treeview)
        self.window.show_all()

    def rightClick(self, widget, event):
        if event.button == 3:
            self.info_at_pos = self.treeview.get_path_at_pos(event.x, event.y)
            if self.info_at_pos != None:
                self.menu.popup(None, None, None, event.button, event.time)

    def mItem_delete(self, widget):
        del self.liststore[self.info_at_pos[0]]
#         question = Test.select(Test.q.question_file == )
        question.destroySelf()

    def mItem_edit(self, widget):
#         self.listore[self.info_at_pos[0]][1]
        #print db.Test.id
        # db.Test(name="test", filename="../QuizsBanks/test.xml")
#         test = db.Test.select(db.Test.q.id > 0).count()
        test = db.Test.select(db.Test.q.filename == "../QuizsBanks/test.xml")[0]
        print test
        # print self.listore[self.info_at_pos[0]][0]
        conf_quiz = configQuiz.ConfigQuiz("/home/peris/Desktop/SDocente/App/ConfQuiz.glade")
        conf_quiz.widgets.get_widget("entryTitle").set_text(test.name)
        if test.start_date != None:
            conf_quiz.widgets.get_widget("checkButtonOpenTest").set_active(True)
            conf_quiz.widgets.get_widget("calendarOpen").select_day(date.day)
            conf_quiz.widgets.get_widget("calendarOpen").select_month(date.month, date.year)
            conf_quiz.widgets.get_widget("spinButtonHourOpen").set_value(date.hour)
            conf_quiz.widgets.get_widget("spinButtonMinuteOpen").set_value(date.minute)
        if test.end_date != None:
            conf_quiz.widgets.get_widget("checkButtonCloseTest").set_active(True)
            conf_quiz.widgets.get_widget("calendarClose").select_day(date.day)
            conf_quiz.widgets.get_widget("calendarClose").select_month(date.month, date.year)
            conf_quiz.widgets.get_widget("spinButtonHourClose").set_value(date.hour)
            conf_quiz.widgets.get_widget("spinButtonMinuteClose").set_value(date.minute)
        if test.timeLimit != None:
            conf_quiz.widgets.get_widget("checkButtonMaxTime").set_active(True)
            conf_quiz.widgets.get_widget("entryMaxTime").set_text(str(Test.timeLimit))

    def mItem_statistics(self, widget):
        dialog = gtk.Dialog()
        dialog.run()

    def make_list(self):
        self.dirname = Quizs.QUESTIONSBANK
        files = [f for f in os.listdir(self.dirname) if f[0] != '.']
        print files
        files.sort()
        listmodel = gtk.ListStore(object)
        for f in files:
            test = db.Test.select(db.Test.q.filename == "../QuizsBanks/test.xml")[0]
            print test
            #print "FICHERO", "../QuizsBank/" + f, "BASE DE DATOS", db.Test.select(db.Test.q.name == "test")
            listmodel.append([test.name])
        return listmodel
         #    listmodel.append([test.name, f])

    def file_name(self, column, cell, model, iter):
        cell.set_property('text', model.get_value(iter, 0))


def main():
    gtk.main()

if __name__ == "__main__":
    app = Quizs()
    main()
