#!/usr/bin/python

try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
except:
	sys.exit(1)

        
class InteractivePangoBuffer :

    desc_to_attr_table = {
		'family':[pango.AttrFamily, ""],
		'style':[pango.AttrStyle, pango.STYLE_NORMAL],        
		'variant':[pango.AttrVariant, pango.VARIANT_NORMAL],
		'weight':[pango.AttrWeight, pango.WEIGHT_NORMAL],
		'stretch':[pango.AttrStretch, pango.STRETCH_NORMAL],
    }

    pango_translation_properties={
        # pango ATTR TYPE : (pango attr property / tag property)
        pango.ATTR_SIZE : 'size',
        pango.ATTR_WEIGHT: 'weight',
        pango.ATTR_UNDERLINE: 'underline',
        pango.ATTR_STRETCH: 'stretch',
        pango.ATTR_VARIANT: 'variant',
        pango.ATTR_STYLE: 'style',
        pango.ATTR_SCALE: 'scale',
        pango.ATTR_STRIKETHROUGH: 'strikethrough',
        pango.ATTR_RISE: 'rise',
    }

    attval_to_markup={
        'underline':{pango.UNDERLINE_SINGLE: 'single',
                     pango.UNDERLINE_DOUBLE: 'double',
                     pango.UNDERLINE_LOW: 'low',
                     pango.UNDERLINE_NONE: 'none'},
        'stretch':{pango.STRETCH_ULTRA_EXPANDED: 'ultraexpanded',
                   pango.STRETCH_EXPANDED: 'expanded',
                   pango.STRETCH_EXTRA_EXPANDED: 'extraexpanded',
                   pango.STRETCH_EXTRA_CONDENSED: 'extracondensed',
                   pango.STRETCH_ULTRA_CONDENSED: 'ultracondensed',                                              
                   pango.STRETCH_CONDENSED: 'condensed',
                   pango.STRETCH_NORMAL: 'normal',
                   },
        'variant':{pango.VARIANT_NORMAL: 'normal',
                   pango.VARIANT_SMALL_CAPS: 'smallcaps',
                   },
        'style':{pango.STYLE_NORMAL: 'normal',
                 pango.STYLE_OBLIQUE: 'oblique',
                 pango.STYLE_ITALIC: 'italic',
                 },
        'stikethrough':{1: 'true',
                        True: 'true',
                        0: 'false',
                        False: 'false'},
    }

    def __init__(self, txt, buf, toggle_widget_alist=[]):		
        #PangoBuffer.__init__(self, txt, buf)
#        if normal_button: normal_button.connect('clicked', 
#																	lambda *args: self.remove_all_tags())
        self.tag_widgets = {}
        self.buf = buf
        self.internal_toggle = False
        self.insert = self.buf.get_insert()
        self.buf.connect('mark-set', self._mark_set_cb)
        self.tagdict = {}
        self.tags = {}
        self.tag = {}
        li = ["6144", "7168", "8192", "10240", "12288", "14336", "16384"]
        for i in range(len(li)):
	    tag = '<span size = "' + li[i] + '"> xx-small </span>'
            a,t,s = pango.parse_markup(tag, u'0')
            font,lang,attrs = a.get_iterator().get_font()
            t = self.buf.create_tag()
            t.set_property('font-desc', font)
            self.tag[i] = t

    def setup_widget_from_pango(self, widg, markupstring):
        #print markupstring
        a,t,s = pango.parse_markup(markupstring, u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        self.setup_widget(widg, font, attrs)
        
    def setup_widget(self, widg, font, attr):
        tags = self.get_tags_from_attrs(font, None, attr)
        #self.tag_widgets[tuple(tags)] = widg
        if isinstance (widg, gtk.ToggleButton) :
            self.tag_widgets[tuple(tags)] = widg
            widg.connect('toggled',self._toggle,tags)
        elif isinstance (widg, gtk.ComboBox) :
            widg.connect('changed', self._change_size, tags)
        else:
            widg.connect('color-set', self._change_color, tags)

    def get_tags_from_attrs(self, font, lang, attrs):
        tags = []
        if font:            
            font, fontattrs = self.fontdesc_to_attrs(font)
            fontdesc = font.to_string()
            if fontattrs:
                attrs.extend(fontattrs)
                if fontdesc and fontdesc != 'Normal':
                    if not self.tags.has_key(font.to_string()):                    
                        tag = self.buf.create_tag()
                        tag.set_property('font-desc', font)
                        if not self.tagdict.has_key(tag): 
                            self.tagdict[tag] = {}
                            self.tagdict[tag]['font_desc'] = font.to_string()
                            self.tags[font.to_string()] = tag
                            tags.append(self.tags[font.to_string()])
        if lang:
                if not self.tags.has_key(lang):
                    tag = self.buf.create_tag()
                    tag.set_property('language', lang)
                    self.tags[lang] = tag
                tags.append(self.tags[lang])
        if attrs:
            #print "PARA LISTA DE ATRIBUTOS:", attrs
            for a in attrs:
                #print "ATRIBUTO", a
                if a.type == pango.ATTR_FOREGROUND:
                    gdkcolor = self.pango_color_to_gdk(a.color)
                    key = 'foreground%s'%self.color_to_hex(gdkcolor)
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property('foreground-gdk', gdkcolor)
                        self.tagdict[self.tags[key]]={}
                        self.tagdict[self.tags[key]]['foreground']="#%s"%self.color_to_hex(gdkcolor)
                    tags.append(self.tags[key])
                if a.type == pango.ATTR_BACKGROUND:
                    gdkcolor = self.pango_color_to_gdk(a.color)
                    tag.set_property('background-gdk',gdkcolor)
                    key = 'background%s'%self.color_to_hex(gdkcolor)
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property('background-gdk',gdkcolor)
                        self.tagdict[self.tags[key]]={}
                        self.tagdict[self.tags[key]]['background']="#%s"%self.color_to_hex(gdkcolor)
                        tags.append(self.tags[key])
                if self.pango_translation_properties.has_key(a.type):
                    prop = self.pango_translation_properties[a.type]
                    #print 'setting property %s of %s (type: %s)'%(prop,a,a.type)
                    val = getattr(a,'value')
                    #print "VALOR DE %s:%s"%(a,val)
                    #tag.set_property(prop,val)
                    mval = val
                    if self.attval_to_markup.has_key(prop):
                        #print 'converting ',prop,' in ',val
                        if self.attval_to_markup[prop].has_key(val):
                            mval = self.attval_to_markup[prop][val]
                    key = "%s%s"%(prop,val)
                    print "KEEEYYYYYYYYYYYYYY:", key
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property(prop,val)
                        #print "PROPIEDAD %s con valor %s"%(prop, val)
                        self.tagdict[self.tags[key]] = {}
                        self.tagdict[self.tags[key]][prop] = mval
                        #print self.tagdict#[self.tags[key]]
                        tags.append(self.tags[key])
                        #print "TAG", self.tags
                else:
                    print "Don't know what to do with attr %s"%a        
		return tags

    def fontdesc_to_attrs(self, font):
        nicks = font.get_set_fields().value_nicks
        #print "NICKS", nicks
        attrs = []
        for n in nicks:
            if self.desc_to_attr_table.has_key(n):
                #print 'attributeifying %s'%n
                Attr,norm = self.desc_to_attr_table[n]
                # create an attribute with our current value
                attrs.append(Attr(getattr(font,'get_%s'%n)()))
                # unset our font's value
                getattr(font,'set_%s'%n)(norm)
        return font, attrs
    def pango_color_to_gdk(self, pc):
        return gtk.gdk.Color(pc.red, pc.green, pc.blue)

    def color_to_hex(self, color):
        hexstring = ""
        for col in 'red','green','blue':
            hexfrag = hex(getattr(color,col) / (16 * 16)).split("x")[1]
            if len(hexfrag) < 2: hexfrag = "0" + hexfrag
            hexstring += hexfrag
        #print 'returning hexstring: ',hexstring            
        return hexstring
            
    def _change_size(self, widget, tags):
        for i in range(7):
            if widget.get_active() == i:
		print "OPCION ACTIVADA", i, "AQUI ANDO"    
                for t in self.tag:	
                    self.remove_tag(self.tag[t])
		self.apply_tag(self.tag[i])
                break
	
    def _change_color(self, widget, tags):
        ltag = self.buf.get_iter_at_mark(self.insert).get_tags()
        for e in ltag:
            if e.get_priority() == 5:
                self.remove_tag(e)
                break
        ta = self.buf.create_tag()
        gdkcolor = widget.get_color()
	print "GDKCOLOR", gdkcolor
        ta.set_property('foreground-gdk',gdkcolor)
        self.apply_tag(ta)
        
    def _toggle(self, widget, tags):    
        if self.internal_toggle: return
        if widget.get_active():
	    for t in tags:
                #print t
                self.apply_tag(t)
        else:
            for t in tags: self.remove_tag(t)

    def _mark_set_cb(self, buffer, iter, mark, *params):
        if mark == self.insert:
            for tags,widg in self.tag_widgets.items():
                active = True
                for t in tags:
                    if not iter.has_tag(t):
                        if isinstance (widg, gtk.ComboBox):
                            active =- 1;
                        else:
                            active = False
                self.internal_toggle = True
                widg.set_active(active)
                self.internal_toggle = False

    def get_selection(self):
        bounds = self.buf.get_selection_bounds()
        if not bounds:
            iter = self.buf.get_iter_at_mark(self.insert)
            if iter.inside_word():
                start_pos = iter.get_offset()
                iter.forward_word_end()
                word_end = iter.get_offset()
                iter.backward_word_start()
                word_start = iter.get_offset()
                iter.set_offset(start_pos)
                bounds = (self.buf.get_iter_at_offset(word_start),
                          self.buf.get_iter_at_offset(word_end))
        return bounds

    def apply_tag(self, tag):    
        selection = self.get_selection()
	if selection:
            self.buf.apply_tag(tag,*selection)

    def remove_tag (self, tag):
        selection = self.get_selection()
	print 
        if selection:
            self.buf.remove_tag(tag,*selection)

	
class GuiEditor:

	buttonTags = {"toggleButtonBold": "<b>bold</b>",
		      "toggleButtonItalic": "<i>italic</i>",
		      "toggleButtonUnderline": "<u>underline</u>",
		      "comboboxSizeFont": '<span size = "6144"> xx-small </span>',
		      "buttonColorFont": '<span color = "green"> prueba </span>',
		      }

	def __init__(self, file):
            self.widgets = gtk.glade.XML(file)
            self.widgets.signal_autoconnect(self)
            self.buffer = self.widgets.get_widget("textView").get_buffer()
	    
            self.ipb = InteractivePangoBuffer("", self.buffer )

	    for n, t in GuiEditor.buttonTags.items():
		widget = self.widgets.get_widget(n)
		self.ipb.setup_widget_from_pango(widget, t)
		
#             buttonBold = self.widgets.get_widget("toggleButtonBold")
#             buttonItalic = self.widgets.get_widget("toggleButtonItalic")
#             buttonUnderline = self.widgets.get_widget("toggleButtonUnderline")
#             comboboxSizeFont = self.widgets.get_widget("comboboxSizeFont")
#             buttonColorFont = self.widgets.get_widget("buttonColorFont")
		
#             self.ipb.setup_widget_from_pango(buttonBold, "<b>bold</b>")      
#             self.ipb.setup_widget_from_pango(buttonItalic, "<i>italic</i>")
#             self.ipb.setup_widget_from_pango(buttonUnderline, "<u>underline</u>")
#             self.ipb.setup_widget_from_pango(comboboxSizeFont, '<span size = "6144"> xx-small </span>')
#             self.ipb.setup_widget_from_pango(buttonColorFont, '<span color = "green"> prueba </span>')

            self.widgets.get_widget("comboboxSizeFont").set_active(3)
            self.windowEditor = self.widgets.get_widget("wEditor")
            self.windowEditor.show()

	def buttonOrdList_clicked(self, widget, data=None):
            tag = self.widgets.get_widget("textView").get_buffer().create_tag()
            tag.set_property('indent', 200)
            bounds = self.buffer.get_selection_bounds()
            self.buffer().apply_tag(tag, *bounds)

	def buttonList_toggled(self, widget, data=None):
            tag = self.widgets.get_widget("textView").get_buffer().create_tag()
            tag.set_property('indent',200)
            bounds = self.buffer().get_selection_bounds()
            self.buffer().apply_tag(tag, *bounds)
		

	def buttonImage_clicked(self, widget, data=None):
            dialog = gtk.FileChooserDialog("Abrir archivo", None,
					   gtk.FILE_CHOOSER_ACTION_OPEN,
                                           (gtk.STOCK_CANCEL, 
					    gtk.RESPONSE_CANCEL,
					    gtk.STOCK_OPEN,
					    gtk.RESPONSE_OK))
            dialog.set_default_response(gtk.RESPONSE_OK)
            filter = gtk.FileFilter()
            filter.set_name("Imagen")
            filter.add_mime_type("image/png")
            filter.add_mime_type("image/jpeg")
            filter.add_mime_type("image/gif")
            filter.add_pattern("*.png")
            filter.add_pattern("*.jpg")
            dialog.add_filter(filter)
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                try:
			pixbuf = gtk.gdk.pixbuf_new_from_file(dialog.get_filename())
			anchor = self.buffer.create_child_anchor(self.buffer.get_iter_at_mark(self.buffer.get_insert()))
			#self.widgets.get_widget("textView").add_child_at_anchor(child, anchor)
			self.buffer.insert_pixbuf(self.buffer.get_iter_at_mark(self.buffer.get_insert()), pixbuf)
			pass
                except IOError :
                    print "El fichero no existe."
            elif response == gtk.RESPONSE_CANCEL:
                print "No hay elementos seleccionados"
            dialog.destroy()

	def buttonTable_clicked(self, widget, data=None):
            dialog = gtk.FileChooserDialog("Abrir archivo", None,
					   gtk.FILE_CHOOSER_ACTION_OPEN,
                                           (gtk.STOCK_CANCEL,
					    gtk.RESPONSE_CANCEL,
					    gtk.STOCK_OPEN,
					    gtk.RESPONSE_OK))
            dialog.set_default_response(gtk.RESPONSE_OK)
            filter = gtk.FileFilter()
            filter.set_name("OOffice")
            filter.add_pattern("*.odt")
            dialog.add_filter(filter)
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                try:
                    pass
                except IOError :
                    print "El fichero no existe."
                    flectura.close()
            elif response == gtk.RESPONSE_CANCEL:
                print "No hay elementos seleccionados"
            dialog.destroy()

	def buttonFormula_clicked(self, widget, data=None):
            dialog = gtk.FileChooserDialog("Abrir archivo", None, 
					   gtk.FILE_CHOOSER_ACTION_OPEN,
                                           (gtk.STOCK_CANCEL, 
					    gtk.RESPONSE_CANCEL, 
					    gtk.STOCK_OPEN,
					    gtk.RESPONSE_OK))
            dialog.set_default_response(gtk.RESPONSE_OK)
            response = dialog.run()
            filter = gtk.FileFilter()
            filter.set_name("MathML")
            filter.add_pattern("*.xml")
            dialog.add_filter(filter)
            if response == gtk.RESPONSE_OK:
                try:
                    pass
                except IOError :
                    print "El fichero no existe."
                    flectura.close()
            elif response == gtk.RESPONSE_CANCEL:
                print "No hay elementos seleccionados"
            dialog.destroy()

	def wEditor_delete_event(self, widget, event, data=None):
            gtk.main_quit()

	def main(self):
            gtk.main()
            return 0

if __name__ == "__main__":
	app = GuiEditor("editor.glade")
	app.main()
