#!/usr/bin/python
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
except:
	sys.exit(1)

import proxy

class GuiConexion:

	#__proxy = proxy.Proxy()

	def __init__(self,file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)

		self.windowConexion = self.widgets.get_widget("windowConexion")
		self.windowConexion.show()
		
	def wMain_delete_event(self, widget, event, data=None):
		gtk.main_quit()

	#Boton windowConexion
	def buttonConectar_clicked(self,widget,data=None):
		prox = proxy.Proxy()
		prox.conectar(self.widgets.get_widget("entryIP").get_text(), 
							self.widgets.get_widget("entryPuerto").get_text())
		self.widgets.get_widget("entryUsuario").set_sensitive(True)
		self.widgets.get_widget("entryPassword").set_sensitive(True)
		self.widgets.get_widget("buttonRegistrarse").set_sensitive(True)
		self.widgets.get_widget("buttonIdentificarse").set_sensitive(True)

	def buttonRegistrarse_clicked(self,widget,data=None):
		prox = proxy.Proxy()
		prox.conectar(self.widgets.get_widget("entryUsuario").get_text(), 
						self.widgets.get_widget("entryPassword").get_text())

	def buttonIdentificarse_clicked(self,widget,data=None):
		prox = proxy.Proxy()
		prox.conectar(self.widgets.get_widget("entryUsuario").get_text(),
						self.widgets.get_widget("entryPassword").get_text())

	def main(self):
		gtk.main()
		return 0

if __name__ == "__main__":
	app = GuiConexion("guiConexion.glade")
	app.main()
