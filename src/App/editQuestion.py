#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
except:
	sys.exit(1)

import choiceMultiple
import extended_text
import text_entry
import inLineChoice

class GuiEditor:

	def __init__(self, file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)
		self.windowEditor = self.widgets.get_widget("wEditQuestion")
		self.windowEditor.show()
		
	def wMain_delete_event(self, widget, event, data=None):
		gtk.main_quit()

	def buttonEditCategory_clicked(self, widget, data=None):
		self.widgets.get_widget("notebook").next_page()

	def comboboxNewQuestion_changed(self, widget, data=None):
		ltq = [choiceMultiple.ChoiceMultiple("choiceMultiple.glade"),
		 		extended_text.ExtendedText("extended_text.glade"),
			   text_entry.TextEntry("text_entry.glade"),
 				inLineChoice.InLineChoice("inLineChoice.glade")]	
		wa = widget.get_active()
		p = ltq[wa]
			
	def create_element(self, questions):
		toolBar = gtk.Toolbar()
		toolBar.set_style(gtk.TOOLBAR_ICONS)
		toolBar.set_icon_size(gtk.ICON_SIZE_MENU)
		toolBar.set_property('width-request',88)
		
		icons = [gtk.STOCK_PRINT_PREVIEW, gtk.STOCK_EDIT, gtk.STOCK_DELETE]
		for icon in icons:
				toolButton = gtk.ToolButton(icon)
				toolBar.add(toolButton)
				
		checkButton = gtk.CheckButton("Prueba", None)
		
		imageTypeQuestion = gtk.image_new_from_file("matchImage")

		tableQ = self.widgets.get_widget("tableQuestion")
		nRows = tableQ.get_property("n-rows")
		tableQ.resize(nRows + 1, 3)
		tableQ.attach(toolBar, 0, 1, 
				nRows, nRows + 1, gtk.FILL, gtk.FILL, 0, 0)
		tableQ.attach(checkButton, 1, 2, 
				nRows, nRows + 1,  gtk.EXPAND|gtk.FILL,
				gtk.EXPAND|gtk.FILL, 0, 0)
		tableQ.attach(imageTypeQuestion, 2, 3, 
				nRows, nRows + 1, gtk.FILL,
				gtk.EXPAND|gtk.FILL, 0, 0)
		tableQ.show_all()
		
	def main(self):
		gtk.main()
		return 0


if __name__ == "__main__":
	app = GuiEditor("EditQuestion.glade")
	app.main()
