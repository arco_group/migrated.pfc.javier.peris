#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import time
import os
import sys
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
	import amara
except:
	sys.exit(1)


sys.path.append('/home/peris/Desktop/SDocente/App')

import db
import logging

import editQuiz

class ConfigQuiz:

	def __init__(self, file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)
		self.cbMaxTime = self.widgets.get_widget("checkbuttonMaxTime")
		self.cOpen = self.widgets.get_widget("calendarOpen")
		self.cOpen.set_sensitive(False)
		self.cClose = self.widgets.get_widget("calendarClose")
		self.cClose.set_sensitive(False)
		self.eMTime = self.widgets.get_widget("entryMaxTime")
		self.eMTime.set_sensitive(False)
		self.sbHourOpen = self.widgets.get_widget("spinbuttonHourOpen")
		self.sbHourOpen.set_sensitive(False)
		self.sbMinuteOpen = self.widgets.get_widget("spinbuttonMinuteOpen")
		self.sbMinuteOpen.set_sensitive(False)
		self.sbHourClose = self.widgets.get_widget("spinbuttonHourClose")
		self.sbHourClose.set_sensitive(False)
		self.sbMinuteClose = self.widgets.get_widget("spinbuttonMinuteClose")
		self.sbMinuteClose.set_sensitive(False)
		year, month, day, hour, min, sec, wday, yday, isdst = time.localtime()
		self.cOpen.select_month(year, month - 1)
		self.cOpen.select_day(day)
		self.cClose.select_month(year, month - 1)
		self.cClose.select_day(day)
		self.widgets.get_widget("window").show_all()
		self.maxTime = 0
		#self.sbHourOpen.set_value()

	def window_delete_event(self, widget, event, data=None):
		gtk.main_quit()
		return False

	def checkbuttonMaxTime_toggled(self, widget, data=None):
		if widget.get_active():
		    self.eMTime.set_sensitive(True)
		else:
		    self.eMTime.set_sensitive(False)

	def checkbuttonOpenTest_toggled(self, widget, data=None):
		if widget.get_active():
		    self.cOpen.set_sensitive(True)
			self.sbHourOpen.set_sensitive(True)
			self.sbMinuteOpen.set_sensitive(True)
		else:
		    self.cOpen.set_sensitive(False)
			self.sbHourOpen.set_sensitive(False)
			self.sbMinuteOpen.set_sensitive(False)

	def checkbuttonCloseTest_toggled(self, widget, data=None):
		if widget.get_active():
			self.cClose.set_sensitive(True)
			self.sbHourClose.set_sensitive(True)
			self.sbMinuteClose.set_sensitive(True)
		else:
 		    self.cClose.set_sensitive(False)
			self.sbHourClose.set_sensitive(False)
			self.sbMinuteClose.set_sensitive(False)

	def buttonValidate_clicked(self, widget, data=None):
		title = self.widgets.get_widget("entryTitle").get_text()
		hasDateOpen = False; hasDateClose = False; hasMaxTime = False
		if self.widgets.get_widget("checkbuttonOpenTest").get_active():
			hasDateOpen = True
		    self.dateOpen = self.cOpen.get_date()
		if self.widgets.get_widget("checkbuttonMaxTime").get_active():
			hasDateClose = True
		    self.dateClose = self.cClose.get_date()
		correcto = True
		a = ""
		if self.cbMaxTime.get_active():
			hasMaxTime = True
		    self.maxTime = self.eMTime.get_text()
			try:
				int(self.maxTime)
		    except:
				correcto = False
				a+= "El tiempo debe ser un numero entero\n"

		if title == "":
			a+= "Se debe rellenar el titulo del test\n"
			correcto = False
		if not correcto:
			dialog = gtk.Dialog("Error", None, gtk.DIALOG_MODAL |
			gtk.DIALOG_DESTROY_WITH_PARENT, (gtk.STOCK_HELP,
					gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
			label = gtk.Label()
			label.set_markup("<span color= 'red'>" + a + "</span>")
			#unicode(a, 'utf-8')
			dialog.get_children()[0].add(label)
			dialog.show_all()
			dialog.set_default_response(gtk.RESPONSE_OK)
			response = dialog.run()
			dialog.destroy()
		else:
		    self.create_XMLTest(title, hasDateOpen, hasDateClose, hasMaxTime)

	def create_XMLTest(self, title, hasDateOpen, hasDateClose, hasMaxTime):
		doc = amara.parse("test.xml")
		doc.assessmentTest.title = unicode(title, 'utf-8')
		if hasDateOpen:
				year, month, day = self.dateOpen
				month += 1
		if hasDateClose:
				year, month, day = self.dateClose
				month += 1
		if hasMaxTime:
				doc.assessmentTest.xml_append(
								doc.xml_create_element(u'timeLimits',
											attributes={
											u'maxTime':  unicode(self.maxTime,
																	'utf-8')}))
		filename = "../Quizs/" + str(title) + ".xml"
		fd = open(filename, 'w')
		fd.write(doc.xml())
		fd.close()
		logging.info(doc.xml())
		self.editQuiz = editQuiz.EditQuiz("EditQuiz.glade", filename)
		self.editQuiz.widgets.get_widget("wEditQuiz").show_all()

	def buttonCancel_clicked(self, widget, data=None):
		pass

	def main(self):
		gtk.main()
		return 0

if __name__ == "__main__":
	app = ConfigQuiz("ConfQuiz.glade")
	app.main()
