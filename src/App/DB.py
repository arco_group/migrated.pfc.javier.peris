#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from sqlobject import *

__mURI="mysql://root:perluja@localhost:3306/sqlobject_demo"
sqlhub.processConnection = connectionForURI(__mURI)

class Profesores(SQLObject):
    nick = StringCol(default = None)
    password = StringCol(default = None)
Profesores.createTable(ifNotExists = True)

class Alumno(SQLObject):
    nick = StringCol(default = None)
    password = StringCol(default = None)
Alumno.createTable(ifNotExists = True)

class Examenes(SQLObject):
    fecha = DateTimeCol(default=None)
    preguntas = RelatedJoin('Preguntas', joinColumn = 'examenes' ,
 							otherColumn = 'preguntas')
Examenes.createTable(ifNotExists = True)

class Temas(SQLObject):
    nombre = StringCol(default = None)
    preguntas = RelatedJoin('Preguntas', joinColumn = 'temas' ,
 							otherColumn = 'preguntas')
Temas.createTable(ifNotExists = True)

class Soluciones(SQLObject):
    solucion = StringCol(default = None)
    correcta = StringCol(default = None)
    preguntas = RelatedJoin('Preguntas', joinColumn = 'soluciones',
 							otherColumn = 'preguntas')
Soluciones.createTable(ifNotExists = True)

class Preguntas(SQLObject):
    pregunta = StringCol(default = None)
    tipo = StringCol(default = None)
    dificultad = FloatCol(default = None)
    temas = RelatedJoin('Temas', joinColumn = 'preguntas',
 						otherColumn = 'temas')
    examenes = RelatedJoin('Examenes', joinColumn = 'preguntas',
	 						otherColumn = 'examenes')
    soluciones = RelatedJoin('Soluciones', joinColumn = 'preguntas',
		 						otherColumn = 'soluciones')
Preguntas.createTable(ifNotExists = True)

class Respuestas(SQLObject):
    respuesta = DateTimeCol(default=None)
    correcta = StringCol(default=None)
    pregunta = ForeignKey('Preguntas')
    examen = ForeignKey('Examenes')
	alumno = ForeignKey('Alumnos')
Respuestas.createTable(ifNotExists=True)

Alumno(usuario = 'prueba', password = 'prueba')
