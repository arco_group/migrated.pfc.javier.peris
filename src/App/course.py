#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
except:
	sys.exit(1)


class Course:

	def __init__(self, file):
		self.widgets = gtk.glade.XML(file)
		self.widgets.signal_autoconnect(self)
		self.window = self.widgets.get_widget("window")
		self.window.show()
		
	def wMain_delete_event(self, widget, event, data=None):
		gtk.main_quit()
		
	def main(self):
		gtk.main()
		return 0


if __name__ == "__main__":
	app = Course("Course.glade")
	app.main()
