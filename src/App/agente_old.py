#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

from sqlobject import *
#from sqlobject.mysql import builder

class Agente:
    
    __mInstancia = None 
    __conn = None
    __mURI="mysql://root:perluja@localhost:3306/sqlobject_demo"

    class Singleton:
        def __init_( self ):
            self.Agente = None
            sqlhub.processConnection = __conn = connectionForURI(__mURI)
            #sqlhub.processConnection = __conn
        
    def __init_( self ):
        if Agente.__mInstancia is None:
            Agente.__mInstancia = Agente.Singleton()
        self.__dict__['_EventHandler_instance'] = Agente.__mInstancia

    def getDB ( self ):
        return connectionForURI(__mURI)
        
    def __getattr__(self, aAttr):
        return getattr(self.__mInstancia, aAttr)
 
    def __setattr__(self, aAttr, aValue):
        return setattr(self.__mInstancia, aAttr, aValue)
