#!/usr/bin/env python

import os
#import statcache
import stat
import time
import pygtk
pygtk.require('2.0')
import gtk
import shutil
import Image
import tempfile

import logging


class FileManager:
    column_names = ['Name', 'Size', 'Mode', 'Last Changed']
    TARGETS = [('text/plain', 0, 1)]
    COPY = 0
    CUT = 1

    def delete_event(self, widget, event, data=None):
        gtk.main_quit()
        False

    def __init__(self, dname = None):
        cell_data_funcs = (None, self.file_size, self.file_mode,
                           self.file_last_changed)

        # Create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        self.window.set_size_request(400, 300)

        self.window.connect("delete_event", self.delete_event)

        listmodel = self.make_list("./images")

        # create the TreeView
        self.treeview = gtk.TreeView()

        # create the TreeViewColumns to display the data
        self.tvcolumn = [None] * len(self.column_names)
        cellpb = gtk.CellRendererPixbuf()
        self.tvcolumn[0] = gtk.TreeViewColumn(self.column_names[0], cellpb)
        self.tvcolumn[0].set_cell_data_func(cellpb, self.file_pixbuf)
        self.cell = gtk.CellRendererText()
        self.cell.set_property( 'editable', False)
        self.cell.connect('edited', self.col0_edited_cb, listmodel)
        self.tvcolumn[0].pack_start(self.cell, False)
        self.tvcolumn[0].set_cell_data_func(self.cell, self.file_name)
        self.treeview.append_column(self.tvcolumn[0])
        for n in range(1, len(self.column_names)):
            cell = gtk.CellRendererText()
            self.tvcolumn[n] = gtk.TreeViewColumn(self.column_names[n], cell)
            if n == 1:
                cell.set_property('xalign', 1.0)
            self.tvcolumn[n].set_cell_data_func(cell, cell_data_funcs[n])
            self.treeview.append_column(self.tvcolumn[n])

        self.treeview.connect('row-activated', self.open_file)
        self.treeview.connect("button-press-event", self.rightClick)
        self.treeview.enable_model_drag_source(gtk.gdk.BUTTON1_MASK,
                                               self.TARGETS,
                                               gtk.gdk.ACTION_DEFAULT)
        self.treeview.enable_model_drag_dest(self.TARGETS,
                                             gtk.gdk.ACTION_DEFAULT)
        self.treeview.connect("drag_data_get", self.drag_data_get_data)
        self.treeview.connect("drag_data_received",
                              self.drag_data_received_data)

        # POP UP MENU
        image = [gtk.STOCK_OPEN, None, gtk.STOCK_CUT, gtk.STOCK_COPY, None,
                 None, None, gtk.STOCK_DELETE, "folder-new.png", gtk.STOCK_NEW,
                 gtk.STOCK_PASTE]
        mItem_func = [self.open, None, self.cut_file, self.copy_file, None,
                      self.edit, None, self.remove_file,
                      self.create_directory, self.upload_file, self.paste_file]
        self.menu = gtk.Menu()
        for i in range(11):
            if i == 1 or i== 4 or i==6:
                menuItem = gtk.SeparatorMenuItem()
            elif i == 5:
                menuItem = gtk.MenuItem("Renombrar...", False)
                menuItem.connect("activate", mItem_func[i])
            elif i == 8:
                menuItem = gtk.ImageMenuItem()
                menuItem.set_image(gtk.image_new_from_file(image[i]))
                menuItem.connect("activate", mItem_func[i])
                label = gtk.Label('Crear una carpeta')
                menuItem.add(label)
                label.show()
            else:
                menuItem = gtk.ImageMenuItem(image[i])
                menuItem.connect("activate", mItem_func[i])
            if i == 9:
                print menuItem.get_children()[0].set_label("Subir fichero")
            if i == 10:
                menuItem.set_sensitive(False)
            menuItem.show()
            self.menu.append(menuItem)

        self.scrolledwindow = gtk.ScrolledWindow()
        self.scrolledwindow.add(self.treeview)
        self.window.add(self.scrolledwindow)
        self.treeview.set_model(listmodel)

        self.window.show_all()

        return

    def make_list(self, dname=None):
        if not dname:
            self.dirname = os.path.expanduser('~')
        else:
            self.dirname = os.path.abspath(dname)
        self.window.set_title(self.dirname)
        files = [f for f in os.listdir(self.dirname) if f[0] != '.']
        files.sort()
        if self.dirname != "/home/peris/Desktop/csl-sdocente/App/images":
            files = ['..'] + files
        listmodel = gtk.ListStore(object)
        for f in files:
            listmodel.append([f])
        return listmodel

    def open_file(self, treeview, path, column):
        model = treeview.get_model()
        iter = model.get_iter(path)
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        if stat.S_ISDIR(filestat.st_mode):
            new_model = self.make_list(filename)
            statcache.reset()
            treeview.set_model(new_model)
        return

    def file_pixbuf(self, column, cell, model, iter):
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        if stat.S_ISDIR(filestat.st_mode):
            #pb = folderpb
            pb = gtk.gdk.pixbuf_new_from_file("gtk-directory.png")
        else:
            #pb = filepb
            pb = gtk.gdk.pixbuf_new_from_file("gtk-file.png")
        cell.set_property('pixbuf', pb)
        return

    def file_name(self, column, cell, model, iter):
        cell.set_property('text', model.get_value(iter, 0))
        return

    def file_size(self, column, cell, model, iter):
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        cell.set_property('text', filestat.st_size)
        return

    def file_mode(self, column, cell, model, iter):
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        cell.set_property('text', oct(stat.S_IMODE(filestat.st_mode)))
        return

    def file_last_changed(self, column, cell, model, iter):
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        cell.set_property('text', time.ctime(filestat.st_mtime))
        return

    def rightClick(self, widget, event):
        if event.button == 3:
            self.info_at_pos = self.treeview.get_path_at_pos(event.x, event.y)
            if self.info_at_pos != None:
                for i in range(8):
                    self.menu.get_children()[i].show()
                for i in range(8, 11):
                    self.menu.get_children()[i].hide()
                path = self.info_at_pos[0]
#                 self.cell.start_editing(gtk.gdk.Event(gtk.gdk.BUTTON_PRESS), self.menu.get_children()[5], str(path),
#                                         self.treeview.get_background_area(path, self.treeview.get_column(1)),
#                                         self.treeview.get_cell_area(path, self.treeview.get_column(1)), gtk.CELL_RENDERER_SELECTED)
            else:
                for i in range(8):
                    self.menu.get_children()[i].hide()
                for i in range(8, 11):
                    self.menu.get_children()[i].show()
            self.menu.popup(None, None, None, event.button, event.time)
        if event == gtk.gdk._2BUTTON_PRESS:
            self.open(widget)

    def drag_data_get_data(self, treeview, context, selection, target_id,
                           etime):
        treeselection = treeview.get_selection()
        model, iter = treeselection.get_selected()
        data = model.get_value(iter, 0)
        print data
        selection.set(selection.target, 8, data)

    def drag_data_received_data(self, treeview, context, x, y, selection,
                                      info, etime):
        model = treeview.get_model()
        data = selection.data
        drop_info = treeview.get_dest_row_at_pos(x, y)
        if drop_info:
            path, position = drop_info
            iter = model.get_iter(path)
            filename = os.path.join(self.dirname, model.get_value(iter, 0))
            filestat = os.stat(filename)
            if stat.S_ISDIR(filestat.st_mode):
                shutil.move(src, dst)
        if context.action == gtk.gdk.ACTION_MOVE:
            context.finish(True, True, etime)
        return

    def upload_file(self, widget):
        dialog = gtk.FileChooserDialog("Abrir archivo", None,
                                       gtk.FILE_CHOOSER_ACTION_OPEN,
                                       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        filter = gtk.FileFilter()
        filter.set_name("Images")
        filter.add_mime_type("image/png")
        filter.add_mime_type("image/jpeg")
        filter.add_mime_type("image/gif")
        filter.add_pattern("*.png")
        filter.add_pattern("*.jpg")
        filter.add_pattern("*.gif")
        dialog.add_filter(filter)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            try:
                filen = dialog.get_filename()
                print filen
                shutil.copy(filen, "/home/peris/Desktop/SDocente/App/images/")
            except IOError :
                logging.warning("The destination location must be writable")
        elif response == gtk.RESPONSE_CANCEL:
            logging.info("There are not selected elements.")
        new_model = self.make_list(self.dirname)
        statcache.reset()
        self.treeview.set_model(new_model)
        dialog.destroy()

    def remove_file(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        filestat = os.stat(filename)
        if stat.S_ISDIR(filestat.st_mode):
            os.rmdir(filename)
        else:
            os.remove(filename)
        new_model = self.make_list(self.dirname)
        statcache.reset()
        self.treeview.set_model(new_model)
        return

    def move_file(self, widget):
        shutil.move(src, dst)

    def cut_file(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        self.filename = os.path.join(self.dirname, model.get_value(iter, 0))
        self.last_action = self.CUT
        self.menu.get_children()[8].set_sensitive(True)

    def copy_file(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        self.filename = os.path.join(self.dirname, model.get_value(iter, 0))
        self.last_action = self.COPY
        self.menu.get_children()[8].set_sensitive(True)

    def paste_file(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        fn_dst = os.path.join(self.dirname, model.get_value(iter, 0))
        if self.last_action == CUT:
            shutil.move(self.filename, fn_dst)
        else:
            shutil.copy(filen, "/home/peris/Desktop/SDocente/App/images/")
            self.menu[8].set_sensitive(False)

    def create_directory(self, widget):
        print "aqui ando"
        os.mkdir(self.dirname + "/carpeta sin titulo" + num)
        print self.dirname
        new_model = self.make_list(self.dirname)
        statcache.reset()
        self.treeview.set_model(new_model)

    def col0_edited_cb(self, cell, path, new_text, model):
        """
        Called when a text cell is edited.  It puts the new text
        in the model so that it is displayed properly.
        """
        print "Change '%s' to '%s'" % (model[path][0], new_text)
        print self.dirname + "/" + model[path][0]
        print self.dirname + "/" + new_text
        shutil.move(self.dirname + "/" + model[path][0], self.dirname + "/" +
                    new_text)
        new_model = self.make_list(self.dirname)
        statcache.reset()
        self.treeview.set_model(new_model)
        model[path][0] = new_text
        print self.dirname
        cell.set_property( 'editable', False)

        return

   #  def get_thumbnail_image(self):
#         #self.info_at_pos
#         img=Image.open("/home/peris/Desktop/SDocente/App/images/erica.JPG")
#         x,y = img.size
#         #del img
#         #s = max(s * x / y, s)
#         tn = tempfile.mktemp(".jpg")
#         #img=Image.open(filename)
#         img = img.copy()
#         img.thumbnail((32, 32), Image.NEAREST)
#         img.save(tn)
#         del img
#         gtk_img = gtk.Image()
#         gtk_img.set_from_file(tn)
#         os.remove(tn)
#         window = gtk.Window()
#         window.add(gtk_img)
#         window.show_all()
#         #return gtk_img

    def open(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        filename = os.path.join(self.dirname, model.get_value(iter, 0))
        image = gtk.image_new_from_file(filename)
        window = gtk.Window()
        window.add(image)
        window.show_all()

    def edit(self, widget):
        model = self.treeview.get_model()
        path = self.info_at_pos[0]
        iter = model.get_iter(path)
        cell = self.treeview.get_cell_area(path, self.treeview.get_column(1))
        self.cell.set_property('editable', True)
        self.cell.set_flags(gtk.CAN_FOCUS)
        self.treeview.set_cursor_on_cell(path, self.treeview.get_column(1),
 											self.cell, True)
        self.treeview.grab_focus()

    # def start_editing(event, widget, path, background_area, cell_area, flags)
#     def start_editing(gtk.gdk.BUTTON_PRESS, self.menu.get_children()[5], path, self.treeview.get_background_area(path, self.treeview.get_column(1)),
#                       self.treeview.get_cell_area(path, self.treeview.get_column(1)), gtk.CELL_RENDERER_SELECTED):


    # def start_editing(self):
#         model = self.treeview.get_model()
#         path = self.info_at_pos[0]
#         iter = model.get_iter(path)


def main():
    gtk.main()

if __name__ == "__main__":
    app = FileManager()
    main()
