#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
import gtk
import sys
sys.path.append('/home/peris/Desktop/SDocente/App')
sys.path.append('/home/peris/Desktop/SDocente/Questions')
import os
import questionManagerController
import persistencia


class TestVocabularies(unittest.TestCase):

    def setUp(self):
        self.a = questionManagerController.QuestionManagerController("../Questions/EditQuestion.glade")

#     def testAddVocabularyNoName(self):
#         self.a.widgets.get_widget("buttonAddVocabulary").clicked()
#         self.a.widgets.get_widget("entryVocabulary").set_text("")
#         self.a.widgets.get_widget("entryDesVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("AcceptVocabulary").clicked()

#     def testAddVocabulary(self):
#         self.agente = persistencia.Agente()
#         vocabulary = self.agente.get_vocabulary("Test")
#         self.assertEquals(vocabulary, None)
#         # Take the last iter (last element)
#         model = self.a.tvVocabularies.get_model()
#         iterator = model.get_iter((0,))
#         while iterator != None:
#             iterator = model.iter_next(iterator)
#         self.a.widgets.get_widget("buttonAddVocabulary").clicked()
#         self.a.widgets.get_widget("entryVocabulary").set_text("Test")
#         self.a.widgets.get_widget("entryDesVocabulary").set_text("Test")
#         self.a.widgets.get_widget("AcceptVocabulary").clicked()
#         vocabulary = self.agente.get_vocabulary("Test")
#         print vocabulary
#         self.assertNotEquals(vocabulary, None)
#         # Would have to be a new iterator with name Prueba
#         iterator = model.iter_next(iterator)
#         self.assertNotEquals(None)
#         self.assertEquals(model[iterator][0],"Test")

    def testDeleteVocabulary(self):
        treeselection = self.a.tvVocabularies.get_selection()
        treeselection.select_path((0,))
        model = self.a.tvVocabularies.get_model()
        vob = model[(0,)][0]
        vocabulary = self.a.agente.get_vocabulary(model[(0,)][0])
        self.assertNotEquals(vocabulary, None)
        self.a.widgets.get_widget("buttonDeleteVocabulary").clicked()
        vocabulary = self.a.agente.get_vocabulary(vob)
        self.assertEquals(vocabulary, None)

#     def testEditVocabulary(self):
#         treeselection = self.tvVocabularies.get_selection()
#         treeselection.select_path((0,))
#         self.assertTrue()

#     def testAddTerm(self):
#         treeselection = self.tvVocabularies.get_selection()
#         treeselection.select_path((0,))
#         self.a.widgets.get_widget("buttonAddVocabulary").clicked()
#         self.a.widgets.get_widget("entryVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("entryDesVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("AcceptVocabulary").clicked()
#         vocabulary = self.agente.get_vocabulary("Prueba")
#         self.assertNotEquals(vocabulary, None)

#     def testAddTermNoName(self):
#         treeselection = self.tvVocabularies.get_selection()
#         treeselection.select_path((0,))
#         self.a.widgets.get_widget("buttonAddVocabulary").clicked()
#         self.a.widgets.get_widget("entryVocabulary").set_text("")
#         self.a.widgets.get_widget("entryDesVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("AcceptVocabulary").clicked()
#         term = self.agente.get_vocabulary("Prueba")
#         self.assertNotEquals(vocabulary, None)

#     def testDeleteTerm(self):
#         treeselection = self.tvVocabularies.get_selection()
#         treeselection.select_path((0,))
#         self.a.widgets.get_widget("buttonAddVocabulary").clicked()
#         self.a.widgets.get_widget("entryVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("entryDesVocabulary").set_text("Prueba")
#         self.a.widgets.get_widget("AcceptVocabulary").clicked()
#         term = self.agente.get_vocabulary("Prueba")
#         self.assertNotEquals(vocabulary, None)

#     def testEditTerm(self):
#         treeselection = self.tvVocabularies.get_selection()
#         treeselection.select_path((0,))
#         treeselection = self.tvTerm.get_selection()
#         treeselection.select_path((0,))

suite = unittest.TestLoader().loadTestsFromTestCase(TestVocabularies)
unittest.TextTestRunner(verbosity=2).run(suite)
