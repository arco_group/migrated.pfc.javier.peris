#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
sys.path.append("../Persistencia/")
sys.path.append("../AuthoringTool/")

import unittest

import persistencia
import vocabulary
import term
import exception
import exam
import questionref
import section
import testpart

class TestPersistanceAgent(unittest.TestCase):

    def setUp(self):
        self.agent = persistencia.Agente()

    def testSaveChoiceMultipleQuestion(self):
        title = "test"
        defaultMark = 0.0
        txtbuffer = gtk.TextBuffer()
        txtbuffer.set_text("Enunciado de prueba")
        wording = InteractivePangoBuffer("", txtbuffer)
        simpleChoice = odict.OrderedDict()
        for key, val in {"A":0,"B":0,"C":1}.items():
            simpleChoice[key] = [val, "Prueba " + val]
        single = False
        ques = choicemultiple.ChoiceMultiple(title, defaultMark, wording,
                                             simpleChoice, single)
        agent.save(ques)

    # If there is any vocabulary, so ask for a particular vocabulary
    # and check if the obtained vocabulary is correct
    def testGetVocabulary(self):
        vocs = self.agent.get_all_vocabularies()
        if vocs:
            voc = self.agent.get_vocabulary(vocs[0].name)
            self.failIf(voc==None)
            self.failIf(voc.name!=vocs[0].name)

    # Create a vocabulary and check we can get it
    def testCreateVocabulary(self):
        voc = self.agent.get_vocabulary("test")
        if not voc:
            vocab = vocabulary.Vocabulary("test", "")
            self.agent.create_vocabulary(vocab)
            voc = self.agent.get_vocabulary(vocab.name)
            self.failIf(voc==None)
            self.failIf(voc.name!=vocab.name)

    # Change the vocabulary's name, check we can get the updated vocabulary
    # and the old one doesn't exist any more
    def testUpdateVocabulary(self):
        vocs = self.agent.get_all_vocabularies()
        if vocs:
            voc = self.agent.get_vocabulary(vocs[0].name)
            vocab = vocabulary.Vocabulary("new_test", "")
            self.agent.update_vocabulary(vocs[0].name, vocab)
            updated_voc = self.agent.get_vocabulary("new_test")
            self.assertEqual(updated_voc.name, "new_test")
            if vocs[0].name != "new_test":
                self.assertEqual(vocs[0].name, None)

    # Add a term to a vocabulary and check whether we can get it
    def testAddTermToVocabulary(self):
        vocs = self.agent.get_all_vocabularies()
        if vocs:
            t = term.Term("newtest","", vocs[0])
            terms = self.agent.get_terms_vocabulary(vocs[0].name)
            try:
                self.agent.add_term_vocabulary(t, vocs[0])
                self.assertEqual(self.agent.get_term(t.name).name==t.name)
            except exception.TermAlreadyExistsException:
                self.assertRaises(exception.TermAlreadyExistsException,
                                  self.agent.add_term_vocabulary, t, vocs[0])

    # Update a term, get it and check whether the data is correct
    def testUpdateTerm(self):
        vocs = self.agent.get_all_vocabularies()
        if vocs:
            t = term.Term("test2","", vocs[0])
            try:
                self.agent.add_term_vocabulary(t, vocs[0])
                new_t = term.Term("test3","")
                self.agent.update_term(vocs[0].name, "test2", new_t)
                # FIXME (get_term): SE DEBERIA PASAR TAMBIEN EL VOCABULARIO PARA IDENTIFICARLO
                # DE FORMA �NIVOCA
                self.assertEqual(self.get_term(t.name)==None)
                self.assertEqual(self.get_term(new_t.name).name==t.name)
                self.agent.testDeleteTerm(vocs[0], "test3")
            except exception.TermAlreadyExistsException:
                self.assertRaises(exception.TermAlreadyExistsException,
                                  self.agent.add_term_vocabulary, t, vocs[0])

    # Delete a vocabulary and check it doesn't exist
    def testDeleteVocabulary(self):
        vocab = vocabulary.Vocabulary("test", "")
        try:
            self.agent.create_vocabulary(vocab)
            voc = self.agent.get_vocabulary(vocab.name)
            self.agent.delete_vocabulary(voc)
            voc = self.agent.get_vocabulary(vocab.name)
            self.assertEqual(voc, None)
        except exception.VocabularyAlreadyExistsException:
            self.assertRaises(exception.VocabularyAlreadyExistsException,
                              self.agent.create_vocabulary,
                              vocab)

    # Delete a term and check it doesn't exist
    def testDeleteTerm(self):
        vocs = self.agent.get_all_vocabularies()
        t = term.Term("anothertest", "", vocs[0])
        try:
            self.agent.add_term_vocabulary(t, vocs[0])
            self.agent.delete_term(vocs[0], t.name)
            self.assertEqual(self.agent.get_term(vocs[0], t.name), None)
        except exception.TermAlreadyExistsException:
            self.assertRaises(exception.TermAlreadyExistsException,
                              self.agent.add_term_vocabulary,
                              t, vocs[0])

    # Ask for a known question and check its attributes
    def testGetQuestion(self):
        # FIXME (get_term): SE DEBERIA PASAR TAMBIEN EL VOCABULARIO PARA IDENTIFICARLO
        # DE FORMA �NIVOCA
        self.load_agents()
        questions = self.agent.get_questions()
        question = self.agent.get_question(questions[0].id)
        self.assertEqual(questions[0].id, question.id)
        self.assertEqual(questions[0].title, question.title)
        self.assertEqual(questions[0].defaultMark, question.defaultMark)

    # Check whether a clone question was created
    def testCloneQuestion(self):
        self.load_agents()

        all_questions = self.agent.get_questions()
        num_all_questions = len(all_questions)
        ques = self.agent.get_questions_by_name(all_questions[0].title)

        self.agent.clone_question(all_questions[0].id)

        updated_ques = self.agent.get_questions()
        num_all_updated_ques = len(updated_ques)

        self.assertEqual(num_all_updated_ques, num_all_questions+1)
        new_questions = self.agent.get_questions_by_name(all_questions[0].title)

        self.assertEqual(len(list(new_questions)), len(list(ques))+1)
        self.assertEqual(num_all_questions + 1, num_all_updated_ques)
        self.assertEqual(new_questions[0].title, new_questions[1].title)
        self.assertEqual(new_questions[0].defaultMark,
                         new_questions[1].defaultMark)

    # Delete a question and check it doesn't exist
    def testDeleteQuestion(self):
        self.load_agents()
        ques = self.agent.get_questions()
        self.agent.clone_question(ques[0].id)

        all_questions = self.agent.get_questions()
        specific_ques = self.agent.get_questions_by_name(ques[0].title)

        self.agent.delete_question(specific_ques[1].id)

        all_updated_questions = self.agent.get_questions()
        specific_updated_ques = self.agent.get_questions_by_name(ques[0].title)
        questions = self.agent.get_questions()

        self.assertEqual(len(all_questions)-1, len(all_updated_questions))
        self.assertEqual(len(specific_ques)-1, len(specific_updated_ques))


    # Create a new exam and check whether we can get it and its data is correct
    def testSaveExam(self):
        self.load_agents()
        exams = self.agent.get_exams()

        all_questions = self.agent.get_questions()
        quesrefs = []
        import random
        num_ques = random.randint(1,len(all_questions))
        if num_ques > 10: num_ques = 10
        for i in range(0, num_ques):
            quesrefs.append(questionref.QuestionRef(all_questions[i].id))
        s = section.Section("first section", quesrefs)
        t = testpart.TestPart("first part", [s])
        import datetime
        tm = datetime.datetime.now()
        # e = exam.Exam("test", [t], tm, subject = ns)
        ns = self.agent.get_subject_names()
        if ns:
            ns = ns[0]

        e = exam.Exam("test", [t], tm, subject=ns)
        import time
        gmtime = time.gmtime()
        fileid = e.title
        for i in range(6):
            fileid += str(gmtime[i])

        e.id = "../QuizsBank/" + fileid + ".xml"

        self.agent.save_exam(e, ns)

        exm = self.agent.get_exam_by_date(tm)
        updated_exams = self.agent.get_exams()
        self.assertEqual(len(exams)+1, len(updated_exams))
        self.assertEqual(e, exm)

    # Update an exam, check whether it was updated correctly
    def testUpdateExam(self):
        self.load_agents()
        exams = self.agent.get_exams()
        if exams:
            ex = exams[0]

        print ex.title

        import datetime
        time = datetime.datetime.now()

        ex.title = "New Test"
        ex.maxTime = 120
        ex.openDate = time
        print ex.openDate

        self.agent.update_exam(ex)

        updated_ex = self.agent.get_exam_by_date(time)

        self.assertEqual(ex, updated_ex)

    # Delete an exam, check it doesn't exist
    def testDeleteExam(self):
        self.load_agents()

        all_questions = self.agent.get_questions()
        quesrefs = []
        for i in range(0,10):
            quesrefs.append(questionref.QuestionRef(all_questions[i].id))
        s = section.Section("first section", quesrefs)
        t = testpart.TestPart("first part", [s])
        import datetime
        tm = datetime.datetime.now()
        e = exam.Exam("test", [t], tm)
        import time
        gmtime = time.gmtime()
        fileid = e.title
        for i in range(6):
            fileid += str(gmtime[i])

        e.id = "../QuizsBank/" + fileid + ".xml"

        ns = self.agent.get_subject_names()
        if ns:
            ns = ns[0]

        self.agent.save_exam(e, ns)

        exams = self.agent.get_exams()
        self.agent.delete_exam(e)
        updated_exams = self.agent.get_exams()

        ex = self.agent.get_exam_by_date(tm)
        print "EXAMEEEEEEEEEEEEEEEEEEEN", ex
        self.assertEqual(ex,None)
        self.assertEqual(len(exams)-1, len(updated_exams))

    # Create an exam which starts now so is not changeable
    def testExamNotChangeable(self):
        self.load_agents()
        exams = self.agent.get_exams()

        all_questions = self.agent.get_questions()
        quesrefs = []
        for i in range(0,10):
            quesrefs.append(questionref.QuestionRef(all_questions[i].id))
        s = section.Section("first section", quesrefs)
        t = testpart.TestPart("first part", [s])
        import datetime
        tm = datetime.datetime.now()
        e = exam.Exam("test", [t], tm)
        import time
        gmtime = time.gmtime()
        fileid = e.title
        for i in range(6):
            fileid += str(gmtime[i])

        e.id = "../QuizsBank/" + fileid + ".xml"

        ns = self.agent.get_subject_names()
        if ns:
            ns = ns[0]

        self.agent.save_exam(e, ns)

        ex = self.agent.get_exam_by_date(tm)
        print ex

        self.assertFalse(self.agent.is_changeable(ex))

    # Create an exam which starts in 3 days so is changeable
    def testExamChangeable(self):
        self.load_agents()
        exams = self.agent.get_exams()

        all_questions = self.agent.get_questions()
        quesrefs = []
        for i in range(0,10):
            quesrefs.append(questionref.QuestionRef(all_questions[i].id))
        s = section.Section("first section", quesrefs)
        t = testpart.TestPart("first part", [s])
        import datetime
        tm = datetime.datetime.now()
        tm += datetime.timedelta(3)
        e = exam.Exam("test", [t], tm)
        import time
        gmtime = time.gmtime()
        fileid = e.title
        for i in range(6):
            fileid += str(gmtime[i])

        e.id = "../QuizsBank/" + fileid + ".xml"
        ns = self.agent.get_subject_names()
        if ns:
            ns = ns[0]

        self.agent.save_exam(e, ns)

        ex = self.agent.get_exam_by_date(tm)

        self.assertTrue(self.agent.is_changeable(ex))

    # Auxilary method to load the different agents
    def load_agents(self):
        plugins = ["Order", "ChoiceMultiple", "ExtendedText", "TextEntry", "InlineChoice"]
        for p in plugins:
            sys.path.append("../AuthoringTool/Questions/" + p + "/")
        import orderagent
        import choicemultipleagent
        import extendedtextagent
        import textentryagent
        import inlinechoiceagent
        persistencia.Agente.register(orderagent.OrderAgent)
        persistencia.Agente.register(choicemultipleagent.ChoiceMultipleAgent)
        persistencia.Agente.register(extendedtextagent.ExtendedTextAgent)
        persistencia.Agente.register(inlinechoiceagent.InlineChoiceAgent)
        persistencia.Agente.register(textentryagent.TextEntryAgent)

suite = unittest.TestLoader().loadTestsFromTestCase(TestPersistanceAgent)
unittest.TextTestRunner(verbosity=2).run(suite)
