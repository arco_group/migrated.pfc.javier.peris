import unittest
import editor
import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade
import pango

class TestEditor(unittest.TestCase):

    def setUp(self):
        self.s = "Frase de prueba"
        self.a = editor.GuiEditor("editor.glade")
        self.a.buffer.set_text(self.s)

    def testbold(self):
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonbold = self.a.widgets.get_widget("toggleButtonBold")
        a,t,s = pango.parse_markup("<b>bold</b>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        tags = [tag]
        buttonbold.set_active(True)
        self.a.ipb._toggle(buttonbold,tags)
        self.assertTrue(self.a.buffer.get_start_iter().has_tag(tag))

    def testitalic(self):
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonitalic = self.a.widgets.get_widget("toggleButtonItalic")
        a,t,s = pango.parse_markup("<i>italic</i>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        tags = [tag]
        buttonitalic.set_active(True)
        self.a.ipb._toggle(buttonitalic,tags)
        self.assertTrue(self.a.buffer.get_start_iter().has_tag(tag))

    def testunderline(self):
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonunderline = self.a.widgets.get_widget("toggleButtonUnderline")
        a,t,s = pango.parse_markup("<u>underline</u>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        tags = [tag]
        buttonunderline.set_active(True)
        self.a.ipb._toggle(buttonunderline,tags)
        self.assertTrue(self.a.buffer.get_start_iter().has_tag(tag))

   #  def testcolor(self):
#         self.a.buffer.select_range(self.a.buffer.get_start_iter(),self.a.buffer.get_end_iter())
#         buttoncolor = self.a.widgets.get_widget("buttonColorFont")
#         a,t,s = pango.parse_markup("<span color = '#00FF00'> prueba</span>",u'0')
#         ai = a.get_iterator()
#         font,lang,attrs = ai.get_font()
#         #print "COLOR. Fuente:", font, "Lenguaje", lang, "Atributos",attrs
#         tag = self.a.buffer.create_tag()
#         tag.set_property('font-desc',font)
#         tags = [tag]
#         gdkcolor = gtk.gdk.Color(0, 255, 0, 0)
#         buttoncolor.set_color(gdkcolor)
#         self.a.ipb._change_color(buttoncolor,tags)
#         print "TAGS", self.a.buffer.get_start_iter().get_tags(), "MI TAG", tag
#         self.assertTrue(self.a.buffer.get_start_iter().has_tag(tag))

    def testsize(self):
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonsize = self.a.widgets.get_widget("comboboxSizeFont")
        a,t,s = pango.parse_markup("<span size = '6144'>  xx-small </span>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        #print "SIZE. Fuente:", font, "Lenguaje", lang, "Atributos",attrs
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        tags = [tag]
        buttonsize.set_active(0)
        #self.a.ipb._change_size(buttonsize,tags)
        print self.a.buffer.get_start_iter().get_tags()
        self.assertTrue(self.a.buffer.get_start_iter().has_tag(tag))


    def testbold_istoggle(self):
        buttonbold = self.a.widgets.get_widget("toggleButtonBold")
        a,t,s = pango.parse_markup("<b>bold</b>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        self.assertTrue(buttonbold.get_active)

    def testitalic_istoggle(self):
        buttonitalic = self.a.widgets.get_widget("toggleButtonItalic")
        a,t,s = pango.parse_markup("<i>Italic</i>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        self.assertTrue(buttonitalic.get_active)

    def testunderline_istoggle(self):
        buttonunderline = self.a.widgets.get_widget("toggleButtonUnderline")
        a,t,s = pango.parse_markup("<u>Italic</u>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag()
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        self.assertTrue(buttonunderline.get_active)

    def testremove_bold(self):
        buttonbold = self.a.widgets.get_widget("toggleButtonBold")
        a,t,s = pango.parse_markup("<b>bold</b>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag("Negrita")
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonbold.set_active(False)
        tags = [tag]
        self.a.ipb._toggle(buttonbold,tags)
        self.assertFalse(self.a.buffer.get_start_iter().has_tag(tag))

    def testremove_italic(self):
        buttonitalic = self.a.widgets.get_widget("toggleButtonItalic")
        a,t,s = pango.parse_markup("<i>italic</i>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag("Cursiva")
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonitalic.set_active(False)
        tags = [tag]
        self.a.ipb._toggle(buttonitalic,tags)
        self.assertFalse(self.a.buffer.get_start_iter().has_tag(tag))

    def testremove_underline(self):
        buttonunderline = self.a.widgets.get_widget("toggleButtonUnderline")
        a,t,s = pango.parse_markup("<u>subrayado</u>",u'0')
        ai = a.get_iterator()
        font,lang,attrs = ai.get_font()
        tag = self.a.buffer.create_tag("Subrayado")
        tag.set_property('font-desc',font)
        self.a.buffer.apply_tag(tag, self.a.buffer.get_start_iter(),
								self.a.buffer.get_end_iter())
        self.a.buffer.select_range(self.a.buffer.get_start_iter(),
									self.a.buffer.get_end_iter())
        buttonunderline.set_active(False)
        tags = [tag]
        self.a.ipb._toggle(buttonunderline,tags)
        self.assertFalse(self.a.buffer.get_start_iter().has_tag(tag))

#     def testseveraltags(self):


# if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestEditor)
unittest.TextTestRunner(verbosity=2).run(suite)
