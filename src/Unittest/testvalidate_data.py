#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
import sys
sys.path.append('/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/')
sys.path.append('/home/peris/Desktop/csl2-sdocente/src/Editor/')
questions = ["ChoiceMultiple", "ExtendedText", "InlineChoice", "TextEntry"]
for q in questions:
    path = '/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/Questions/' + q + '/'
    sys.path.append(path)
#     print path

import validator
import choicemultiplevalidator
import extendedtextvalidator
import textentryvalidator
import inlinechoicevalidator
import question
import extendedtext
import choicemultiple
import textentry
import inlinechoice
import editor
import gtk
import exception


class TestValidateData(unittest.TestCase):

    # def setUp(self):
#         self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade")

# TITLE, QUESTION, TYPE, LENGTH, SC, DMARK

    def setUp(self):
        self.w = [editor.InteractivePangoBuffer("Pregunta", gtk.TextBuffer())]
        self.w_empty = [editor.InteractivePangoBuffer("", gtk.TextBuffer())]
        self.w_tentry = [editor.InteractivePangoBuffer("Pregunta []", gtk.TextBuffer())]

    def testNoTitleCM(self):
        t = ""
        d = "0"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        question = choicemultiple.ChoiceMultiple(t, d, self.w, sc, True)
        validator = choicemultiplevalidator.ChoiceMultipleValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoDefaultMarkCM(self):
        t = "Prueba"
        d = ""
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        question = choicemultiple.ChoiceMultiple(t, d, self.w, sc, True)
        validator = choicemultiplevalidator.ChoiceMultipleValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoAnswersCM(self):
        t = "Prueba"
        d = "0"
        sc = {}
        question = choicemultiple.ChoiceMultiple(t, d, self.w, sc, True)
        validator = choicemultiplevalidator.ChoiceMultipleValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoQuestionCM(self):
        t = "Prueba"
        d = "0"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        question = choicemultiple.ChoiceMultiple(t, d, self.w_empty, sc, True)
        validator = choicemultiplevalidator.ChoiceMultipleValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoNumberDMarkCM(self):
        t = "Prueba"
        d = "a6"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        question = choicemultiple.ChoiceMultiple(t, d, self.w, sc, True)
        validator = choicemultiplevalidator.ChoiceMultipleValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

#     def testDatosCorrectosCM(self):
#         t = "Prueba"
#         d = "2"
#         sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
#         w = "Pregunta"
#         question = choicemultiple.ChoiceMultiple(t, d, w, sc, True)
#         validator = choicemultiplevalidator.ChoiceMultipleValidator()

    def testNoTitleET(self):
        t = ""
        l = "0"
        s = 5
        question = extendedtext.ExtendedText(t, 0, self.w, l, "Prueba", s)
        validator = extendedtextvalidator.ExtendedTextValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoIntNumberLengthET(self):
        t = "A"
        l = "100.3"
        s = 5
        question = extendedtext.ExtendedText(t, 0, self.w, l, "Prueba", s)
        validator = extendedtextvalidator.ExtendedTextValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoQuestionET(self):
        t = "A"
        l = "100"
        w = ""
        s = 5
        question = extendedtext.ExtendedText(t, 0, self.w_empty, l, "Prueba", s)
        validator = extendedtextvalidator.ExtendedTextValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

#     def testDatosCorrectosET(self):
#         t = "A"
#         l = "100"
#         w = "Pregunta"
#         question = extendedtext.ExtendedText(t, 0, w, l, "Prueba")
#         validator = extendedtextvalidator.ExtendedTextValidator()

    def testNoTitleTE(self):
        t = ""
        d = "0"
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w_tentry, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoDefaultMarkTE(self):
        t = "Prueba"
        d = ""
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w_tentry, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoAnswersTE(self):
        t = "Prueba"
        d = "0"
        sc = {}
        w = "Pregunta"
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w_tentry, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoQuestionTE(self):
        t = "Prueba"
        d = "1"
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w_empty, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoGapTE(self):
        t = "Prueba"
        d = "1"
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoNumberDMarkTE(self):
        t = "Prueba"
        d = "a6"
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        l = "25"
        question = textentry.TextEntry(t, d, sc, self.w_tentry, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoBracketsTE(self):
        t = "Prueba"
        d = "2"
        l = "100"
        sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
        # w = "Pregunta"
        l = "25"
        w = editor.InteractivePangoBuffer("Pregunta", gtk.TextBuffer())
        question = textentry.TextEntry(t, 0, sc, self.w_tentry, l)
        validator = textentryvalidator.TextEntryValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

#     def testCorrectDataTE(self):
#         t = "Prueba"
#         d = "2"
#         l = "100"
#         sc = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
#         w = "Pregunta []"
#         l = "25"
#         question = textentry.TextEntry(t, 0, self.w_tentry, l, "Prueba")
#         validator = textentryvalidator.TextEntryValidator()

    def testNoTitleIC(self):
        t = ""
        d = "0"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        w = "Pregunta []"
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoDefaultMarkIC(self):
        t = "Prueba"
        d = ""
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        w = "Pregunta []"
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoAnswersIC(self):
        t = "Prueba"
        d = "0"
        sc = {}
        w = "Pregunta []"
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoQuestionIC(self):
        t = "Prueba"
        d = ""
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        w = "Pregunta []"
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoNumberDMarkIC(self):
        t = "Prueba"
        d = "a6"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        w = "Pregunta []"
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

    def testNoBracketIC(self):
        t = "Prueba"
        d = "2"
        sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
        w = "Pregunta "
        question = inlinechoice.InlineChoice(t, d, self.w, sc)
        validator = inlinechoicevalidator.InlineChoiceValidator()
        self.assertRaises(exception.DataException, validator.validate, question)

#     def testDatosCorrectosIC(self):
#         t = "Prueba"
#         d = "2"
#         sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
#         w = "Pregunta []"
#         question = inlinechoice.InlineChoice(t, d, w, sc)
#         validator = inlinechoicevalidator.InlineChoiceValidator()

suite = unittest.TestLoader().loadTestsFromTestCase(TestValidateData)
unittest.TextTestRunner(verbosity=2).run(suite)
