#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
sys.path.append("../AuthoringTool/")
sys.path.append("../Persistencia/")
sys.path.append("../AuthoringTool/Questions/TextEntry/")
import unittest
# import pygtk
# pygtk.require('2.0')
import gtk
# import gtk.glade
import amara
#import pango

import persistencia
import textentry
import textentrycontroller


class TestTextEntry(unittest.TestCase):

    def setUp(self):
        path = "../AuthoringTool/Questions/TextEntry/textentry.glade"
        self.agent = persistencia.Agente()
        self.vocabularies = self.agent.get_all_vocabularies()
        self.dict_vocabularies = {}
        for vocab in self.vocabularies:
            terms = self.agent.get_terms_vocabulary(vocab.name)
            self.dict_vocabularies[vocab.name] = terms
        self.tec = textentrycontroller.TextEntryController(self.dict_vocabularies, path)

#     def testtextentryModificator(self):
#         title = "test"
#         defaultMark = "0"
#         simpleChoice = {"A": ["Respuesta A",  "1"], "B": ["Respuesta B", "0"]}
#         question = "Pregunta de prueba"
#         length = "25"
#         #         self.assertTrue(os.path.exists("../Tests/test.xml"))
#         doc = amara.parse("../QuestionBank/test.xml")
#         self.a.choiceModificator(title,defaultMark,simpleChoice,question,length)
#         self.assertEqual(doc.assessmentItem.title,"Prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue,"0")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt,"Pregunta de prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey,"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue,"1")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey,"B")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue,"0")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0],"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[1],"B")
#         self.assertEqual(doc.assessmentItem.itemBody.textEntryInteraction.expectedLength,"25")
#         #         os.remove("../Tests/test.xml")


    def testCreateTextEntry(self):
        self.tec.template.set_title("Text Entry")
		self.tec.widgets.get_widget("entryLength").set_text("25")
		self.tec.template.widgets.get_widget("entryMarkDefault").set_text("0")

		self.tec.ans["A"] = "1"
        self.tec.ans["B"] = "0"

		buf = self.tec.editor.get_textview().get_buffer()
		question = buf.set_text("Pregunta de prueba")

#         print self.agent.get_question("Text Entry")
#         self.assertEquals(q, None)
        widget.clicked()
        print self.agente.get_question("Text Entry")
        q = self.agente.get_question("Text Entry")
        self.assertNotsEquals(q, None)
        doc = amara.parse(q.filename)
        self.assertEqual(doc.assessmentItem.title,"Text Entry")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue, "0")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt, "Pregunta de prueba")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey, "A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue, "1")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey,"B")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue, "0")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0], "A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[1], "B")
        self.assertEqual(doc.assessmentItem.itemBody.textEntryInteraction.expectedLength, "25")


#     def testMoreAns(self):
#         widget = self.a.widgets.get_widget("buttonMoreAns")
#         self.a.buttonMoreAns_clicked(widget)
#         elementos = 0
#         labels = 0
#         vbox = 0
#         for e in self.a.widgets.get_widget("tableAns").get_children():
#              elementos += 1
#              if isinstance(e,gtk.VBox) :
#                  labels+=1
#              else:
#                  vbox+=1
#         self.assertEquals(elementos,12)
#         self.assertEquals(labels,6)
#         self.assertEquals(vbox,6)


    # if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestTextEntry)
unittest.TextTestRunner(verbosity=2).run(suite)
