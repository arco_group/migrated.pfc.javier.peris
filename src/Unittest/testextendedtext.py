#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
# import pygtk
# pygtk.require('2.0')
import gtk
# import gtk.glade
import amara
#import pango

import extended_text

class TestchoiceMultiple(unittest.TestCase):

    def setUp(self):
        self.a = extended_text.ExtendedText("extended_text.glade")

#     def testextendedTestModificator(self):
#         title = "prueba"
#         length = "100"
#         question = "Pregunta de prueba"
#         self.a.extendedText_modificator(title,length,question)
#         doc = amara.parse("../QuestionsBank/prueba.xml")
#         self.assertEqual(doc.assessmentItem.title,"prueba")
#         self.assertEqual(doc.assessmentItem.itemBody.extendedTextInteraction.expectedLength,"100")
#         self.assertEqual(doc.assessmentItem.itemBody.extendedTextInteraction.prompt[0],"Pregunta de prueba")

    def testCreateExtendedTest(self):
        print self.agente.get_question("Prueba")
        self.assertEquals(q, None)
        title = self.widgets.get_widget("entryTitleQuestion").set_text("Prueba")
		eLength = self.widgets.get_widget("entryExpectedLength").set_text("100")
		buf = self.editor.get_textview().get_buffer()
		wording = buf.set_text("Pregunta de prueba")
        widget.clicked()
        print self.agente.get_question("Prueba")
        q = self.agente.get_question("Prueba")
        self.assertNotsEquals(q, None)
        doc = amara.parse(q.filename)
        self.assertEqual(doc.assessmentItem.title,"Prueba")
        self.assertEqual(doc.assessmentItem.itemBody.extendedTextInteraction.expectedLength, "100")
        self.assertEqual(doc.assessmentItem.itemBody.extendedTextInteraction.prompt[0], "Pregunta de prueba")

    # if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestchoiceMultiple)
unittest.TextTestRunner(verbosity=2).run(suite)
