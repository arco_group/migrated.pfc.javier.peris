#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
# import pygtk
# pygtk.require('2.0')
import gtk
# import gtk.glade
import amara
#import pango
import os

import configQuiz

class TestConfigQuiz(unittest.TestCase):

    def setUp(self):
        self.a = configQuiz.ConfigQuiz("ConfQuiz.glade")

#     def tearDown(self):
#         os.remove("../Quizs/test.xml")
        
    def testCreate_XMLTest(self):
        self.a.widgets.get_widget("entryTitle").set_text("test")
        self.a.cbMaxTime.set_active(True)
        self.a.eMTime.set_text("120")
        self.maxTime = 120
        self.a.widgets.get_widget("buttonValidate").clicked()
        self.assertTrue(os.path.exists("../Quizs/test.xml"))
        doc = amara.parse("../Quizs/test.xml")
        self.assertEqual(doc.assessmentTest.title,"test")
        self.assertEqual(doc.assessmentTest.timeLimits.maxTime, "120")
        os.remove("../Quizs/test.xml")

    def testNoDefaultTitle(self):
        self.a.widgets.get_widget("entryTitle").set_text("")
        self.a.widgets.get_widget("buttonValidate").clicked()
        self.assertFalse(os.path.exists("../Quizs/test.xml"))

    def testTimeNoNumber(self):
        self.a.widgets.get_widget("entryTitle").set_text("Prueba")
        self.a.cbMaxTime.set_active(True)
        self.a.widgets.get_widget("entryMaxTime").set_text("120.1")
        self.a.widgets.get_widget("buttonValidate").clicked()
        self.assertFalse(os.path.exists("../Tests/Prueba.xml"))
        
    def testMaxTimeActiveOnToggled(self):
        self.assertFalse(self.a.eMTime.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonMaxTime").set_active(True)
        self.assertTrue(self.a.eMTime.get_property("sensitive"))
                
    def testOpenWidgetsActiveOnToggled(self):
        self.assertFalse(self.a.cOpen.get_property("sensitive"))
        self.assertFalse(self.a.sbHourOpen.get_property("sensitive"))
        self.assertFalse(self.a.sbMinuteOpen.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonOpenTest").set_active(True)
        self.assertTrue(self.a.cOpen.get_property("sensitive"))
        self.assertTrue(self.a.sbHourOpen.get_property("sensitive"))
        self.assertTrue(self.a.sbMinuteOpen.get_property("sensitive"))

    def testCloseWidgetsActiveOnToggled(self):
        self.assertFalse(self.a.cClose.get_property("sensitive"))
        self.assertFalse(self.a.sbHourClose.get_property("sensitive"))
        self.assertFalse(self.a.sbMinuteClose.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonCloseTest").set_active(True)
        self.assertTrue(self.a.cClose.get_property("sensitive"))
        self.assertTrue(self.a.sbHourClose.get_property("sensitive"))
        self.assertTrue(self.a.sbMinuteClose.get_property("sensitive"))

    def testMaxTimeDeactivateOnToggled(self):
        self.a.widgets.get_widget("checkbuttonMaxTime").set_active(True)        
        self.assertTrue(self.a.eMTime.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonMaxTime").set_active(False)
        self.assertFalse(self.a.eMTime.get_property("sensitive"))
        
    def testOpenWidgetsDeActiveOnToggled(self):
        self.a.widgets.get_widget("checkbuttonOpenTest").set_active(True)
        self.assertTrue(self.a.cOpen.get_property("sensitive"))
        self.assertTrue(self.a.sbHourOpen.get_property("sensitive"))
        self.assertTrue(self.a.sbMinuteOpen.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonOpenTest").set_active(False)
        self.assertFalse(self.a.cOpen.get_property("sensitive"))
        self.assertFalse(self.a.sbHourOpen.get_property("sensitive"))
        self.assertFalse(self.a.sbMinuteOpen.get_property("sensitive"))
        
    def testCloseWidgetsDeActiveOnToggled(self):
        self.a.widgets.get_widget("checkbuttonCloseTest").set_active(True)
        self.assertTrue(self.a.cClose.get_property("sensitive"))
        self.assertTrue(self.a.sbHourClose.get_property("sensitive"))
        self.assertTrue(self.a.sbMinuteClose.get_property("sensitive"))
        self.a.widgets.get_widget("checkbuttonCloseTest").set_active(False)
        self.assertFalse(self.a.cClose.get_property("sensitive"))
        self.assertFalse(self.a.sbHourClose.get_property("sensitive"))
        self.assertFalse(self.a.sbMinuteClose.get_property("sensitive"))

    # if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestConfigQuiz)
unittest.TextTestRunner(verbosity=2).run(suite)
