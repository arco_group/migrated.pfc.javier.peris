#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
# import pygtk
# pygtk.require('2.0')
import gtk
# import gtk.glade
import amara
#import pango
import os

import choiceMultiple


class TestchoiceMultiple(unittest.TestCase):

    def setUp(self):
        self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade")

    
#     def testchoiceModificatorMultiple(self):
#         title = "prueba"
#         defaultMark = "0"
#         sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
#         question = "Pregunta de prueba"
#         single = False
#         self.a.choiceModificator(title, defaultMark, sc, question, single)
#         doc = amara.parse("../QuestionsBank/prueba.xml")
#         self.assertEqual(doc.assessmentItem.title,"prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue,"0")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt,"Pregunta de prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mapKey,"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mappedValue,"1")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mapKey,"B")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mappedValue,"0")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value,"A")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[0],"Respuesta A")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[1],"Respuesta B")
#         os.remove("../QuestionsBank/prueba.xml")

#     def testchoiceModificatorSingle(self):
#         title = "prueba"
#         defaultMark = "0"
#         sc = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
#         question = "Pregunta de prueba"
#         single = True
#         self.a.widgets.get_widget("checkbutton1Ans").set_active(True)
#         self.a.choiceModificator(title, defaultMark, sc, question, single)
#         doc = amara.parse("../QuestionsBank/prueba.xml")
#         self.assertEqual(doc.assessmentItem.title,"prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue,"0")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt,"Pregunta de prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mapKey,"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mappedValue,"1")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mapKey,"B")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mappedValue,"0")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0],"A")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[0],"Respuesta A")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[1],"Respuesta B")
#         self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.maxChoices,"1")
#         os.remove("../QuestionsBank/prueba.xml")

    def testCreateChoiceMultipleSingle(self):
        self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade",
                                               None, True)
        print self.agente.get_question("Choice Multiple I")
        self.assertEquals(q, None)
        title = self.widgets.get_widget("entryTitleQuestion").set_text("Choice Multiple I")
		defaultMark = self.widgets.get_widget("entryMarkDefault").set_text("0")
		buf = self.editor.get_textview().get_buffer()
		wording = buf.set_text("Pregunta de prueba")
        self.ans["A"] = ["1", "Respuesta A"]
        self.ans["B"] = ["0", "Respuesta B"]
        widget.clicked()
        print self.agente.get_question("Choice Multiple I")
        q = self.agente.get_question("Choice Multiple I")
        self.assertNotsEquals(q, None)
        doc = amara.parse(q.filename)
        self.assertEqual(doc.assessmentItem.title, "Choice Multiple I")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue,"0")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt,"Pregunta de prueba")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mapKey,"A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mappedValue,"1")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mapKey,"B")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mappedValue,"0")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value,"A")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[0],"Respuesta A")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[1],"Respuesta B")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.maxChoices,"1")

    def testCreateChoiceMultipleMultiple(self):
        self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade", None, False)
        print self.agente.get_question("choicemultiple")
        self.assertEquals(q, None)
        title = self.widgets.get_widget("entryTitleQuestion").set_text("Choice Multiple II")
		defaultMark = self.widgets.get_widget("entryMarkDefault").set_text("0")
		buf = self.editor.get_textview().get_buffer()
		wording = buf.set_text("Pregunta de prueba")
        self.ans["A"] = ["1", "Respuesta A"]
        self.ans["B"] = ["0", "Respuesta B"]
        widget.clicked()
        print self.agente.get_question("Prueba")
        q = self.agente.get_question("Prueba")
        self.assertNotsEquals(q, None)
        doc = amara.parse(q.filename)
        self.assertEqual(doc.assessmentItem.title, "Choice Multiple II")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue, "0")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.prompt, "Pregunta de prueba")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mapKey, "A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry.mappedValue, "1")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mapKey, "B")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.mapEntry[1].mappedValue, "0")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0], "A")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[0], "Respuesta A")
        self.assertEqual(doc.assessmentItem.itemBody.choiceInteraction.simpleChoice[1], "Respuesta B")

#     def testMoreAns(self):
#         widget = self.a.widgets.get_widget("buttonMoreAns")
#         self.a.buttonMoreAns_clicked(widget)
#         elementos = 0
#         labels = 0
#         vbox = 0
#         for e in self.a.widgets.get_widget("tableAns").get_children():
#              elementos += 1
#              if isinstance(e,gtk.VBox) :
#                  labels+=1
#              else:
#                  vbox+=1
#         self.assertEquals(elementos,12)
#         self.assertEquals(labels,6)
#         self.assertEquals(vbox,6)


    # if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestchoiceMultiple)
unittest.TextTestRunner(verbosity=2).run(suite)
