#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
# import pygtk
# pygtk.require('2.0')
import gtk
# import gtk.glade
import amara
#import pango

import inLineChoice


class TestInLineChoice(unittest.TestCase):

    def setUp(self):
        self.a = inLineChoice.InLineChoice("inLineChoice.glade")

#     def testchoiceModificatorMultiple(self):
#         title = "test" 
#         defaultMark = "0"
#         simpleChoice = {"A": ["1", "Respuesta A"], "B": ["0", "Respuesta B"]}
#         question = "Pregunta de prueba []"
#         doc = amara.parse("inlinechoice.xml")
#         self.a.xmlModificator(title,defaultMark,simpleChoice,question)
#         self.assertEqual(doc.assessmentItem.title,"Prueba")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0],"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[1],"B")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue,"0")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey,"A")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue,"1")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey,"B")
#         self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue,"0")
#         self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[0],"Respuesta A")
#         self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[0].identifier,"A")
#         self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[1],"Respuesta B")
#         self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[1].identifier,"B")

    def testCreateInlineChoice(self):
        self.widgets.get_widget("entryTitleQuestion").set_text("Inline Choice")
        self.widgets.get_widget("entryMarkDefault").set_text("0")
        self.ans["A"] = ["1", "Respuesta A"]
        self.ans["B"] = ["0", "Respuesta B"]        
        buf = self.editor.get_textview().get_buffer()
        wording = buf.set_text("Pregunta de prueba []")
        widget.clicked()
        print self.agente.get_question("Inline Choice")
        q = self.agente.get_question("Inline Choice")
        self.assertNotsEquals(q, None)
        doc = amara.parse("inlinechoice.xml")
        self.assertEqual(doc.assessmentItem.title,"Inline Choice")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[0], "A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.correctResponse.value[1], "B")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapping.defaultValue, "0")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey, "A")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue, "1")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mapKey, "B")
        self.assertEqual(doc.assessmentItem.responseDeclaration.mapEntry.mappedValue, "0")
        self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[0], "Respuesta A")
        self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[0].identifier, "A")
        self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[1], "Respuesta B")
        self.assertEqual(doc.assessmentItem.itemBody.inlineChoiceInteraction.inlineChoice[1].identifier, "B")


#     def testMoreAns(self):
#         widget = self.a.widgets.get_widget("buttonMoreAns")
#         self.a.buttonMoreAns_clicked(widget)
#         elementos = 0
#         labels = 0
#         vbox = 0
#         for e in self.a.widgets.get_widget("tableAns").get_children():
#              elementos += 1
#              if isinstance(e,gtk.VBox) :
#                  labels+=1
#              else:
#                  vbox+=1                 
#         self.assertEquals(elementos,12)
#         self.assertEquals(labels,6)
#         self.assertEquals(vbox,6)
        
        
    # if __name__ =' __main__' :
#     unittest.main()
suite = unittest.TestLoader().loadTestsFromTestCase(TestInLineChoice)
unittest.TextTestRunner(verbosity=2).run(suite)
