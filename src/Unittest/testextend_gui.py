#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import unittest
import gtk
import amara
import os

import extend_gui
import choiceMultiple
import text_entry

class TestExtendGui(unittest.TestCase):

    # def setUp(self):
#         self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade")

    def testMoreAnsCM(self):
        self.a = choiceMultiple.ChoiceMultiple("choiceMultiple.glade")
        widget = self.a.widgets.get_widget("buttonMoreAns")
        self.a.buttonMoreAns_clicked(widget)
        elementos = 0
        labels = 0
        vbox = 0
        for e in self.a.widgets.get_widget("tableAns").get_children():
             elementos += 1
             if isinstance(e,gtk.VBox) :
                 labels+=1
             else:
                 vbox+=1
        self.assertEquals(elementos,12)
        self.assertEquals(labels,6)
        self.assertEquals(vbox,6)

    def testMoreAnsTE(self):
        widget = self.a.widgets.get_widget("buttonMoreAns")
        self.a.buttonMoreAns_clicked(widget)
        elementos = 0
        labels = 0
        vbox = 0
        for e in self.a.widgets.get_widget("tableAns").get_children():
             elementos += 1
             if isinstance(e,gtk.VBox) :
                 labels+=1
             else:
                 vbox+=1
        self.assertEquals(elementos,12)
        self.assertEquals(labels,6)
        self.assertEquals(vbox,6)

    def testMoreAns(self):
        self.a = text_entry.TextEntry("text_entry.glade")
        widget = self.a.widgets.get_widget("buttonMoreAns")
        self.a.buttonMoreAns_clicked(widget)
        elementos = 0
        labels = 0
        vbox = 0
        for e in self.a.widgets.get_widget("tableAns").get_children():
             elementos += 1
             if isinstance(e,gtk.VBox) :
                 labels+=1
             else:
                 vbox+=1
        self.assertEquals(elementos,12)
        self.assertEquals(labels,6)
        self.assertEquals(vbox,6)


suite = unittest.TestLoader().loadTestsFromTestCase(TestValidateData)
unittest.TextTestRunner(verbosity=2).run(suite)
