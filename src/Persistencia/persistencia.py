# -*- coding: iso-8859-15; tab-width:4 -*-

import datetime
import sys
sys.path.append('../AuthoringTool/')
sys.path.append('../Editor')

try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import amara
except:
	sys.exit(1)
import logging
from sqlobject import AND, OR, IN

import editor
import table
import image
import vocabulary
import term
import question
import exam
import testpart
import section
import questionref
import exception
import db


class Agente:

	DIR_QUESTIONS = "../QuestionsBank/"
	DIR_QUIZS = "../QuizsBank/"
	agents = {}

	@classmethod
	def register(cls, cls_agent):
		Agente.agents[cls_agent.name] = cls_agent
		for k, v in Agente.agents.items():
			logging.info("%s agent is registered" % k)

	def get_answers(self, id):
		try:
			q = db.Question.select(db.Question.q.id==id)[0]
		except:
			logging.exception("The question with id[%s] doesn't exists" % id)
		answers = []
		default = 0
		if isinstance(q, db.ChoiceMultiple):
			for ans in q.answers:
				if ans.calification > 0:
					answers.append(ans)
		else:
			for ans in q.answers:
				if ans.calification > default:
					default = ans.calification
					answers = [ans]
		return answers

	def get_all_vocabularies(self):
		vocabularies = []
		vocas = db.Vocabulary.select()
		for v in vocas:
			voca = vocabulary.Vocabulary(v.name, v.description)
			vocabularies.append(voca)
		return vocabularies

	def get_vocabulary(self, name):
		try:
		    voc = db.Vocabulary.select(db.Vocabulary.q.name == name)[0]
		    voca = vocabulary.Vocabulary(voc.name, voc.description)
		except:
			voca = None
		return voca

	def get_terms_vocabulary(self, voc_name):
		try:
			selected_voc = db.Vocabulary.select(db.Vocabulary.q.name==voc_name)[0]
            terms = db.Term.select(db.Term.q.vocabulary==selected_voc.id)
			list_terms = []
			for t in terms:
				voc = vocabulary.Vocabulary(selected_voc.name,
		 									selected_voc.description)
				ter = term.Term(t.name, t.description, voc)
				list_terms.append(ter)
			return list_terms
		except:
			logging.exception("There is no terms for the selected vocabulary")


    def get_questions_by_term(self, ter):
		voc = db.Vocabulary.select(db.Vocabulary.q.name==ter.vocabulary)
		term = db.Term.select(AND(db.Term.q.name==ter.name,
									  db.Term.q.vocabulary==voc[0]))
	    questions = []
		try:
			for q in term[0].term_questions:
				questions.append(self.create_question(q))
		except:
			logging.exception("The term doesn't exist")
		return questions

	def get_questions_by_name(self, title):
		ques = db.Question.select(db.Question.q.title==title)
		questions = []
		for q in ques:
			questions.append(self.create_question(q))
		return questions

	def get_questions(self):
		questions = []
		ques = db.Question.select()
		for q in ques:
			questions.append(self.create_question(q))
		return questions

	def get_questions_by_terms(self, terms):
# 		terms = []
# 		for i in range(len(terms)):
# 		    terms.append(db.Term.select(db.Term.q.name==names_term[i])[0].name)
# 		query = ""
# 		for i in range(len(terms)):
# 			query = "term=" + terms[i]
# 			if i != len(terms):
# 				query += " or "
# 		return db.Term_questions.select("" + query + "")
		questions = []
		if len(terms):
			if terms[0] == None:
				db_questions = db.Question.select()
				for db_question in db_questions:
					if not len(db_question.term_questions):
						ques = self.create_question(db_question)
						if ques:
							questions.append(ques)
				terms = terms[1:len(terms)]
			for term in terms:
				voc = db.Vocabulary.select(db.Vocabulary.q.name==term.vocabulary)
				results = db.Term.select(AND(db.Term.q.name==term.name,
											 db.Term.q.vocabulary==voc[0]))[0]
# 				results = db.Term.select(db.Term.q.name==term)[0]
				for result in results.term_questions:
					questions.append(self.create_question(result))
		return questions
		# db.Term_question.select(
		# 		db.Question.select(EXISTS(Select(db.Term_question.q.
		# 		Test1.select(EXISTS(Select(Test2.q.col2, where=(Outer(Test1).q.col1 == Test2.q.col2))))

	def create_vocabulary(self, vocabulary):
		try:
			db.Vocabulary(name=vocabulary.name,
						  description=vocabulary.description)
		except:
			msg = "El vocabulario ya existe"
			raise exception.VocabularyAlreadyExistsException(msg)

	def update_vocabulary(self, name, new_vocabulary):
		try:
			vocabulary = db.Vocabulary.select(db.Vocabulary.q.name==name)[0]
			vocabulary.name = new_vocabulary.name
			new_vocabulary.description = new_vocabulary.description
		except:
			raise exception.NotVocabularyException("No existe el vocabulario")
		# for term in terms:
# 			db.Term.select(db.Term.q.name==term.name)

    def add_term_vocabulary(self, new_term, vocabulary):
		related_vocabulary = db.Vocabulary.select(db.Vocabulary.q.name==vocabulary.name)[0]
		for term in related_vocabulary.terms:
			if term.name == new_term.name:
				raise exception.TermAlreadyExistsException("Existe el t�rmino")
		db.Term(name=new_term.name, description=new_term.description,
	 			vocabulary=related_vocabulary)

    def update_term(self, voc_name, name_term, new_term):
		related_vocabulary = db.Vocabulary.select(db.Vocabulary.q.name==voc_name)[0]
		logging.debug("Vocabulary of the term to update: %s" % related_vocabulary.name)
		try:
			term = db.Term.select(AND(db.Term.q.name==name_term,
									  db.Term.q.vocabulary==related_vocabulary))[0]
		except:
			logging.exception("The term %s doesn't exist" % name_term)
			raise exception.NotTermException("The term doesn't exist")
		term.name = new_term.name
		term.description = new_term.description
		logging.debug("Term after updating %s" % term.name)

	def delete_vocabulary(self, old_vocabulary):
		selected_voc = db.Vocabulary.select(db.Vocabulary.q.name==old_vocabulary.name)[0]
		selected_voc.destroySelf()

	def delete_term(self, related_vocabulary, name):
		vocabulary = self.get_vocabulary(related_vocabulary.name)
		voc = db.Vocabulary.select(db.Vocabulary.q.name==vocabulary.name)[0]
		term = db.Term.select(AND(db.Term.q.name==name,
 								db.Term.q.vocabulary==voc.id))[0]
		if term:
			db.Term.delete(term.id)

	def get_term(self, voc, name_term):
		selected_voc = db.Vocabulary.select(db.Vocabulary.q.name == voc.name)[0]
		ter = db.Term.select(db.Term.q.vocabulary==selected_voc.id)
		te = None
		ter_requested = None
		for t in ter:
			if t.name==name_term:
				te = t
		if te:
			ter_requested = term.Term(te.name, te.description,
									  vocabulary.Vocabulary(te.vocabulary.name,
															te.vocabulary.description))
		logging.debug("Requested term: %s" % ter_requested.name)
		return ter_requested
# 		t = db.Term.select(AND(db.Term.q.vocabulary==selected_voc.id),
# 						   db.Term.q.name==name_term)
# 		if t:
# 			return term.Term(t.name, t.description, vocabulary.Vocabulary(t.vocabulary.name,
# 																		  t.vocabulary.description))
# 		else:
# 			return None

    def get_question(self, filename):
		try:
			q = db.Question.select(db.Question.q.filename==filename)[0]
		except:
			logging.exception("The %s question is not in the db" % filename)
			msg = "The question is not in the db"
			raise exception.NotQuestionException(msg)
		return self.create_question(q)

	def delete_question(self, filename):
		question = db.Question.select(db.Question.q.filename==filename)[0]
		db.Question.delete(question.id)
		#question.destroySelf()

	def is_used(self, question):
		q = db.Question.select(db.Question.q.filename==question.id)[0]
		logging.debug("Question to search %s" % q.title)
		used = False
		try:
			qrs = db.QuestionRef.select(db.QuestionRef.q.question==q.id)
		except:
			logging.info("No aparece en ningun examen")
		for qr in qrs:
			secs = db.Section.select(db.Section.q.questionref==qr.id)
			for sec in secs:
				parts = db.Part.select(db.Part.q.sections==sec.id)
				for part in parts:
					exams = db.Test.select(db.Test.q.part==part.id)
					for exam in exams:
						if exam.start_date < datetime.datetime.now():
							used = True
		return used

	def create_question(self, q):
		wording = []
		for content in q.wording:
			if isinstance(content, db.PangoBuffer):
				textbuffer = gtk.TextBuffer()
# 				u_text = content.text.decode("latin-").encode("utf-8")
# 				textbuffer.set_text(u_text)
				textbuffer.set_text(content.text)
				pb = editor.InteractivePangoBuffer("", textbuffer)
				wording.append(pb)
			elif isinstance(content, db.TableI):
				tbl = table.Table(content.filename, "", content.scale)
				wording.append(tbl)
			elif isinstance(content, db.Image):
				img = image.Image(content.filename, "", content.scale)
				wording.append(img)
			elif isinstance(content, db.Formula):
				frmula = formula.Formula(content.filename)
				wording.append(frmula)
		try:
			ques = Agente.agents[q._parent.childName]().create(q, wording)
			ques.set_filename(q.filename)
			ques.set_id(q.id)
			terms = []
			for ter in q.term_questions:
				voc = vocabulary.Vocabulary(ter.vocabulary.name,
											ter.vocabulary.description)
				terms.append(term.Term(ter.name, ter.description, voc))
			ques.set_terms(terms)
		except:
			logging.exception("There is not agent to create the question %s" % q._parent.childName)
			ques = q
		return ques

	def save_question(self, question):
		for k, v in Agente.agents.items():
			logging.debug("Agent: %s", k)
		logging.debug("Requested agent: %s" % question.name)
		Agente.agents[question.name]().save(question)

	def clone_question(self, filename):
		self.save_question(self.get_question(filename))

	def get_questions_by_id(self, ids):
		ts = db.Question.select(IN(db.Question.q.id, list(ids)))
		try:
			q = list(ts)
		except:
			q = []
		return q

	def get_question_by_id(self, id):
		return db.Question.select(db.Question.q.id==id)[0]

	def get_exams_by_subject(self, ids):
		ts = db.Test.select(IN(db.Test.q.subjectID, list(ids)))
		return list(ts)

	def get_exams_by_id(self, ids):
		ts = db.Test.select(IN(db.Test.q.id, list(ids)))
		return list(ts)

	def get_exam_by_id(self, id):
		ts = db.Test.select(db.Test.q.id==id)[0]
		return ts

	def get_exam_by_date(self, tm):
		test = db.Test.select(db.Test.q.start_date==tm)
		try:
			return self._create_exam(test[0])
		except:
			return None
		tests = db.Test.select()
		for t in tests:
			logging.debug("Start date test %s" % t.start_date.__str__())


	#WARNING: REVIEW WHETHER THE SUBJECT STRING CODING IS CORRECT
	def save_exam(self, exam, name_subject, rep_type):
		import time
		gmtime = time.gmtime()
		fileid = exam.title
		for i in range(6):
			fileid += str(gmtime[i])
		logging.debug("File id: %s" % fileid)
		exam.filename = Agente.DIR_QUIZS + fileid + ".xml"


		name_subject = name_subject.decode("utf-8").encode("latin-")
		s = db.Subject.select(db.Subject.q.name==name_subject)
		test = db.Test(name=exam.title, filename=exam.filename,
					   start_date=exam.open_date, end_date=exam.close_date,
					   timeLimit=exam.max_time, subject=s[0],
					   description=exam.description,
					   representation_type=rep_type)
		for part in exam.testparts:
			parte = db.Part(title=part.id, test=test)
			for sec in part.sections:
				sect = db.Section(title=sec.title, part=parte)
				for q in sec.questionrefs:
					ques = db.Question.select(db.Question.q.filename==q.question)[0]
					db.QuestionRef(weight=q.weight, section=sect,
								   question=ques)

		doc = self.create_exam_xml(exam)
		try:
			fd = open(exam.filename, 'w')
		except IOError:
			logging.exception("Error saving the xml exam")
		else:
			fd.write(doc.xml())
			fd.close()

		exam.id = test.id

	def get_exams(self):
		list_exams = []
		exams = db.Test.select()
		for ex in exams:
			sub = db.Subject.select(db.Subject.q.id == ex.subject)
			exa = exam.Exam(ex.name, [], o_date=ex.start_date,
 							c_date=ex.end_date, subject=sub[0].name)
			exa.id = ex.filename
			for p in ex.parts:
				par = testpart.TestPart(p.title,[])
				for s in p.sections:
					sec = section.Section(s.title, [])
					for qr in s.questionref:
						ques = self.create_question(qr.question)
						q = questionref.QuestionRef(ques, qr.weight)
						sec.add_question(q)
					par.add_section(sec)
				exa.add_part(par)
			list_exams.append(exa)
		return list_exams

	def _create_exam(self, ex):
		sub = db.Subject.select(db.Subject.q.id == ex.subject)
		exa = exam.Exam(ex.name, [], oDate=ex.start_date, cDate=ex.end_date,
	 					subject=sub[0].name, description=ex.description)
		exa.id = ex.filename
		for p in ex.parts:
			par = testpart.TestPart(p.title,[])
			for s in p.sections:
				sec = section.Section(s.title, [])
				for qr in s.questionref:
					ques = self.create_question(qr.question)
					q = questionref.QuestionRef(ques, qr.weight)
					sec.add_question(q)
				par.add_section(sec)
			exa.add_part(par)
		logging.debug(exa.__str__())
		return exa

	#HAY QUE BORRAR EL ANTIGUO XML Y ACTUALIZAR LA ASIGNATURA
	def update_exam(self, exam):
		e = db.Test.select(db.Test.q.filename==exam.id)[0]
		e.name = exam.title
		e.start_date = exam.open_date
		e.end_date = exam.close_date
		e.timeLimit = exam.max_time
		logging.debug("Update exam open date: %s" % e.start_date.__str__())

		for part in e.parts:
			for sec in part.sections:
				for qref in sec.questionref:
					db.QuestionRef.delete(qref.id)
				db.Section.delete(sec.id)
			db.Part.delete(part.id)

		for part in exam.testparts:
			logging.debug(str(part.id))
			parte = db.Part(title=part.id, test=e)
			for sec in part.sections:
				logging.debug(sec.title)
				sect = db.Section(title=sec.title, part=parte)
				for q in sec.questionrefs:
					ques = db.Question.select(db.Question.q.filename==q.question)
					db.QuestionRef(weight=q.weight, section=sect,
 									question=ques)

		doc = self.create_exam_xml(exam)
		try:
			fd = open(exam.filename, 'w')
		except:
			logging.exception("Error updating the xml exam")
		else:
			fd.write(doc.xml())
			fd.close()

	def create_exam_xml(self, exam):
		num_part = 0
		num_section = 0
		num_question = 0
		doc = amara.parse("../Templates/test.xml")
		doc.assessmentTest.title = unicode(exam.title, 'utf-8')
		if exam.max_time:
			doc.assessmentTest.xml_append(
					doc.xml_create_element(u'timeLimits',
										   attributes={
									u'maxTime':  unicode(str(exam.max_time),
														 'utf-8')}))

		for part in exam.testparts:
			doc.assessmentTest.xml_append(
					doc.xml_create_element(u'testPart',
										   attributes={u'identifier': unicode(part.id, 'utf-8'),
													   u'navigationMode': u'linear',
													   u'submissionMode': u'individual'}))
			for section in part.sections:
				title = unicode(section.title, 'utf-8')
				doc.assessmentTest.testPart[num_part].xml_append(
						doc.xml_create_element(u'assessmentSection',
											   attributes={ u'identifier': title
															}))
				for qref in section.questionrefs:
					#print "NUMERO DE PREGUNTA", num_question
					doc.assessmentTest.testPart[num_part].assessmentSection[num_section].xml_append(
							doc.xml_create_element(u'assessmentItemRef', attributes={
											u'href': unicode(qref.question,'utf-8')}))
					print doc.assessmentTest.testPart[num_part].assessmentSection[num_section]
					weight = unicode(str(qref.weight), 'utf-8')
					value = unicode(str(qref.weight), 'utf-8')
# 					print "NUM_PART", num_part
# 					print "NUM_SECTION", num_section
# 					print "NUM_QUESTION", num_question
					doc.assessmentTest.testPart[num_part].assessmentSection[num_section].assessmentItemRef[num_question].xml_append(
							doc.xml_create_element(u'weight', attributes={
											u'WEIGHT': weight,
											u'value': value}))
					num_question = num_question + 1
				num_question = 0
				num_section = num_section + 1
			num_section = 0
			num_part = num_part + 1
		return doc

	def get_subject_names(self):
		names = []
		subjects = db.Subject.select(db.Subject.q.id!=0)
		for subject in subjects:
			names.append(subject.name)
		return names

	def delete_exam(self, exam):
		test_selected = db.Test.select(db.Test.q.filename==exam.id)[0]

		# import os
		# os.remove(exam.filename)
		# if test_selected.representation_type == "Web":
		# 	os.remove(exam.filename.split(".xml")[0] + ".html")
		# elif test_selected.representation_type == "Paper":
		# 	os.remove(exam.filename.split(".xml")[0] + ".tex")
		# elif test_selected.representation_type == "Both":
		# 	os.remove(exam.filename.split(".xml")[0] + ".html")
		# 	os.remove(exam.filename.split(".xml")[0] + ".tex")
		# elif test_selected.representation_type == "Other":
		# 	os.remove(exam.filename.split(".xml")[0] + ".txt")

		test_selected.destroySelf()

	def is_changeable(self, exam):
		t = db.Test.select(db.Test.q.filename==exam.id)[0]
		is_changeable = True
		if t.start_date < datetime.datetime.now():
			logging.debug("The exam is not changeable")
			is_changeable = False
		logging.debug("Open date exam %s" % t.start_date.__str__())
		logging.debug("Open date exam %s" % datetime.datetime.now().__str__())
		return is_changeable

	def extend_exams(self):
		for t in db.Test.select():
			t.end_date = datetime.datetime.now() + datetime.timedelta(7)

	def close_exams(self):
		for t in db.Test.select():
			t.end_date = datetime.datetime.now()

	def open_exams(self):
		for t in db.Test.select():
			t.start_date = datetime.datetime.now() + datetime.timedelta(0,7)
			t.end_date = datetime.datetime.now() + datetime.timedelta(7)

	def is_extended_text(self, q):
		return (isinstance(q, db.ExtendedText) and [True] or [False])[0]

	def get_answer(self, id, q):
		answer = db.Answer.select(AND(db.Answer.q.ide==id,
									  db.Answer.q.question==q))[0]
		return answer

	def get_calification(self, value, question):
		try:
			answer = db.Answer.select(AND(db.Answer.q.value==value,
										  db.Answer.q.question==question))[0]
			return answer.calification
		except:
			return 0

	def get_questions_from_exam(self, exam_id):
		ques_id = []
		test = db.Test.get(id=exam_id)
		for part in test.parts:
			for sec in part.sections:
				for qref in sec.questionref:
					ques_id.append(qref.questionID)
		return ques_id


	def get_question_name(self, question_id):
		return db.Question.get(id=question_id).title





