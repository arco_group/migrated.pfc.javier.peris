#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-
import sys
sys.path.append('../Persistencia')
import time
import amara
import logging

import db
import persistencia


class AgenteIMS:

    def __init__(self):
        self.agente = persistencia.Agente()

    def create_multiple_choice_item(self, title, defaultMark, simpleChoice,
		 										question, single, category):
        doc = amara.parse("../Questions/choice_multiple.xml")
        if single:
            doc.assessmentItem.itemBody.choiceInteraction.maxChoices = \
                                                                     unicode("1",'utf-8')
        doc.assessmentItem.title = unicode(title, 'utf-8')
        #     doc.assessmentItem.adaptive = u'true'
        #     doc.assessmentItem.timeDependent = u'true'

        doc.assessmentItem.itemBody.choiceInteraction.xml_append(
            doc.xml_create_element(u'prompt',
                                   content=unicode(question, 'utf-8')))
        for k, v  in simpleChoice.items():
            if float(v[0]) > 0:
                #logging.info("V[0]:" + v[0] + str(type(v[0])))
                doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
                    doc.xml_create_element(u'value',
                                           content=unicode(k, 'utf-8')))
            doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
                                                                        unicode(defaultMark, 'utf-8')
            doc.assessmentItem.responseDeclaration.mapping.xml_append(
                    doc.xml_create_element(u'mapEntry',
                                           attributes={u'mapKey': unicode(k, 'utf-8'),
                                                       u'mappedValue': unicode(v[0], 'utf-8')}))
            doc.assessmentItem.itemBody.choiceInteraction.xml_append(
                doc.xml_create_element(u'simpleChoice',
                                       attributes={u'identifier':  unicode(k, 'utf-8'),
                                                   u'fixed': u'false'},
                                       content = unicode(v[1], 'utf-8')))
	    self.agente.save_question(category, title, "choiceMultiple", doc)

    def create_extended_text_item(self, title, expectedLength, question, category):
		doc = amara.parse("../Questions/extended_text.xml")
		doc.assessmentItem.title = unicode(title, 'utf-8')
		# doc.assessmentItem.adaptive = u'true'
		# doc.assessmentItem.timeDependent = u'true'
		doc.assessmentItem.itemBody.extendedTextInteraction.xml_append(
			doc.xml_create_element(u'prompt',
					       content = unicode(question, 'utf-8')))
		doc.assessmentItem.itemBody.extendedTextInteraction.expectedLength = \
			unicode(expectedLength, 'utf-8')
		self.agente.save_question(category, title, "choiceMultiple", doc)

    def create_text_entry_item(self, title, eLength, ans, defaultMark,
 								question, category):
		doc = amara.parse("../Questions/text_entry.xml")

		doc.assessmentItem.title = unicode(title, 'utf-8')
        # 		doc.assessmentItem.adaptive = u'true'
        # 		doc.assessmentItem.timeDependent = u'true'

        for k, v  in ans.items():
            if v > 0:
                doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
                    doc.xml_create_element(u'value', content=unicode(k,
                                                                     'utf-8')))
                doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
                                                                            unicode(defaultMark, 'utf-8')
                doc.assessmentItem.responseDeclaration.mapping.xml_append(
                    doc.xml_create_element(u'mapEntry',
                                           attributes={u'mapKey': unicode(k, 'utf-8'),
                                                       u'mappedValue': unicode(v, 'utf-8')}))

        qs = question.split("[]")
        se = question.find("")
        for i in range(len(qs)):
            if se:
                doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                if qs[i] == "":
                    doc.assessmentItem.itemBody.xml_append(
                        doc.xml_create_element(u'textEntryInteraction',
                                               attributes={u'responseIdentifier': u'RESPONSE',
                                                           u'expectedLength': unicode(eLength, 'utf-8')}))
                else:
                    if i == len(qs) - 1:
                        doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                    else:
                        doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                        doc.assessmentItem.itemBody.xml_append(
                            doc.xml_create_element(u'textEntryInteraction',
                                                   attributes={u'responseIdentifier': u'RESPONSE',
                                                               u'expectedLength': unicode(eLength, 'utf-8')}))

		print "PARSEADOOOOOOOOOOOOOOOOOOO"
        self.agente.save_question(category, title, "textEntry", doc)


    def create_inline_choice_item(self, title, defaultMark, simpleChoice,
									question, category):
		doc = amara.parse("../Questions/inline_choice.xml")
		doc.assessmentItem.title = unicode(title, 'utf-8')
# 		doc.assessmentItem.adaptive = u'true'
# 		doc.assessmentItem.timeDependent = u'true'

		for k, v  in simpleChoice.items():
			if v[0] > 0:
				doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
					doc.xml_create_element(u'value',
							       content=unicode(k, 'utf-8')))
			doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
				unicode(defaultMark, 'utf-8')
			doc.assessmentItem.responseDeclaration.mapping.xml_append(
							doc.xml_create_element(u'mapEntry',
								attributes={u'mapKey': unicode(k, 'utf-8'),
	 								u'mappedValue': unicode(v[0], 'utf-8')}))
		qs = question.split("[]")
		se = question.find("")
		for i in range(len(qs)):
			if se:
				doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
				if qs[i] == "":
					doc.assessmentItem.itemBody.xml_append(
						doc.xml_create_element(u'inlineChoiceInteraction',
								       attributes={u'responseIdentifier': u'RESPONSE',
										   u'shuffle': u'false'}))
					ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
					for k, v in simpleChoice.items():
						ilci.xml_append(
							doc.xml_create_element(u'inlineChoice',
										attributes={u'identifier':  unicode(k, 'utf-8'),
										u'fixed': u'false'},
										content = unicode(v[1], 'utf-8')))
			else:
				if i == len(qs) - 1:
					doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
				else:
					doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
					doc.assessmentItem.itemBody.xml_append(
						doc.xml_create_element(u'inlineChoiceInteraction',
								       attributes={u'responseIdentifier': u'RESPONSE',
										   u'shuffle': u'false'}))
					ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
					for k, v in simpleChoice.items():
						ilci.xml_append(
							doc.xml_create_element(u'inlineChoice',
										attributes={u'identifier':  unicode(k, 'utf-8'),
										u'fixed': u'false'},
									   content = unicode(v[1], 'utf-8')))
		self.agente.save_question(category, title, "inLineChoice", doc)
