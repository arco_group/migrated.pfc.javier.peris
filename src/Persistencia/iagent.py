import logging
import zope.interface


def methods_invariant(ob):
    assert(hasattr(ob,"create") and callable(ob.create)),
    "you must provide a create method"
    assert(hasattr(ob,"save") and callable(ob.save)),
    "you must provide a save method"
    assert(hasattr(ob, "name")),
    "you must provide a name attribute"

class IAgent(zope.interface.Interface):

    name = zope.interface.Attribute("""agent name""")

    zope.interface.invariant(methods_invariant)
