#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys
import os
from sqlobject import *
from sqlobject.inheritance import InheritableSQLObject

_mURI="mysql://sdocenteuser:45sNjW24@161.67.38.144:3306/sdocente"
#_mURI="mysql://peris:perluja@localhost:3306/sdocente"
sqlhub.processConnection = connectionForURI(_mURI)


class Vocabulary(SQLObject):
    name = StringCol(default=None, length=255, unique=True)
    description = StringCol(default=None)
    terms = MultipleJoin('Term')
Vocabulary.createTable(ifNotExists=True)


class Term(SQLObject):
    name = StringCol(default=None)
    description = StringCol(default=None)
    vocabulary = ForeignKey('Vocabulary', cascade=True)
    term_questions = RelatedJoin('Question', joinColumn='question',
                                 otherColumn='term',
                                 intermediateTable='Term_questions')


class Question(InheritableSQLObject):
    filename = StringCol(default=None, length=255, unique=True)
    title = StringCol(default=None)
    default_mark = FloatCol(default=0.0)
    wording = RelatedJoin('Content')
    term_questions = RelatedJoin('Term', joinColumn='term',
                                 otherColumn='question',
                                 intermediateTable='Term_questions')
    answers = MultipleJoin('Answer')
    questionref = MultipleJoin('QuestionRef')
Term.createTable(ifNotExists=True)
# Question.sqlmeta.addColumn(FloatCol(name='default_mark'),
#                          changeSchema=True)
# Question.sqlmeta.delColumn('qtype', changeSchema=True)


class Content(InheritableSQLObject):
    questions = RelatedJoin('Question')
Content.createTable(ifNotExists=True)
Question.createTable(ifNotExists=True)


class PangoBuffer(Content):
    _inheritable = False
    text = StringCol(default=None)
PangoBuffer.createTable(ifNotExists=True)


class TableI(Content):
    _inheritable = False
    filename = StringCol(default=None)
    scale = FloatCol(default=1.0)
TableI.createTable(ifNotExists=True)
#TableI.sqlmeta.addColumn(FloatCol(name='scale'),
#changeSchema=True)


class Image(Content):
    _inheritable = False
    filename = StringCol(default=None)
    scale = FloatCol(default=1.0)
Image.createTable(ifNotExists=True)
#Image.sqlmeta.addColumn(FloatCol(name='scale'),
#changeSchema=True)


class Formula(Content):
    _inheritable = False
    filename = StringCol(default=None)
Formula.createTable(ifNotExists=True)


class ExtendedText(Question):
    _inheritable = False
    eLength = IntCol(default=None)
    mScore = FloatCol(default=1.0)
#    solution = StringCol(default=None)
#    solution = MultipleJoin('Answer')

ExtendedText.createTable(ifNotExists=True)
# ExtendedText.sqlmeta.addColumn(FloatCol(name='mScore'),
#                                changeSchema=True)


class ChoiceMultiple(Question):
    _inheritable = False
    single = BoolCol()
#    answers = StringCol(default=None)
#    answers = MultipleJoin('Answer')
ChoiceMultiple.createTable(ifNotExists=True)


class TextEntry(Question):
    _inheritable = False
    eLength = IntCol(default=None)
#    answers = StringCol(default=None)
#    answers = MultipleJoin('Answer')
TextEntry.createTable(ifNotExists=True)


class InlineChoice(Question):
    _inheritable = False
#    answers = StringCol(default=None)
#    answers = MultipleJoin('Answer')
InlineChoice.createTable(ifNotExists=True)


class Answer(SQLObject):
    value = StringCol(default=None)
    calification = FloatCol(default=None)
    ide = StringCol(default=None)
    question = ForeignKey('Question')
Answer.createTable(ifNotExists=True)


class Test(SQLObject):
    name = StringCol(default=None)
    filename = StringCol(default=None, length=255, unique=True)
    description = StringCol(default=None)
#    visibility = StringCol(default=None)
    start_date = DateTimeCol(default=None)
    end_date = DateTimeCol(default=None)
    timeLimit = IntCol(default=None)
    parts = MultipleJoin('Part')
    subject = ForeignKey('Subject')
    representation_type = EnumCol(enumValues=["Paper", "Web", "Both", "Other"],
                                  notNull=True)
Test.createTable(ifNotExists=True)


# Test.sqlmeta.delColumn("representation_type", changeSchema=True)
# Test.sqlmeta.addColumn(EnumCol(name='representation_type',
#                                enumValues=["Paper", "Web", "Both", "Other"],
#                                notNull=True), changeSchema=True)

# Question.sqlmeta.delColumn(ForeignKey(name='subject'),
#                            changeSchema=True)


class Part(SQLObject):
    title = StringCol(default=None)
    test = ForeignKey('Test')
    sections = MultipleJoin('Section')
Part.createTable(ifNotExists=True)


class Section(SQLObject):
    title = StringCol(default=None)
    part = ForeignKey('Part')
    questionref = MultipleJoin('QuestionRef')
Section.createTable(ifNotExists=True)


class QuestionRef(SQLObject):
    weight = StringCol(default=None)
    section = ForeignKey('Section')
    question = ForeignKey('Question')
QuestionRef.createTable(ifNotExists=True)


class Subject(SQLObject):
    name = StringCol(length=128)
    uri = StringCol(length=128)
Subject.createTable(ifNotExists=True)


print "DATABASE LOADED WITH SUCCESS"
