/* -*- mode:c++ -*- */

module Persistencia {

    struct Vocabulary {
       string name;
       string description;
    };

    struct Term {
      string name;
      string description;
      Vocabulary voc;
    };


    class Content{};


    class InteractivePangoBuffer extends Content{
      string buffer;
    };


    class Table extends Content{
      string path;
      string description;
      float scale;
    };


    class Image extends Content{
      string path;
      string description;
      float scale;
    };


    class Formula extends Content{
      string path;
    };


    sequence<Content> Wording;

    class Question {
      string title;
      float defaultMark;
      Wording wor;
    };


    class ExtendedText extends Question{
      int eLength;
      string solution;
    };


    struct Option {
      float mark;
      string textAnswer;
    };

    dictionary<string, Option> Answer;

    class ChoiceMultiple extends Question{
      Answer answers;
      bool single;
    };


    dictionary<string, float> Entry;

    class TextEntry extends Question{
      int eLength;
      Entry answers;
    };


    class InLineChoice extends Question{
      Answer simpleChoice;
    };

    struct OptionOrder {
      int order;
      string id;
    };

    dictionary<string, OptionOrder> AnswerOrder;

    class Order extends Question{
      bool shuffle;
      AnswerOrder simpleChoice;
    };

    class QuestionRef{
      double weight;
      Question ques;
    };


    sequence<QuestionRef> Questionrefs;


    class Section{
      string title;
      Questionrefs querefs;
    };


    sequence<Section> Sections;


    class TestPart{
      string id;
      Sections sec;
    };


    sequence<TestPart> Testparts;


    class Exam{
      string title;
      Testparts tesparts;
    };


    sequence<Vocabulary> Vocabularies;
    sequence<Term> Terms;
    sequence<Question> Questions;


    interface Agente {
        Vocabularies  getAllVocabularies();
       	Vocabulary getVocabulary(string name);
       	Terms getTermsVocabulary(string nameVocabulary);
        Questions getQuestionsByTerm(string nameTerm);
	//	sequence<Question>  getQuestionsByTerms(sequence<string> nameTerms);
	void createVocabulary(Vocabulary voc);
	void createTerm(Vocabulary voc, Term ter);
	void updateVocabulary(string name, Vocabulary newVocabulary);
	void addTermVocabulary(Term newTerm, Vocabulary relatedVocabulary);
	void updateTerm(string nameVocabulary, string nameTerm, Term newTerm);
	void deleteVocabulary(string oldVocabulary);
	void deleteTerm(Vocabulary relatedVocabulary, string name);
	Term getTerm(string nameTerm);
	Question getQuestion(string filename);
	void deleteQuestion(string filename);
	bool isUsed(Question ques);
	void createQuestion(Question ques);
	void saveExam(Exam ex);
	void save(Question question);
	void update(Question question);
// 	void saveInlineChoice(InLineChoice question);
// 	void saveChoiceMultiple(ChoiceMultiple question);
// 	void saveTextEntry(TextEntry question);
// 	void saveExtendedText(ExtendedText question);
// 	void saveOrder(Order question);
    };
};
