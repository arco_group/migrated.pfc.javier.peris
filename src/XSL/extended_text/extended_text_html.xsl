<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:output 
     method = "xml"
     encoding = "iso-8859-1"
     omit-xml-declaration = "no"
     doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
     doctype-system = "DTD/xhtml1-transitional.dtd"
     indent = "yes" />
  
  <xsl:template match="a|a/@*|b|em|it|tt|p|p/@*|form|form/@*|
                       img|img/@*|br|br/@*|table|table/@*|tr|tr/@*|td|td/@*|
                       center|img|img/@*|ul|li">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="/assessmentItem">
    <html>
      <body>
	<form ACTION="" METHOD="POST">
	  <xsl:apply-templates/>
	</form>
      </body>
    </html>
  </xsl:template> 

  <xsl:template name="css">
    <style type="text/css">
      H1 {
      color: #000000;
      font-family: helvetica,MS Sans Serif;
      font-weight: bold;
      font-size: 26px;
      }
      H2 {
      font-family: helvetica,MS Sans Serif;
      font-weight: bold;
      font-size: 20px; 
      text-decoration: none
      }
      
      a:link {
      text-decoration : none;
      color: #0000DD;
      } 
      
      a:visited {
      text-decoration : none;
      color: #0000DD;
      } 
      
      a:hover {
      text-decoration : underline; 
      } 
      
      .questionNumber {
      font-weight : bold;
      font-size : 16px;
      }
      
      .questionText {
      font-weight : bold;
      }

      .grade {}
      
      .optionNumber {
      font-weight : bold;
      font-size : 16px;
      }

      .markAnswer {
      background: #cccccc;
      }
      
      body { 
      background: url(pic/bg.jpg) fixed; 
      margin-top : 10px; 
      margin-right : 20px; 
      margin-bottom : 20px; 
      margin-left : 20px; 
      font-weight : normal;
      font-size : 14px;
      }

    </style>
  </xsl:template>

  <xsl:param name="readonly"/>

  
  <xsl:template match="/extendedTextInteraction">
    <xsl:value-of select='prompt'/> 
    <tr>
      <td>&#160;</td>
      <td>
        <xsl:element name="textarea">
	  <xsl:attribute name="name">
	    <xsl:value-of select="$id"/>
	  </xsl:attribute>
	  <xsl:attribute name="rows">
	    <xsl:value-of select="lines"/>
	  </xsl:attribute>
	  <xsl:attribute name="cols">
	    25
	  </xsl:attribute>
        </xsl:element>
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match="img">
    <xsl:element name="img">
      <xsl:attribute name="src">
	<xsl:value-of select="@src"/>
      </xsl:attribute>  
      <xsl:attribute name="alt">
	<xsl:value-of select="@src"/>
      </xsl:attribute>    
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>
