<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output
    method = "text"
    encoding = "iso-8859-1"
    omit-xml-declaration = "yes"
    doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system = "DTD/xhtml1-transitional.dtd"
    indent = "no" />

<xsl:template match='/assessmentItem'>
  <xsl:text>
  \documentclass[12pt]{exam}
  \usepackage{graphicx}
  \begin{document}
  \begin{questions}
  \question
  </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>
  \end{questions}
  \end{document}
  </xsl:text>
</xsl:template>

<xsl:template match='textEntryInteraction'>
  <xsl:text>\fillwithlines{</xsl:text>
  <xsl:value-of select='@expectedLength'/>
  <xsl:text>pc}</xsl:text>
</xsl:template>

<xsl:template match="img">
  <xsl:text>
  \begin{center}
  \includegraphics[scale=1]{../../App/</xsl:text>
  <xsl:value-of select="@src"/>
  <xsl:text>}
  \end{center}
  </xsl:text>
</xsl:template>

<xsl:template match="i">
  <xsl:text>\emph{</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="b">
  <xsl:text>\textbf{</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>}</xsl:text>
</xsl:template>

</xsl:stylesheet>
