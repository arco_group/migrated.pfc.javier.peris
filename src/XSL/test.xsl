<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="../choice/choice.xsl"/>
  <xsl:import href="../extended_text/extended_text.xsl"/>
  <xsl:import href="../text_entry/text_entry.xsl"/>
  <xsl:import href="../in_line_choice/in_line_choice.xsl"/>
<!--   <xsl:import href="questions.xsl"/> -->

  <xsl:output 
    method = "text"
    encoding = "iso-8859-1"
    omit-xml-declaration = "yes"
    doctype-public = "-//W3C//DTD XHTML 1.0 Transitional//EN"
    doctype-system = "DTD/xhtml1-transitional.dtd"
    indent = "no" />
  

  <xsl:template match="/"> 
    <xsl:text>
      \documentclass[pdftex,10pt,a4paper,spanish,color]{exam}
      \usepackage{times}
      
      \global\let\lhead\undefined
      \global\let\chead\undefined
      \global\let\rhead\undefined
      \global\let\lfoot\undefined
      \global\let\cfoot\undefined
      \global\let\rfoot\undefined
      
      %\usepackage{poppy}
      \usepackage{color}
      \usepackage{graphicx}
      \usepackage{tabularx}
      \usepackage{verbatim}
      \usepackage{pifont}
      \usepackage{rotating}
      \usepackage{latexsym}
      \usepackage{multicol}
      \usepackage{babel}
      \usepackage{fancyhdr}
      \usepackage{afterpage}
      \usepackage{geometry}
      
      \usepackage{amssymb}

      \usepackage[latin1]{inputenc}
      \usepackage[T1]{fontenc}
    
      \usepackage{listings}
      \lstloadlanguages{}
      \definecolor{Gris}{gray}{0.5}
      \lstdefinestyle{code}{basicstyle=\scriptsize\ttfamily,%
      commentstyle=\color{Gris},%
      keywordstyle=\bfseries,%
      showstringspaces=false,%
      extendedchars=true,%
      numbers=left,
      numberstyle=\tiny,
      stepnumber=2,
      numbersep=8pt,
      xleftmargin=15pt,
      frame=single}
      
      \lstdefinestyle{screen}{basicstyle=\scriptsize\ttfamily,%
      commentstyle=\color{Gris},%
      keywordstyle=\bfseries,%
      showstringspaces=false,%
      extendedchars=true,%
      xleftmargin=15pt}

      \newcommand{\ifcolor}[1]{#1}

      \usepackage[bf,small]{caption2}
      \setlength{\captionmargin}{0.2cm}
      
      \geometry{margin=1.8cm,top=2.5cm,bottom=2.5cm}
      
      \graphicspath{{/home/peris/SDocente/images/} {images/} {./}}
      
      \definecolor{uclm}{cmyk}{0.2,1,0.6,0.5}
      \definecolor{uclm-light}{cmyk}{0,0.1,0.1,0}
      \definecolor{arco}{cmyk}{0.6,0.4,0,0.3}
      \definecolor{arco-light}{cmyk}{0.1,0,0,0}
      
      \newcommand{\UCLMcolor}[1]{\textcolor{uclm}{#1}}
      \newcommand{\UCLMbgcolor}[1]{\color{uclm-light}#1}
      
      \newcommand{\UCLMlogo}[1][height=1.3cm]{\UCLMcolor{\includegraphics[#1]{uclm.pdf}}}

      \newcommand{\UCLM}{{UCLM}}
      \newcommand{\UCLMname}{{\includegraphics[width=.4\textwidth]{uclmtext.pdf}}}
      
      \newcommand{\CSname}{\textbf{\textsf{Departamento de Imform�tica}}}
      \newcommand{\ESIname}{\textbf{\textsf{Escuela Superior de Inform�tica}}}

      \newcommand{\ESIhead}{\UCLMcolor{%
      \UCLMlogo[height=0.8cm]%
      \begin{tabular}[b]{l}
      {\large\UCLMname}\\
      {\normalsize\ESIname}
      \end{tabular}}}

      \newcommand{\UCLMbglogo}{\ifcolor{%
      \begin{picture}(0,0)
      \put(0,0){\centerline{%
      \begin{tabular}[t]{c}
      \\
      \rule{0pt}{0.20\textheight}\\
      \UCLMbgcolor{\includegraphics[width=0.7\textwidth]{uclm.pdf}}\\
      \rule{0pt}{0.10\textheight}\\
      %\UCLMbgcolor{\fontsize{120}{150pt}\UCLM}
      \end{tabular}}}
      \end{picture}}}

      % Eliminar la marca de agua del escudo
      \renewcommand{\UCLMbglogo}{}
      
      \newcommand{\headframe}{%
      \setlength{\fboxsep}{0mm}%
      \setlength{\fboxrule}{1pt}%
      \setlength{\unitlength}{1pt}%
      \begin{picture}(0,0)(0,0)
      \centerline{%
      \raisebox{-2mm}{%
      \UCLMcolor{\fbox{\UCLMbgcolor{%
      \rule{4mm}{1.2cm}%
      \rule{\textwidth}{1.2cm}}}}}}
      \end{picture}}

      \header{\UCLMbglogo\headframe\UCLMcolor{\ESIhead}}{}{%
    </xsl:text>

    <xsl:call-template name="subject"/>
    <xsl:text>{</xsl:text>
    <xsl:call-template name="title"/>
    <xsl:text>, </xsl:text>
    <xsl:call-template name="date"/>

    <xsl:text>}}
    \footer{}{P�g. \thepage{}/\numpages}{}

    % exam class
    \pointname{p}
    \addpoints

    % longitudes
    \newlength{\altolinea}
    \addtolength{\altolinea}{0.5cm}
    \setlength{\fboxsep}{.2cm}

    \begin{document}
    
    \pagestyle{headandfoot}
	    
    \renewcommand{\theenumi}{(\alph{enumi})}

    </xsl:text>

<!--     <xsl:if test="count(instructions)"> -->
<!--       <xsltext> -->
<!-- 	\begin{center}  -->
<!--       \parbox{14cm}{\emph{ </xsltext> -->
<!--       <xsl:apply-templates select="instructions"/> -->
<!--       <xsl:text>}} -->
<!--       \end{center} -->
<!--       </xsl:text> -->
<!--     </xsl:if> -->
    
    <xsl:text>
      \bigskip
      \noindent
      Apellidos: \enspace\rule{0.44\textwidth}{.5pt}\enspace Nombre: \enspace\rule{0.23\textwidth}{.5pt}\enspace Grupo:\enspace\hrulefill
      \bigskip
    </xsl:text>
    
    <xsl:apply-templates select="hline"/>

    <xsl:if test="count(assessmentTest/testPart/assessmentSection/assessmentItemRef)">
      <xsl:text>\begin{questions} &#10;</xsl:text>
      <xsl:for-each select='assessmentTest/testPart/assessmentSection/assessmentItemRef' >
	<xsl:text>\begin{minipage}{.95\textwidth} &#10;</xsl:text>
	<xsl:variable name="ref" select="@href"/>
	<!--         <xsl:copy-of -->
	<!--         select="document(concat('/home/peris/XML/extended_text/',@href,'.xml'))"/> -->
	<xsl:text>\question</xsl:text>
	<xsl:apply-templates select="document($ref)/assessmentItem"/>
	<!--     <xsl:apply-templates select="question"/>  -->
	<xsl:text>\end{minipage} &#10;</xsl:text>
	<xsl:if test="position()!=last()">
	  <xsl:text>\mbox{} \\[0.5cm] &#10;</xsl:text>
	</xsl:if>
      </xsl:for-each> 
	<xsl:text>&#10; \end{questions} &#10;</xsl:text>
    </xsl:if>
    <xsl:text>\end{document} &#10;</xsl:text>
  </xsl:template>

  <xsl:template match="assessmentItem">
    <xsl:apply-imports/>
<!--     <xsl:if test="count(itemBody/choiceInteraction)"> -->
<!--       <xsl:apply-imports/> -->
<!--     </xsl:if> -->
<!--     <xsl:if test="count(itemBody/extendedTextInteraction)">  -->
<!--       <xsl:apply-imports/> -->
<!--     </xsl:if> -->
<!--     <xsl:if test="count(itemBody/textEntryInteraction)"> -->
<!--       <xsl:apply-imports/> -->
<!--     </xsl:if> -->
<!--     <xsl:if test="count(itemBody/inlineChoiceInteraction)"> -->
<!--       <xsl:apply-imports/> -->
<!--     </xsl:if> -->
  </xsl:template> 

  <xsl:template match="extendedTextInteraction">
    <xsl:param name="lines" select="@expectedLines*1.5"/>
    <xsl:value-of select='prompt'/>
    <xsl:text> \fillwithlines{</xsl:text>
    <xsl:value-of select='$lines'/>
    <xsl:text>pc}</xsl:text>
  </xsl:template>
  
  <xsl:template match="img">
    <xsl:text>
      \begin{center}
    \includegraphics[scale=1]{../../App/</xsl:text>
    <xsl:value-of select="@src"/>
    <xsl:text>}
    \end{center}
    </xsl:text>
  </xsl:template>

<!--   <xsl:template match="extendedTextInteraction"> -->
<!--     <xsl:param name="lines" select="@expectedLines*1.5"/> -->
<!--     <xsl:text>\question </xsl:text> -->
<!--     <xsl:value-of select='prompt'/> -->
<!--     <xsl:text> \fillwithlines{</xsl:text> -->
<!--     <xsl:value-of select='$lines'/> -->
<!--     <xsl:text>cc}</xsl:text> -->
<!--   </xsl:template> -->

  <!-- el nombre de la asignatura -->
  <xsl:template name="subject">
    <xsl:text>\textbf{\large </xsl:text>
    <xsl:value-of select="@course"/>
    <xsl:text> }\\ </xsl:text>
  </xsl:template>

  <!-- el motivo del examen -->
  <xsl:template name="title">
    <xsl:text>\small </xsl:text>
    <xsl:value-of select="assessmentTest/@title"/>
  </xsl:template>
      
  <!-- La fecha -->
  <xsl:template name="date">
    <xsl:call-template name="parsedate">
      <xsl:with-param name="date" select="@from"/>
    </xsl:call-template>
  </xsl:template>

  <!-- <xsl:template match="question"> -->
<!--     <xsl:text>\begin{minipage}{.95\textwidth} &#10;</xsl:text> -->
<!--     <xsl:text>\question</xsl:text> -->
<!--     <xsl:if test="count(/exam_view/@show_points)=0 or /exam_view/@show_points='yes'"> -->
<!--       <xsl:text>[</xsl:text> -->
<!--       <xsl:value-of select="@grade"/> -->
<!--       <xsl:text>] </xsl:text> -->
<!--     </xsl:if> -->

<!--     <xsl:text> &#10;</xsl:text>     -->
<!--     <xsl:apply-templates select="part"/> -->
    
<!--     <xsl:if test="count(part)=0"> -->
<!--       <xsl:call-template name="part"/> -->
<!--     </xsl:if> -->
<!--     <xsl:text>\end{minipage} &#10;</xsl:text> -->

<!--     <xsl:if test="position()!=last()"> -->
<!--       <xsl:text>\mbox{} \\[0.5cm] &#10;</xsl:text> -->
<!--     </xsl:if> -->
    
<!--   </xsl:template> -->

    <xsl:template match="hline">
      <xsl:text>
	\hbox to \textwidth{\enspace\hrulefill}
	\bigskip
      </xsl:text>
    </xsl:template>

    <xsl:template name="parsedate">
    <xsl:param name="date"/>
    <xsl:value-of select="substring($date,7)"/>
    <xsl:text> de </xsl:text>
    <xsl:variable name="month" select="substring($date,5,2)"/>
    <xsl:choose>
      <xsl:when test="$month=01">enero</xsl:when>
      <xsl:when test="$month=02">febrero</xsl:when>
      <xsl:when test="$month=03">marzo</xsl:when>
      <xsl:when test="$month=04">abril</xsl:when>
      <xsl:when test="$month=05">mayo</xsl:when>
      <xsl:when test="$month=06">junio</xsl:when>
      <xsl:when test="$month=07">julio</xsl:when>
      <xsl:when test="$month=08">agosto</xsl:when>
      <xsl:when test="$month=09">septiembre</xsl:when>
      <xsl:when test="$month=10">octubre</xsl:when>
      <xsl:when test="$month=11">noviembre</xsl:when>
      <xsl:when test="$month=12">diciembre</xsl:when>
      <xsl:otherwise><xsl:value-of select="$month"/>
      </xsl:otherwise>	
    </xsl:choose>
    <xsl:text> de </xsl:text>
    <xsl:value-of select="substring($date,1,4)"/>
  </xsl:template>

</xsl:stylesheet>
