# -*- coding: iso-8859-15; tab-width:4 -*-

"""Extends the gui with the possibilty of adding more answers"""
import sys
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)


def add_more_answers(tableAns, ans, numAns, *id):
    """Add 3 more field sets to the form (dialog)"""

    tableAns.resize(numAns + 3, 2)
    ini = id[0]
    for i in range(3):
        label = gtk.Label()
        label.set_markup("<b>Respuesta " + str(numAns + 1) + "</b>")
        label.set_alignment(0.5, 0.0)
        labelMark = gtk.Label("Calificación".decode("latin-").encode("utf-8"))
        entryMark = gtk.Entry(0)

        scrolled_window = gtk.ScrolledWindow()
        textView = gtk.TextView()
        textView.set_wrap_mode(gtk.WRAP_WORD)
        scrolled_window.add(textView)
        scrolled_window.set_shadow_type(gtk.SHADOW_IN)
        scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        try:
            labelAns = gtk.Label("Respuesta")
            entryAns = gtk.Entry(0)
            entryAns.set_property('editable', False)
            txt = ord(chr(ini))
            ini = ini + 1
            entryAns.set_text(chr(txt))
            table = gtk.Table(rows=2, columns=2, homogeneous=False)
            table.attach(labelAns, 0, 1, 0, 1, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            table.attach(entryAns, 1, 2, 0, 1, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            table.attach(labelMark, 0, 1, 1, 2, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            table.attach(entryMark, 1, 2, 1, 2, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            ans[entryAns] = [entryMark, textView]
        except:
            table = gtk.Table(rows=2, columns=1, homogeneous=False)
            table.attach(labelMark, 0, 1, 0, 1, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            table.attach(entryMark, 1, 2, 0, 1, gtk.EXPAND|gtk.FILL,
                         gtk.EXPAND|gtk.FILL, 0, 0)
            ans[textView] = [entryMark]

#       if has_id:
#           labelAns = gtk.Label("Respuesta")
#           entryAns = gtk.Entry(0)
#           table = gtk.Table(rows=2 ,columns=2 ,homogeneous=False)
#           table.attach(labelAns, 0, 1, 0, 1, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#           table.attach(entryAns, 1, 2, 0, 1, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#           table.attach(labelMark, 0, 1, 1, 2, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#           table.attach(entryMark, 1, 2, 1, 2, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#           ans[entryAns] = [entryMark, textView]
#       else:
#           table = gtk.Table(rows=2 ,columns=1 ,homogeneous=False)
#             table.attach(labelMark, 0, 1, 0, 1, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#             table.attach(entryMark, 1, 2, 0, 1, gtk.EXPAND|gtk.FILL,
#                           gtk.EXPAND|gtk.FILL, 0, 0)
#             ans[textView] = [entryMark]

        box = gtk.VBox(False, 0)
        box.add(table)
        box.add(scrolled_window)

        tableAns.attach(label, 0, 1, numAns, numAns + 1, gtk.FILL, gtk.FILL,
                        0, 0)
        tableAns.attach(box, 1, 2, numAns, numAns + 1, gtk.EXPAND|gtk.FILL,
                        gtk.EXPAND|gtk.FILL, 0, 0)
        numAns += 1

    tableAns.show_all()

# def create_vocabulary_view(self, vocabulary, terms):
#     scrolledwindow = gtk.ScrolledWindow()

#     liststore = gtk.ListStore(str)
#     treeview = gtk.TreeView(self.liststore)
#     cell = gtk.CellRendererText()
#     tvcolumn = gtk.TreeViewColumn(vocabulary, self.cell, text=0)
#     treeview.append_column(self.tvcolumn)
#     treeview.set_search_column(0)
#     tvcolumn.set_sort_column_id(0)

#     scrolledwindow.add(self.treeview)
#     self.widgets.get_widget("vboxCategories").add(scrolledwindow)
#     scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
