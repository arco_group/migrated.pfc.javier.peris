# -*- coding: iso-8859-15; tab-width:4 -*-
"""Contains QuestionRef class"""

import logging


class QuestionRef:
    """A question in an exam is represented by this class"""

    def __init__(self, question=None, weight=1):
        """QuestionRef constructor"""
        self.question = question
        self.weight = weight

    def __str__(self):
        """Returns the representation of the question"""
#        print self.question
        return "Peso: " + self.weight

    def __eq__(self, qref):
        """Returns whether two questionrefs are equals"""
#         if self.question == qref.question.id and self.weight == qref.weight:
#             print "Questionref True"
#             return True
        if self.question == qref.question.id:
            logging.debug("QuestionRefs id are equal")
            if self.weight == float(qref.weight):
                logging.debug("QuestionRefs weight are equal")
                return True
        logging.debug("QuestionRefs are NOT equal")
        logging.debug("Question : %s" % self.question)
        logging.debug("Question : %s" % qref.question.id)
        logging.debug("QuestionRef 1 weight: %s" % self.weight)
        logging.debug("QuestionRef 2 weight: %s" % qref.weight)
        return False

    def __ne__(self, qref):
        """Returns whether two questionrefs are not equals"""
        result = self.__eq__(qref)
        return not result
