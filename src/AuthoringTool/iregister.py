"""Register interface"""

import zope.interface

class IRegister:
    """Register interface"""

    @classmethod
    def create(cls, cls_class):
        """Compulsory method for a Register class"""
        raise NotImplementedError
