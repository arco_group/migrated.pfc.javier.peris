#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
sys.path.append('../Questions')

import persistencia


class QuestionManager:

    def __init__(self):
        self.agente = persistencia.Agente()
        self.questions = []
        self.vocabularies = []

    def get_all_vocabularies(self):
        return self.agente.get_all_vocabularies()
