# -*- codingf: iso-8859-15; tab-width:4 -*-

"""Module to create exam objects"""

import logging


class Exam:
    """Represents the exam model logic"""

    def __init__(self, title, testpart, o_date, m_time=None, c_date=None,
                 subject=None, description=""):
        self.title = title
        self.testparts = testpart
        self.max_time = m_time
        self.open_date = o_date
        self.close_date = c_date
        self.subject = subject
        self.description = description

    def __str__(self):
        rep = "Examen: " + self.title
        rep += " Se abre: " +  self.open_date.__str__()
        rep += " Se cierra: " + self.close_date.__str__()
#         for tp in self.testparts:
#             rep += tp.__str__()
        return rep

    def __eq__(self, exam):
        import datetime
        equals = True

        if self.title == exam.title and self.testparts == exam.testparts \
                and self.subject == exam.subject:
            one_sec = datetime.timedelta(seconds=1)
            if self.open_date:
                if not self.open_date - exam.open_date < one_sec:
                    msg  = "Open date exam: " + self.open_date.__str__() + \
                        "\n Open date exam 2: " + exam.open_date.__str__()
                    logging.debug(msg)
                    equals = False
            else:
                if not self.open_date == exam.open_date:
                    msg  = "Open date exam: " + self.open_date.__str__() + \
                        "\n Open date exam 2: " + exam.open_date.__str__()
                    logging.debug(msg)
                    equals = False
            if self.close_date:
                if not self.close_date - exam.close_date < one_sec:
                    msg  = "Open date exam: " + self.close_date.__str__() + \
                        "\n Open date exam 2: " + exam.close_date.__str__()
                    logging.debug(msg)
                    equals = False
            else:
                if not self.close_date == exam.close_date:
                    msg  = "Open date exam: " + self.close_date.__str__() + \
                        "\n Open date exam 2: " + exam.close_date.__str__()
                    logging.debug(msg)
                    equals = False
        else:
            msg = "Title: " + self.title + " Title 2: " + exam.title
            logging.debug(msg)
            msg = "Title: " + self.testparts + " Title 2: " + exam.testparts
            logging.debug(msg)
            msg = "Title: " + self.subject + " Title 2: " + exam.subject
            logging.debug(msg)
            equals = False
        return equals

    def __ne__(self, exam):
        """Returns whether two exams are different"""
        result = self.__eq__(exam)
        return not result

    def add_part(self, part):
        """Adds a part to the exam"""
        self.testparts.append(part)

    def set_max_time(self, time):
        """Sets the max time to do the exam"""
        self.max_time = time

    def set_opendate(self, date):
        """Sets the date to do available the exam"""
        self.open_date = date

    def set_closedate(self, date):
        """Sets the limit date in which the exam is available"""
        self.close_date = date

#     def get_numparts(self):
#         return len(self.testparts)
