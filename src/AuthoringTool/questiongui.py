# -*- coding: iso-8859-15; tab-width:4 -*-
"""Controls the running of the BasicGUI of a question (the
part in common of the dialog)"""
import sys
sys.path.append('../../../Editor')
sys.path.append('../../../Persistencia')

try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import term

class BasicGUI():
    """Provides the functions to work with the common part of
    all type of questions"""

    l_attach = 0
    r_attach = 2
    be_attach = 3
    te_attach = 2
    bg_attach = 5
    tg_attach = 4
    xoptions = gtk.FILL|gtk.EXPAND
    yoptions = gtk.FILL|gtk.EXPAND
    xpadding = 0
    ypadding = 0

    def __init__(self):
        glade_file = "../Templates/template.glade"
        self.widgets = gtk.glade.XML(glade_file)
        self.widgets.signal_autoconnect(self)
        self.wtable = self.widgets.get_widget("table")

    def get_title(self):
        """Return the title of the question"""
        return self.widgets.get_widget("entryTitleQuestion").get_text()

    def get_default_mark(self):
        """Return the default mark of the question"""
        return self.widgets.get_widget("entryMarkDefault").get_text()

    def set_title(self, title):
        """Set a title to the info dialog of the question"""
        text = "<b>" + title.decode("latin-").encode("utf-8") + "</b>"
        self.widgets.get_widget("labelTitle").set_markup(text)
        self.widgets.get_widget("labelTitle").show()

    def set_text(self, p_text, s_text):
        """Set the info of the specific question"""
        p_text = "<b>" + p_text + "</b>"
        txt = p_text + "\n \n" + s_text
        text = txt.decode("latin-").encode("utf-8")
        self.widgets.get_widget("labelInfo").set_markup(text)
        self.widgets.get_widget("labelInfo").show()

    def buttonInfo_clicked(self, widget):
        """Run the info dialog of the question"""
        self.widgets.get_widget("dialogInfo").run()
        self.widgets.get_widget("dialogInfo").hide()

    def attach_gui(self, child):
        """Attach the specific interaction part of the question"""
        self.wtable.attach(child, self.l_attach, self.r_attach,
                           self.tg_attach, self.bg_attach, self.xoptions,
                           self.yoptions, self.xpadding, self.ypadding)

    def attach_editor(self, child):
        """Attach the editor"""
        self.wtable.attach(child, self.l_attach, self.r_attach,
                        self.te_attach, self.be_attach, self.xoptions,
                        self.yoptions, self.xpadding, self.ypadding)

    def create_more_ans_widget(self, obj):
        """Add the button to allow more answers"""
        label = "3 respuestas m�s".decode('latin-').encode('utf-8')
        button = gtk.Button(label)
        self.wtable.attach(button, 0, 2, 5, 6, gtk.FILL, gtk.FILL)#,0, 0)
        button.connect("clicked", obj.buttonMoreAns_clicked)
        button.show()

    def fill_vocabularies(self, vocabularies, dict):
        """Create the vocabularies which exist at present"""
        self.vbCategories = self.widgets.get_widget("vboxCategories")
#         self.vocabularies = {}
        for k, val in vocabularies.items():
            self.create_vocabulary_view(k, val, dict, self.vbCategories)

    def create_vocabulary_view(self, vocabulary, terms, vocabularies, vbox):
        """Create the part of the gui to allow the terms selection within
        a vocabulary"""
        scrolledwindow = gtk.ScrolledWindow()
        liststore = gtk.ListStore(str, str, str)
        treeview = gtk.TreeView(liststore)
        vocabularies[vocabulary] = treeview
        treeselection = treeview.get_selection()
        treeselection.set_mode(gtk.SELECTION_MULTIPLE)
        cell = gtk.CellRendererText()
        tvcolumn = gtk.TreeViewColumn(vocabulary, cell, text=0)
        treeview.append_column(tvcolumn)
        treeview.set_search_column(0)
        treeview.connect("button-press-event", self.tv_vocabulary_clicked)
        tvcolumn.set_sort_column_id(0)
        for ter in terms:
            liststore.append([ter.name, ter.description, vocabulary])
        scrolledwindow.add(treeview)
        vbox.add(scrolledwindow)
        scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrolledwindow.show_all()

    def tv_vocabulary_clicked(self, widget, event):
        """Control when terms are selected and unselected"""
        if event.button == 1:
            info_at_pos = widget.get_path_at_pos(int(event.x), int(event.y))
#           logging.info("INFO AT POS:" + str(info_at_pos))
            if info_at_pos == None:
                widget.get_selection().unselect_all()

    def run_dialog(self):
        """Run the dialog"""
#        geo = self.widgets.get_widget("dialogQuestion").get_geometry()
#        938x580
        self.widgets.get_widget("dialogQuestion").parse_geometry("628x825+149+30")
        response = self.widgets.get_widget("dialogQuestion").run()
        print "Ejecutado Run template"
        return  response

    def hide_dialog(self):
        """Hide the dialog"""
        self.widgets.get_widget("dialogQuestion").hide()

    def destroy_dialog(self):
        """Destroy the dialog"""
        self.widgets.get_widget("dialogQuestion").destroy()

    def get_selected_terms(self):
        """Return the terms selected"""
        terms = []
        swindows = [child for child in self.vbCategories.get_children()]
        for child in swindows:
            treeselection = child.get_children()[0].get_selection()
            (model, pathlist) = treeselection.get_selected_rows()
            for path in pathlist:
                terms.append(term.Term(model[path][0], model[path][1],
                                       model[path][2]))
        return terms

    def set_question_title(self, title):
        """Set a title to the title question entry"""
        self.widgets.get_widget("entryTitleQuestion").set_text(title)

    def set_default_mark(self, mark):
        """Set the default_mark"""
        self.widgets.get_widget("entryMarkDefault").set_text(str(mark))

