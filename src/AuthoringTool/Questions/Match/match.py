# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides the Match class """

import sys
sys.path.append('../../AuthoringTool/')
import logging
import question


class Match(question.Question):
    """Represents the match question"""

    description = 'Plugin Match'
    name = 'Match'
    icon = '../Icons/match.png'

    def __init__(self, title, default_mark, wording):
        question.Question.__init__(self, title, default_mark, wording)

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Returns the best mark a candidate can get in this question"""
        return self.maximumScore

def load():
    """Loads the plugin"""
    import question
#     import controller
#     import matchcontroller
#     import doctexrender
#     import texrendermatch
#     import gtkrender
#     import gtkrendermatch
#     question.Question.register(Match)
#     cmcontroller = matchcontroller.MatchController
#     controller.Controller.register(cmcontroller)
#     texrenderm = texrendermatch.TeXRenderMatch
#     doctexrender.DocTeXRender.register(texrenderm)
#     gtkrenderm = gtkrendermatch.GtkRenderMatch
#     gtkrender.GtkRender.register(gtkrenderm)
    load_agent()
    load_html_render()

def load_agent():
    """Loads the agent"""
    import sys
    sys.path.append('../../../Persistencia/')
#     import persistencia
#     import matchagent
#     persistencia.Agente.register(matchagent.MatchAgent)


def load_html_render():
    """Loads the html render"""
    pass
#     import htmlrendermatch
#     import dochtmlrender
#     htmlrenderm = htmlrendermatch.HTMLRenderMatch
#     dochtmlrender.DocHTMLRender.register(htmlrenderm)

