# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provide a class to represent in tex choice multiple question"""

import sys
sys.path.append('../../../AuthoringTool/Renders')

import doctexrender


class TeXRenderOrder(doctexrender.DocTeXRender):
    """Create a TeX representation of a choice multiple question"""

    name = "Ord"

    def __init__(self, solution):
        self.solutions = solution

    def render(self, question, texfile):
        """Create the tex representation"""
        wording = "\question (" + str(question.best_mark()) +"p)\n"
        wording += self.render_wording(question)
        onepar = self.one_line(question)
        if onepar:
            env = [" \n \n \\begin{oneparchoices} \n",
                   "\end{oneparchoices} \n"]
        else:
            env = [" \n \n \\begin{enumerate} \n ",
                   "\end{enumerate} \n"]
        content = str(wording) + env[0]
        vals = question.answers.values()
        pos = []
        for val in vals:
            if val[0] > 0:
                pos.append(vals.index(val))
        i = 0
        for k, val in question.answers.items():
            if  self.solutions:
                if onepar:
                    content += "\choice " + str(val[1]) + \
                        " \\thinspace \underline{\\textbf{" + \
						str(val[0]) + "}} \n"
                else:
                    content += "\item " + str(val[1]) + \
                        " \\thinspace \underline{\\textbf{" + \
						str(val[0]) + "}} \n"
            else:
                if onepar:
                    content += "\choice " + str(val[1]) + \
                        " \\thinspace \\rule{12pt}{0.5pt} \n"
                else:
                    content += "\item " + str(val[1]) + \
                        " \\thinspace \\rule{12pt}{0.5pt} \n"
            i = i + 1
        content += env[1]
        texfile.write(content.decode("utf-8").encode("latin-"))

    def one_line(self, question):
        """Check whether the answers fit in one line"""
        one_line = True
        length = 0
        for k, val in question.answers.items():
            length += len(val[1])
        if length > 85:
            one_line = False
        return one_line
