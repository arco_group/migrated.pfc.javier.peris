# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provide a validator for choice multiple questions"""

import sys
sys.path.append('../../../AuthoringTool')

import validator
import exception


class OrderValidator(validator.Validator):
    """Validator for order question"""

    def validate(self, question):
        """Validate whether all the info is correct"""
        correct_t, t_msg = self.check_title(question.title)
        correct_w, w_msg = self.check_wording(question.wording)
        correct_d, dm_msg = self.check_default_mark(question.defaultMark)
        correct_a, a_msg = self._check_answers(question.answers)
        correct = correct_t and correct_w and correct_d and correct_a
        if not correct:
            error_msg = t_msg + w_msg + dm_msg + a_msg
            raise exception.DataException(error_msg)

    def _check_answers(self, answers):
        """Own method to validate the wording"""
        correct = True
        msg_error = ""
        num_answers = len(answers)
        num_order = []
        if num_answers > 0:
            for k, val in answers.items():
                try:
                    num_order.append(int(val[0]))
                except ValueError:
                    correct = False
                    msg_error += "Se debe insertar un numero para" + \
                                 "indicar el orden \n"
            num_correct_order = []
            for i in range(1, num_answers+1):
                num_correct_order.append(i)
            num_order.sort()
            if num_correct_order != num_order:
                correct = False
                msg_error += "Los numeros de orden deben comenzar" + \
                              " en uno ser consecutivos y no se" + \
                              " pueden repetir\n"

        else:
            correct = False
            msg_error += "Se debe rellenar al menos una respuesta\n"
        return correct, msg_error
