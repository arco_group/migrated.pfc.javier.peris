# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to create an order controller"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../../Persistencia')
sys.path.append('../../../AuthoringTool')

try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import ordervalidator
import extend_gui
import alert
import exception
import editor
import order
import controller
import orderagent
import questiongui
import odict


class OrderController(controller.Controller):
    """A controller for an order dialog"""


    name = "Ordenar"
    p_text = "Ordenaci�n"
    s_text = "Este tipo de pregunta presenta un conjunto de " + \
        "respuestas al alumno. �ste deber� seleccionar una o m�s " + \
        "opciones. Para crear este tipo de preguntas deber�:\n \n " + \
        "Seleccionar uno o m�s t�rminos a los que pertenece. \n" + \
        "Un nombre para poder identificarla posteriormente en el " + \
        "banco de preguntas. \n" + \
        "Un identificador, una calificaci�n y el texto para cada " + \
        "respuesta que se quiere crear. \n" + \
        "Si se necesitan m�s de tres respuestas, puede ampliarlo " + \
        "mediante el bot�n pertinente al final del formulario. "

    def __init__(self, vocabularies):
        self.template = questiongui.BasicGUI()
        self.editor = editor.Editor('../Editor/editor.glade')
        self.numAns = 3
        self.id = ord('D')

        vbox = self.editor.get_main_vbox()
        vbox.unparent()
        self.template.attach_editor(vbox)

        file = "./Questions/Order/order.glade"
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
        table = self.widgets.get_widget("tableAns")
        table.unparent()
        self.template.attach_gui(table)

        self.dict_vocabularies = {}
        self.template.fill_vocabularies(vocabularies, self.dict_vocabularies)

        self.ans = odict.OrderedDict()
        for i in range(3):
            self.ans[self.widgets.get_widget("entryChoice" + str(i + 1))] = \
                [self.widgets.get_widget("entryOrder" + str(i + 1)),
                 self.widgets.get_widget("textview" + str(i + 1))]

        self.template.set_title("Ordenaci�n")
        self.template.set_text(OrderController.p_text, OrderController.s_text)
        self.template.create_more_ans_widget(self)

    def run(self, question=None):
        """Run the dialog"""
        end = False
        while(not end):
            try:
                response = self.template.run_dialog()
                end = True
                if response == 0:
                    self.template.hide_dialog()
                elif response == 1:
                    self.save_question(question)
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.template.destroy_dialog()
            except exception.DataException:
                end = False

    #simpleChoice[id]=[Order, TextAnswer]
    def save_question(self, ques):
        """If the questions is correct, it's saved"""
        title, wording, simpleChoice, defaultMark, shuffle = self.take_data()
        question = order.Order(title, defaultMark, wording,
                               simpleChoice, shuffle)
        try:
            validator = ordervalidator.OrderValidator()
            validator.validate(question)
            for key, val in question.answers.items():
                val[0] = int(val[0])
            question.defaultMark = float(question.defaultMark)
            terms = self.template.get_selected_terms()
            question.set_terms(terms)
            agent = orderagent.OrderAgent()
            if ques:
                question.set_id(ques.id)
                question.filename = ques.filename
                agent.update(question)
            else:
                agent.save(question)
            self.template.hide_dialog()
        except exception.DataException, e:
            edata = alert.ErrorData()
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n" + e.msg
            edata.label_info_set_text(all_msg)
            raise exception.DataException(e.msg)

    def take_data(self):
        """Obtain the info from the dialog"""
        title = self.template.get_title()
        shuffle = self.widgets.get_widget("chb_shuffle").get_active()
        simpleChoice = odict.OrderedDict()
        for k, val  in self.ans.items():
            buff = val[1].get_buffer()
            start_iter = buff.get_start_iter()
            end_iter = buff.get_end_iter()
            if k.get_text() != "" and val[0].get_text() !="" and \
                    buff.get_text(start_iter, end_iter) != "":
                simpleChoice[k.get_text()] = [val[0].get_text(),
                                              buff.get_text(start_iter,
                                                            end_iter, False)]
        defaultMark = self.template.get_default_mark()
        wording = self.editor.contents.values()
        return title, wording, simpleChoice, defaultMark, shuffle

    def buttonMoreAns_clicked(self, widget, data=None):
        """Extend the gui with 3 more possible answers"""
        extend_gui.add_more_answers(self.widgets.get_widget("tableAns"),
                                    self.ans, self.numAns, self.id)
        self.numAns = self.numAns + 3
        self.id = self.id + 3

