# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to administer the order question persistence"""

import sys
sys.path.append('../../../Persistencia')
sys.path.append('../../../AuthoringTool')
sys.path.append('../../../Editor')
import time
import amara
import logging

import orderpersistence
import order
import db
import editor
import table
import image
import odict

class OrderAgent:
    """Agent to provide the CRUD for order question"""

    name = "Ord"
    DIR_NAME = "../QuestionsBank/"

    def save(self, question):
        """Create/save an order question in the database"""
        gmtime = time.gmtime()
        fileid = question.title
        for i in range(6):
            fileid += str(gmtime[i])
        filename = OrderAgent.DIR_NAME + fileid + ".xml"
        doc = self.create_xml(question)
        try:
            fd = open(filename, 'w')
        except IOError:
            logging.exception("Error creating the xml for an order question")
        else:
            fd.write(doc.xml())
            fd.close()

        q = orderpersistence.Ord(filename=filename,
                                 title=question.title,
                                 default_mark=question.defaultMark,
                                 shuffle=question.shuffle)
        for k, val  in question.answers.items():
            orderpersistence.OrderChoice(value=val[1], ord=val[0], ide=k,
                                         question=q)

        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

    def create(self, ques, wording):
        """Read an order question"""
        choices = odict.OrderedDict()
        for answer in ques.orderanswers:
            choices[answer.ide] = [answer.ord, answer.value]
        question = order.Order(ques.title, ques.default_mark,
                               wording, choices, ques.shuffle)
        return question

    # REVISAR ESTE METODO
    def update(self, question):
        """Update the order question"""
        q = db.Ord.select(db.Ord.q.id==question.id)[0]
        q.title = question.title
        q.shuffle = question.shuffle
        answer = db.Answer.select(db.Answer.q.question==q.id)[0]
        answer.question = None
        for term in q.term_questions:
            q.removeTerm(term)
        for item in q.wording:
            q.removeContent(item)
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])
        doc = self.create_xml(question)

        try:
            fd = open(question.id, 'w')
        except IOError:
            logging.exception("Error creating the xml for an order question")
        else:
            fd.write(doc.xml())
            fd.close()

    def create_xml(self, question):
        """Create the XML in agreement with IMS QTI v2.0"""
        doc = amara.parse("../Templates/order.xml")
        aItem = doc.assessmentItem
        aItem.title = unicode(question.title, 'utf-8')
        shuffle = question.shuffle.__str__()
        aItem.itemBody.orderInteraction.shuffle = unicode(shuffle,
                                                          'utf-8')
        wording = ""
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                wording += part.get_text()
            elif isinstance(part, table.Table):
                filename = part.path
                wording += "<img src=" + part.path + ">"
            elif isinstance(part, image.Image):
                content = part.path
                wording += "<img src=" + part.path + ">"
            else:
                content = part.path
                wording += "<img src=" + part.path + ">"
        aItem.itemBody.orderInteraction.xml_append(
            doc.xml_create_element(u'prompt',
                                   content=unicode(wording, 'utf-8')))

        list = question.answers.values()
        for i in range(1, len(question.answers) + 1):
            for e in list:
                if e[0] == i:
                    aItem.responseDeclaration.correctResponse.xml_append(
                        doc.xml_create_element(u'value',
                                               content=unicode(e[1], 'utf-8')))
        for k, val  in question.answers.items():
            aItem.itemBody.orderInteraction.xml_append(
                doc.xml_create_element(u'simpleChoice',
                                       attributes={u'identifier': unicode(k, 'utf-8')},
                                       content = unicode(val[1], 'utf-8')))

        return doc
