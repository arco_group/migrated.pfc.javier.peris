# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
sys.path.append('../../../AuthoringTool/')
sys.path.append('../../Renders/')
sys.path.append('../../../Persistencia/')

import order
import question
import orderagent
import persistencia
import ordercontroller
import controller
import Renders.doctexrender
import texrenderorder

__all__= ["ordercontroller", "ordervalidator", "saveorder", "order",
 "texrenderorder"]

question.Question.register(order.Order)
controller.Controller.register(ordercontroller.OrderController)
Renders.doctexrender.DocTeXRender.register(texrenderorder)
persistencia.Agente.register(orderagent.OrderAgent)
