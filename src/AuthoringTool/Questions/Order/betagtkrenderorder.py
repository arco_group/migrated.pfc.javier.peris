# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a gtk representation for order question"""

import sys
sys.path.append('../Editor')
sys.path.append('../Questions')
import logging

import editor
import table
import image
#import formula
import extend_gui
import gtkrender
import ordercontroller


class GtkRenderOrder(gtkrender.GtkRender):
    """Renders the order question as a Gtk dialog"""

    name = "Order"

    def __init__(self):
        gtkrender.GtkRender.__init__(self)
        vocabularies = self.agente.get_all_vocabularies()
        dict_vocabs = {}
        for vocab in vocabularies:
            terms = self.agente.get_terms_vocabulary(vocab.name)
            dict_vocabs[vocab.name] = terms
        self.oc = ordercontroller.OrderController(dict_vocabs)
        self.id = ord('D')

    def render(self, question):
        """Fills the dialog with the info of the question"""
        title = question.title
        d_mark = question.defaultMark
        wording = question.wording
        first_text = True
        self.oc.template.set_question_title(title)
        self.oc.template.set_default_mark(str(d_mark))

        for content in wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                txt = content.buf.get_text(content.buf.get_start_iter(),
                                            content.buf.get_end_iter(),
                                            True)
                if first_text:
                    ipb = self.oc.editor.contents.values()[0]
                    ipb.buf.set_text(txt)
#                   self.oc.editor.get_textview().set_buffer(content.buf)
                    first_text = False
                else:
                    swindow, textview = self.oc.editor.create_textpart()
                    buf = content.buf
                    textview.set_buffer(buf)
                    ipb = editor.InteractivePangoBuffer("", buf)
                    self.oc.editor.contents[swindow] = ipb
            elif isinstance(content, table.Table):
                button_table = self.oc.editor.create_imagepart()
                self.oc.editor.contents[button_table] = content
            elif isinstance(content, image.Image):
                button_image = self.oc.editor.create_imagepart(content)
                self.oc.editor.contents[button_image] = content
            else:
                logging.debug("I don't have powers of super cow")

        num_extend = (len(question.answers.items()) - 1) / 3
        for val in range(0, num_extend):
            extend_gui.add_more_answers(self.oc.widgets.get_widget("tableAns"),
                                        self.oc.ans, self.oc.numAns,
                                        self.id)
            self.oc.numAns += 3
            self.id += 3
        i = 0
        keys = self.oc.ans.keys()
        for k, val in question.answers.items():
            keys[i].set_text(k)
            self.oc.ans[keys[i]][0].set_text(str(val[0]))
            self.oc.ans[keys[i]][1].get_buffer().set_text(str(val[1]))
            i += 1

        self.activate_terms(question, self.oc.dict_vocabularies)

        self.oc.run(question)
