# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provide the InlineChoice class """

import sys
sys.path.append('../../../AuthoringTool/')

import question


class Order(question.Question):
    """Represent the inlinechoice question"""

    name = "Ord"
    description = 'Plugin Order'
    icon = ''

    def __init__(self, title, defaultMark, wording, simpleChoice, shuffle):
        question.Question.__init__(self, title, defaultMark, wording)
        self.answers = simpleChoice
        self.shuffle = shuffle

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Return the best mark a candidate can get in this question"""
        return self.defaultMark

def load():
    """Load the plugin"""
    import question
    import controller
    import doctexrender
#     import persistencia
    import ordercontroller
    import texrenderorder
#     import orderagent
    question.Question.register(Order)
    controller.Controller.register(ordercontroller.OrderController)
    doctexrender.DocTeXRender.register(texrenderorder.TeXRenderOrder)
#     persistencia.Agente.register(orderagent.OrderAgent)
    load_agent()
    load_html_render()

def load_agent():
    """Load the agent"""
    import persistencia
    import orderagent
    persistencia.Agente.register(orderagent.OrderAgent)

def load_html_render():
    """Load the html render"""
    pass
#     import htmlrenderorder
#     import dochtmlrender
#    htmlrendercm = htmlrenderextendedtext.HTMLRenderOrder
#     dochtmlrender.DocHTMLRender.register(htmlrendercm)
