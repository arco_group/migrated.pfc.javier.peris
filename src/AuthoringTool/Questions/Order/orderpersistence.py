#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys
sys.path.append("../../../Persistencia/")
from sqlobject import *
from sqlobject.inheritance import InheritableSQLObject

import db

_mURI = "mysql://sdocenteuser:45sNjW24@draper.mine.nu:3306/sdocente"
sqlhub.processConnection = connectionForURI(_mURI)

class Ord(db.Question):
    _inheritable = False
    shuffle = BoolCol()
    orderanswers = MultipleJoin('OrderChoice', joinColumn='question_id')

class OrderChoice(SQLObject):
    value = StringCol(default=None)
    ord = IntCol(default=None)
    ide = StringCol(default=None)
    question = ForeignKey('Ord')

Ord.createTable(ifNotExists=True)
OrderChoice.createTable(ifNotExists=True)
