# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides the InlineChoice class """

import sys
sys.path.append('../../../AuthoringTool/')

import question


class InlineChoice(question.Question):
    """Represents the inlinechoice question"""

    description = 'Plugin InlineChoice'
    name = 'InlineChoice'
    icon = '../Icons/inLineChoice.png'

    def __init__(self, title, defaultMark, wording, simpleChoice):
        #simpleChoice[id]=[Mark, entryText]
        question.Question.__init__(self, title, defaultMark, wording)
        self.answers = simpleChoice

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Returns the best mark a candidate can get in this question"""
        max_mark = self.answers.items()[0][1][0]
        for key, val in self.answers.items():
            if val[0] > max_mark:
                max_mark = val[0]
        return max_mark

def load():
    """Loads the plugin"""
    import question
    import controller
#     import inlinechoiceagent
#     import persistencia
    import inlinechoicecontroller
#     import dochtmlrender
#     import htmlrenderinlinechoice
    import doctexrender
    import texrenderinlinechoice
    import gtkrender
    import gtkrenderinlinechoice
    question.Question.register(InlineChoice)
    iccontroller = inlinechoicecontroller.InlineChoiceController
    controller.Controller.register(iccontroller)
#   htmlrenderic = htmlrenderinlinechoice.HTMLRenderInlineChoice
#     dochtmlrender.DocHTMLRender.register(htmlrenderic)
    texrenderic = texrenderinlinechoice.TeXRenderInlineChoice
    doctexrender.DocTeXRender.register(texrenderic)
    gtkrender.GtkRender.register(gtkrenderinlinechoice.GtkRenderInlineChoice)
#     persistencia.Agente.register(inlinechoiceagent.InlineChoiceAgent)
    load_agent()
    load_html_render()

def load_agent():
    """Loads the agent"""
    import persistencia
    import inlinechoiceagent
    persistencia.Agente.register(inlinechoiceagent.InlineChoiceAgent)

def load_html_render():
    """Loads the html render"""
    import htmlrenderinlinechoice
    import dochtmlrender
    htmlrendercm = htmlrenderinlinechoice.HTMLRenderInlineChoice
    dochtmlrender.DocHTMLRender.register(htmlrendercm)
