# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a gtk representation for inlinechoice question"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../Questions')
import logging

import editor
import table
import image
#import formula
import extend_gui
import gtkrender
import inlinechoicecontroller


class GtkRenderInlineChoice(gtkrender.GtkRender):
    """Renders the inlinechoice question as a Gtk dialog"""

    name = "InlineChoice"

    def __init__(self):
        gtkrender.GtkRender.__init__(self)
        vocabularies = self.agente.get_all_vocabularies()
        dict_vocabs = {}
        for vocab in vocabularies:
            terms = self.agente.get_terms_vocabulary(vocab.name)
            dict_vocabs[vocab.name] = terms
        self.icc = inlinechoicecontroller.InlineChoiceController(dict_vocabs)
        self.id = ord('D')

    def render(self, question):
        """Fills the dialog with the info of the question"""
        title = question.title
        d_mark = question.defaultMark
        wording = question.wording
        first_text = True
        self.icc.template.set_question_title(title)
        self.icc.template.set_default_mark(str(d_mark))

        for content in wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                txt = content.buf.get_text(content.buf.get_start_iter(),
                                            content.buf.get_end_iter(),
                                            True)
                if first_text:
                    ipb = self.icc.editor.contents.values()[0]
                    ipb.buf.set_text(txt)
#                   self.icc.editor.get_textview().set_buffer(content.buf)
                    first_text = False
                else:
                    swindow, textview = self.icc.editor.create_textpart()
                    buf = content.buf
                    textview.set_buffer(buf)
                    ipb = editor.InteractivePangoBuffer("", buf)
                    self.icc.editor.contents[swindow] = ipb
            elif isinstance(content, table.Table):
                button_table = self.icc.editor.create_imagepart()
                self.icc.editor.contents[button_table] = content
            elif isinstance(content, image.Image):
                prefix = "../QuestionsBank/images/"
                div = content.path.split(".")
                content.path = prefix + div[0].split("/")[-1] + "." + div[1]
                button_image = self.icc.editor.create_imagepart(content)
                self.icc.editor.contents[button_image] = content
            else:
                logging.debug("I don't have powers of super cow")

        num_extend = (len(question.answers.items()) - 1) / 3
        for val in range(0, num_extend):
            extend_gui.add_more_answers(self.icc.widgets.get_widget("tableAns"),
                                        self.icc.ans, self.icc.numAns,
                                        self.id)
            self.icc.numAns += 3
            self.id += 3
        i = 0
        keys = self.icc.ans.keys()
        for k, val in question.answers.items():
            keys[i].set_text(k)
            self.icc.ans[keys[i]][0].set_text(str(val[0]))
            self.icc.ans[keys[i]][1].get_buffer().set_text(str(val[1]))
            i += 1

        self.activate_terms(question, self.icc.dict_vocabularies)

        self.icc.run(question)
