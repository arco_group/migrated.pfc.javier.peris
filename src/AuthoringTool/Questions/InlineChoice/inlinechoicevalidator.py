# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a validator for choice multiple questions"""

import sys
sys.path.append('../../../AuthoringTool')
sys.path.append('../../../Editor')
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
import logging

import validator
import exception
import editor


class InlineChoiceValidator(validator.Validator):
    """Validator for inlinechoice question"""

    def validate(self, question):
        """Validates whether all the info is correct"""
        correct_t, t_msg = self.check_title(question.title)
        correct_w, w_msg = self._check_wording(question.wording)
        correct_d, dm_msg = self.check_default_mark(question.defaultMark)
        correct_a, a_msg = self._check_answers(question.answers)
        correct = correct_t and correct_w and correct_d and correct_a
        if not correct:
            error_msg = t_msg + w_msg + dm_msg + a_msg
            raise exception.DataException(error_msg)


    def _check_wording(self, wording):
        """Own method to validate the wording"""
        correcto = False
        msg_error = ""
        if len(wording) == 0:
            msg_error += "Se debe rellenar la pregunta\n"
            correcto = False
        else:
            logging.debug("Content type: %s" % str(type(wording)))
            for part in wording:
                if isinstance(part, editor.InteractivePangoBuffer):
                    logging.debug("Content: %s" % part.get_text())
                    if part.get_text().count("[]") == 1:
                        correcto = True
                    else:
                        msg_error += "La pregunta debe contener " + \
                         "exactamente una vez los caracteres [], en lugar " + \
                         "donde ira el espacio en blanco mostrado al " + \
                         " alumno donde tendra que elegir entre las " + \
                         "respuestas dadas."
            if not correcto:
                msg_error += "El bloque de texto debe rellenarse\n"
        return correcto, msg_error

    def _check_answers(self, answers):
        """Own method to validate the possible answers"""
        correcto = True
        msg_error = ""
        if len(answers) > 0:
            for key, val in answers.items():
                try:
                    float(val[0])
                except ValueError:
                    msg_error += "Se debe insertar una puntuación para la" + \
                                    "respuesta\n"
        else:
            correcto = False
            msg_error += "Se deben rellenar al menos una respuesta\n"
        return correcto, msg_error

