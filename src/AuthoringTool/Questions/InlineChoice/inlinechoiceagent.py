# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to administer the inlinechoice question persistence"""

import time
import sys
sys.path.append('../AuthoringTool')
sys.path.append('../Editor')
import amara
import logging

import editor
import table
import image
import term
import inlinechoice
import db
import odict


class InlineChoiceAgent:
    """Agent to provide the CRUD for choice multiple question"""

    name = "InlineChoice"
    DIR_NAME = "../QuestionsBank/"

    def save(self, question):
        """Create/save an inlinechoice in the database"""
        gmtime = time.gmtime()
        fileid = question.title
        for i in range(6):
            fileid += str(gmtime[i])
        filename = InlineChoiceAgent.DIR_NAME + fileid + ".xml"
        doc = self.create_xml(question)
#       doc = amara.parse("../Templates/inline_choice.xml")
#       doc.assessmentItem.title = unicode(question.title, 'utf-8')
#     #         doc.assessmentItem.adaptive = u'true'
#     #         doc.assessmentItem.timeDependent = u'true'
#       for k, v  in question.answers.items():
#           if v[0] > 0:
#               doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
#                       doc.xml_create_element(u'value',
#                                              content=unicode(k, 'utf-8')))
#           doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
#               unicode(str(question.defaultMark), 'utf-8')
#           doc.assessmentItem.responseDeclaration.mapping.xml_append(
#                   doc.xml_create_element(u'mapEntry',
#                                          attributes={u'mapKey': unicode(k, 'utf-8'),
#                                                      u'mappedValue': unicode(str(v[0]), 'utf-8')}))
#       qs = question.wording[0].get_text().split("[]")
#       se = question.wording[0].get_text().find("")
#       for i in range(len(qs)):
#           if se:
#               doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#               if qs[i] == "":
#                   doc.assessmentItem.itemBody.xml_append(
#                       doc.xml_create_element(u'inlineChoiceInteraction',
#                                              attributes={u'responseIdentifier': u'RESPONSE',
#                                                          u'shuffle': u'false'}))
#                   ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
#                   for k, v in simpleChoice.items():
#                       ilci.xml_append(
#                           doc.xml_create_element(u'inlineChoice',
#                                                  attributes={u'identifier':  unicode(k, 'utf-8'),
#                                                              u'fixed': u'false'},
#                                                  content = unicode(v[1], 'utf-8')))
#           else:
#               if i == len(qs) - 1:
#                   doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#               else:
#                   doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#                   doc.assessmentItem.itemBody.xml_append(
#                       doc.xml_create_element(u'inlineChoiceInteraction',
#                                              attributes={u'responseIdentifier': u'RESPONSE',
#                                                          u'shuffle': u'false'}))
#                   ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
#                   for k, v in question.answers.items():
#                       ilci.xml_append(
#                               doc.xml_create_element(u'inlineChoice',
#                                           attributes={u'identifier':  unicode(k, 'utf-8'),
#                                                       u'fixed': u'false'},
#                                                      content = unicode(v[1], 'utf-8')))
        try:
            fd = open(filename, 'w')
        except IOError:
            logging.exception("")
        else:
            fd.write(doc.xml())
            fd.close()

        #REVISAR SI UNA PREGUNTA YA SE ENCUENTRA
        q = db.InlineChoice(filename=filename, title=question.title,
                            default_mark=question.defaultMark)
        for k, v  in question.answers.items():
            db.Answer(value=v[1], calification=v[0], ide=k, question=q)
        for part in question.wording:
            logging.debug("Adding content of type: %s" % str(type(part)))
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)

        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

    def create(self, q, wording):
        """Reads a choice multiple question"""
        choices = odict.OrderedDict()
        for answer in q.answers:
            choices[answer.ide] = [answer.calification, answer.value]
        ques = inlinechoice.InlineChoice(q.title, q.default_mark, wording,
                                     choices)
#       ques.set_id(q.filename)
        return ques

    def update(self, question):
        """Updates the choice multiple question"""
        q = db.InlineChoice.select(db.InlineChoice.q.id==question.id)[0]
        q.title = question.title
        answers = db.Answer.select(db.Answer.q.question==q.id)
        for ans in answers:
#            ans.question = None
            db.Answer.delete(ans.id)
        for k, v  in question.answers.items():
            db.Answer(value=v[1], calification=v[0], ide=k, question=q)
        for term in q.term_questions:
            q.removeTerm(term)
        for item in q.wording:
            q.removeContent(item)
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

        doc = self.create_xml(question)
        try:
            fd = open(question.filename, 'w')
        except IOError:
            logging.exception("")
        else:
            fd.write(doc.xml())
            fd.close()

    def create_xml(self, question):
        """Creates the XML in agreement with IMS QTI v2.0"""
        doc = amara.parse("../Templates/inline_choice.xml")
        doc.assessmentItem.title = unicode(question.title, 'utf-8')
    #       doc.assessmentItem.adaptive = u'true'
    #       doc.assessmentItem.timeDependent = u'true'
        for k, v  in question.answers.items():
            if v[0] > 0:
                doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
                        doc.xml_create_element(u'value',
                                               content=unicode(k, 'utf-8')))
            doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
                unicode(str(question.defaultMark), 'utf-8')
            doc.assessmentItem.responseDeclaration.mapping.xml_append(
                    doc.xml_create_element(u'mapEntry',
                                           attributes={u'mapKey': unicode(k, 'utf-8'),
                                                       u'mappedValue': unicode(str(v[0]), 'utf-8')}))
        qs = question.wording[0].get_text().split("[]")
        se = question.wording[0].get_text().find("")
        for i in range(len(qs)):
            if se:
                doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                if qs[i] == "":
                    doc.assessmentItem.itemBody.xml_append(
                        doc.xml_create_element(u'inlineChoiceInteraction',
                                               attributes={u'responseIdentifier': u'RESPONSE',
                                                           u'shuffle': u'false'}))
                    ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
                    for k, v in question.answers.items():
                        ilci.xml_append(
                            doc.xml_create_element(u'inlineChoice',
                                                   attributes={u'identifier':  unicode(k, 'utf-8'),
                                                               u'fixed': u'false'},
                                                   content = unicode(v[1], 'utf-8')))
            else:
                if i == len(qs) - 1:
                    doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                else:
                    doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                    doc.assessmentItem.itemBody.xml_append(
                        doc.xml_create_element(u'inlineChoiceInteraction',
                                               attributes={u'responseIdentifier': u'RESPONSE',
                                                           u'shuffle': u'false'}))
                    ilci = doc.assessmentItem.itemBody.inlineChoiceInteraction
                    for k, v in question.answers.items():
                        ilci.xml_append(
                                doc.xml_create_element(u'inlineChoice',
                                            attributes={u'identifier':  unicode(k, 'utf-8'),
                                                        u'fixed': u'false'},
                                                       content = unicode(v[1], 'utf-8')))
        return doc
