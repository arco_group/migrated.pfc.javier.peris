# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in html inlinechoice question"""

import sys
sys.path.append('../../../Renders')
sys.path.append('../../../Editor')

import logging
import editor
import table
import image
#import formula
import dochtmlrender


class HTMLRenderInlineChoice(dochtmlrender.DocHTMLRender):
    """Creates an HTML representation of a inlinechoice question"""

    name = 'InlineChoice'

    def __init__(self, solution, numquestion):
        self.solutions = solution
        self.numquestion = numquestion

    def render(self, question, htmlfile):
        """Creates the html representation"""
        wording = ""
        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                aux = self.parser(buff.get_text(buff.get_start_iter(),
                                                buff.get_end_iter(),
                                                True))
                wording += "<font class='questionText'>"
                if aux.count("[]") == 1:
                    aux1 = aux[0:aux.find("[]")]
                    aux2 = aux[aux.find("[]") + 2:]
                    logging.debug("Aux2 %s" % aux2)
                    wording += aux1.decode("utf-8").encode("latin-") + \
                        """</font>
                <select name='""" + str(question.id) + """'> \n"""
                    i = 0
                    vals = question.answers.values()
                    pos = vals.index(max(vals))
                    for k, val in question.answers.items():
                        if self.solutions and i == pos:
                            wording += '<option value="' + k + \
                                '" selected="selected">' + \
                                str(val[1]).decode("utf-8").encode("latin-") + \
                                """ </option> \n"""
                        else:
                            wording += '<option value="' + k +'">' + \
                                str(val[1]).decode("utf-8").encode("latin-") + \
                                """</option> \n"""
                        i = i + 1
                    wording += "</select> \n </p> \n"
                    if not aux2.isspace():
                        wording += """<font class='questionText'> \n""" + \
                            aux2.decode("utf-8").encode("latin-") + \
                            """</font>"""
#                 else:
#                     wording += aux.decode("utf-8").encode("latin-")
#                 wording += """</font>"""
            elif isinstance(content, table.Table):
                wording += "<img src='"+ content.path + "'>"
            elif isinstance(content, image.Image):
                wording += "<img src='"+ content.path + "'>"
            else:
                wording += "<img src='"+ content.path + "'>"

        content = """
        <tr>
            <td class="questionNumber" valign="top">
            <p>""" + str(self.numquestion) + """</p>
           </td>
           <td valign="top">
               <p>
               <font class="grade">(""" + str(question.best_mark()) + \
                   """p)</font>
       <input hidden="hidden" type="hidden"
              name="nombre" value='""" + str(question.id) +"""'/>
       <input hidden="hidden" type="hidden"
              name="type" value="inlineChoice"/>
       """ + wording + """
       <p></p>
           </td>
        </tr>
        """
        htmlfile.write(content.decode("latin-").encode("utf-8"))
