# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in tex inlinechoice question"""

import sys
sys.path.append('../../../AuthoringTool/Renders')
sys.path.append('../../../Editor')
import logging

import doctexrender
import editor
import table
import image
#import formula


class TeXRenderInlineChoice(doctexrender.DocTeXRender):
    """Creates a TeX representation of a choice multiple question"""

    name = 'InlineChoice'

    def __init__(self, solution):
        self.solutions = solution

    def render(self, question, texfile):
        """Creates the tex representation"""
        wording = "\question(" + str(question.best_mark()) + "p)\n"

        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                aux = self.parser(buff.get_text(buff.get_start_iter(),
                                                buff.get_end_iter(),
                                                True))
#                 content = str(wording)
#                 texfile.write(content.decode("utf-8").encode("latin-"))

                if aux.count("[]") == 1:
                    aux1 = aux[0:aux.find("[]")]
                    aux2 = aux[aux.find("[]") + 2:]
                    wording += aux1.decode("utf-8").encode("latin-")
                    wording += """
        \\begin{oneparchoices} """
                    vals = question.answers.values()
                    pos = []
                    for val in vals:
                        if val[0] > 0:
                            pos.append(vals.index(val))
                    i = 0
                    for key, val in question.answers.items():
                        if self.solutions and i == vals.index(max(vals)):
                            choice = val[1].decode("utf-8").encode("latin-")
                            wording += "\correctchoice " + choice + "\n"
                        else:
                            choice = val[1].decode("utf-8").encode("latin-")
                            wording += "\choice " + choice + "\n"
                        i = i + 1
                    wording += """\end{oneparchoices} \n"""
                    wording += aux2.decode("utf-8").encode("latin-")
                else:
                    wording += aux.decode("utf-8").encode("latin-")
            elif isinstance(content, table.Table):
                ucontent = content.description.decode("utf-8").encode("latin-")
                wording += """
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \caption{""" + ucontent + """}
                \end{figure}
                """
            elif isinstance(content, image.Image):
                u_content = content.description.decode("utf-8").encode("latin-")
                wording += """
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \caption{""" + u_content + """}
                \end{figure}
                """
            else:
                wording +="""
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=1]{""" + content.path + """}
                \caption{prueba}
                \end{figure}
                """


#       wording += self.render_wording(question)
#       content = str(wording) + """
#       \\begin{oneparchoices}"""
#       vals = question.answers.values()
#       pos = []
#       for v in vals:
#           if v > 0:
#               pos.append(vals.index(v))
#       i = 0
#       for k, v in question.answers.items():
#           if self.solutions and i == vals.index(max(vals)):
#               content += "\correctchoice " + v[1] + "\n"
#           else:
#               content += "\choice " + v[1] + "\n"
#       content += """
#       \end{oneparchoices} \n"""

        texfile.write(self.refine_quotation_marks(wording))

