# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to create a inlinechoice controller"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../../AuthoringTool')
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import controller
import inlinechoicevalidator
import extend_gui
import alert
import editor
import exception
import inlinechoiceagent
import inlinechoice
import questiongui
import odict


class InlineChoiceController(controller.Controller):
    """A controller for a inlinechoice dialog"""

    name = 'Elecci�n en l�nea'
    p_text = "Entra en l�nea"
    s_text = "Este tipo de pregunta interact�a con el alumno mediante un " + \
        "conjunto de respuestas, cada una de las cuales es un peque�o " + \
        "texto. El alumno tiene que elegir una de las opciones. A " + \
        "diferencia de las preguntas de elecci�n m�ltiple el sistema " + \
        "permite que la opciones  est�n empotradas dentro de un texto. " + \
        "Para crear una pregunta de este tipo se deber�: \n \n" + \
        "Seleccionar uno o m�s t�rminos a los que pertenece. \n" + \
        "Un nombre para poder identificarla posteriormente en el banco de " + \
        "preguntas. \n" + \
        "Una calificaci�n por defecto. \n" + \
        "Un identificador, una calificaci�n y el texto para cada una de " + \
        "las opciones que se quieren presentar. \n" + \
        "Se puede ampliar el n�mero de respuestas mediante el bot�n " + \
        " situado al final del formulario."

    def __init__(self, vocabularies):
        self.template = questiongui.BasicGUI()
        self.editor = editor.Editor('../Editor/editor.glade', True)
#       self.button = gtk.Button(label="<?>")
#       self.button.connect("clicked", self.set_gap)
#       self.editor.widgets.get_widget("hbox2").add(self.button)
#       self.button.show()
        self.numAns = 3
        self.id = ord('D')

        vbox = self.editor.get_main_vbox()
        vbox.unparent()
        self.template.attach_editor(vbox)

        file = "./Questions/InlineChoice/inlinechoice.glade"
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
        table = self.widgets.get_widget("tableAns")
        table.unparent()
        self.template.attach_gui(table)

        self.ans = odict.OrderedDict()
        for i in range(3):
            self.ans[self.widgets.get_widget("entryChoice"+str(i + 1))] = \
                        [self.widgets.get_widget("entryMark"+str(i + 1)),
                         self.widgets.get_widget("textview"+str(i + 1))]

        self.dict_vocabularies = {}
        self.template.fill_vocabularies(vocabularies, self.dict_vocabularies)
#       self.vbCategories = self.widgets.get_widget("vboxCategories")
#       for k, v in vocabularies.items():
#               self.create_vocabulary_view(k, v, self.vocabularies,
#                                           self.vbCategories)

        self.template.set_title("Entrada en l�nea")
        self.template.set_text(InlineChoiceController.p_text,
                                InlineChoiceController.s_text)
        self.template.create_more_ans_widget(self)

    def run(self, question=None):
        """Runs the dialog"""
        end = False
        while(not end):
            try:
                response =  self.template.run_dialog()
                end = True
                if response == 0:
                    self.template.hide_dialog()
                elif response == 1 :
                    self.save_question(question)
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.template.hide_dialog()
            except exception.DataException:
                end = False

    #simpleChoice[id]=[Mark, entryText]
    def save_question(self, ques):
        """If the questions is correct, it's saved"""
        title, wording, simple_choice, defaultMark   = self.take_data()
        question = inlinechoice.InlineChoice(title, defaultMark, wording,
                                             simple_choice)
        try:
            validator = inlinechoicevalidator.InlineChoiceValidator()
            validator.validate(question)
            for key, val in question.answers.items():
                val[0] = float(val[0])
            question.defaultMark = float(question.defaultMark)
            terms = self.template.get_selected_terms()
            question.set_terms(terms)
            agent = inlinechoiceagent.InlineChoiceAgent()
            if ques:
                question.set_id(ques.id)
                question.filename = ques.filename
                agent.update(question)
            else:
                agent.save(question)
            self.template.hide_dialog()
        except exception.DataException, e:
            edata = alert.ErrorData()
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n" + e.msg
            edata.label_info_set_text(all_msg)
            raise exception.DataException(e.msg)

    def take_data(self):
        """Obtains the info from the dialog"""
        title = self.template.get_title()
        defaultMark = self.template.get_default_mark()
        simple_choice = odict.OrderedDict()
        for k, val  in self.ans.items():
            buf = val[1].get_buffer()
            if k.get_text() != "" and val[0].get_text() !="" and \
                    buf.get_text(buf.get_start_iter(), buf.get_end_iter()) != "":
                simple_choice[k.get_text()] = [val[0].get_text(),
                                               buf.get_text(buf.get_start_iter(),
                                                            buf.get_end_iter(), False)]

#        buff = self.editor.get_textview().get_buffer()
        wording = self.editor.contents.values()

        return title, wording, simple_choice, defaultMark

    def buttonMoreAns_clicked(self, widget, data=None):
        """Extends the gui with 3 more possible answers"""
        extend_gui.add_more_answers(self.widgets.get_widget("tableAns"),
                                    self.ans, self.numAns, self.id)
        self.numAns = self.numAns + 3
        self.id = self.id + 3
