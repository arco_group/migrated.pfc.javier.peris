# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in tex choice multiple question"""

import sys
sys.path.append('../../../AuthoringTool/Renders')

import doctexrender


class TeXRenderChoiceMultiple(doctexrender.DocTeXRender):
    """Creates a TeX representation of a choice multiple question"""

    name = "ChoiceMultiple"

    def __init__(self, solution):
        self.solutions = solution

#     def render(self, question, texfile):
#       wording = "\question (" + str(question.best_mark()) +"p)\n"
#       wording += self.render_wording(question)
#       onepar = self.one_line(question)
#       if onepar:
#           env = [" \n \n \\begin{oneparchoices} \n", "\end{oneparchoices} \n"]
#       else:
#           env = [" \n \n \\begin{enumerate} \n ",
#                  "\end{enumerate} \n"]
#       content = str(wording) + env[0]
#       vals = question.answers.values()
#       pos = []
#       for v in vals:
#             print "V de vals:", v[0]
#           if v[0] > 0:
#               pos.append(vals.index(v))
#       i = 0
#       for k, v in question.answers.items():
#           if  self.solutions:
#               if  i == vals.index(max(vals)) and question.single:
#                   if onepar:
#                       content += "\correctchoice " + v[1] + "\n"
#                   else:
#                       content += """\\renewcommand\\theenumi{\Alph{enumi}.}\n\\renewcommand{\labelenumi}{\colorbox{negro}{%
# \\begin{minipage}{12pt}\\textbf{\\textcolor{blanco}{\\textbf{\\theenumi}}}\end{minipage}}} \item """ + \
#                           v[1] + " \n \\renewcommand{\labelenumi}{\\theenumi} \n"
#               elif i in pos and not question.single:
#                   if onepar:
#                       content += "\correctchoice " + v[1] + "\n"
#                   else:
#                       content += """\\renewcommand\\theenumi{\Alph{enumi}.}\\renewcommand{\labelenumi}{\colorbox{negro}{%
# \\begin{minipage}[c][12pt][c]{12pt}\\textbf{\\textcolor{blanco}{\\textbf{\theenumi}}}\end{minipage}}} \item """ + \
#                           v[1] + "\n \renewcommand{\labelenumi}{\theenumi} \n"
#               elif onepar:
#                   content += "\choice " + v[1] + "\n"
#               else:
#                   content += "\item " + v[1] + "\n"
#           elif onepar:
#               content += "\choice " + v[1] + "\n"
#           else:
#               content += "\item " + v[1] + "\n"
#           i = i + 1
#       content += env[1]
#       texfile.write(content.decode("utf-8").encode("latin-"))


    def render(self, question, texfile):
        """Creates the tex representation"""
        wording = "\n \question (" + str(question.best_mark()) +"p)\n"
        wording += self.render_wording(question)
        onepar = self.one_line(question)
        if onepar:
            env = [" \n \n \\begin{oneparchoices} \n", "\end{oneparchoices} \n"]
        else:
            env = [" \n \n\\begin{choices} \n ",
                   "\end{choices} \n"]
        content = str(wording) + env[0]
        vals = question.answers.values()
        pos = []
        for val in vals:
            if val[0] > 0:
                pos.append(vals.index(val))
        i = 0
        for k, val in question.answers.items():
            if  self.solutions:
                if  i == vals.index(max(vals)) and question.single:
                    content += "\correctchoice " + val[1] + "\n"
                elif i in pos and not question.single:
                    content += "\correctchoice " + val[1] + "\n"
                else:
                    content += "\choice " + val[1] + "\n"
            else:
                content += "\choice " + val[1] + "\n"
            i = i + 1
        content += env[1]
        texfile.write(content.decode("utf-8").encode("latin-"))

    def one_line(self, question):
        """Checks whether the answers fit in one line"""
        one_line = True
        length = 0
        for k, val in question.answers.items():
            length += len(val[1])
        if length > 85:
            one_line = False
        return one_line
