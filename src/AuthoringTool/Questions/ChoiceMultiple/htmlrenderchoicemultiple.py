# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in html choice multiple question"""

import sys
sys.path.append('/../../Renders')
sys.path.append('../../../Editor')

import logging
#import render
import dochtmlrender


class HTMLRenderChoiceMultiple(dochtmlrender.DocHTMLRender):
    """Creates an HTML representation of a choice multiple question"""

    name = 'ChoiceMultiple'

    def __init__(self, solution, numquestion):
        self.solutions = solution
        self.numquestion = numquestion

    def render(self, question, htmlfile):
        """Creates the html representation"""
        logging.debug("Question single: %s" % str(question.single))
        content = self.render_wording(question)
        content += """
        <input hidden="hidden" type="hidden"
        name="nombre" value='""" + str(question.id) + """'/>
        <input hidden="hidden" type="hidden"
               name="type" value="choiceMultiple"/>
          </td>
        </tr>
        """
        i = 0
        vals = question.answers.values()
        pos = []
        for val in vals:
            if val[0] > 0:
                pos.append(vals.index(val))
        lquestions = question.answers.items()
        for k, val in lquestions:
            if question.single:
                content += """
                <tr>
                    <td></td>
                    <td>
                        <input type='radio' name='""" + str(question.id) + \
                    "' value='" + k + "'"
                if i == vals.index(max(vals)) and self.solutions:
                    content = content + " checked='checked'"
                content = content + """/>
                            <font class="optionNumber">
                            """ + str(i + 1) + """.
                            </font>""" + \
                                str(val[1]).decode("utf-8").encode("latin-")
                if len(lquestions) - 1 == lquestions.index((k, val)):
                    content = content + """
                        <p></p>"""
                content = content + """
                            </td>
                         </tr>"""
            else:
                content += """
                <tr>
                    <td></td>
                    <td>
                        <input type='checkbox' name='""" + str(question.id) + \
                    "' value='" + k + "'"
                if i in pos and self.solutions:
                    content = content + "checked"
                u_val = str(val[1]).decode("utf-8").encode("latin-")
                content = content + """>
                            <font class="optionNumber">
                        """ + str(i + 1) + """</font>
                            """ + u_val + """
                            </input>"""
                if len(lquestions) - 1 == lquestions.index((k, val)):
                    content = content + """
                        <p></p>"""
                content += """
                            </td>
                         </tr>"""
            i += 1
        htmlfile.write(content.decode("latin-").encode("utf-8"))

#       for k, v in lquestions:
#             print "KEY, VALUE:", k, v
#           if question.single:
#               content += """
#               <tr>
#                   <td></td>
#                   <td>
#                       <input type='radio' value="""
#               if i-1 == vals.index(max(vals)) and self.solutions:
#                   content = content + "checked"
#               content = content + """>
#                           <font class="optionNumber">
#                           """ + str(i + 1) + """. \n
#                           </font>
#                           </input>""" + \
#                               str(v[1]).decode("utf-8").encode("latin-")
#               if len(lquestions) - 1 == lquestions.index((k,v)):
#                   content = content + """
#                       <p></p>"""
#               content = content + """
#                           </td>
#                        </tr>"""
#           else:
#               content += """
#               <tr>
#                   <td></td>
#                   <td>
#                       <input type='checkbox'"""
#               if i in pos and self.solutions:
#                   content = content + "checked"
#               content = content + """>
#                           <font class="optionNumber">
#                           """ + str(i + 1) + """
#                           </font>
#                           """ + str(v[1]).decode("utf-8").encode("latin-") + \
#                           """
#                           </input>"""
#               if len(lquestions) - 1 == lquestions.index((k,v)):
#                   content = content + """
#                       <p></p>"""
#               content + content + """
#                           </td>
#                        </tr>"""
#           i += 1
#       htmlfile.write(content)
