# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to administer the choicemultiple question persistence"""

from __future__ import with_statement
import time
import sys
sys.path.append('../../../Persistencia')
sys.path.append('../../../AuthoringTool')
sys.path.append('../../../Editor')
import amara
import logging

import editor
import table
import image
#import term
import choicemultiple
import db
import odict


class ChoiceMultipleAgent:
    """Agent to provide the CRUD for choice multiple question"""

    name = "ChoiceMultiple"
    DIR_NAME = "../QuestionsBank/"

    def save(self, question):
        """Create/save a choicemultiple in the database"""
        gmtime = time.gmtime()
        fileid = question.title
        for i in range(6):
            fileid += str(gmtime[i])
        filename = ChoiceMultipleAgent.DIR_NAME + fileid + ".xml"
        doc = self.create_xml(question)
#         doc = amara.parse("../Templates/choice_multiple.xml")
#         if question.single:
#             doc.assessmentItem.itemBody.choiceInteraction.maxChoices = \
#                 unicode("1",'utf-8')
#         else:
#             doc.assessmentItem.itemBody.choiceInteraction.maxChoices = \
#                 unicode("0",'utf-8')
#         doc.assessmentItem.title = unicode(question.title, 'utf-8')
#         #     doc.assessmentItem.adaptive = u'true'
#         #     doc.assessmentItem.timeDependent = u'true'

#         wording = ""
#         for part in question.wording:
#             if isinstance(part, editor.InteractivePangoBuffer):
#                 wording += part.get_text()
#             elif isinstance(part, table.Table):
#                 filename=part.path
#                 wording += "<img src=" + part.path + ">"
#             elif isinstance(part, image.Image):
#                 content = part.path
#                 wording += "<img src=" + part.path + ">"
#             else:
#                 content = part.path
#                 wording += "<img src=" + part.path + ">"
#         doc.assessmentItem.itemBody.choiceInteraction.xml_append(
#             doc.xml_create_element(u'prompt',
#                                    content=unicode(wording, 'utf-8')))
#         for k, v  in question.answers.items():
#             if float(v[0]) > 0:
#                 doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
#                     doc.xml_create_element(u'value',
#                                            content=unicode(k, 'utf-8')))
#             doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
#                 unicode(str(question.defaultMark), 'utf-8')
#             doc.assessmentItem.responseDeclaration.mapping.xml_append(
#                 doc.xml_create_element(u'mapEntry',
#                                        attributes={u'mapKey': unicode(k, 'utf-8'),
#                                                    u'mappedValue': unicode(str(v[0]), 'utf-8')}))
#             doc.assessmentItem.itemBody.choiceInteraction.xml_append(
#                 doc.xml_create_element(u'simpleChoice',
#                                        attributes={u'identifier':  unicode(k, 'utf-8'),
#                                                    u'fixed': u'false'},
#                                        content = unicode(v[1], 'utf-8')))

        with open(filename, 'w') as fd:
            fd.write(doc.xml())

#         try:
#             fd = open(filename, 'w')
#             fd.write(doc.xml())
#             fd.close()
#         except:
#             logging.exception("Error creating the xml question")

        ques = db.ChoiceMultiple(filename=filename, title=question.title,
                              default_mark=question.defaultMark,
                              single=question.single)
        for k, val  in question.answers.items():
            db.Answer(value=val[1], calification=val[0], ide=k, question=ques)
        for part in question.wording:
            logging.debug("Adding content of type: %s" % str(type(part)))
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                ques.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                ques.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                ques.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                ques.addContent(content)
        for term in question.terms:
            ques.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

    def create(self, q, wording):
        """Reads a choice multiple question"""
#        choices = {}
        choices = odict.OrderedDict()
        for answer in q.answers:
            choices[answer.ide] = [answer.calification, answer.value]
        ques = choicemultiple.ChoiceMultiple(q.title, q.default_mark, wording,
                                             choices, q.single)
#        ques.set_id(q.filename)
        return ques

    def update(self, question):
        """Updates the choice multiple question"""
        q = db.ChoiceMultiple.select(db.ChoiceMultiple.q.id==question.id)[0]
        q.title = question.title
        q.single = question.single
        answers = db.Answer.select(db.Answer.q.question==q.id)
        for ans in answers:
#            ans.question = None
            db.Answer.delete(ans.id)
        for k, val  in question.answers.items():
            db.Answer(value=val[1], calification=val[0], ide=k, question=q)
        for term in q.term_questions:
            q.removeTerm(term)
        for item in q.wording:
            q.removeContent(item)
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

        doc = self.create_xml(question)

        #REVISAR AHORA ES QUESTION.FILENAME ??
#         fd = open(question.filename, 'w')
        with open(question.filename, 'w') as fd:
            fd.write(doc.xml())
#         fd.close()

    def create_xml(self, question):
        """Creates the XML in agreement with IMS QTI v2.0"""
        doc = amara.parse("../Templates/choice_multiple.xml")
        if question.single:
            doc.assessmentItem.itemBody.choiceInteraction.maxChoices = \
                unicode("1",'utf-8')
        else:
            doc.assessmentItem.itemBody.choiceInteraction.maxChoices = \
                unicode("0",'utf-8')
        doc.assessmentItem.title = unicode(question.title, 'utf-8')
        #     doc.assessmentItem.adaptive = u'true'
        #     doc.assessmentItem.timeDependent = u'true'

        wording = ""
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                wording += part.get_text()
            elif isinstance(part, table.Table):
                filename = part.path
                wording += "<img src=" + part.path + ">"
            elif isinstance(part, image.Image):
                filename = part.path
                wording += "<img src=" + part.path + ">"
            else:
                filename = part.path
                wording += "<img src=" + part.path + ">"
        doc.assessmentItem.itemBody.choiceInteraction.xml_append(
            doc.xml_create_element(u'prompt',
                                   content=unicode(wording, 'utf-8')))
        for k, val  in question.answers.items():
            if float(val[0]) > 0:
                doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
                    doc.xml_create_element(u'value',
                                           content=unicode(k, 'utf-8')))
            doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
                unicode(str(question.defaultMark), 'utf-8')
            doc.assessmentItem.responseDeclaration.mapping.xml_append(
                doc.xml_create_element(u'mapEntry',
                                       attributes={u'mapKey': unicode(k, 'utf-8'),
                                                   u'mappedValue': unicode(str(val[0]), 'utf-8')}))
            doc.assessmentItem.itemBody.choiceInteraction.xml_append(
                doc.xml_create_element(u'simpleChoice',
                                       attributes={u'identifier':  unicode(k, 'utf-8'),
                                                   u'fixed': u'false'},
                                       content = unicode(val[1], 'utf-8')))
        return doc

