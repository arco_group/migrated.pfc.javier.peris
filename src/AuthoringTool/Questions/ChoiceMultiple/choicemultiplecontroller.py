# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to create a choicemultiple controller"""

import sys
import logging
sys.path.append('../../../Editor')
sys.path.append('../../../Persistencia')
sys.path.append('../../../AuthoringTool')

try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import controller
import choicemultiplevalidator
import extend_gui
import alert
import exception
import editor
import choicemultipleagent
import choicemultiple
import questiongui
import odict


class ChoiceMultipleController(controller.Controller):
    """A controller for a choicemultiple dialog"""

    name = 'Elecci�n m�ltiple'
    p_text = "Ordenaci�n"
    s_text = "Este tipo de pregunta presenta un conjunto de respuestas al " + \
        "alumno. �ste deber� seleccionar una o m�s opciones. Para crear " + \
        "este tipo de preguntas deber�: " + "\n \n" + \
        "Seleccionar uno o m�s t�rminos a los que pertenece. \n" + \
        "Un nombre para poder identificarla posteriormente en el banco " + \
        "de preguntas. \n" + \
        "Un identificador, una calificaci�n y el texto para cada " + \
        "respuesta que se quiere crear. \n" + \
        "Si se necesitan m�s de tres respuestas, puede ampliarlo mediante " + \
        "el bot�n pertinente al final del formulario. "
    args = [False, True]
    surname = [" - Respuesta m�ltiple", " - Respuesta �nica"]

    def __init__(self, vocabularies, single=False):
        self.template = questiongui.BasicGUI()
        self.editor = editor.Editor('../Editor/editor.glade')
        self.numAns = 3
        self.id = ord('D')
        self.single = single
        logging.debug("The question is single: %s" % str(self.single))

        vbox = self.editor.get_main_vbox()
        vbox.unparent()
        self.template.attach_editor(vbox)

        file = "./Questions/ChoiceMultiple/choicemultiple.glade"
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
        table = self.widgets.get_widget('tableAns')
        table.unparent()
        self.template.attach_gui(table)

        self.ans = odict.OrderedDict()
        for i in range(3):
            self.ans[self.widgets.get_widget("entryChoice"+str(i + 1))] = \
                [self.widgets.get_widget("entryMark"+str(i + 1)),
                 self.widgets.get_widget("textview"+str(i + 1))]

#       self.vbCategories = self.widgets.get_widget("vboxCategories")
        self.dict_vocabularies = {}
        self.template.fill_vocabularies(vocabularies, self.dict_vocabularies)

        self.template.set_title("Elecci�n m�ltiple")
        self.template.set_text(ChoiceMultipleController.p_text,
                                ChoiceMultipleController.s_text)
        self.template.create_more_ans_widget(self)

    def run(self, question=None):
        """Runs the dialog"""
        dialog = self.template.widgets.get_widget("dialogQuestion")
        if gtk.gdk.screen_width() < 602:
            screen_width = gtk.gdk.screen_width()
            ratio = 602 / screen_width
            dialog.size_request(int(820/ratio), int(602/ratio))
        elif gtk.gdk.screen_height() < 820:
            screend_height = gtk.gdk.screen_height()
            ratio = 820 / screen_width
            dialog.size_request(int(820/ratio), int(602/ratio))
        end = False
        while(not end):
            try:
                response = self.template.run_dialog()
                end = True
                if response == 0:
                    self.template.hide_dialog()
                elif response == 1:
                    self.save_question(question)
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.template.hide_dialog()
            except exception.DataException, e:
                end = False

    #If ques = None a new question will be created, otherwise
    #the existing question will be updated
    #simpleChoice[id]=[Mark, TextAnswer]
    def save_question(self, ques):
        """If the questions is correct, it's saved"""
        title, wording, simpleChoice, defaultMark = self.take_data()
        question = choicemultiple.ChoiceMultiple(title, defaultMark, wording,
                                                 simpleChoice, self.single)
        try:
            validator = choicemultiplevalidator.ChoiceMultipleValidator()
            print "VALIDATOR", validator
            validator.validate(question)
            print "2"
            for k, v in question.answers.items():
                v[0] = float(v[0])
            question.defaultMark = float(question.defaultMark)
            terms = self.template.get_selected_terms()
            question.set_terms(terms)
            print "3"
            agent = choicemultipleagent.ChoiceMultipleAgent()
            if ques:
                question.set_id(ques.id)
                question.filename = ques.filename
                logging.debug("Id de la pregunta: %s" % ques.id)
                agent.update(question)
            else:
                print "4"
                agent.save(question)
            self.template.hide_dialog()
        except exception.DataException, e:
            ed = alert.ErrorData()
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n" + e.msg
            ed.label_info_set_text(all_msg)
            raise exception.DataException(e.msg)

    def take_data(self):
        """Obtains the info from the dialog"""
        title = self.template.get_title()
        simpleChoice = odict.OrderedDict()
        for k, v  in self.ans.items():
            buff = v[1].get_buffer()
            if k.get_text() != "" and v[0].get_text() !="" and \
                    buff.get_text(buff.get_start_iter(),
                                  buff.get_end_iter()) != "":
                simpleChoice[k.get_text()] = [v[0].get_text(),
                                              buff.get_text(buff.get_start_iter(),
                                                            buff.get_end_iter(), False)]
        defaultMark = self.template.get_default_mark()
        buf = self.editor.get_textview().get_buffer()
        wording = self.editor.contents.values()
#        wording[0].get_text()
        return title, wording, simpleChoice, defaultMark

    def buttonMoreAns_clicked(self, widget, data=None):
        """Extends the gui with 3 more possible answers"""
        extend_gui.add_more_answers(self.widgets.get_widget("tableAns"),
                                    self.ans, self.numAns, self.id)
        self.numAns = self.numAns + 3
        self.id = self.id + 3
