# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides the ChoiceMultiple class """

import sys
sys.path.append('../../..AuthoringTool/')

import question


class ChoiceMultiple(question.Question):
    """Represents the choice multiple question"""

    name = 'ChoiceMultiple'
    description = 'Plugin ChoiceMulitple'
    icon = '../Icons/choice.png'

    def __init__(self, title, defaultMark, wording, simpleChoice, single):
        question.Question.__init__(self, title, defaultMark, wording)
        self.answers = simpleChoice
        self.single = single

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Returns the best mark a candidate can get in this question"""
        max_mark = 0
        if self.single:
            for key, val in self.answers.items():
                if val[0] > max_mark:
                    max_mark = val[0]
        else:
            for key, val in self.answers.items():
                if val[0] > 0:
                    max_mark = max_mark + val[0]
        return max_mark

def load():
    """Loads the plugin"""
    import question
    import controller
#     import choicemultipleagent
#     import persistencia
    import choicemultiplecontroller
#     import dochtmlrender
#     import htmlrenderchoicemultiple
    import doctexrender
    import texrenderchoicemultiple
    import gtkrender
    import gtkrenderchoicemultiple
    question.Question.register(ChoiceMultiple)
    cmcontroller = choicemultiplecontroller.ChoiceMultipleController
    controller.Controller.register(cmcontroller)
#   htmlrendercm = htmlrenderchoicemultiple.HTMLRenderChoiceMultiple
#     dochtmlrender.DocHTMLRender.register(htmlrendercm)
    texrendercm = texrenderchoicemultiple.TeXRenderChoiceMultiple
    doctexrender.DocTeXRender.register(texrendercm)
    gtkrendercm = gtkrenderchoicemultiple.GtkRenderChoiceMultiple
    gtkrender.GtkRender.register(gtkrendercm)
#     persistencia.Agente.register(choicemultipleagent.ChoiceMultipleAgent)
    load_agent()
    load_html_render()

def load_agent():
    """Loads the agent"""
    import sys
    sys.path.append('../../../Persistencia/')
    import persistencia
    import choicemultipleagent
    persistencia.Agente.register(choicemultipleagent.ChoiceMultipleAgent)

def load_html_render():
    """Loads the html render"""
    import htmlrenderchoicemultiple
    import dochtmlrender
    htmlrendercm = htmlrenderchoicemultiple.HTMLRenderChoiceMultiple
    dochtmlrender.DocHTMLRender.register(htmlrendercm)
