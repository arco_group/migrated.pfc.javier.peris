# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a gtk representation for choice multiple question"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../Questions')
# sys.path.append('../../Renders')
import logging

import editor
import table
import image
# import formula
import choicemultiplecontroller
import extend_gui
import gtkrender


class GtkRenderChoiceMultiple(gtkrender.GtkRender):
    """Renders the choice multiple question as a Gtk dialog"""

    name = "ChoiceMultiple"

    def __init__(self):
        gtkrender.GtkRender.__init__(self)
        self.id = ord('D')

    def render(self, question):
        """Fills the dialog with the info of the question"""
        title = question.title
        d_mark = question.defaultMark
        wording = question.wording
        single = question.single
        vocabularies = self.agente.get_all_vocabularies()
        dict_vcs = {}
        for vocab in vocabularies:
            terms = self.agente.get_terms_vocabulary(vocab.name)
            dict_vcs[vocab.name] = terms

        self.chmc = choicemultiplecontroller.ChoiceMultipleController(dict_vcs,
                                                                      single)
        self.chmc.template.set_question_title(title)
        self.chmc.template.set_default_mark(d_mark)
        first_text = True
        for content in wording:
            logging.debug("Type: %s" % str(type(content)))
            logging.debug("Content: %s" % str(content))
            if isinstance(content, editor.InteractivePangoBuffer):
                txt = content.buf.get_text(content.buf.get_start_iter(),
                                           content.buf.get_end_iter(),
                                           True)
                if first_text:
                    ipb = self.chmc.editor.contents.values()[0]
                    ipb.buf.set_text(txt)
#                   self.chmc.editor.get_textview().set_buffer(content.buf)
                    first_text = False
                else:
                    swindow, textview = self.chmc.editor.create_textpart()
                    buf = content.buf
                    textview.set_buffer(buf)
                    ipb = editor.InteractivePangoBuffer("", buf)
                    self.chmc.editor.contents[swindow] = ipb
            elif isinstance(content, table.Table):
                button_table = self.chmc.editor.create_imagepart()
                self.chmc.editor.contents[button_table] = content
            elif isinstance(content, image.Image):
                prefix = "QuestionsBank/images/"
                content.path = prefix + content.path.split(".")[0].split("/")[-1]
                button_image = self.chmc.editor.create_imagepart(content)
                self.chmc.editor.contents[button_image] = content
            else:
                logging.debug("I have no powers of super cow")

        num_extend = (len(question.answers.items()) - 1) / 3
        msg = "Number of times the interface has to be extended " + \
            str(num_extend)
        logging.debug(msg)
        for val in range(0, num_extend):
            extend_gui.add_more_answers(self.chmc.widgets.get_widget("tableAns"),
                                        self.chmc.ans, self.chmc.numAns,
                                        self.id)
            self.chmc.numAns += 3
            self.id = self.id + 3
        i = 0
        keys = self.chmc.ans.keys()
        for k, val in question.answers.items():
#             keys[i].get_buffer().set_text(k)
#             self.chmc.ans[keys[i]].set_text(str(val[0]))
            keys[i].set_text(k)
            self.chmc.ans[keys[i]][0].set_text(str(val[0]))
            self.chmc.ans[keys[i]][1].get_buffer().set_text(str(val[1]))
            i += 1

        self.activate_terms(question, self.chmc.dict_vocabularies)
        self.chmc.run(question)
