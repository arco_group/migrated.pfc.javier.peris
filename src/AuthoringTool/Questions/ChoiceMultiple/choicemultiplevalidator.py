# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a validator for choice multiple questions"""

import sys
sys.path.append("../../../AuthoringTool/")

import validator
import exception


class ChoiceMultipleValidator(validator.Validator):
    """Validator for choice multiple question"""


    def validate(self, question):
        """Validates whether all the info is correct"""
        correct_t, msg_a = self.check_title(question.title)
        correct_w, msg_b = self.check_wording(question.wording)
        correct_d, msg_c = self.check_default_mark(question.defaultMark)
        correct_a, msg_d = self._check_answers(question.answers)
        correct = correct_t and correct_w and correct_d and correct_a
        print "TODO CORRECTO", correct
        if not correct:
            raise exception.DataException(msg_a + msg_b + msg_c + msg_d)

    def _check_answers(self, answers):
        """Own method to validate the possible answers"""
        correcto = True
        msg_error = ""
        if len(answers) > 0:
            for k, val in answers.items():
                try:
                    float(val[0])
                except ValueError:
                    msg_error += "Se debe insertar una puntuación para la" + \
                                 "respuesta \n"
        else:
            correcto = False
            msg_error += "Se debe rellenar al menos una respuesta\n"
        return correcto, msg_error

