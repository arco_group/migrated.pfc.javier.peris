# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a validator for textentry questions"""

import sys
sys.path.append('../../../AuthoringTool')
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
import logging

import validator
import exception
import editor


class TextEntryValidator(validator.Validator):
    """Validator for textentry question"""

    def validate(self, question):
        """Validates whether all the info is correct"""
        correct_t, t_msg = self.check_title(question.title)
#       print "T", correct_t, a
        correct_w, w_msg = self._check_wording(question.wording)
#       print "W", correct_w, b
        correct_d, dm_msg = self.check_default_mark(question.defaultMark)
#       print "D", correct_d, c
        correct_l, el_msg = self._check_expected_length(question.eLength)
#       print "L", correct_l, d
        correct_a, a_msg = self._check_answers(question.answers)
#       print "A", correct_a, e
        correct = correct_t and correct_w and correct_d and correct_l and \
			correct_a
        if not correct:
            error_msg = t_msg + w_msg + dm_msg + el_msg + a_msg
            raise exception.DataException(error_msg)


    def _check_wording(self, wording):
        """Own method to validate the possible answers"""
        correcto = True
        msg_error = ""
        if len(wording) == 0:
            msg_error += "Se debe rellenar la pregunta\n"
            correcto = False
        else:
            for part in wording:
                if isinstance(part, editor.InteractivePangoBuffer):
                    logging.debug("Wording part: %s" % part.get_text())
                    if part.get_text().count("[]") != 1:
                        correcto = False
                        num = str(part.get_text().count("[]"))
                        logging.debug("Num. of square brackets %s" % str(num))
                        msg_error += "La pregunta debe contener " + \
                         "exactamente una vez los caracteres [], en lugar " + \
                         "donde ira el espacio en blanco mostrado al alumno"
            if not correcto:
                msg_error += "El bloque de texto debe rellenarse\n"
        return correcto, msg_error

    def _check_expected_length(self, expected_length):
        """Own method to validate the expected length"""
        correcto = True
        msg_error = ""
        try:
            int(expected_length)
        except ValueError:
            correcto = False
            msg_error = "La longitud debe ser un n�mero entero"
        return correcto, msg_error

    def _check_answers(self, solutions):
        """Own method to validate the possible answers"""
        correcto = True
        msg_error = ""
        if len(solutions) > 0:
            for k, val in solutions.items():
                try:
                    float(val[0])
                except ValueError:
                    msg_error += "Se debe insertar una puntuaci�n para la " + \
                                    "respuesta\n"
        else:
            correcto = False
            msg_error += "Se deben rellenar al menos una respuesta\n"
        return correcto, msg_error
