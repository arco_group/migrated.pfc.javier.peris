# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to create a textentry controller"""

import sys
import logging
sys.path.append('../../../Editor')
sys.path.append('../../../Persistencia')
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import controller
import textentryvalidator
import exception
import extend_gui
import editor
import alert
import textentryagent
import textentry
import questiongui
import odict


class TextEntryController(controller.Controller):
    """A controller for a textentry dialog"""

    name = 'Entrada de texto'
    p_text = "<b>Entrada de texto</b>"
    s_text ="Este tipo de pregunta interact�a con el alumno mediante un " + \
        "peque�o texto. El sistema deja un espacio para que el alumno " + \
        "interact�e con el texto que rodea. Para crear este tipo de " + \
        "preguntas deber�: \n \n" + \
        "Seleccionar uno o m�s t�rminos a los que pertenece. \n" + \
        "Un nombre para poder identificarla posteriormente en el banco de " + \
        "preguntas. \n" + \
        "Una calificaci�n por defecto. \n" + \
        "Una calificaci�n y el texto para cada respuesta que se quiera " + \
        "comprobar en la correcci�n (tambi�n se podr�an penalizar ciertas " + \
        "respuestas otorg�ndoles un valor negativo). \n" + \
        "Se puede ampliar el n�mero de respuestas mediante el bot�n " + \
        "situado al final del formulario."

    def __init__(self, vocabularies):
        self.template = questiongui.BasicGUI()
        self.editor = editor.Editor('../Editor/editor.glade', True)
        self.numAns = 3

        vbox = self.editor.get_main_vbox()
        vbox.unparent()
        self.template.attach_editor(vbox)

        file = "./Questions/TextEntry/textentry.glade"
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
        table = self.widgets.get_widget("tableAns")
        table.unparent()
        self.template.attach_gui(table)

        self.ans = odict.OrderedDict()
        for i in range(3):
            self.ans[self.widgets.get_widget("textview" + str(i + 1))] = \
                self.widgets.get_widget("entryMark" + str(i + 1))

        self.dict_vocabularies = {}
        self.template.fill_vocabularies(vocabularies, self.dict_vocabularies)
# 		for k, v in vocabularies.items():
# 				self.create_vocabulary_view(k, v, self.vocabularies,
# 											self.vbCategories)

        self.template.set_title("Pregunta de entrada de texto")
        self.template.set_text(TextEntryController.p_text,
                                TextEntryController.s_text)
        self.template.create_more_ans_widget(self)

    def run(self, question=None):
        """Runs the dialog"""
        end = False
        while(not end):
            try:
                response = self.template.run_dialog()
                end = True
                if response == 0:
                    self.template.hide_dialog()
                elif response == 1:
                    self.save_question(question)
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.template.hide_dialog()
            except exception.DataException:
                end = False

#   def set_gap(self, widget):
#       for children in self.editor.widgets.get_widget("vbox3").get_children():
#           if isinstance(children, gtk.ScrolledWindow):
#               for child in children.get_children():
#                   print "GRANDCHILDREN", child
#                   if isinstance(child, gtk.TextView):
# #                         iter = child.get_buffer().get_iter_at_offset(0)
#                       iter = child.get_buffer().get_start_iter()
#                       print "ITER", iter
#                       anchor = child.get_buffer().create_child_anchor(iter)
#                       #child.get_buffer().insert_child_anchor(iter, anchor)
#                       button = gtk.Button(label="<?>")
#                       child.add_child_at_anchor(button, anchor)
#                       child.show()
#                       button.show()
#                       print "Everything is done"

    #simple_choice[answer]=[Mark]
    def save_question(self, ques):
        """If the questions is correct, it's saved"""
        title, e_length, simple_choice, d_mark, wording = self.take_data()
        question = textentry.TextEntry(title, d_mark, simple_choice, wording,
                                       e_length)
        try:
            validator = textentryvalidator.TextEntryValidator()
            validator.validate(question)
            question.eLength = int(question.eLength)
            for k, val in question.answers.items():
                val = float(val)
                question.answers[k] = val
            for k, val in question.answers.items():
                logging.debug("Mark: %s" % str(val))
                logging.debug("Mark type: %s" % str(type(val)))
            terms = self.template.get_selected_terms()
            question.set_terms(terms)
            agent = textentryagent.TextEntryAgent()
            if ques:
                question.set_id(ques.id)
                question.filename = ques.filename
                agent.update(question)
            else:
                agent.save(question)
            self.template.hide_dialog()
        except exception.DataException:
            edata = alert.ErrorData()
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n"
            edata.label_info_set_text(all_msg)
            logging.info("Los campos no son correctos")
            raise exception.DataException(edata.message)

    def take_data(self):
        """Obtains the info from the dialog"""
        title = self.template.get_title()
        e_length = self.widgets.get_widget("entryLength").get_text()
        d_mark = self.template.get_default_mark()

        simple_choice = odict.OrderedDict()
        for k, val  in self.ans.items():
            buff = k.get_buffer()
            if buff.get_text(buff.get_start_iter(), buff.get_end_iter(),
                             False) != "" and val.get_text() !="":
                simple_choice[buff.get_text(
                        buff.get_start_iter(),
                        buff.get_end_iter(), False)] =  val.get_text()

        #buf = self.editor.get_textview().get_buffer()
        wording = self.editor.contents.values()

        return title, e_length, simple_choice, d_mark, wording

    def buttonMoreAns_clicked(self, widget, data= None):
        """Extends the gui with 3 more possible answers"""
        extend_gui.add_more_answers(self.widgets.get_widget("tableAns"),
                                    self.ans, self.numAns)
        self.numAns = self.numAns + 3
