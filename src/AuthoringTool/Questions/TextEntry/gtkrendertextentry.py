# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a gtk representation for textentry question"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../Questions')
import logging

import editor
import table
import image
#import formula
import extend_gui
import gtkrender
import textentrycontroller


class GtkRenderTextEntry(gtkrender.GtkRender):
    """Renders the text entry question as a Gtk dialog"""

    name = 'TextEntry'

    def __init__(self):
        gtkrender.GtkRender.__init__(self)
        vocabularies = self.agente.get_all_vocabularies()
        dict_vocabularies = {}
        for vocab in vocabularies:
            terms = self.agente.get_terms_vocabulary(vocab.name)
            dict_vocabularies[vocab.name] = terms
        self.tec = textentrycontroller.TextEntryController(dict_vocabularies)
        self.id = ord('D')

    def render(self, question):
        """Fills the dialog with the info of the question"""
        title = question.title
        d_mark = question.defaultMark
        wording = question.wording
        e_length = question.eLength
        first_text = True
        self.tec.template.set_question_title(title)
        self.tec.template.set_default_mark(d_mark)
        self.tec.widgets.get_widget("entryLength").set_text(str(e_length))
        for content in wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                txt = content.buf.get_text(content.buf.get_start_iter(),
                                            content.buf.get_end_iter(),
                                            True)
                if first_text:
                    ipb = self.tec.editor.contents.values()[0]
                    ipb.buf.set_text(txt)
#                   self.tec.editor.get_textview().set_buffer(content.buf)
                    first_text = False
                else:
                    swindow, textview = self.tec.editor.create_textpart()
                    buf = content.buf
                    textview.set_buffer(buf)
                    ipb = editor.InteractivePangoBuffer("", buf)
                    self.tec.editor.contents[swindow] = ipb
            elif isinstance(content, table.Table):
                button_table = self.tec.editor.create_imagepart()
                self.tec.editor.contents[button_table] = content
            elif isinstance(content, image.Image):
                prefix = "QuestionsBank/images/"
                content.path = prefix + content.path.split(".")[0].split("/")[-1]
                button_image = self.tec.editor.create_imagepart(content)
                self.tec.editor.contents[button_image] = content
            else:
                logging.debug("I don't have powers of super cow")

        num_extend = (len(question.answers.items()) - 1) / 3
        for val in range(0, num_extend):
            extend_gui.add_more_answers(self.tec.widgets.get_widget("tableAns"),
                                        self.tec.ans, self.tec.numAns,
                                        self.id)
            self.tec.numAns += 3
            self.id += 3
        i = 0
        keys = self.tec.ans.keys()
        for k, val in question.answers.items():
            keys[i].get_buffer().set_text(k)
            self.tec.ans[keys[i]].set_text(str(val))
#           self.tec.ans[keys[i]][0].set_text(str(val[0]))
#           self.tec.ans[keys[i]][1].get_buffer().set_text(val[1])
            i += 1

        self.activate_terms(question, self.tec.dict_vocabularies)

        self.tec.run(question)
