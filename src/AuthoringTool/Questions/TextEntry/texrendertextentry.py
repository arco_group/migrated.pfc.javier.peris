# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in tex textentry question"""

import sys
sys.path.append('../../../AuthoringTool/Renders')
sys.path.append('../../../Editor')
import logging

import doctexrender
import editor
import table
import image
#import formula


class TeXRenderTextEntry(doctexrender.DocTeXRender):
    """Creates a TeX representation of a textentry question"""

    name = 'TextEntry'

    def __init__(self, solution):
        self.solutions = solution

    def render(self, question, texfile):
        """Creates the tex representation"""
        wording = "\question (" + str(question.best_mark())+ "p)\n"

        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                aux = self.parser(buff.get_text(buff.get_start_iter(),
                                                buff.get_end_iter(),
                                                True))
#                 content = str(wording)
#                 texfile.write(content.decode("utf-8").encode("latin-"))

                if aux.count("[]") == 1:
                    aux1 = aux[0:aux.find("[]")]
                    aux2 = aux[aux.find("[]") + 2:]
                    wording += aux1.decode("utf-8").encode("latin-")
                    len =  question.eLength
                    ans = question.answers
                    sol = ans.keys()[ans.values().index(max(ans.values()))]
                    usol = sol.decode('utf-8').encode('latin-')
                    if self.solutions:
                        wording += "\underline{" + usol + "}"
                    else:
                        wording += "\underline{\\textcolor{blanco}{" + \
                            "-" * len + "}}"
#                     wording += """
#       \\begin{solutionorlines}[""" + \
#                         str(len) + """pt]\n""" + usol + \
#         """
#       \end{solutionorlines}
#       """
                    wording += aux2.decode("utf-8").encode("latin-")
                else:
                    wording += aux.decode("utf-8").encode("latin-")
            elif isinstance(content, table.Table):
                udes = content.description.decode("utf-8").encode("latin-")
                wording += """
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \caption{""" + udes + """}
                \end{figure}
                """
            elif isinstance(content, image.Image):
                udes = content.description.decode("utf-8").encode("latin-")
                wording += """
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \caption{""" + udes + """}
                \end{figure}
                """
            else:
                wording +="""
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=1]{""" + content.path + """}
                \caption{prueba}
                \end{figure}
                """

#       len = question.eLength * 1.5
#       content = str(wording) + """
#       \\begin{solutionorlines}[""" + \
#       str(lon) + """pc]\n""" + \
#       """
#       \end{solutionorlines}
#       """
        texfile.write(self.refine_quotation_marks(wording))
