# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to administer the choicemultiple question persistence"""

import time
import sys
sys.path.append('../../AuthoringTool')
sys.path.append('../../Editor')
import amara
import logging

import editor
import table
import image
import textentry
import db
import odict


class TextEntryAgent:
    """Agent to provide the CRUD for textentry question"""

    name = "TextEntry"
    DIR_NAME = "../QuestionsBank/"

    def save(self, question):
        """Create/save a textentry in the database"""
        gmtime = time.gmtime()
        fileid = question.title
        for i in range(6):
            fileid += str(gmtime[i])
        filename = TextEntryAgent.DIR_NAME + fileid + ".xml"
        doc = self.create_xml(question)
#       doc = amara.parse("../Templates/text_entry.xml")

#       doc.assessmentItem.title = unicode(question.title, 'utf-8')
#       #       doc.assessmentItem.adaptive = u'true'
#       #       doc.assessmentItem.timeDependent = u'true'

#       for k, v  in question.answers.items():
#           if float(v) > 0:
#               doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
#                   doc.xml_create_element(u'value', content=unicode(k,
#                                                                      'utf-8')))
#           doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
#               unicode(str(question.defaultMark), 'utf-8')
#           doc.assessmentItem.responseDeclaration.mapping.xml_append(
#                   doc.xml_create_element(u'mapEntry',
#                                          attributes={u'mapKey': unicode(k, 'utf-8'),
#                                                      u'mappedValue': unicode(str(v), 'utf-8')}))

#       qs = question.wording[0].get_text().split("[]")
#       se = question.wording[0].get_text().find("")
#       print "WORDING", question.wording
#       print "QS", qs
#       print "SE", se
#       for i in range(len(qs)):
#           print "ENTRO EN EL P.. FOR"
#           if se:
#               print "ENTRO EN SE", qs[i]
#               doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#               if qs[i] == "":
#                   print "ENTRO EN QS BLANQUITO", qs[i]
#                   doc.assessmentItem.itemBody.xml_append(
#                           doc.xml_create_element(u'textEntryInteraction',
#                                                  attributes={u'responseIdentifier': u'RESPONSE',
#                                                              u'expectedLength': unicode(eLength, 'utf-8')}))
#           else:
#               print "ENTRO EN QS NOOO BLANQUITO", qs[i]
#               if i == len(qs) - 1:
#                   print "ENTRO EN QS NOOO BLANQUITO", qs[i]
#                   doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#               else:
#                   print "ENTRO EN QS NOOO BLANQUITO", qs[i]
#                   doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
#                   doc.assessmentItem.itemBody.xml_append(
#                           doc.xml_create_element(u'textEntryInteraction',
#                                                  attributes={u'responseIdentifier': u'RESPONSE',
#                                                              u'expectedLength': unicode(str(question.eLength), 'utf-8')}))
        try:
            fd = open(filename, 'w')
        except IOError:
            logging.exception("Error creating the xml for a textentry question")
		else:
			fd.write(doc.xml())
            fd.close()

        q = db.TextEntry(filename=filename, title=question.title,
                         eLength=question.eLength)
        for k, val  in question.answers.items():
            db.Answer(value=k, calification=val, question=q)
        for part in question.wording:
            logging.debug("Adding content of type: %s" % str(type(part)))
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)

        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

    def create(self, ques, wording):
        """Reads a textentry question"""
        choices = odict.OrderedDict()
        for answer in ques.answers:
            choices[answer.value] = answer.calification
		question = textentry.TextEntry(ques.title, ques.default_mark, choices,
								   wording, ques.eLength)
#       question.set_id(ques.filename)
        return question

    def update(self, question):
        """Updates the choice multiple question"""
        q = db.TextEntry.select(db.TextEntry.q.id==question.id)[0]
        q.title = question.title
        q.eLength = question.eLength
        answers = db.Answer.select(db.Answer.q.question==q.id)
        for ans in answers:
#            ans.question = None
            db.Answer.delete(ans.id)
        for k, v  in question.answers.items():
            db.Answer(value=k, calification=v, question=q)
        for term in q.term_questions:
            q.removeTerm(term)
        for item in q.wording:
            q.removeContent(item)
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

        doc = self.create_xml(question)
        try:
			fd = open(question.filename, 'w')
        except IOError:
            logging.error("Error creating the xml for a textentry question")
		else:
			fd.write(doc.xml())
            fd.close()



    def create_xml(self, question):
        """Creates the XML in agreement with IMS QTI v2.0"""
        doc = amara.parse("../Templates/text_entry.xml")

        doc.assessmentItem.title = unicode(question.title, 'utf-8')
        #       doc.assessmentItem.adaptive = u'true'
        #       doc.assessmentItem.timeDependent = u'true'

        for k, v  in question.answers.items():
            if float(v) > 0:
                doc.assessmentItem.responseDeclaration.correctResponse.xml_append(
                    doc.xml_create_element(u'value', content=unicode(k,
                                                                     'utf-8')))
            doc.assessmentItem.responseDeclaration.mapping.defaultValue = \
                unicode(str(question.defaultMark), 'utf-8')
            doc.assessmentItem.responseDeclaration.mapping.xml_append(
                    doc.xml_create_element(u'mapEntry',
                                           attributes={u'mapKey': unicode(k, 'utf-8'),
                                                       u'mappedValue': unicode(str(v), 'utf-8')}))

        qs = question.wording[0].get_text().split("[]")
        se = question.wording[0].get_text().find("")
        for i in range(len(qs)):
            if se:
                logging.debug("Part of the split question: %s" % qs[i])
                doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                if qs[i] == "":
                    doc.assessmentItem.itemBody.xml_append(
                            doc.xml_create_element(u'textEntryInteraction',
                                                   attributes={u'responseIdentifier': u'RESPONSE',
                                                               u'expectedLength': unicode(eLength, 'utf-8')}))
            else:
                logging.debug("Part of the split question: %s" % qs[i])
                if i == len(qs) - 1:
                    doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                else:
                    doc.assessmentItem.itemBody.xml_append(unicode(qs[i],'utf-8'))
                    doc.assessmentItem.itemBody.xml_append(
                            doc.xml_create_element(u'textEntryInteraction',
                                                   attributes={u'responseIdentifier': u'RESPONSE',
                                                               u'expectedLength': unicode(str(question.eLength), 'utf-8')}))
        return doc

