# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in html textentry question"""

import sys
sys.path.append('../../../Renders')
sys.path.append('../../../Editor')

import logging
import editor
import table
import image
#import formula
import dochtmlrender


class HTMLRenderTextEntry(dochtmlrender.DocHTMLRender):
    """Creates an HTML representation of a textentry question"""

    name = 'TextEntry'

    def __init__(self, solution, numquestion):
        self.solutions = solution
        self.numquestion = numquestion

    def render(self, question, htmlfile):
        """Creates the html representation"""
        wording = ""
        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                aux = self.parser(buff.get_text(buff.get_start_iter(),
                                                buff.get_end_iter(),
                                                True))
                wording += "<font class='questionText'>"
                if aux.count("[]") == 1:
                    aux1 = aux[0:aux.find("[]")]
                    aux2 = aux[aux.find("[]") + 2:]
                    vals = question.answers.values()
                    pos = vals.index(max(vals))
                    sol = question.answers.keys()[pos]
                    if self.solutions:
                        wording += aux1 + """</font>
                    <input type='TEXT' maxlength='""" + str(question.eLength) + \
                        "' value='" + sol + "'> "
                    else:
                        wording += aux1 + """</font>
                    <input type='TEXT' name='""" + str(question.id) + """' maxlength='""" + \
                            str(question.eLength) + """'/> """
                    if not aux2.isspace():
                        wording += """
            <font class='questionText'> \n""" + aux2
                        logging.debug("Auxiliary 1: %s" % aux1)
                else:
                    wording += aux
                wording += """\n </font>"""
            elif isinstance(content, table.Table):
                wording += "<img src='"+ content.path + "'>"
            elif isinstance(content, image.Image):
                wording += "<img src='"+ content.path + "'>"
            else:
                wording += "<img src='"+ content.path + "'>"
        content = """
    <tr>
       <td class="questionNumber">
       <p>""" + str(self.numquestion) + """</p>
       </td>
       <td>
       <font class="grade">(""" + str(question.best_mark()) + \
                   """p)</font>
        <input hidden="hidden" type="hidden"
               name="nombre" value='""" + str(question.id) +"""'/>
        <input hidden="hidden" type="hidden"
               name="type" value="textEntry"/>""" + wording + """
           <p></p>
       </td>
       </tr>
       """
        htmlfile.write(content.decode("utf-8").encode("latin-"))
