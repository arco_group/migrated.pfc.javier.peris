# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides the TextEntry class """

import sys
sys.path.append('../../AuthoringTool/')

import question


class TextEntry(question.Question):
    """Represents the textentry question"""

    description = 'Plugin TextEntry'
    name = 'TextEntry'
    icon = '../Icons/textEntry.png'

    def __init__(self, title, default_mark, simple_choice, wording, e_length):
        question.Question.__init__(self, title, default_mark, wording)
        self.eLength = e_length
        self.answers = simple_choice

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Returns the best mark a candidate can get in this question"""
        max = self.answers.items()[0][1]
        for k, v in self.answers.items():
            if v > max:
                max = v
        return max

def load():
    """Loads the plugin"""
    import question
    import controller
#     import textentryagent
#     import persistencia
    import textentrycontroller
#     import dochtmlrender
#     import htmlrendertextentry
    import doctexrender
    import texrendertextentry
    import gtkrender
    import gtkrendertextentry
    question.Question.register(TextEntry)
    controller.Controller.register(textentrycontroller.TextEntryController)
#   htmlrenderte = htmlrendertextentry.HTMLRenderTextEntry
#     dochtmlrender.DocHTMLRender.register(htmlrenderte)
    doctexrender.DocTeXRender.register(texrendertextentry.TeXRenderTextEntry)
    gtkrender.GtkRender.register(gtkrendertextentry.GtkRenderTextEntry)
#     persistencia.Agente.register(textentryagent.TextEntryAgent)
    load_agent()
    load_html_render()

def load_agent():
    """Loads the agent"""
    import persistencia
    import textentryagent
    persistencia.Agente.register(textentryagent.TextEntryAgent)

def load_html_render():
    """Loads the html render"""
    import htmlrendertextentry
    import dochtmlrender
    htmlrendercm = htmlrendertextentry.HTMLRenderTextEntry
    dochtmlrender.DocHTMLRender.register(htmlrendercm)
