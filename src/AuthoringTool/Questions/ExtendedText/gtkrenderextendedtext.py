# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a gtk representation for choice multiple question"""

import sys
sys.path.append('../../../Editor')
sys.path.append('../../Questions')
import logging

import editor
import table
import image
#import formula
import gtkrender
import extendedtextcontroller


class GtkRenderExtendedText(gtkrender.GtkRender):
    """Renders the choice multiple question as a Gtk dialog"""

    name = "ExtendedText"

    def __init__(self):
        gtkrender.GtkRender.__init__(self)
        vocabularies = self.agente.get_all_vocabularies()
        dict_vocabs = {}
        for vocab in vocabularies:
            terms = self.agente.get_terms_vocabulary(vocab.name)
            dict_vocabs[vocab.name] = terms
        self.etc = extendedtextcontroller.ExtendedTextController(dict_vocabs)

    def render(self, question):
        """Fills the dialog with the info of the question"""
        title = question.title
        e_length = question.eLength
        wording = question.wording
        solution = question.solution
        d_mark = question.defaultMark
        m_score = question.maximumScore
        first_text = True
        for content in wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                txt = content.buf.get_text(content.buf.get_start_iter(),
                                           content.buf.get_end_iter(),
                                           True)
                if first_text:
                    ipb = self.etc.editor.contents.values()[0]
                    ipb.buf.set_text(txt)
#                   self.etc.editor.get_textview().set_buffer(content.buf)
                    first_text = False
                else:
                    swindow, textview = self.etc.editor.create_textpart()
                    buf = content.buf
                    textview.set_buffer(buf)
                    ipb = editor.InteractivePangoBuffer("", buf)
                    self.etc.editor.contents[swindow] = ipb
            elif isinstance(content, table.Table):
                button_table = self.etc.editor.create_imagepart()
                self.etc.editor.contents[button_table] = content
            elif isinstance(content, image.Image):
                prefix = "QuestionsBank/images/"
                content.path = prefix + content.path.split(".")[0].split("/")[-1]
                button_image = self.etc.editor.create_imagepart(content)
                self.etc.editor.contents[button_image] = content
            else:
                logging.debug("I don't have powers of super cow")

        self.etc.template.set_question_title(title)
        e_expected_length = self.etc.widgets.get_widget("entryExpectedLength")
        e_expected_length.set_text(str(e_length))
        self.etc.template.set_default_mark(d_mark)
        self.etc.widgets.get_widget("entryMaximumScore").set_text(str(m_score))
        tv_solution = self.etc.widgets.get_widget("textviewsolution")
        tv_solution.get_buffer().set_text(solution)

        self.activate_terms(question, self.etc.dict_vocabularies)
        for con in self.etc.editor.contents.values():
            logging.debug("In the editor there is: %s" % con.get_text())

        self.etc.run(question)


