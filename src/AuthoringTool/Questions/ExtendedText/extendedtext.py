# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides the ExtendedText class """

import sys
sys.path.append('../../AuthoringTool/')

import question


class ExtendedText(question.Question):
    """Represents the extendedtext question"""

    description = 'Plugin ExtendedText'
    name = 'ExtendedText'
    icon = '../Icons/extendedText.png'

    def __init__(self, title, defaultMark, wording, eLength, solution, score):
        question.Question.__init__(self, title, defaultMark, wording)
        self.eLength = eLength
        self.solution = solution
        self.maximumScore = score

    def set_solution(self, solution):
        """Sets the solution"""
        self.solution = solution

    def accept(self, visitor):
        """The method to tell the visitor can start the visit"""
        visitor.visit(self)

    def best_mark(self):
        """Returns the best mark a candidate can get in this question"""
        return self.maximumScore

def load():
    """Loads the plugin"""
    import question
    import controller
    import extendedtextagent
#     import persistencia
    import extendedtextcontroller
    import dochtmlrender
#     import htmlrenderextendedtext
    import doctexrender
    import texrenderextendedtext
    import gtkrender
    import gtkrenderextendedtext
    question.Question.register(ExtendedText)
    etcontroller = extendedtextcontroller.ExtendedTextController
    controller.Controller.register(etcontroller)
#   htmlrenderet = htmlrenderextendedtext.HTMLRenderExtendedText
#     dochtmlrender.DocHTMLRender.register(htmlrenderet)
    texrenderet = texrenderextendedtext.TeXRenderExtendedText
    doctexrender.DocTeXRender.register(texrenderet)
    gtkrender.GtkRender.register(gtkrenderextendedtext.GtkRenderExtendedText)
#     persistencia.Agente.register(extendedtextagent.ExtendedTextAgent)
    load_agent()
    load_html_render()

def load_agent():
    """Loads the agent"""
    import persistencia
    import extendedtextagent
    persistencia.Agente.register(extendedtextagent.ExtendedTextAgent)

def load_html_render():
    """Loads the html render"""
    import htmlrenderextendedtext
    import dochtmlrender
    htmlrendercm = htmlrenderextendedtext.HTMLRenderExtendedText
    dochtmlrender.DocHTMLRender.register(htmlrendercm)
