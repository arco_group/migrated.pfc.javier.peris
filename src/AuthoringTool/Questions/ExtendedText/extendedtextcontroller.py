# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to create a choicemultiple controller"""

import sys
import logging
sys.path.append('../../../Editor')
sys.path.append('../../../Persistencia')
sys.path.append('../../../AuthoringTool')

try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import locale
import gettext

import controller
import extendedtextvalidator
import alert
import editor
import exception
import extendedtextagent
import extendedtext
import questiongui


class ExtendedTextController(controller.Controller):
    """A controller for a choicemultiple dialog"""


    name = 'Ensayo'
    p_text = "<b>Acerca de Ensayo</b>"
    s_text = " Las preguntas de tipo ensayo son id�neas cuando se " + \
        "requiere una respuesta de gran extensi�n. Deber� asignarle: \n" + \
        "Uno o m�s t�rminos a los que pertenece. \n" + \
        "Un nombre para poder identificarla posteriormente en el banco de " + \
        "preguntas. \n" + \
        "Una calificaci�n si la respuesta se deja sin responder. \n" + \
        "Opcionalmente, puede a�adir una soluci�n que usted considere " + \
        "�ptima. \n" + \
        "Longitud de la respuesta en l�neas."

    def __init__(self, vocabularies):
        self.vocabularies = vocabularies
        self.template = questiongui.BasicGUI()
        self.editor = editor.Editor('../Editor/editor.glade')

        vbox = self.editor.get_main_vbox()
        vbox.unparent()

        self.template.attach_editor(vbox)

        file = "./Questions/ExtendedText/extended_text.glade"
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
        table = self.widgets.get_widget("tableAns")
        table.unparent()
        self.template.attach_gui(table)

        self.dict_vocabularies = {}
        self.template.fill_vocabularies(vocabularies, self.dict_vocabularies)

        self.template.set_title("Entrada de texto")
        self.template.set_text(self.p_text, self.s_text)

    def run(self, question=None):
        """Run the dialog"""
        end = False
        while(not end):
            try:
                response = self.template.run_dialog()
                end = True
                if response == 0:
                    self.template.hide_dialog()
                elif response == 1:
                    self.save_question(question)
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.template.hide_dialog()
            except exception.DataException, e:
                end = False

#   TODO: ALMACENAR EL VALOR POR DEFECTO EN EL XML
    def save_question(self, ques):
        """If the questions is correct, it's saved"""
        title, defaultMark, wording, eLength, solution, score = self.take_data()
        question = extendedtext.ExtendedText(title, defaultMark, wording,
                                                eLength, solution, score)
        try:
            validator = extendedtextvalidator.ExtendedTextValidator()
            validator.validate(question)
            question.eLength = int(question.eLength)
            question.defaultMark = float(question.defaultMark)
            question.maximumScore = float(question.maximumScore)
            terms = self.template.get_selected_terms()
            question.set_terms(terms)
            agent = extendedtextagent.ExtendedTextAgent()
            if ques:
                question.set_id(ques.id)
                question.filename = ques.filename
                logging.debug("Now the question has id %s" % str(question.id))
                agent.update(question)
            else:
                agent.save(question)
            self.template.hide_dialog()
        except exception.DataException, e:
            edata = alert.ErrorData()
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n" + e.msg
            edata.label_info_set_text(all_msg)
            raise exception.DataException(e.msg)

    def take_data(self):
        """Obtains the info from the dialog"""
        title = self.template.get_title()
        eLength = self.widgets.get_widget("entryExpectedLength").get_text()
        defaultMark = self.template.get_default_mark()
        score = self.widgets.get_widget("entryMaximumScore").get_text()
        wording = self.editor.contents.values()
        for part in wording:
            logging.debug("Content: %s" % part.get_text())
        bufsol = self.widgets.get_widget("textviewsolution").get_buffer()
        solution = bufsol.get_text(bufsol.get_start_iter(),
                                   bufsol.get_end_iter(), False)
        return title, defaultMark, wording, eLength, solution, score

#   def buttonInfo_clicked(self, widget):
#       self.widgets.get_widget("dialogInfo").run()
#       self.widgets.get_widget("dialogInfo").hide()
