# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to administer the extendedtext question persistence"""

from __future__ import with_statement
import time
import sys
sys.path.append('../../AuthoringTool')
sys.path.append('../../../Editor')
import amara
import logging

import editor
import table
import image
import extendedtext
import db


class ExtendedTextAgent:
    """Agent to provide the CRUD for extendedtext question"""

    name = "ExtendedText"
    DIR_NAME = "../QuestionsBank/"

    def save(self, question):
        """Create/save an extendedtext in the database"""
        gmtime = time.gmtime()
        fileid = question.title
        for i in range(6):
            fileid += str(gmtime[i])
        filename = ExtendedTextAgent.DIR_NAME + fileid + ".xml"
        doc = self.create_xml(question)

        with open(filename, 'w') as fd:
            fd.write(doc.xml())

#         fd = open(filename, 'w')
#         fd.write(doc.xml())
#         fd.close()
        logging.debug("Wording length: %s" % str(len(question.wording)))
        ques = db.ExtendedText(filename=filename, title=question.title,
                            default_mark=question.defaultMark,
                            eLength=question.eLength,
                            mScore=question.maximumScore)
        db.Answer(value=question.solution, question=ques)
        for part in question.wording:
            logging.debug("Adding content of type: " % str(type(part)))
            if isinstance(part, editor.InteractivePangoBuffer):
                content = db.PangoBuffer(text=part.get_text())
                ques.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                ques.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                ques.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                ques.addContent(content)
        for term in question.terms:
            ques.addTerm(db.Term.select(db.Term.q.name==term.name)[0])
        print ques

    def create(self, q, wording):
        """Reads an extendedtext question"""
        ques = extendedtext.ExtendedText(q.title, q.default_mark, wording,
                                         q.eLength, q.answers[0].value,
                                         q.mScore)
        return ques

    def update(self, question):
        """Updates the extendedtext question"""
        q = db.ExtendedText.select(db.ExtendedText.q.id==question.id)[0]
        q.title = question.title
        q.eLength = question.eLength
        q.mScore = question.maximumScore
        answer = db.Answer.select(db.Answer.q.question==q.id)[0]
        print answer
        answer.question = None
        for term in q.term_questions:
            q.removeTerm(term)
        for item in q.wording:
            q.removeContent(item)
        answer = db.Answer(value=question.solution, question=q)
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                logging.info("Content: %s" % part.get_text())
                content = db.PangoBuffer(text=part.get_text())
                q.addContent(content)
            elif isinstance(part, table.Table):
                content = db.TableI(filename=part.path)
                q.addContent(content)
            elif isinstance(part, image.Image):
                content = db.Image(filename=part.path)
                q.addContent(content)
            else:
                content = db.Formula(filename=part.path)
                q.addContent(content)
        for term in question.terms:
            logging.debug("It has the term: %s" % term.name)
            q.addTerm(db.Term.select(db.Term.q.name==term.name)[0])

        doc = self.create_xml(question)
#         fd = open(question.filename, 'w')
        with open(question.filename, 'w') as fd:
            fd.write(doc.xml())
#         fd.close()

    def create_xml(self, question):
        """Creates the XML in agreement with IMS QTI v2.0"""
        doc = amara.parse("../Templates/extended_text.xml")
        doc.assessmentItem.title = unicode(question.title, 'utf-8')
        mScore = unicode(str(question.maximumScore), 'utf-8')
        doc.assessmentItem.outcomeDeclaration.maximumScore = mScore
        if question.solution != "":
            doc.assessmentItem.xml_append_fragment(
                '<responseDeclaration cardinality="single" identifier="RESPONSE">' + \
                    '"baseType="string">\n  <correctResponse>\n<value>' + \
                    question.solution + '</value>\n  </correctResponse>\n </responseDeclaration>')
        else:
            doc.assessmentItem.xml_append(
                doc.xml_create_element(u'responseDeclaration',
                                       attributes={u'cardinality': u'single',
                                                   u'identifier': u'RESPONSE',
                                                   u'baseType': u'string'}))

        wording = ""
        for part in question.wording:
            if isinstance(part, editor.InteractivePangoBuffer):
                wording += part.get_text()
            elif isinstance(part, table.Table):
                wording += "<img src=" + part.path + ">"
            elif isinstance(part, image.Image):
                wording += "<img src=" + part.path + ">"
            else:
#                content = part.path
                wording += "<img src=" + part.path + ">"
        doc.assessmentItem.itemBody.extendedTextInteraction.xml_append(
                doc.xml_create_element(u'prompt',
                                       content = unicode(wording, 'utf-8')))
        doc.assessmentItem.itemBody.extendedTextInteraction.expectedLines = \
            unicode(str(question.eLength), 'utf-8')

        return doc
