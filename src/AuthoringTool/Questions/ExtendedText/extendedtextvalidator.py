# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a validator for extendedtext questions"""

import sys
sys.path.append("../../../AuthoringTool/")

import validator
import exception


class ExtendedTextValidator(validator.Validator):
    """Validator for choice multiple question"""

    def validate(self, question):
        """Validates whether all the info is correct"""
        correct_t, t_msg = self.check_title(question.title)
        correct_w, w_msg = self.check_wording(question.wording)
        correct_d, dm_msg = self.check_default_mark(question.defaultMark)
        correct_m, ms_msg = self._check_maximum_score(question.maximumScore)
        correct_l, el_msg = self._check_expected_length(question.eLength)
        correct = correct_t and correct_w and correct_d and correct_m and \
            correct_l
        if not correct:
            error_msg = t_msg + w_msg + dm_msg + ms_msg + el_msg
            raise exception.DataException(error_msg)

    def _check_expected_length(self, expected_length):
        """Own method to validate the expected length"""
        correcto = True
        msg_error = ""
        try:
            int(expected_length)
        except ValueError:
            correcto = False
            msg_error += "La longitud debe ser un n�mero entero"
        return correcto, msg_error

    def _check_maximum_score(self, maximum_score):
        """Own method to validate the maximum score"""
        correcto = True
        msg_error = ""
        try:
            float(maximum_score)
        except ValueError:
            correcto = False
            msg_error += "Se debe introducir un n�mero en la puntuaci�n " + \
                         "m�xima\n"
        return correcto, msg_error
