# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in tex extendedtext question"""

import sys
sys.path.append('../../../AuthoringTool/Renders')

import doctexrender


class TeXRenderExtendedText(doctexrender.DocTeXRender):
    """Creates a TeX representation of a extendedtext question"""

    name = "ExtendedText"

    def __init__(self, solution):
        self.solutions = solution

    def render(self, question, texfile):
        """Creates the tex representation"""
        wording = "\n \question (" + str(question.best_mark()) + "p) "
        wording += self.render_wording(question)
        lon = question.eLength * 1.5
        content = wording.decode("utf-8").encode("latin-") + """
  \\begin{solutionorlines}[""" + \
        str(lon) + """pc]\n""" + \
        str(question.solution) + \
        """
        \end{solutionorlines}
        """
        texfile.write(self.refine_quotation_marks(content))
#       \end{questions}
#       \end{document}"""

#       \\fillwithlines{""" + question.eLength * 1.5 + """pc}
