# -*- coding: iso-8859-15; tab-width:4 -*-

"""Provides a class to represent in html extendedtext question"""

import sys
sys.path.append('../../../App')
sys.path.append('../../../Editor')

import dochtmlrender


class HTMLRenderExtendedText(dochtmlrender.DocHTMLRender):
    """Creates an HTML representation of a extendedtext question"""

    name = 'ExtendedText'

    def __init__(self, solution, numquestion):
        self.solutions = solution
        self.numquestion = numquestion

    def render(self, question, htmlfile):
        """Creates the html representation"""
        content = self.render_wording(question)
        content += """
        <input hidden="hidden" type="hidden"
               name="nombre" value='""" + str(question.id) + """'/>
        <input hidden="hidden" type="hidden"
               name="type" value="extendedText"/>
          </td>
        </tr>
        """
        content +="""
            <tr>
                <td class="invisible">
                    <input class="mark" type="text" size="1px" name="mark"/>
                </td>
                <td>
                    <textarea rows='""" + str(question.eLength) + \
            "' cols='107' name='" + str(question.id) + "'>"
        if self.solutions:
            u_solution = question.solution
            content = content + u_solution
        content = content + """ </textarea>
                   <p></p>
                </td>
            </tr>
        """
        htmlfile.write(content.decode("latin-").encode("utf-8"))
