"""Render interface"""

import zope.interface


def methods_invariant(ob):
    """A render class must have a register and visit method and
    name and icon atrrs"""
    assert(hasattr(ob,"register") and callable(ob.register)), "you must provide a register method"
    assert(hasattr(ob,"visit") and callable(ob.visit)), "you must provide a visit method"
    assert(hasattr(ob, "name")), "you must provide a name attribute"
    assert(hasattr(ob, "icon")), "you must provide a icon attribute"

class IRender(zope.interface.Interface):
    """Render interface"""

    name = zope.interface.Attribute("""render name""")
    icon = zope.interface.Attribute("""icon name""")

    zope.interface.invariant(methods_invariant)
