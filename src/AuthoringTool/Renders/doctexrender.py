# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to work with tex docs"""

import sys
sys.path.append('../../../AuthoringTool')
sys.path.append('../../../Editor')
import logging
import time

import render
import editor
import table
import image


class DocTeXRender(render.Render):
    """Generates a tex doc"""

    name = "TeX"
    icon = "../Icons/pdf.png"
    ext = ".pdf"
    ext_comp = ".tex"
    app_comp = "pdflatex"
    app = "okular"
    renders = {}

#   def __init__(self):
#       self.solutions = solutions

    @classmethod
    def register(cls, cls_render):
        """Registers html render for specific type of questions"""
#         border = "---------------------------------" + len(cls_render.name) * "-"
#         logging.info(border)
        logging.info("%s is registered in TeXRender" % cls_render.name)
#         logging.info(border)
        DocTeXRender.renders[cls_render.name] = cls_render

    def create_header(self, solutions, preview, filename="../Previews/tmp1",
                      subject="Prueba", part="Prueba", section="prueba",
                      date=[], id=0, action="/report/", name="",
                      surname=""):
        """Creates the header for the html doc"""
        if not date:
            year, month, day, hour, min, sec, wday, yday, isd = time.localtime()
            date.append(year)
            date.append(month-1)
            date.append(day)

#       path = "../Previews/tmp.tex"
        self.solutions = solutions
        try:
            self.texfile = open(filename + ".tex", 'w')
        except:
            logging.exception("Error creating the TeX doc")
        content = """\documentclass[pdftex,10pt,a4paper,spanish,color]{exam}
\usepackage{times}

\global\let\lhead\undefined
\global\let\chead\undefined
\global\let\\rhead\undefined
\global\let\lfoot\undefined
\global\let\cfoot\undefined
\global\let\\rfoot\undefined

%\usepackage{poppy}
\usepackage{color}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{verbatim}
\usepackage{pifont}
\usepackage{rotating}
\usepackage{latexsym}
\usepackage{multicol}
\usepackage{babel}
\usepackage{fancyhdr}
\usepackage{afterpage}
\usepackage{geometry}
\usepackage{amssymb}
\usepackage{color,soul}
\definecolor{amarillo}{rgb}{1,0.9,0} % valores de las componentes rojo, verde y azul (RGB)
\definecolor{blanco}{rgb}{1,1,1}
\setulcolor{amarillo}

\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{listings}
\lstloadlanguages{}
\definecolor{Gris}{gray}{0.5}
\lstdefinestyle{code}{basicstyle=\scriptsize\\ttfamily,%
  commentstyle=\color{Gris},%
  keywordstyle=\\bfseries,%
  showstringspaces=false,%
  extendedchars=true,%
  numbers=left,
  numberstyle=\\tiny,
  stepnumber=2,
  numbersep=8pt,
  xleftmargin=15pt,
  frame=single}

\lstdefinestyle{screen}{basicstyle=\scriptsize\\ttfamily,%
commentstyle=\color{Gris},%
  keywordstyle=\\bfseries,%
  showstringspaces=false,%
  extendedchars=true,%
  xleftmargin=15pt}

\\newcommand{\ifcolor}[1]{#1}

\usepackage[bf,small]{caption}
\setlength{\captionmargin}{0.2cm}

\geometry{margin=1.8cm,top=2.5cm,bottom=2.5cm}

\graphicspath{{.}{images/} {./}{../Previews/}}

\definecolor{uclm}{cmyk}{0.2,1,0.6,0.5}
\definecolor{uclm-light}{cmyk}{0,0.1,0.1,0}
\definecolor{arco}{cmyk}{0.6,0.4,0,0.3}
\definecolor{arco-light}{cmyk}{0.1,0,0,0}
\definecolor{negro}{rgb}{0,0,0}
\definecolor{blanco}{rgb}{1,1,1}

\\newcommand{\UCLMcolor}[1]{\\textcolor{uclm}{#1}}
\\newcommand{\UCLMbgcolor}[1]{\color{uclm-light}#1}

\\newcommand{\UCLMlogo}[1][height=1.3cm]{\UCLMcolor{\includegraphics[#1]{uclm.pdf}}}

\\newcommand{\UCLM}{{UCLM}}
\\newcommand{\UCLMname}{{\includegraphics[width=.4\\textwidth]{uclmtext.pdf}}}

\\newcommand{\CSname}{\\textbf{\\textsf{Departamento de Imform�tica}}}
\\newcommand{\ESIname}{\\textbf{\\textsf{Escuela Superior de Inform�tica}}}

\\newcommand{\ESIhead}{\UCLMcolor{%
\UCLMlogo[height=0.8cm]%
\\begin{tabular}[b]{l}
{\large\UCLMname}\\\ 
{\\normalsize\ESIname}
\end{tabular}}}

\\newcommand{\UCLMbglogo}{\ifcolor{%
\\begin{picture}(0,0)
\put(0,0){\centerline{%
\\begin{tabular}[t]{c}
\\\ 
\\rule{0pt}{0.20\\textheight}\\\ 
\UCLMbgcolor{\includegraphics[width=0.7\\textwidth]{uclm.pdf}}\\
\\rule{0pt}{0.10\\textheight}\\\ 
%\UCLMbgcolor{\\fontsize{120}{150pt}\UCLM}
\end{tabular}}}
\end{picture}}}

% Eliminar la marca de agua del escudo
\\renewcommand{\UCLMbglogo}{}

\\newcommand{\headframe}{%
\setlength{\\fboxsep}{0mm}%
\setlength{\\fboxrule}{1pt}%
\setlength{\unitlength}{1pt}%
\\begin{picture}(0,0)(0,0)
\centerline{%
\\raisebox{-2mm}{%
\UCLMcolor{\\fbox{\UCLMbgcolor{%
\\rule{4mm}{1.2cm}%
\\rule{\\textwidth}{1.2cm}}}}}}
\end{picture}}

\header{\UCLMbglogo\headframe\UCLMcolor{\ESIhead}}{}{%
\\textbf{\large """ + subject.decode("utf-8").encode("latin-") + \
            """}\\\ {\small """+ part.decode("utf-8").encode("latin-") \
            + " (" + section.decode("utf-8").encode("latin-") + """), """ + \
            self.render_date(date) +"""}}

\\footer{}{P�g. \\thepage{}/\\numpages}{}

% exam class
\pointname{p}
\\addpoints

% longitudes
\\newlength{\\altolinea}
\\addtolength{\\altolinea}{0.5cm}
\setlength{\\fboxsep}{.2cm}

\\renewcommand{\solutiontitle}{\\noindent\\textbf{Soluci�n:}\enspace}

\\renewcommand{\\theenumi}{\Alph{enumi}.}

\\begin{document}

\pagestyle{headandfoot}

\\renewcommand{\\theenumi}{(\\alph{enumi})}

\\vspace*{4mm}
\\noindent
Apellidos: \enspace\\rule{0.43\\textwidth}{.5pt}\enspace Nombre:
\enspace\\rule{0.24\\textwidth}{.5pt} \enspace Grupo:\enspace\hrulefill

\\vspace*{4mm}

\\begin{questions}

        """
        if self.solutions:
            content = content + "\printanswers "
        # Si se quieren imprimir las soluciones \printanswers
        self.texfile.write(content)

    def visit(self, question):
        """Method to call the visitor to start the visit"""
        try:
            DocTeXRender.renders[question.name](self.solutions).render(question,
                                                                       self.texfile)
        except KeyError:
            wording = "\question (" + str(question.best_mark()) + "p) \n \n"
            wording += self.render_wording(question)
            self.texfile.write(wording.decode("utf-8").encode("latin-"))
            print "No hay render para " + question.name + " de tipo LaTeX"
            print "IMPLEMENTAR RENDER BASICO!!"
            for key in DocTeXRender.renders.keys():
                logging.debug(key)
        except:
            logging.debug("Problema dentro de un tipo de render tex concreto")

    def create_section(self, name):
        """Creates a new section"""
        content = "\section*{" + name.decode("utf-8").encode("latin-") + "}"
        self.texfile.write(content)

    def close_part(self, i=0, part=""):
        """Closes the part"""
        content = """
        \end{questions}
        \end{document}"""
        self.texfile.write(content)
        self.texfile.close()

#     def close_document(self):
#         content = """
#         \end{questions}
#         \end{document}"""
#         self.texfile.write(content)
#         self.texfile.close()

    # Revisar este parser
    def parser(self, wording):
        """Parses the pangobuffer tags and convert them to tex commands"""
        logging.debug("DOCTEX ENUNCIADO: %s" % wording)
        ini = wording.find("<span")
        while not ini == -1:
            offset = ini + 13
            end = wording[ini+13:].find('"') + offset
            tag = wording[offset:end]
            logging.debug("Selected tag %s", tag)
            new_open_tag = ""
            new_close_tag = ""
            if tag == "weight=700":
                new_open_tag = "\textbf{"
                new_close_tag = "}"
            elif tag == "italic":
                new_open_tag = "\emph{"
                new_close_tag = "}"
            elif tag == 'underline="single':
                new_open_tag = "<u>"
                new_close_tag = "</u>"
            if tag == 'font-desc="Normal 6"':
                new_open_tag = "{\scriptsize"
                new_close_tag = "}"
            elif tag == 'font-desc="Normal 7"':
                new_open_tag = "{\footnotesize"
                new_close_tag = "}"
            elif tag == 'font-desc="Normal 8"':
                new_open_tag = "{\small"
                new_close_tag = "}"
            elif tag == 'font-desc="Normal 12"':
                new_open_tag = "{\large"
                new_close_tag = "}"
            elif tag == 'font-desc="Normal 14"':
                new_open_tag = "{\Large"
                new_close_tag = "}"
            elif tag == 'font-desc="Normal 16"':
                new_open_tag = "{\LARGE"
                new_close_tag = "}"
            if tag[0:10] == 'foreground':
                preambule = "\definecolor{" + tag[12:18] + "}{rgb}{" + \
                    self._HTMLColorToRGB(tag[12:18])+ "}"
                tfile = self.texfile.read()
                start = tfile.find("\definecolor")
                new_file = tfile[:start] + preambule + tfile[start + 37:]
                self.textfile.close()
                self.textfile.write(new_file)
                new_open_tag = "\color{" + tag[12:18] + "}{"
                new_close_tag = "}"
            wording = wording[:ini] + new_open_tag + wording[end+2:]
            logging.debug("Initial tag changed %s", wording)
            ini_close = wording.find("</span>")
            end_close = ini_close + 7
            wording = wording[:ini_close] + new_close_tag + wording[end_close:]
            logging.debug("Changed Tags: %s", wording)
            ini = wording.find("<span style=")
        return wording

    def render_wording(self, question):
        """A general render for the wording of the question"""
        wording = ""
        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                wording += self.parser(buff.get_text(buff.get_start_iter(),
                                                     buff.get_end_iter(),
                                                     True))
            elif isinstance(content, table.Table):
                wording += """
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \caption{""" + content.description + """}
                \end{figure}
                """
            elif isinstance(content, image.Image):
                wording += """
                \\begin{center}
                \includegraphics[scale=""" + str(content.scale) + "]{" + \
                    content.path + """}
                \end{center}
                """

#               wording += """
#               \\begin{figure}[!ht]
#               \centering
#               \includegraphics[scale=""" + str(content.scale) + "]{" + \
#                   content.path + """}
#               \caption{""" + content.description + """}
#               \end{figure}
#               """
            else:
                wording +="""
                \\begin{figure}[!ht]
                \centering
                \includegraphics[scale=1]{""" + content.path + """}
                \caption{prueba}
                \end{figure}
                """
        return wording

    def refine_quotation_marks(self, wording):
        """Refines the quoation marks, putting the tex quotation marks """
        aux = wording
#        n = 0
        ini = fin = 0
        while aux.count('"'):
            ini = aux.find('"')
            logging.debug("[INI] %s" % ini)
            fin = aux.find('"', ini+1)
            logging.debug("[FIN] %s" % fin)
#            n = n + 2
            if ini != -1 and fin != -1:
                aux = aux[0:ini] + "``" + aux[ini+1:fin] + "''" + aux[fin+1:]
                logging.debug("[AUX] %s" % aux)
        return aux

    def _HTMLColorToRGB(self, colorstring):
        """ Converts #RRGGBB to an (R, G, B) tuple """
        colorstring = colorstring.strip()
        if colorstring[0] == '#':
            colorstring = colorstring[1:]
        if len(colorstring) != 6:
            raise ValueError, "input #%s is not in #RRGGBB format" % colorstring
        red, green, blue = colorstring[:2], colorstring[2:4], colorstring[4:]
        red, green, blue = [int(n, 16) for n in (red, green, blue)]
        return (red/255.0, green/255.0, blue/255.0)


def load():
    """Registers this class as a type of available render"""
    import render
    render.Render.register(DocTeXRender)

