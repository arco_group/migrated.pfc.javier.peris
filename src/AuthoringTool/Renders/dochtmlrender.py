# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to work with html docs"""

# EL RENDER DEBE GENERAR UN FICHERO EN UTF-8 PARA TRABAJAR CON DJANGO
import sys
sys.path.append("../../Editor/")
import logging
import time

import render
import editor
import table
import image


class DocHTMLRender(render.Render):
    """Render an exam in html"""

    name = "HTML"
    icon = "../Icons/html.png"
    app = "iceweasel"
    ext = ".html"
    renders = {}

    def __init__(self):
        self.numquestion = 1

    @classmethod
    def register(cls, cls_render):
        """Registers html render for specific type of questions"""
#         border = "---------------------------------" + len(cls_render.name) * "-"
#         logging.info(border)
        logging.info("%s is registered in GTKRender" % cls_render.name)
#         logging.info(border)
        DocHTMLRender.renders[cls_render.name] = cls_render

    def create_header(self, solutions, preview, filename="../Previews/tmp1",
                      subject="Prueba", part="Prueba", section="prueba",
                      date=[], id="", action="/report/", name=" ",
                      surname=" "):
        """Creates the header for the html doc"""

        if not date:
            year, month, day, h, m, s, wday, yday, isdst = time.localtime()
            date.append(year)
            date.append(month-1)
            date.append(day)

        self.solutions = solutions
        self.filename = filename
        try:
            self.htmlfile = open(self.filename + DocHTMLRender.ext, 'w')
        except:
            logging.exception("Error creating the html doc")
        self.preview = preview
        if preview:
            url_prefix_img = ""
            url_prefix_style = ""
        else:
            url_prefix_img = "/static/"
            url_prefix_style = "/static/css/"
        content = """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Examen de """ + subject +"""</title>
    <link rel="stylesheet" href='""" + url_prefix_style + """plantilla.css'
          type="text/css" media="all"/>
  </head>

  <body style="margin: 0; padding: 0 1em; color: #1a1a1a; font: message-box;
               background-color: #edeceb;">

    <div id="examPage">
      <span id="examHeader">
        <span id="examHeaderLeft">
          <img src='""" + url_prefix_img + """headleft.jpg' id="examHeaderLeft"/>
        </span>
        <span id="examHeaderRight">
          <label id="bold">"""  + subject + """<br id="espaciado">""" + \
               part + " (" + section.decode("utf-8").encode("latin-") + \
               "), " + self.render_date(date) + """ </br>
          </label>
         </span>
      </span>
      <span style="float:left;width:800px;padding-top:10px;">
        <form style="padding:0;margin:0;" action="./?" method="post" name="login">
          <span style="float:left;width:70px;margin:9px 10px 0 0;text-align:right">
            <label for="Apellidos">Apellidos</label>
          </span>
          <span style="float:left;width:300px;margin-top:5px;text-align:left">
             <input type="text" name="surname" id='surname'
                    value='""" + surname + """' disabled='disabled'
                    style="width:300px"/>
          </span>
          <span style="float:left;width:70px;margin:9px 10px 0 0;text-align:right">
            <label for="Nombre">Nombre</label>
          </span>
          <span style="float:left;width:220px;margin-top:5px;text-align:left">
            <input type="text" name="name" id="name" value='""" + name + """'
			       disabled='disabled' style='width:220px'/>
          </span>
          <span style="float:left;width:60px;margin:9px 10px 0 0;text-align:right">
            <label for="Nombre">Grupo</label>
          </span>
          <span style="float:left;width:50px;margin-top:5px;text-align:left">
            <input type="text" name="group" id="group" style="width:50px"
                   disabled='disabled'/>
          </span>
        </form>
      </span>
      <span id="questions">
        <form action='""" + action

        if id:
            content += str(id) + "/"
        content += """' method="post">
          <table cellspacing="2px">"""
        self.htmlfile.write(content.decode("latin-").encode("utf-8"))

    def visit(self, question):
        """Method to call the visitor to start the visit"""
        try:
            DocHTMLRender.renders[question.name](self.solutions,
                                                 self.numquestion).render(question,
                                                                          self.htmlfile)
        except:
#             content = "Alla vamos: <br>"
#             for k, v in DocHTMLRender.renders.items():
#                 content += k.__str__() + "<br>"
            content = self.render_wording(question) + question.name
            content += """
         </td>
     </tr>
     <tr>
         <td></td>
         <td>
             <strong> AVISO: No se ha podido renderizar la pregunta correctamente
             </strong>
         </td>
     </tr>
"""
            self.htmlfile.write(content.decode("latin-").encode("utf-8"))
            msg = "No hay render HTML para esa pregunta de tipo " + \
                question.name
            logging.debug(msg)
            logging.debug("Los renders disponibles son:")
            for key in DocHTMLRender.renders.keys():
                logging.debug(str(key))
        finally:
            self.numquestion = self.numquestion + 1

    def close_document(self):
        """Closes the html document"""
        content = """
        </table>"""
        if not self.preview:
            content += """
       <div id="footer">
        <input type="submit" value="Calificar"/>
       </div>"""
        content += """
      </form>
     </span>
    </div>
  </body>
</html>
"""
        self.htmlfile.write(content)
        self.htmlfile.close()

    def close_part(self, num=0, last=True):
        """Closes the part exam"""
        if not last:
            content = """
          </table>
          <div id="footer">
            <input type="submit" value="Seguir"/>
          </div>
        </form>
      </span>
    </div>
  </body>
</html>
        """
            self.htmlfile.write(content)
            self.htmlfile.close()

    def parser(self, wording):
        """Parses the pangobuffer tags and convert them to html tags"""
        ini = wording.find("<span")
        while not ini == -1:
            relatived_end = wording[ini:].find('">')
            end = ini + relatived_end
            tag = wording[ini+6:end+1]
#             offset = ini + 13
#             end = wording[ini+13:].find('"') + offset
#             tag = wording[offset:end]
            logging.debug("Selected tag %s" % tag)
            new_open_tag = ""
            new_close_tag = ""
            if tag == 'weight="700"':
                new_open_tag = "<strong>"
                new_close_tag = "</strong>"
            if tag == 'style="italic"':
                new_open_tag = "<em>"
                new_close_tag = "</em>"
            if tag == 'underline="single"':
                new_open_tag = "<u>"
                new_close_tag = "</u>"
            if tag == 'font-desc="Normal 6"':
                new_open_tag = "<font size='-4'>"
                new_close_tag = "</font>"
            elif tag == 'font-desc="Normal 7"':
                new_open_tag = "<font size='-3'>"
                new_close_tag = "</font>"
            elif tag == 'font-desc="Normal 8"':
                new_open_tag = "<font size='-2'>"
                new_close_tag = "</font>"
            elif tag == 'font-desc="Normal 12"':
                new_open_tag = "<font size='2'>"
                new_close_tag = "</font>"
            elif tag == 'font-desc="Normal 14"':
                new_open_tag = "<font size='4'>"
                new_close_tag = "</font>"
            elif tag == 'font-desc="Normal 16"':
                new_open_tag = "<font size='6'>"
                new_close_tag = "</font>"
            if tag[0:10] == 'foreground':
                new_open_tag = "<font color=#'"+ tag[12:17] +"'>"
                new_close_tag = "</font>"
            if new_open_tag == "":
                logging.debug("Something without parsing %s" % wording)
            wording = wording[:ini] + new_open_tag + wording[end+2:]
            logging.debug("Initilal tag changed %s" % wording)
            ini_close = end + wording[end:].find("</span>")
            end_close = ini_close + 7
            wording = wording[:ini_close] + new_close_tag + wording[end_close:]
            logging.debug("Changed tags: %s" % wording)
            ini = wording.find("<span style=")
        return wording

    def render_wording(self, question):
        """A general render for the wording of the question"""
        wording = ""
        logging.debug("Content length: %s" % str(len(question.wording)))
        for content in question.wording:
            logging.debug("Content type: %s" % str(type(content)))
            if isinstance(content, editor.InteractivePangoBuffer):
                buff = content.buf
                buff.get_text(buff.get_start_iter(),
                              buff.get_end_iter(),
                              True)
                wording += self.parser(buff.get_text(buff.get_start_iter(),
                                                     buff.get_end_iter(),
                                                     True))
            elif isinstance(content, table.Table):
                wording += "<img src=" + content.path + " width='" + \
                    str(content.scale*100) + "%' height='" + \
                    str(content.scale*100) + "%'>"
            elif isinstance(content, image.Image):
                wording = "<img src='" + content.path + "' width='" + \
                    str(content.scale*100) + "%' height='" + \
                    str(content.scale*100) + "%'>"
            else:
                wording += "<img src='"+ content.path + "'>"

        content ="""
        <tr>
            <td class="questionNumber" valign="top"> <p>""" + \
            str(self.numquestion) + \
            """</p>
            </td>
            <td>
                <font class="grade">(""" + str(question.best_mark()) + \
                    """p)</font>
                <font class="questionText">""" + \
                    wording.decode("utf-8").encode("latin-") + """</font>"""
        return content

def load():
    """Registers this class as a type of available render"""
    import render
    render.Render.register(DocHTMLRender)
