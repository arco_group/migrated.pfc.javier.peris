# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to work with plain text docs"""

import sys
sys.path.append('../../App')
sys.path.append('../../Editor')
import logging
import time

import render
import editor


class PlainTextRender(render.Render):
    """Generate a plain text doc"""

    name = "Plain text"
    icon = "../Icons/ptext.png"
    app = "gedit"
    ext = ".txt"
    renders = {}

    def __init__(self):
        self.numquestion = 1

    @classmethod
    def register(cls, cls_render):
        """Register plain text render for specific type of questions"""
        PlainTextRender.renders[cls_render.name] = cls_render

    def create_header(self, solutions, preview, filename="../Previews/tmp1",
                      subject="Prueba", part="Prueba", section="prueba",
                      date=[], id=0, action="/report/", name="",
                      surname=""):
        """Create the header for the html doc"""

        if not date:
            year, month, day, h, min, sec, wday, yday, isdst = time.localtime()
            date.append(year)
            date.append(month-1)
            date.append(day)

        self.solutions = solutions
        try:
            self.textfile = open(filename + PlainTextRender.ext, 'w')
        except:
            logging.exception("Error creating the plain text doc")
        rdate = self.render_date(date)
        maxi = len(max(subject, part, section, rdate, key=len))
        content = """
      ********************************************************""" +  \
maxi * "*" + """
                        ..                                 """ + \
subject.decode('utf-8').encode('latin-') +  (maxi - len(subject)) * " " + "\n" + \
"""                 .7. = :.
                I,..$  ,I .7.+,:                           """ + \
part.decode('utf-8').encode('latin-') + (maxi - len(part)) * " " +  "\n" + \
"""             Z.DODOO8:NOZ$I.
                . 8$=Z$7ZOZN7+I8                           """ + \
section.decode('utf-8').encode('latin-') + (maxi - len(section)) * " " +  "\n" + \
"""              .8OZ???I??I$O.
                  .??NDDDDN8Z.
            .  ..                , .                       """ + \
rdate + (maxi - len(date)) * " " +  "\n" + \
"""        ,..,,               . =: .
         .~:,~:,        7      :~I=~?:
        :,=~==: .       7        ~:=~~~.
       .~,$?++~    O====$==   ?  .::?:~,
      ,.+~++~,  .Z~=====+===~O.    .,.+$.
     7::=$7=: . O+==~  .=??=.:Z     , ?~=,
     .+~?$=,   :Z==~    =   +==O     :+=$,
     ::=Z?:O   O8==     = I ??=~.    .~~~?.
     ,+~O+~:   OD==    .=I   +~==     :,===
     ,I:7?.+   ZO==     ~.   +Z~=.    ,==7.
    .+8?D=~~...?O~=+  : =    =+~~     ,=:=,
     ,:~$I=,    OD=~.8  =    +:=~     +?~~=
     +~~?Z~,I   ,Z~=+   =    ?==~    =,=I.
      ,,~+?=,+.   ZD===Z~..N~===    :.=7:.
       ~~:$~~:.  .8Z$==========.   ~I?$:~
      .$. ~I?=.  =  .778~===7.    .=~=+ .
        .,~~:O .       .         ++??~
         ..  ZDN                 ?~=,.
                 OO..  ..   . 7$.
                   DO$7+Z$?$ZD,
                 $O D?     .Z.8I

      ********************************************************""" + maxi * "*" + \
"\n \n"
        self.textfile.write(content)

    def render(self, question):
        """A general render for the wording of the question"""
        wording = str(self.numquestion) + ". "
        for content in question.wording:
            if isinstance(content,
                          editor.InteractivePangoBuffer):
                buff = content.buf
                txt = buff.get_text(buff.get_start_iter(),
                                    buff.get_end_iter(),
                                    True)
                wording += txt.decode('utf-8').encode('latin-') + "\n"
            else:
                wording += content.path + "\n"

        self.textfile.write(wording)

    def create_section(self, name):
        """Creates a new section"""
        content = """
--- """ + "-" * len(name) + "--- \n"
        content += "   " + name.decode("utf-8").encode("latin-") + "   \n"
        content += "---" + "-" * len(name) + "--- \n"
        self.textfile.write(content)

    def close_document(self):
        """Close the html document"""
        self.textfile.close()

    def visit(self, question):
        """Method to call the visitor to start the visit"""
        key = question.name
        try:
            sp_render = PlainTextRender.renders[key]
            sp_render(self.solutions,
                      self.numquestion).render(question,
                                               self.textfile)
        except KeyError:
            self.render(question)
        except:
            logging.exception("Error in a specific plaintexrender")
        finally:
            self.numquestion = self.numquestion + 1


def load():
    """Register this class as a type of available render"""
    import render
    render.Render.register(PlainTextRender)
