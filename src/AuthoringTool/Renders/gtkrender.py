# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to render the question dialogs"""

import sys
sys.path.append("../../Persistencia/")
sys.path.append("../")
try:
    import gtk
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
import logging

import render
import persistencia
import alert


class GtkRender(render.Render):
    """Generates a Gtk representation"""

    name = "GTK"
    icon = gtk.STOCK_EDIT
    renders = {}

    def __init__(self):
        self.agente = persistencia.Agente()

    @classmethod
    def register(cls, cls_render):
        """Registers gtk render for specific type of questions"""
#         border = "---------------------------------" + len(cls_render.name) * "-"
#         logging.info(border)
        logging.info("%s is registered in GTKRender" % cls_render.name)
#         logging.info(border)
        GtkRender.renders[cls_render.name] = cls_render

    def visit(self, question):
        """Method to call the visitor to start the visit"""
        logging.debug("Registered renders")
        for key, val in GtkRender.renders.items():
            logging.debug(str(key))
        logging.debug("Requested Render: %s" % question.name)

#         try:
        GtkRender.renders[question.name]().render(question)
#         except KeyError:
#             edata = alert.ErrorData()
#             msg = "No existe un render gtk para las preguntas de tipo " + \
#                 question.name.decode('latin-').encode('utf-8')
#             all_msg = "<big><b>No hay render</b></big> \n" + msg
#             edata.label_info_set_text(all_msg)
#         except:
#             print "Unexpected error:", sys.exc_info()[1]

    def _get_path(self, treeview, name):
        """Gets the path of an element of the treeview"""
        liststore = treeview.get_model()
        iter = liststore.get_iter_first()
        path = liststore.get_path(iter)
        path_result = None
        if name == liststore[path][0]:
            path_result = path
            return path_result
        else:
            while iter:
                iter = liststore.iter_next(iter)
                path = liststore.get_path(iter)
                logging.debug("Searching %s" % name)
                logging.debug("Comparing %s" % liststore[path][0])
                if name == liststore[path][0]:
                    path_result = path
                    return path_result

    def activate_terms(self, question, vocabularies):
        """Activate terms"""
        for term in question.terms:
            logging.debug("Vocabulary %s" % term.vocabulary.name)
            treeview = vocabularies[term.vocabulary.name]
            path = self._get_path(treeview, term.name)
            logging.debug("Path: %s", path)
            if path != None:
                logging.debug("Activatign term %s" % term.name)
                logging.debug("of the vocabulary %s" % term.vocabulary.name)
                logging.debug("with path: %s" % str(path))
                treeview.row_activated(path, treeview.get_column(0))

def load():
    """Registers this class as a type of available render"""
    import render
    render.Render.register(GtkRender)
