# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to work with different kinds of renders"""
import sys
sys.path.append('../')
import os
import logging
import zope.interface
import ifactory

LOG = logging.getLogger('render')

class Render:
    """ Class father of the different kinds of renders"""
    zope.interface.implements(ifactory.IFactory)

    renders = {}

    @classmethod
    def register(cls, cls_render):
        """Registers different kinds of renders"""
#         LOG.info("**************" + len(cls_render.name) * "*")
        LOG.info("%s is registered" % cls_render.name)
#         LOG.info("**************" + len(cls_render.name) * "*")
        Render.renders[cls_render.name] = cls_render

    @classmethod
    def create(cls, id):
        """Creates a specific render"""
        for key, val in Render.renders.items():
            LOG.debug("Render: %s" % str(key))
        LOG.debug("Requested render %s" % str(id))
        return Render.renders[id]()

    @classmethod
    def load_plugins(cls):
        """Loads of the available plugins"""
        for dir in os.listdir('./Renders/'):
            if not dir.startswith(".") and not dir.startswith("render.py") and \
                        not dir.startswith("#") and not dir.endswith(".pyc"):
                mod = __import__(os.path.splitext(dir)[0]).load()

    def create_header(self, solutions, preview, filename="../Previews/tmp1",
					  subject="Prueba", part="Prueba", section="prueba",
					  date=[2009,0,1], id=0, action="/report/", name="",
					  surname=""):
        """Creates the header for the exam representation"""
        pass

    def visit(self, question):
        """Method to call the visitor to start the visit"""
        pass

    def create_section(self, name):
        """Creates the section exam"""
        pass

    def close_part(self, num=0, last=False):
        """Closes the part exam"""
        pass

    def close_document(self):
        """Closes the exam"""
        pass

    def render_wording(self, question):
        """A general render for the wording of the question"""
        pass

    def render_date(self, date):
        """Months are 0-based"""
        months = ["enero", "febrero", "marzo", "abril", "mayo", "junio",
                  "julio", "agosto", "septiembre","octubre","noviembre",
                  "diciembre"]
        rndr = str(date[2]) + " de " + months[date[1]] + " de " + str(date[0])
        return rndr
