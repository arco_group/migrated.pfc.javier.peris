"""Factory interface"""

import zope.interface


def methods_invariant(ob):
    """A factory must have a create and register method"""
    txt = "you must provide a create method"
    assert(hasattr(ob,"create") and callable(ob.create)), txt
    txt = "you must provide a register method"
    assert(hasattr(ob,"register") and callable(ob.register)), txt

class IFactory(zope.interface.Interface):
    """Factory interface"""

    zope.interface.invariant(methods_invariant)

#     @classmethod
#     def create(cls, productId):
#         raise NotImplementedError

#     def register


