#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# this screencast is prepared for a screen size of 1200x900

import gtk
import thread
import sys
import os
import signal

import gext
#from arconotes import Notes

# avoid use of ScreenCaster... :P
gtk.gdk.threads_init()
thread.start_new_thread(gtk.main, tuple())

# gext objects
kb = gext.kb
ms = gext.ms

# globals
WIN_POS = None
SAD = None
rmd = None
notes = None


# FIXME: use pauser as a module!!
ncount = 1
def waitForOneNote(secs):
    if notes:
        notes.nextNote()
        gext.doWait(secs)
        notes.hideNote()
        gext.doWait(.5)

    # FIXME: use pauser as a module!!
    else:
        global ncount
        gext.launchApp("arconotes/tools/pauser.py 2 985 2 %d" % ncount)
        gext.doWait(2.5)
        ncount += 1


def setup():
    global WIN_POS, SAD, rmd

    # minimize terminal window and place mouse far away
    kb.generateKeyEvents("<Alt><F9>")
    ms.moveAbsTo(400, 525, wait=4)

    rmd = gext.launchApp("recordmydesktop -o %s --no-frame --no-sound "
                         "-fps 30  --width 1200 --height 900 " % sys.argv[1],
                         wait=1)

    #    launch TP, (FIXME: with a relative path?)
    os.chdir("../")
    sad = gext.launchApp("/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/sdocente", wait=1)
    sadwin = gext.Window("SAD*")
    assert sadwin.waitTillExists(wait=0), "AuthoringTool could not be launched"

    #get real window pos
    WIN_POS = sadwin.getGeometry().next()[2:]

def create_new_term():
    # Fill name
    ms.moveRelTo(-293,-165)
    ms.leftClick()
    kb.generateKeyEvents("DocBook",mode=kb.MODE_NATURAL)
    # Move to description
    ms.moveRelTo(176,0)
    ms.leftClick()
    # Fill description
    kb.generateKeyEvents("DocBook",mode=kb.MODE_NATURAL)
    ms.moveRelTo(103,54)
    ms.leftClick()

def create_new_category():
    # Fill name
    ms.moveRelTo(25,-167)
    ms.leftClick()
    kb.generateKeyEvents("Sistemas de comunicaciones",mode=kb.MODE_NATURAL)
    # Move to description
    ms.moveRelTo(176,0)
    ms.leftClick()
    # Fill description
    kb.generateKeyEvents("Prueba",mode=kb.MODE_NATURAL)
    ms.moveRelTo(103,54)
    ms.leftClick()

def performActions():
    # move to categories tab
    ms.moveAbsTo(562,70)
    ms.leftClick()

    # # select lenguajes de marcado category
    ms.moveRelTo(-92,104)
    ms.leftClick()

    # click to add term button
    ms.moveRelTo(508,506)
    ms.leftClick()

    create_new_term()

    # click to add category button
    ms.moveAbsTo(660,678)
    ms.leftClick()

    create_new_category()

    # edit Materias category
    ms.moveAbsTo(453,150)
    ms.leftClick()
    ms.leftClick()
    kb.generateKeyEvents("<BackSpace>", mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("Asignaturas", mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("<Enter>", mode=kb.MODE_NATURAL)

    # move to questions tab
    ms.moveAbsTo(460,70)
    ms.leftClick()

    # expand the categories
    ms.moveRelTo(20,89)
    ms.leftClick()

    # move to the right scrolledbar
    ms.moveRelTo(552,69)
    ms.leftClick()

    ms.dragRelTo(0,60)
    gext.doWait(3)

    # scroll the new term
    ms.moveRelTo(-15,-73)
    ms.dragRelTo(0,15)

    gext.doWait(3)

    ms.moveRelTo(15,58)
    ms.dragRelTo(0,63)

    gext.doWait(3)

    # ms.moveRelTo(-16,-69)
    # ms.dragRelTo(0,20)

    # categories tab
    ms.moveAbsTo(562,70)
    ms.leftClick()

    ms.moveAbsTo(450,214)
    ms.leftClick()

    ms.moveAbsTo(500,677)
    ms.leftClick()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: %s video_name.ogv [notes_file]" % sys.argv[0]
        sys.exit(-1)

    setup()
    performActions()
    os.kill(rmd.pid, signal.SIGTERM)
    print "OK"
