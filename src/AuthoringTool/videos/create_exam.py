#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# this screencast is prepared for a screen size of 1200x900

import gtk
import thread
import sys
import os
import signal

import gext
#from arconotes import Notes

# avoid use of ScreenCaster... :P
gtk.gdk.threads_init()
thread.start_new_thread(gtk.main, tuple())

# gext objects
kb = gext.kb
ms = gext.ms

# globals
WIN_POS = None
SAD = None
rmd = None
notes = None


# FIXME: use pauser as a module!!
ncount = 1
def waitForOneNote(secs):
    if notes:
        notes.nextNote()
        gext.doWait(secs)
        notes.hideNote()
        gext.doWait(.5)

    # FIXME: use pauser as a module!!
    else:
        global ncount
        gext.launchApp("arconotes/tools/pauser.py 2 985 2 %d" % ncount)
        gext.doWait(2.5)
        ncount += 1


def setup():
    global WIN_POS, SAD, rmd

    # minimize terminal window and place mouse far away
    kb.generateKeyEvents("<Alt><F9>")
    ms.moveAbsTo(400, 525, wait=4)

    rmd = gext.launchApp("recordmydesktop -o %s --no-frame --no-sound "
                         "-fps 30  --width 1200 --height 900 " % sys.argv[1],
                         wait=1)

    #    launch TP, (FIXME: with a relative path?)
    os.chdir("../")
    sad = gext.launchApp("/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/sdocente", wait=0)
    sadwin = gext.Window("SAD*")
    assert sadwin.waitTillExists(wait=0), "AuthoringTool could not be launched"

    #get real window pos
    WIN_POS = sadwin.getGeometry().next()[2:]

def select_questions():
    # Select/Unselect all questions
    ms.moveRelTo(193,162)
    ms.leftClick()
    gext.doWait(4)
    ms.leftClick()

    # Show the tooltip of the first question
    ms.moveAbsTo(713,503)
    gext.doWait(2)
    # Drag n' drop the first question
    ms.dragRelTo(-504,-180)

    # Expand part
    ms.moveAbsTo(46,148)
    ms.leftClick()
    # Expand section
    ms.moveRelTo(16,22)
    ms.leftClick()

    # Select the second question
    ms.moveAbsTo(713,541)
    ms.leftClick()
    # Select questions
    for i in range(0,3):
        ms.moveRelTo(0,38)
        ms.leftClick()

    # move to bankquestion scrolledbar
    ms.moveAbsTo(1031,490)
    ms.dragRelTo(0,58)

    ms.moveAbsTo(743,490)
    ms.leftClick()
    # Select questions
    for i in range(0,3):
        ms.moveRelTo(0,37)
        ms.leftClick()

def config_exam():
    # Fill title
    ms.moveAbsTo(690,248)
    ms.leftClick()
    kb.generateKeyEvents("Examen Final",mode=kb.MODE_NATURAL)

    # Fill description
    # ms.moveRelTo(0,40)
    # ms.leftClick()
    # kb.generateKeyEvents("cadena",mode=kb.MODE_NATURAL)

    # Select subject
    ms.moveRelTo(0,80)
    ms.leftClick()
    ms.moveRelTo(0,260)
    ms.leftClick()

    # Open exam
    ms.moveAbsTo(591,476)
    ms.leftClick()
    # Select day
    ms.moveRelTo(309,15)
    ms.leftDoubleClick()
    # Select hour
    ms.moveRelTo(204,-59)
    ms.leftDoubleClick()
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("10",mode=kb.MODE_NATURAL)
    ms.moveRelTo(0,90)
    ms.leftDoubleClick()
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("0",mode=kb.MODE_NATURAL)

    # Move abs to close exam combobox
    ms.moveAbsTo(591,671)
    ms.leftClick()
    # Select day
    ms.moveRelTo(309,15)
    ms.leftDoubleClick()
    ms.moveRelTo(204,-59)
    ms.leftDoubleClick()
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("14",mode=kb.MODE_NATURAL)
    ms.moveRelTo(0,90)
    ms.leftDoubleClick()
    kb.generateKeyEvents("<BackSpace>",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("0",mode=kb.MODE_NATURAL)

    # Click apply button
    ms.moveRelTo(-19,120)
    ms.leftClick()

def select_output_menu():
    ms.moveAbsTo(261,66)
    ms.leftClick()

def performActions():
    # Move to categories treeview in question tab
    ms.moveAbsTo(480,159)
    ms.leftClick()
    # Select Cine category
    ms.moveRelTo(21,152)
    ms.leftClick()

    select_questions()

    # Add questions to exam (<< button)
    ms.moveAbsTo(430,392)
    ms.leftClick()

    # # Show parts (si no estuvieran desplegadas, ahora no es necesario)
    # ms.moveAbsTo(45,148)
    # ms.leftClick()
    # # Show sections
    # ms.moveAbsTo(62,170)
    # ms.leftClick()

    # Show the menu to add a part
    ms.moveAbsTo(113,428)
    ms.rightClick()
    gext.doWait(2)

    # Show the menu to add a section
    ms.moveAbsTo(45,148)
    ms.leftClick()
    # ms.leftClick()
    ms.rightClick()

    gext.doWait(2)

    # Show editar menu
    ms.moveAbsTo(94,65)
    ms.leftClick()
    ms.leftClick()
    gext.doWait(2)

    # Show configurar menu
    ms.moveAbsTo(200,64)
#    ms.leftClick()
    gext.doWait(4)
    ms.moveAbsTo(200,90)
    ms.leftClick()

    config_exam()

    # Show salida menu
    select_output_menu()
    gext.doWait(5)

    # Select output formats
    ms.moveRelTo(0,51)
    gext.doWait(3)
    ms.leftClick()

    select_output_menu()
    ms.moveRelTo(0,75)
    ms.leftClick()

    select_output_menu()
    ms.moveRelTo(0,100)
    ms.leftClick()

    # Save exam
    ms.moveAbsTo(351,641)
    ms.leftClick()

    gext.doWait(5)

    # Exam tab
    ms.moveAbsTo(669,68)
    ms.leftClick()

    ms.moveRelTo(359,187)
    ms.leftClick()
    ms.dragRelTo(0,280)
    ms.moveRelTo(-256,118)
    ms.rightClick()
    ms.rightClick()
    gext.doWait(3)

    # Preview a collection of questions
    ms.moveAbsTo(464,64)
    ms.leftClick()
    ms.leftClick()
    # ms.moveAbsTo(1030,580)
    # ms.dragRelTo(0,-81)

    ms.moveAbsTo(494,431)
    ms.leftClick()
    gext.doWait(2)
    ms.moveAbsTo(1176,220)
    ms.dragRelTo(0,100)
    gext.doWait(2)
    ms.dragRelTo(0,270)
    gext.doWait(2)
    kb.generateKeyEvents("<Alt><F9>")
    gext.doWait(2)

    ms.moveAbsTo(525,431)
    ms.leftClick()
    gext.doWait(4)
    ms.moveAbsTo(1170,220)
    ms.dragRelTo(0,130)
    gext.doWait(2)
    kb.generateKeyEvents("<Alt><F9>")
    gext.doWait(2)

    ms.moveAbsTo(564,431)
    ms.leftClick()
    gext.doWait(6)
    # ms.moveAbsTo(1176,220)
    # ms.dragRelTo(0,100)
    gext.doWait(5)
    # ms.dragRelTo(0,170)
    # gext.doWait(2)
    kb.generateKeyEvents("<Alt><F9>")
    gext.doWait(2)



    # ms.moveAbsTo(142,66)
    # ms.leftClick()
    # ms.leftClick()
    # ms.moveRelTo(0,22)
    # ms.leftClick()

    # gext.doWait(6)
    # kb.generateKeyEvents("<Alt><F9>")
    # gext.doWait(2)

    # ms.moveAbsTo(142,66)
    # ms.leftClick()
    # ms.moveRelTo(0,44)
    # ms.leftClick()

    # gext.doWait(6)
    # kb.generateKeyEvents("<Alt><F9>")
    # gext.doWait(2)

    # ms.moveAbsTo(142,66)
    # ms.leftClick()
    # ms.moveRelTo(0,66)
    # ms.leftClick()

    # gext.doWait(6)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: %s video_name.ogv [notes_file]" % sys.argv[0]
        sys.exit(-1)

    setup()
    performActions()
    os.kill(rmd.pid, signal.SIGTERM)

    print "OK"
