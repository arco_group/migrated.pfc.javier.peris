#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# this screencast is prepared for a screen size of 1200x900

import gtk
import thread
import sys
import os
import signal

import gext
from arconotes import Notes

# avoid use of ScreenCaster... :P
gtk.gdk.threads_init()
thread.start_new_thread(gtk.main, tuple())

# gext objects
kb = gext.kb
ms = gext.ms

# globals
WIN_POS = None
SAD = None
rmd = None
notes = None


# FIXME: use pauser as a module!!
ncount = 1
def waitForOneNote(secs):
    if notes:
        notes.nextNote()
        gext.doWait(secs)
        notes.hideNote()
        gext.doWait(.5)

    # FIXME: use pauser as a module!!
    else:
        global ncount
        gext.launchApp("arconotes/tools/pauser.py 2 985 2 %d" % ncount)
        gext.doWait(2.5)
        ncount += 1


def setup():
    global WIN_POS, SAD, rmd

    # minimize terminal window and place mouse far away
    kb.generateKeyEvents("<Alt><F9>")
    ms.moveAbsTo(400, 525)

    rmd = gext.launchApp("recordmydesktop -o %s --no-frame --no-sound "
                         "-fps 30  --width 1200 --height 900 " % sys.argv[1],
                         wait=1)

    #    launch TP, (FIXME: with a relative path?)
    sad = gext.launchApp("/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/sdocente --geometry 457x527+104+120", wait=0)
    sadwin = gext.Window("SAD*")
    assert sadwin.waitTillExists(wait=0), "AuthoringTool could not be launched"

    #get real window pos
    WIN_POS = sadwin.getGeometry().next()[2:]

def create_new_term():
    # Fill name
    ms.leftClick()
    ms.generateKeyEvents("DocBook",mode=gext.MODE_NATURAL)
    # Move to description
    ms.moveRelTo(176,0)
    ms.leftClick()
    # Fill description
    ms.generateKeyEvents("DocBook",mode=gext.MODE_NATURAL)
    ms.moveRelTo(103,54)
    ms.leftClick()

def do_exam():
    pass

def enter():
    # write user
    ms.moveRelTo(242,285)
    ms.leftClick()
    ms.generateKeyEvents("test",mode=gext.MODE_NATURAL)

    # write password
    ms.moveRelTo(0,27)
    ms.leftClick()
    ms.generateKeyEvents("sdocente",mode=gext.MODE_NATURAL)

    # click entrar button
    ms.moveRelTo(117,29)
    ms.leftClick()

def performActions():
    # write url
    ms.moveAbsTo(253,111)
    ms.leftClick()
    ms.generateKeyEvents("http://draper.mine.nu",mode=gext.MODE_NATURAL)
    ms.generateKeyEvents("[ENTER]")
    ms.doWait(3)

    enter()

    # close save exam
    ms.moveAbsTo(1024,147)
    ms.leftClick()

    do_exam()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: %s video_name.ogv [notes_file]" % sys.argv[0]
        sys.exit(-1)

    setup()
    performActions()

    print "OK"
