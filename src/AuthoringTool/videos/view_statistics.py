#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# this screencast is prepared for a screen size of 1200x900

import gtk
import thread
import sys
import os
import signal

import gext
#from arconotes import Notes

# avoid use of ScreenCaster... :P
gtk.gdk.threads_init()
thread.start_new_thread(gtk.main, tuple())

# gext objects
kb = gext.kb
ms = gext.ms

# globals
WIN_POS = None
SAD = None
rmd = None
notes = None

# FIXME: use pauser as a module!!
ncount = 1
def waitForOneNote(secs):
    if notes:
        notes.nextNote()
        gext.doWait(secs)
        notes.hideNote()
        gext.doWait(.5)

    # FIXME: use pauser as a module!!
    else:
        global ncount
        gext.launchApp("arconotes/tools/pauser.py 2 985 2 %d" % ncount)
        gext.doWait(2.5)
        ncount += 1


def setup():
    global WIN_POS, SAD, rmd

    # minimize terminal window and place mouse far away
    kb.generateKeyEvents("<Alt><F9>")
    ms.moveAbsTo(400, 525)

    rmd = gext.launchApp("recordmydesktop -o %s --no-frame --no-sound "
                         "-fps 30  --width 1200 --height 900 " % sys.argv[1],
                         wait=1)

    #    launch TP, (FIXME: with a relative path?)
#    sad = gext.launchApp("iceweasel --geometry 1100x800+5+30", wait=0)
    icewin = gext.Window("Icew*")
    assert icewin.waitTillExists(wait=0), "AuthoringTool could not be launched"

    #get real window pos
    WIN_POS = icewin.getGeometry().next()[2:]


def enter():
    # write user
    ms.moveRelTo(278,284)
    ms.leftClick()
    kb.generateKeyEvents("bacterio",mode=kb.MODE_NATURAL)
#    kb.generateKeyEvents("<Enter>")

    # write password
    ms.moveRelTo(0,27)
    ms.leftClick()
    kb.generateKeyEvents("prueba",mode=kb.MODE_NATURAL)

    # click entrar button
    ms.moveRelTo(119,30)
    ms.leftClick()

def performActions():
    # write url
    ms.moveAbsTo(255,88)
    ms.leftClick()
    kb.generateKeyEvents("http://draper.mine.nu",mode=kb.MODE_NATURAL)
    kb.generateKeyEvents("<Enter>")
    sadwin = gext.Window("SAD*")
    assert sadwin.waitTillExists(wait=2), "SAD could not be launched"

    enter()
    gext.doWait(3)
    # # Close save password
    # ms.moveAbsTo(1024,147)
    # ms.leftClick()

    # Show statistics section
    ms.moveAbsTo(769,512)
    ms.leftClick()

    # Select a test
    ms.moveRelTo(-475,-130)
    ms.leftClick()
    gext.doWait(3)

    # Select a question
    ms.moveRelTo(265,33)
    ms.leftClick()

    # Show statistics
    ms.moveRelTo(127,183)
    ms.leftClick()
    gext.doWait(4)

    # Show statistics section
    ms.moveAbsTo(769,512)
    ms.leftClick()

    # Select a test
    ms.moveRelTo(-475,-130)
    ms.leftClick()
    gext.doWait(3)

    # Select all questions
    ms.moveRelTo(250,-20)
    ms.leftClick()

    ms.moveRelTo(135,230)
    ms.leftClick()
    gext.doWait(4)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: %s video_name.ogv [notes_file]" % sys.argv[0]
        sys.exit(-1)

    setup()
    performActions()
    os.kill(rmd.pid, signal.SIGTERM)

    print "OK"
