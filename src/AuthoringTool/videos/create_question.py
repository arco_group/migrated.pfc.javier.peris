#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# this screencast is prepared for a screen size of 1200x900

import gtk
import thread
import sys
import os
import signal

import gext
#from arconotes import Notes

# avoid use of ScreenCaster... :P
gtk.gdk.threads_init()
thread.start_new_thread(gtk.main, tuple())

# gext objects
kb = gext.kb
ms = gext.ms

# globals
WIN_POS = None
SAD = None
rmd = None
notes = None


# FIXME: use pauser as a module!!
ncount = 1
def waitForOneNote(secs):
    if notes:
        notes.nextNote()
        gext.doWait(secs)
        notes.hideNote()
        gext.doWait(.5)

    # FIXME: use pauser as a module!!
    else:
        global ncount
        gext.launchApp("arconotes/tools/pauser.py 2 985 2 %d" % ncount)
        gext.doWait(2.5)
        ncount += 1


def setup():
    global WIN_POS, SAD, rmd

    # minimize terminal window and place mouse far away
    kb.generateKeyEvents("<Alt><F9>")
    ms.moveAbsTo(400, 525,wait=4)

    rmd = gext.launchApp("recordmydesktop -o %s --no-frame --no-sound "
                         "-fps 30  --width 1200 --height 900 " % sys.argv[1],
                         wait=1)

    #    launch TP, (FIXME: with a relative path?)
    os.chdir("../")
    sad = gext.launchApp("/home/peris/Desktop/csl2-sdocente/src/AuthoringTool/sdocente --geometry 457x527+104+120", wait=0)
    sadwin = gext.Window("SAD*")
    assert sadwin.waitTillExists(wait=0), "AuthoringTool could not be launched"

    #get real window pos
    WIN_POS = sadwin.getGeometry().next()[2:]

def fill_question():
    # Fill title
    ms.moveAbsTo(491,152)
    ms.leftClick()
    kb.generateKeyEvents("El show de Truman",mode=kb.MODE_NATURAL)
    # Move scrolledbar
    ms.moveRelTo(274,114)
    ms.leftClick()
    ms.dragRelTo(0,30)
    # Fill the wording
    ms.moveRelTo(-585,-39)
    ms.leftClick()
    kb.generateKeyEvents("Como acaba el show de Truman",mode=kb.MODE_NATURAL)
    # From categories to mark 1
    ms.moveRelTo(306,146)
    ms.leftClick()
    kb.generateKeyEvents("1",mode=kb.MODE_NATURAL)
    # Move to answer 1
    ms.moveRelTo(-211,29)
    ms.leftClick()
    kb.generateKeyEvents("Truman abandona el mundo irreal en el que vive",mode=kb.MODE_NATURAL)
    # Move to mark 2
    ms.moveRelTo(214,117)
    ms.leftClick()
    kb.generateKeyEvents("0",mode=kb.MODE_NATURAL)
    # Move to answer 2
    ms.moveRelTo(-214,27)
    ms.leftClick()
    kb.generateKeyEvents("Truman muere ahogado en el mar",mode=kb.MODE_NATURAL)
    # Move to mark 3
    ms.moveRelTo(213,116)
    ms.leftClick()
    kb.generateKeyEvents("0",mode=kb.MODE_NATURAL)
    # Move to answer 3
    ms.moveRelTo(-209,25)
    ms.leftClick()
    kb.generateKeyEvents("Truman inicia su carrera de cantante",mode=kb.MODE_NATURAL)

    # Move to categories
    ms.moveAbsTo(184,118)
    ms.leftClick()

    # Select category cine
    ms.moveRelTo(12,147)
    ms.leftClick()

    ms.moveRelTo(-12,-147)
    ms.leftClick()

    # Move to accept button
    ms.moveAbsTo(730,843)
    ms.leftClick()

def show_the_added_question():
    # Move to categories treeview in question tab
    ms.moveAbsTo(480,159)
    ms.leftClick()
    # Select Cine category
    ms.moveRelTo(21,149)
    ms.leftClick()
    # Move to the questionbank scrolledbar
    ms.moveRelTo(532,221)
    # Show the added_question
    ms.dragRelTo(0,120)
    ms.moveRelTo(-262,12)
    gext.doWait(5)


def performActions():
    # move to questions combobox
    ms.moveAbsTo(1010,267)
    ms.leftClick()
    # select choicemultiple question
    ms.moveRelTo(0,52)
    ms.leftClick()

    fill_question()

    show_the_added_question()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: %s video_name.ogv [notes_file]" % sys.argv[0]
        sys.exit(-1)

    setup()
    performActions()
    os.kill(rmd.pid, signal.SIGTERM)

    print "OK"
