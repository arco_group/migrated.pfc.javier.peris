# -*- coding: iso-8859-15; tab-width:4 -*-

"""Exceptions Module of AuthoringTool"""
class DataException(Exception):
    """Some wrong data was put in the dialog"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


# class NotImplementedError(Exception):
#     """A method is not implemented"""
#     pass


class WeightException(Exception):
    """The question weight is not a number"""
    pass


class TermAlreadyExistsException(Exception):
    """The term is trying to put in th db already exists"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


class NotTermException(Exception):
    """The term is trying to put in th db already exists"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


class NotVocabularyException(Exception):
    """The term is trying to put in th db already exists"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


class VocabularyAlreadyExistsException(Exception):
    """The term is trying to put in th db already exists"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


class NotQuestionException(Exception):
    """The question does not exists"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return "Error " + str(self.msg)


# class TitleException(Exception):

#     def __init__(self, msg):
#         self.msg = msg

#     def __str__(self):
#         return "Error " + str(self.msg)


# class DefaultMarkException(Exception):
#     ""

#     def __init__(self, msg):
#         self.msg = msg

#     def __str__(self):
#         return "Error " + str(self.msg)


# class WordingException(Exception):
#     ""

#     def __init__(self, msg):
#         self.msg = msg

#     def __str__(self):
#         return "Error " + str(self.msg)


# class ExpectedLengthException(Exception):
#     ""

#     def __init__(self, msg):
#         self.msg = msg

#     def __str__(self):
#         return "Error " + str(self.msg)


# class SolutionsException(Exception):
#     ""

#     def __init__(self, msg):
#         self.msg = msg

#     def __str__(self):
#         return "Error " + str(self.msg)
