import os

LANGS = ["en_GB", "en_US", "it_IT"]

for lan in LANGS:
    print "IDIOMA:", lan
    print "CREAMOS DIRECTORIO locale/" + lan + "/LC_MESSAGES"
    print "CREAMOS /tmp/outAux.po CON  ../Templates/po/" + lan + "_template.po"
    os.system("mkdir -p locale/" + lan + "/LC_MESSAGES")
    os.system("cat ../Templates/po/" + lan + "-template.po > /tmp/outAux.po")
    for path in os.listdir('./po/'):
        if path.startswith(lan) and path.endswith(".po"):
            print "MSGMERGE DE /tmp/outAux.po Y", path, "SALIDA /tmp/outAux.po"
            os.system('msgmerge /tmp/outAux.po po/' + path + " -o /tmp/outAux.po")
    for path in os.listdir('./Questions/'):
        plugin_path = './Questions/' + path
        if os.path.isdir(plugin_path) and not path.startswith(".svn"):
            print "PLUGIN:", path
            path_po = 'Questions/' + path + "/po"
            if os.path.exists(path_po) and os.path.isdir(path_po):
                print "Tiene traduccion"
                for po_file in os.listdir(path_po):
                    if po_file.startswith(lan) and po_file.endswith(".po"):
                        print "MSGMERGE outAux.PO y Questions/" + path + "po/" + po_file + " en /tmp/outAux.po"
                        os.system('msgmerge /tmp/outAux.po Questions/' + path + '/po/' + po_file + ' -o /tmp/outAux.po')
    print "sed -i 's/\#~ //g' /tmp/outAux.po"
    print "msgfmt /tmp/outAux.po -o locale/"+ lan + "/LC_MESSAGES/SAD.mo"
    os.system("sed -i 's/\#~ //g' /tmp/outAux.po")
    os.system("msgfmt /tmp/outAux.po -o locale/"+ lan + "/LC_MESSAGES/SAD.mo")


