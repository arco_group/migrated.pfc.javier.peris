# -*- mode: python; coding: utf-8 -*-

import logging

class ColorFormatter(logging.Formatter):
    "A formatter wich add support on logging for colors."

    codes = {\
        None:       (0, 0,   0), # default
        'DEBUG':    (0, 0,   2), # gray
        'INFO':     (0, 0,   0), # normal
        'WARNING':  (38,5, 208), # orange
        'ERROR':    (0, 1,  31), # red
        'CRITICAL': (0, 1, 101), # black with red background
        }

    def color(self, level=None):
        return (chr(27)+'[%d;%d;%dm') % self.codes[level]


    def format(self, record):
        retval = logging.Formatter.format(self, record)
        return self.color(record.levelname) + retval + self.color()
