# -*- coding: iso-8859-15; tab-width:4 -*-

"""Testpart module provides the testpart class"""
import logging

class TestPart:
    """ Each test is divided into one or more parts which may in turn
    be divided into sections. A testPart represents a major division
    of the test."""

    def __init__(self, ide, sections):
        self.id = ide
        self.sections = sections

    def __str__(self):
        """Returns the representation of the term"""
        msg = "Parte: " + self.id
        for sec in self.sections:
            msg += sec.__str__()
        return msg

    def __eq__(self, tpart):
        """Returns whether two testparts are equals"""
        if self.id == tpart.id and self.sections == tpart.sections:
            logging.debug("Testparts are equals")
            return True
        logging.debug("Testparts are NOT equals")
        return False

    def __ne__(self, tpart):
        """Returns whether two testparts are not equals"""
        result = self.__eq__(tpart)
        return not result

    def add_section(self, section):
        """Adds a section to the testpart"""
        self.sections.append(section)
