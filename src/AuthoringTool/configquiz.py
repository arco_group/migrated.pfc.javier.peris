# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to control the dialog to configure the general exam parameters"""


import time
import sys
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)
import logging
import datetime

import alert
import exception


class ConfigQuiz:
    """Control the dialog to configure an exam"""

    def __init__(self, glade_file, subjects):
        """Load the dialog"""

        import locale
        import gettext
        import os

        realpath = os.path.realpath('.')
        DIR = realpath + '/locale'

        locale.setlocale(locale.LC_ALL, '')
        # gettext.bindtextdomain("error", DIR)
        # gettext.textdomain("error")
        gtk.glade.bindtextdomain("confiquiz", DIR)
        gtk.glade.textdomain("confiquiz")

        self.widgets = gtk.glade.XML(glade_file)
        self.widgets.signal_autoconnect(self)
        self.cb_opentest = self.widgets.get_widget("checkbuttonOpenTest")
        self.cb_closetest = self.widgets.get_widget("checkbuttonCloseTest")
        self.cal_open = self.widgets.get_widget("calendarOpen")
        self.cal_close = self.widgets.get_widget("calendarClose")
        self.sb_houropen = self.widgets.get_widget("spinbuttonHourOpen")
        self.sb_minuteopen = self.widgets.get_widget("spinbuttonMinuteOpen")
        self.sb_hourclose = self.widgets.get_widget("spinbuttonHourClose")
        self.sb_minuteclose = self.widgets.get_widget("spinbuttonMinuteClose")
        self.cb_maxtime = self.widgets.get_widget("checkbuttonMaxTime")
        self.e_maxtime = self.widgets.get_widget("entryMaxTime")
        self.e_maxtime.set_sensitive(False)
        self.e_title = self.widgets.get_widget("entryTitle")
        self.e_description = self.widgets.get_widget("entryDescription")
        self.render_current_date()
        self.correcto = False
        self.max_time = None
        self.dopen = None
        self.dclose = None
        self.cb_subjects = self.widgets.get_widget("comboboxSubjects")
        self.cb_subjects.set_active(0)
        for subject in subjects:
            u_subject = subject.decode("latin-").encode("utf-8")
            self.cb_subjects.append_text(u_subject)

    def render_current_date(self):
        """Selects the current date in the calendar widgets"""
        year, month, day, hour, min, sec, wday, yday, isdst = time.localtime()
        min += 1
        date_widgets = [self.cal_open, self.cal_close]
        hour_widgets = [self.sb_houropen, self.sb_hourclose]
        min_widgets = [self.sb_minuteopen, self.sb_minuteclose]
        for i in range(2):
            date_widgets[i].select_month(month-1, year)
            date_widgets[i].select_day(day)
            date_widgets[i].set_sensitive(False)
            hour_widgets[i].set_value(hour)
            hour_widgets[i].set_sensitive(False)
            min_widgets[i].set_value(min)
            min_widgets[i].set_sensitive(False)

    def run(self):
        """Runs the dialog"""
        end = False
        self.render_current_date()
        self.cb_opentest.set_active(False)
        self.cb_closetest.set_active(False)
        self.cb_maxtime.set_active(False)
        while(not end):
            try:
                response = self.widgets.get_widget("dialogConfig").run()
                end = True
                if response == 0:
                    self.widgets.get_widget("dialogConfig").hide()
                elif response == 1:
                    self.validate()
                elif response == gtk.RESPONSE_DELETE_EVENT:
                    self.widgets.get_widget("dialogConfig").hide()
            except exception.DataException:
                end = False

    def checkbuttonMaxTime_toggled(self, widget, data=None):
        """Controls the sensitive of the entry for a maximum time"""
        if widget.get_active():
            self.e_maxtime.set_sensitive(True)
        else:
            self.e_maxtime.set_sensitive(False)

    def checkbuttonOpenTest_toggled(self, widget, data=None):
        """ Sets widgets'sensitivity based on checkbutton widget"""
        w_open_date = [self.cal_open, self.sb_houropen, self.sb_minuteopen]
        for i in range(len(w_open_date)):
            w_open_date[i].set_sensitive(widget.get_active())

    def checkbuttonCloseTest_toggled(self, widget, data=None):
        """ Sets widgets'sensitivity based on checkbutton widget"""
        w_close_date = [self.cal_close, self.sb_hourclose, self.sb_minuteclose]
        for i in range(len(w_close_date)):
            w_close_date[i].set_sensitive(widget.get_active())

    def validate(self):
        """Validates the user info"""
        self.title = self.e_title.get_text().decode("utf-8").encode("latin-")
        self.des = self.e_description.get_text()
        self.des = self.des.decode("utf-8").encode("latin-")
#         hasDateOpen = False; hasDateClose = False
        has_maxtime = False
        self.correcto = True
        msg = ""
        if self.cb_maxtime.get_active():
#             has_maxtime = True
            self.max_time = self.e_maxtime.get_text()
            try:
                self.max_time = int(self.max_time)
            except ValueError:
                self.correcto = False
                msg += "El tiempo debe ser un numero entero\n"

        if self.cb_opentest.get_active() and self.cb_closetest.get_active():
#             hasDateOpen = True
            self.date_open = list(self.cal_open.get_date())
            self.date_open.append(self.sb_houropen.get_value_as_int())
            self.date_open.append(self.sb_minuteopen.get_value_as_int())
            self.dopen = datetime.datetime(self.date_open[0],
                                        self.date_open[1] + 1,
                                        self.date_open[2], self.date_open[3],
                                        self.date_open[4])
            current = datetime.datetime.now()
            if current > self.dopen:
                msg += "La fecha de inicio no puede ser anterior a la actual"
                self.correcto = False

            if self.correcto and self.cb_closetest.get_active():
                self.date_close = list(self.cal_close.get_date())
                self.date_close.append(self.sb_hourclose.get_value_as_int())
                self.date_close.append(self.sb_minuteclose.get_value_as_int())
                logging.debug("Open date: %s" % self.date_open.__str__())
                logging.debug("Close date: %s" % self.date_close.__str__())
                for i in range(0, 5):
                    if self.date_open[i] > self.date_close[i]:
                        self.correcto = False
                        break
                    elif self.date_open[i] < self.date_close[i]:
                        break
                    else:
                        if i == 4:
                            self.correcto = False

                self.dclose = datetime.datetime(self.date_close[0],
                                            self.date_close[1] + 1,
                                            self.date_close[2],
                                            self.date_close[3],
                                            self.date_close[4])

                if self.correcto:
                    if has_maxtime:
                        ddate = self.dclose - self.dopen
                        dds = ddate.days*24*3600
                        if hasattr(ddate, "seconds"):
                            dds += ddate.seconds
                        if dds < int(self.max_time) * 60:
                            self.correcto = False
                            msg += "El tiempo m�ximo supera la fecha de cierre"
                else:
                    msg += "La fecha de cierre debe ser posterior a la de" + \
                        "inicio"
        else:
            self.correcto = False
            msg += "Ha de especificar el d�a de apertura del examen\n"

        if self.title == "":
            msg += "Se debe rellenar el titulo del test\n"
            self.correcto = False

        self.subject = self.cb_subjects.get_active_text()
        if not self.get_subject():
            self.correcto = False
            msg += "Debe seleccionar una asignatura\n"
        if not self.correcto:
            error_msg = msg.decode("latin-").encode("utf-8")
            all_msg = "<big><b>Datos incorrectos</b></big> \n \n" + error_msg
            edata = alert.ErrorData()
            edata.label_info_set_text(all_msg)
            raise exception.DataException(error_msg)
        else:
            self.widgets.get_widget("dialogConfig").hide()
        logging.debug("Subject: %s" % self.subject)

    def get_subject(self):
        """Returns the selected subject"""
        if self.subject == "------------------------------------------":
            self.subject = None
        return self.subject

    def get_open_date(self):
        """Returns the open date"""
        return self.dopen

    def get_close_date(self):
        """Returns the close date"""
        return self.dclose

    def get_max_time(self):
        """Returns the maximum time"""
        return self.max_time

    def mtime_toggled(self):
        """Toggles the checkbutton related to the maximum time"""
        self.cb_maxtime.toggled()

    def set_title(self, txt):
        """Sets the title in the dialog"""
        self.e_title.set_text(txt)

    def set_open_date(self, date):
        """Sets the open date in the calendar and spinbuttons"""
        self.cb_opentest.select_month(date[0], date[1])
        self.cb_opentest.select_day(date[2])
        self.sb_houropen.set_value(date[3])
        self.sb_minuteopen.set_value(date[4])

    def set_close_date(self, date):
        """Sets the close date in the calendar and spinbuttons"""
        self.cb_closetest.select_month(date[0], date[1])
        self.cb_closetest.select_day(date[2])
        self.sb_hourclose.set_value(date[3])
        self.sb_minuteclose.set_value(date[4])

    def set_subject(self, subject):
        """Sets the subject in the combobox"""
        print "MODELO DE ASIGNATURAS ", self.cb_subjects.get_model()

