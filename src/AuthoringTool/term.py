# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module to work with terms"""


class Term:
    """Represents an element which a group of questions have any connection"""

    def __init__(self, name, description, vocabulary, questions=None):
        self.name = name
        self.description = description
        self.vocabulary = vocabulary
        self.questions = questions

#     def set_questions(self, questions):
#         self.questions = questions

    def __str__(self):
        return self.name
