# -*- coding: iso-8859-15; tab-width:4 -*-

"""Module which controls the behaviour of the categories tab"""

import sys
sys.path.append('../Persistencia')
import gtk

import persistencia
import vocabulary
import term
import alert
import exception

import logging
LOG = logging.getLogger('tabcategories')

class TabCategories:
    """Controls the interaction with the categories tab"""

    def __init__(self, widgets, tabquestions):
        self.widgets = widgets
        self.widgets.signal_autoconnect(self)
        self.tabquestions = tabquestions
        self.dCreateVocabulary = self.widgets.get_widget("d_CreateVocabulary")
        self.dCreateTerm = self.widgets.get_widget("dialogCreateTerm")
        self.eDes_vocabulary = self.widgets.get_widget("entryDesVocabulary")
        self.eName_vocabulary = self.widgets.get_widget("entryVocabulary")
        self.agente = persistencia.Agente()
        self.dict_vocabularies = {}

    def load_categories_tab(self):
        """Loads the treeviews with the corresponding info"""
        treeviews = [gtk.TreeView(), gtk.TreeView()]
        scrolledwindows = ["scrolledwindowVocabularies", "scrolledwindowTerms"]
        func_cell_name = [self.cell_name_vocabulary_edited,
                          self.cell_name_term_edited]
        func_cell_description = [self.cell_description_vocabulary_edited,
                                 self.cell_description_term_edited]
        for i in range(2):
            liststore = gtk.ListStore(str, str)
            treeviews[i].set_model(liststore)
            cell_name = gtk.CellRendererText()
            cell_name.set_property("editable", True)
            cell_name.connect('edited', func_cell_name[i], liststore)
            tvcolumn_name = gtk.TreeViewColumn("Nombre", cell_name, text=0)
            cell_description = gtk.CellRendererText()
            cell_description.set_property("editable", True)
            cell_description.connect('edited', func_cell_description[i],
                                     liststore)
            txt = "Descripci�n"
            txt = txt.decode('latin-').encode('utf-8')
            tvcolumn_description = gtk.TreeViewColumn(txt,
                                                      cell_description, text=1)
            treeviews[i].append_column(tvcolumn_name)
            treeviews[i].append_column(tvcolumn_description)
            treeviews[i].set_search_column(0)
            tvcolumn_name.set_sort_column_id(0)
            self.widgets.get_widget(scrolledwindows[i]).add(treeviews[i])
        self.tvVocabularies = treeviews[0]
        self.tvVocabularies.get_selection().connect("changed",
                                                    self.changed_selection)
        self.tvTerms = treeviews[1]

    def buttonCreateVocabulary_clicked(self, widget, data=None):
        """Controls the creation of a new vocabulary"""
        name_vocabulary = self.eName_vocabulary.get_text()
        des_vocabulary = self.eDes_vocabulary.get_text()
        self.dCreateVocabulary.hide()
        self.eName_vocabulary.set_text("")
        self.eDes_vocabulary.set_text("")

        new_vocabulary = vocabulary.Vocabulary(name_vocabulary, des_vocabulary)
        try:
            self.agente.create_vocabulary(new_vocabulary)
            model = self.tvVocabularies.get_model()
            model.append([name_vocabulary, des_vocabulary])
        except exception.VocabularyAlreadyExistsException:
            logging.info("El vocabulario ya existe")
            edata = alert.ErrorData()
            edata.label_info_set_text("<big><b>Error insertando</b><big> \n \n" +
                                      "El vocabulario ya existe")

        self.refresh_vocabularies()

    def buttonCreateVocabularyCancel_clicked(self, widget, data=None):
        """Cancels the creation of a new vocabulary"""
        self.dCreateVocabulary.hide()
        self.eName_vocabulary.set_text("")
        self.eDes_vocabulary.set_text("")

    def cell_name_vocabulary_edited(self, cell, path, new_text, model):
        """Controls the edition of the name of an existing vocabulary"""
        name = model[path][0]
        new_vocabulary = vocabulary.Vocabulary(new_text, model[path][1])
        try:
            self.agente.update_vocabulary(name, new_vocabulary)
            model[path][0] = new_text
        except exception.NotVocabularyException:
            edata = alert.ErrorData()
            msg = "<big><b>Error editando vocabularios</b></big> \n \n" + \
                "El vocabulario ya existe"
            edata.label_info_set_text(msg)
        self.refresh_vocabularies()

    def cell_description_vocabulary_edited(self, cell, path, new_text, model):
        """Controls the edition of the des. of an existing vocabulary"""
        name = model[path][0]
        new_vocabulary = vocabulary.Vocabulary(model[path][0], new_text)
        try:
            self.agente.update_vocabulary(name, new_vocabulary)
            model[path][1] = new_text
        except exception.NotVocabularyException:
            edata = alert.ErrorData()
            edata.label_info_set_text("<big><b>Error insertando</b></big>")

    def buttonCreateTerm_clicked(self, widget, data=None):
        """Controls the creation of a new term"""
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            name_term = self.widgets.get_widget("entryTerm").get_text()
            des_term = self.widgets.get_widget("entryDesTerm").get_text()
            voc = self.agente.get_vocabulary(model[iter][0])
            new_term = term.Term(name_term, des_term, voc)
            try:
                self.agente.add_term_vocabulary(new_term, voc)
                self.dCreateTerm.hide()
                model = self.tvTerms.get_model()
                model.append([name_term, des_term])
            except exception.TermAlreadyExistsException:
                msg = u"Ya existe ese t�rmino en este vocabulario."
                edata = alert.ErrorData()
                all_msg = u"<big><b>Error insertando</b></big> \n \n" + msg
                edata.label_info_set_text(all_msg)
        self.refresh_vocabularies()

    def buttonCreateTermCancel_clicked(self, widget, data=None):
        """Cancels the creation of a new term"""
        self.dCreateTerm.hide()

    def cell_name_term_edited(self, cell, path, new_text, model):
        """Controls the edition of the name of an existing term"""
        treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
            #voc = self.agente.get_vocabulary(model_vocabularies[iter][0])
            name_vocabulary = model_vocabularies[iter][0]
            des_vocabulary = model_vocabularies[iter][1]
            name = model[path][0]
            try:
                update_term = term.Term(model[path][0], model[path][1],
                                        vocabulary.Vocabulary(name_vocabulary,
                                                              des_vocabulary))
                self.agente.update_term(name_vocabulary, name, update_term)
                model[path][0] = new_text
            except exception.NotTermException:
                edata = alert.ErrorData()
                msg = "<big><b>Error insertando</b><big>"
                edata.label_info_set_text(msg)
        self.refresh_vocabularies()

    def cell_description_term_edited(self, cell, path, new_text, model):
        """Controls the edition of the description of an existing term"""
        treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
            name_vocabulary = model_vocabularies[path][0]
            des_vocabulary = model_vocabularies[iter][1]
            name = model[path][0]
            try:
                update_term = term.Term(model[path][0], new_text,
                                        vocabulary.Vocabulary(name_vocabulary,
                                                              des_vocabulary))
                self.agente.update_term(name_vocabulary, name, update_term)
                logging.debug("Text to put: %s" % new_text)
                logging.debug("Path: %s" % path)
                model[path][1] = new_text
            except exception.NotTermException:
                edata = alert.ErrorData()
                msg = "<big><b>Error insertando</b></big> \n"
                edata.label_info_set_text(msg)

    def changed_selection(self, selection, *args):
        """Controls which terms display depending on the selected vocabulary"""
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            self.tvTerms.get_model().clear()
            terms = self.agente.get_terms_vocabulary(model[iter][0])
            model = self.tvTerms.get_model()
            if terms != None:
                for ter in terms:
                    model.append([ter.name, ter.description])

    def buttonRemoveVocabulary_clicked(self, widget, data=None):
        """Removes the selected vocabulary"""
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            old_vocabulary = vocabulary.Vocabulary(model[iter][0],
                                                    model[iter][1])
            self.agente.delete_vocabulary(old_vocabulary)
            model.remove(iter)
        self.tvTerms.get_model().clear()
        self.refresh_vocabularies()

    def buttonAddVocabulary_clicked(self, widget, data=None):
        """Runs the dialog to create a new vocabulary"""
        response = self.dCreateVocabulary.run()
        if response == gtk.RESPONSE_DELETE_EVENT:
            self.dCreateVocabulary.hide()
            self.eName_vocabulary.set_text("")
            self.eDes_vocabulary.set_text("")

    def buttonRemoveTerm_clicked(self, widget, data=None):
        """Removes the selected term"""
        treeselection = self.tvVocabularies.get_selection()
        model_vocabularies, iter = treeselection.get_selected()
        if iter != None:
            n_vocabulary = model_vocabularies[iter][0]
            selected_vocabulary = self.agente.get_vocabulary(n_vocabulary)
            treeselection = self.tvTerms.get_selection()
            model, iter = treeselection.get_selected()
            if iter != None:
                self.agente.delete_term(selected_vocabulary, model[iter][0])
                model.remove(iter)
            self.refresh_vocabularies()

    def buttonAddTerm_clicked(self, widget, data=None):
        """Runs the dialog to create a new term"""
        treeselection = self.tvVocabularies.get_selection()
        model, iter = treeselection.get_selected()
        if iter != None:
            response = self.dCreateTerm.run()
            self.widgets.get_widget("entryTerm").set_text("")
            self.widgets.get_widget("entryDesTerm").set_text("")

    def load_vocabularies(self):
        """Load the vocabularies info in the treeview"""
        vocabularies = self.refresh_vocabularies()
        for vocab in vocabularies:
            model = self.tvVocabularies.get_model()
            model.append([vocab.name, vocab.description])

    def refresh_vocabularies(self):
        """Refresh the treeview getting the info from the database"""
        self.dict_vocabularies = {}
        vocabularies = self.agente.get_all_vocabularies()
        vbox = self.widgets.get_widget('vboxCategories')
        children = vbox.get_children()
        for child in children:
            vbox.remove(child)
        for vocab in vocabularies:
            LOG.debug(vocab)
            terms = self.agente.get_terms_vocabulary(vocab.name)
            self.tabquestions.create_vocabulary_view(vocab.name, terms)
            self.dict_vocabularies[vocab.name] = terms
        return vocabularies

    def get_dict_vocabularies(self):
        """Returns the dictionary of the vocabularies"""
        return self.dict_vocabularies


