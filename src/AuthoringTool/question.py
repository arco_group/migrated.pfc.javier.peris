# -*- coding: iso-8859-15; tab-width:4 -*-
"""Module which contains the base class of the different
types of questions"""
import sys
sys.path.append('../Persistencia')
import os
import zope.interface
import logging

import persistencia
import exception
import ifactory


class Question():
    """Base class of the different types of questions"""
    zope.interface.implements(ifactory.IFactory)

    #__metaclass__ = utils.Singleton
    plugins = {}

    def __init__(self, title, default_mark, wording):
        """Question constructor"""
        self.title = title
        self.defaultMark = default_mark
        self.wording = wording
        self.agente = persistencia.Agente()

    def __str__(self):
        """Returns the representation of the question"""
        return self.title

    def __eq__(self, ques):
        """Returns whether two questions are equals"""
        if self.id == ques:
            logging.debug("The questions are equal")
            return True
        logging.debug("The questions are NOT equal")
        return False

    def __ne__(self, ques):
        """Returns whether two questions are not equals"""
        result = self.__eq__(ques)
        return not result

    def set_id(self, id):
        """Sets an id to a question"""
        self.id = id

    def set_filename(self, filename):
        """Set the filename to a question"""
        self.filename = filename

    def set_terms(self, terms):
        """Set the terms to a question"""
        self.terms = terms

    def best_mark(self):
        """All types of questions must implement best_mark method"""
        raise NotImplementedError

    @classmethod
    def register(cls, cls_plugin):
        """Register the types of questions"""
        Question.plugins[cls_plugin.name] = cls_plugin

    @classmethod
    def cancel(cls, question):
        """Removes a specific type of question (NOT IMPLEMENTED)"""
        pass

    @classmethod
    def create(cls, filename):
        """Create factory  method which returns a new question"""
        try:
            return persistencia.Agente().get_question(filename)
        except:
            raise exception.NotQuestionException

    @classmethod
    def load_plugins(cls):
        """Loads all types of questions available"""
        for dir in os.listdir('./Questions/'):
            if not dir.startswith(".") and not dir.startswith("__init__"):
                mod = __import__(os.path.splitext(dir.lower())[0]).load()
