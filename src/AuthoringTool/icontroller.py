"""Controller interface"""
import zope.interface


def methods_invariant(ob):
    """A controller must have a run method and a name attr"""
    assert(hasattr(ob,"run") and callable(ob.run)), "you must provide an run method"
    assert(hasattr(ob, "name"))

class IController(zope.interface.Interface):
    """Controller Interface"""

    name = zope.interface.Attribute("""controller name""")

    zope.interface.invariant(methods_invariant)
