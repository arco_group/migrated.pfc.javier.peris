# -*- coding: iso-8859-15; tab-width:4 -*-
"""Module which contains the base class of the different
types of validators"""
import sys
sys.path.append('../Editor')
import logging

from editor import InteractivePangoBuffer


class Validator():
    """Base class of the validators, it contains the methods to check
    the common parts of all type of question (the attr of Question
    class)"""

#   @classmethod
#   def register(cls, validator):
#       logging.debug("Registring validator" % validator.__str__())

    def check_title(self, title):
        """Checks whether the title was correctly filled out"""
        correcto = True
        msg_error = ""
        if title == "":
            msg_error += "Se debe rellenar el t�tulo de la pregunta\n"
            correcto = False
        return correcto, msg_error

    def check_wording(self, wording):
        """Checks whether the wording was correctly filled out, so the
        wording has, at least, one text block"""
        correcto = False
        msg_error = ""
        error = False
        if len(wording) == 0:
            msg_error += "Se debe especificar un enunciado\n"
            correcto = False
        else:
            for part in wording:
                logging.debug("Element: %s" % str(part))
                if isinstance(part, InteractivePangoBuffer):
                    if part.get_text() !="":
                        correcto = True
                else:
                    if not error:
                        msg_error += "Debe insertar texto a la pregunta\n"
                    error = True
            if not correcto:
                msg_error += "El bloque de texto debe rellenarse\n"
        return correcto, msg_error

    def check_default_mark(self, default_mark):
        """Checks whether the default mark was correctly filled out"""
        correcto = True
        msg_error = ""
        try:
            float(default_mark)
        except ValueError:
            correcto = False
            msg_error += "Se debe introducir un n�mero en la puntuaci�n " + \
                         "por defecto\n"
        return correcto, msg_error


