"""Module to create collections of questions"""

import logging

import question
import exception

class Collection:
    """Represents a list of questions"""

    def __init__(self, filenames):
        """Creates a new colection from an array"""
        self.filenames = filenames
        self.questions = []

    def create(self):
        """Returns a list of questions"""
        for filename in self.filenames:
            try:
                self.questions.append(question.Question.create(filename))
            except:
                logging.warning("Question %s doesn't exists" % filename)
                raise exception.NotQuestionException
        return self.questions

