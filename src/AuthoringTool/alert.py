"""Module to display alert dialogs which provides information about the state
of the application  system, or asks for essential information about how to
proceed with a particular task"""

# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
import os
try:
    import pygtk
    pygtk.require('2.0')
except ImportError:
    pass
try:
    import gtk
    import gtk.glade
except ImportError:
    sys.exit(1)


class ErrorData:
    """Display an error alert when a user-requested operation cannot be
    sucessfully completed"""

    def __init__(self, glade_file="error.glade"):
        """Creates a error dialog"""

        import locale
        import gettext

        realpath = os.path.realpath('.')
        DIR = realpath + '/locale'

        locale.setlocale(locale.LC_ALL, '')
        # gettext.bindtextdomain("error", DIR)
        # gettext.textdomain("error")
        gtk.glade.bindtextdomain("error", DIR)
        gtk.glade.textdomain("error")
        self.widgets = gtk.glade.XML(glade_file)
        self.widgets.signal_autoconnect(self)

    def label_info_set_text(self, text):
        """Set the info label to the error dialog"""
        label = self.widgets.get_widget("labelInfo")
        label.set_markup(text.decode("latin-").encode("utf-8"))
        response = self.widgets.get_widget("dialogError").run()
        self.widgets.get_widget("dialogError").destroy()


class Confirmation:
    """Present a confirmation alert when the user's command may destroy
    their data"""

    def __init__(self, glade_file="alert.glade"):
        """Create a confirmation dialog"""
        import locale
        import gettext

        realpath = os.path.realpath('.')
        DIR = realpath + '/locale'

        locale.setlocale(locale.LC_ALL, '')
        # gettext.bindtextdomain("error", DIR)
        # gettext.textdomain("error")
        gtk.glade.bindtextdomain("alert", DIR)
        gtk.glade.textdomain("alert")


        self.widgets = gtk.glade.XML(glade_file)
        self.widgets.signal_autoconnect(self)

    def label_info_set_text(self, text):
        """Sets the info label to the confirmation dialog"""
        label = self.widgets.get_widget("labelInfo")
        label.set_markup(text.decode("latin-").encode("utf-8"))

    def run(self):
        """Runs the confirmation dialog"""
        return self.widgets.get_widget("dialogConfirmation").run()


