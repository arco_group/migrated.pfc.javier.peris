# -*- coding: iso-8859-15; tab-width:4 -*-

"""Modules which provides the class vocabulary to classify
questions"""


class Vocabulary:
    """Provides a list of terms which will be used to classify
    questions"""

    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.terms = []

    def __str__(self):
        return "Vocabulary: " + self.name + " " + self.description
