"""Module to register and create controller of a specific question GUI"""

import logging
import zope.interface

import ifactory


class Controller():
    """A factory to create a controller of a specific question GUI"""
    zope.interface.implements(ifactory.IFactory)

    controllers = {}

    @classmethod
    def create(cls, product_id, dic, *args):
        """Create a controller of a specific question GUI"""
        for item in Controller.controllers.keys():
            logging.debug("CONTROLLER %s" % item)
        logging.debug("Requested controller: %s" % product_id)
        return Controller.controllers[product_id](dic, *args)

    @classmethod
    def register(cls, cls_controller):
        """Register a controller of a specific question GUI
        for after creating the controller """
        Controller.controllers[cls_controller.name] = cls_controller

    def run(self):
        """Run the dialog of the GUI"""
        raise NotImplementedError

    def save_question(self):
        """The controller must have a save_question method
        in order to allow the storage of the question"""
        raise NotImplementedError

    def take_data(self):
        """This method will take the data of the form (represented
        as a dialog)"""
        raise NotImplementedError
