"""Question interface"""

import zope.interface


def methods_invariant(ob):
    """A question must have an accept and best_mark method and
    name and icon attributes"""
    assert(hasattr(ob,"accept") and callable(ob.accept)), "you must provide an accept method"
    assert(hasattr(ob,"best_mark") and callable(ob.best_mark)), "you must provide a best_mark method"
    assert(hasattr(ob, "name"))
    assert(hasattr(ob, "icon"))

class IQuestion(zope.interface.Interface):
    """Question interface"""

    name = zope.interface.Attribute("""question name""")
    description = zope.interface.Attribute("""question description""")
    icon = zope.interface.Attribute("""question icon path""")

    zope.interface.invariant(methods_invariant)
