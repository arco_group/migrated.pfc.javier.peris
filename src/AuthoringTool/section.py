# -*- coding: iso-8859-15; tab-width:4 -*-

"""Section module provides a class to group together individual
item references"""

import logging


class Section:
    """Groups questionrefs and gives to the group a title"""

    def __init__(self, title, questionrefs):
        """Section constructor"""
        self.title = title
        self.questionrefs = questionrefs

    def __str__(self):
        """Returns the representation of a section"""
        return "Secci�n: " + self.title
#         for qr in self.questionrefs:
#             print qr

    def __eq__(self, sec):
        """Returns whether two sections are equals"""
        if self.title == sec.title and self.questionrefs == sec.questionrefs:
            logging.debug("Sections are equal")
            return True
        logging.debug("Sections are NOT equal")
        return False

    def __ne__(self, sec):
        """Returns whether two sections are not equals"""
        result = self.__eq__(sec)
        return not result

    def add_question(self, question):
        """Adds a questionref to the list of questions of the section"""
        self.questionrefs.append(question)
