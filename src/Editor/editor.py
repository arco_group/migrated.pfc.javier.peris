#!/usr/bin/python
# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
sys.path.append("../AuthoringTool/")
import pygtk
pygtk.require('2.0')
try:
	import gtk
	import gtk.glade
	import pango
except:
	sys.exit(1)
import xml.sax.saxutils
import os
import logging


import image
import table
import formula
import table
import sort_dict
import odict

class InteractivePangoBuffer :

    desc_to_attr_table = {
		'family':[pango.AttrFamily, ""],
		'style':[pango.AttrStyle, pango.STYLE_NORMAL],
		'variant':[pango.AttrVariant, pango.VARIANT_NORMAL],
		'weight':[pango.AttrWeight, pango.WEIGHT_NORMAL],
		'stretch':[pango.AttrStretch, pango.STRETCH_NORMAL],
    }

    pango_translation_properties={
        # pango ATTR TYPE : (pango attr property / tag property)
        pango.ATTR_SIZE : 'size',
        pango.ATTR_WEIGHT: 'weight',
        pango.ATTR_UNDERLINE: 'underline',
        pango.ATTR_STRETCH: 'stretch',
        pango.ATTR_VARIANT: 'variant',
        pango.ATTR_STYLE: 'style',
        pango.ATTR_SCALE: 'scale',
        pango.ATTR_STRIKETHROUGH: 'strikethrough',
        pango.ATTR_RISE: 'rise',
    }

    attval_to_markup={
        'underline':{pango.UNDERLINE_SINGLE: 'single',
                     pango.UNDERLINE_DOUBLE: 'double',
                     pango.UNDERLINE_LOW: 'low',
                     pango.UNDERLINE_NONE: 'none'},
        'stretch':{pango.STRETCH_ULTRA_EXPANDED: 'ultraexpanded',
                   pango.STRETCH_EXPANDED: 'expanded',
                   pango.STRETCH_EXTRA_EXPANDED: 'extraexpanded',
                   pango.STRETCH_EXTRA_CONDENSED: 'extracondensed',
                   pango.STRETCH_ULTRA_CONDENSED: 'ultracondensed',
                   pango.STRETCH_CONDENSED: 'condensed',
                   pango.STRETCH_NORMAL: 'normal',
                   },
        'variant':{pango.VARIANT_NORMAL: 'normal',
                   pango.VARIANT_SMALL_CAPS: 'smallcaps',
                   },
        'style':{pango.STYLE_NORMAL: 'normal',
                 pango.STYLE_OBLIQUE: 'oblique',
                 pango.STYLE_ITALIC: 'italic',
                 },
        'strikethrough':{1: 'true',
                        True: 'true',
                        0: 'false',
                        False: 'false'},
    }

	# EL PARAMETRO TXT YA NO TIENE SENTIDO, ACTUALIZAR #
    def __init__(self, txt, buf, toggle_widget_alist=[]):
        #PangoBuffer.__init__(self, txt, buf)
#        if normal_button: normal_button.connect('clicked',
#												lambda *args: self.remove_all_tags())
        self.tag_widgets = {}
        self.buf = buf
        self.internal_toggle = False
        self.insert = self.buf.get_insert()
        self.buf.connect('mark-set', self._mark_set_cb)
        self.tagdict = {}
        self.tags = {}
        self.tag = {}
        li = ["6144", "7168", "8192", "10240", "12288", "14336", "16384"]
        for i in range(len(li)):
		    tag = '<span size = "' + li[i] + '"> xx-small </span>'
			a, t, s = pango.parse_markup(tag, u'0')
			font, lang, attrs = a.get_iterator().get_font()
			t = self.buf.create_tag()
			t.set_property('font-desc', font)
			self.tag[i] = t
			self.tagdict[t] = {}
			self.tagdict[t]['font-desc'] = font.to_string()

	def set_text(self, txt):
		gtk.TextBuffer.set_text(self,"")
        try:
            self.parsed,self.txt,self.separator = pango.parse_markup(txt,u'\x00')
        except:
            logging.exception('Escaping text, we seem to have a problem here!')
            txt=xml.sax.saxutils.escape(txt)
            self.parsed,self.txt,self.separator = pango.parse_markup(txt,u'\x00')
        self.attrIter = self.parsed.get_iterator()
        self.add_iter_to_buffer()
        while self.attrIter.next():
            self.add_iter_to_buffer()


    def setup_widget_from_pango(self, widg, markupstring):
        #print markupstring
        a, t, s = pango.parse_markup(markupstring, u'0')
        ai = a.get_iterator()
        font, lang, attrs = ai.get_font()
        self.setup_widget(widg, font, attrs)

    def setup_widget(self, widg, font, attr):
        tags = self.get_tags_from_attrs(font, None, attr)
        #self.tag_widgets[tuple(tags)] = widg
        if isinstance (widg, gtk.ToggleButton) :
            self.tag_widgets[tuple(tags)] = widg
            widg.connect('toggled', self._toggle, tags)
        elif isinstance(widg, gtk.ComboBox) :
			self.tag_widgets[tuple(tags)] = widg
            widg.connect('changed', self._change_size, tags)
        else:
			pass
#			widg.connect('color-set', self._change_color, tags)


    def get_tags_from_attrs(self, font, lang, attrs):
        tags = []
        if font:
            font, fontattrs = self.fontdesc_to_attrs(font)
            fontdesc = font.to_string()
            if fontattrs:
                attrs.extend(fontattrs)
                if fontdesc and fontdesc != 'Normal':
                    if not self.tags.has_key(font.to_string()):
                        tag = self.buf.create_tag()
                        tag.set_property('font-desc', font)
                        if not self.tagdict.has_key(tag):
                            self.tagdict[tag] = {}
                            self.tagdict[tag]['font_desc'] = font.to_string()
                            self.tags[font.to_string()] = tag
                            tags.append(self.tags[font.to_string()])
        if lang:
            if not self.tags.has_key(lang):
                tag = self.buf.create_tag()
				tag.set_property('language', lang)
				self.tags[lang] = tag
                tags.append(self.tags[lang])
        if attrs:
            for a in attrs:
                if a.type == pango.ATTR_FOREGROUND:
                    gdkcolor = self.pango_color_to_gdk(a.color)
                    key = 'foreground%s'%self.color_to_hex(gdkcolor)
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property('foreground-gdk', gdkcolor)
                        self.tagdict[self.tags[key]]={}
                        self.tagdict[self.tags[key]]['foreground']="#%s"%self.color_to_hex(gdkcolor)
                    tags.append(self.tags[key])
                if a.type == pango.ATTR_BACKGROUND:
                    gdkcolor = self.pango_color_to_gdk(a.color)
                    tag.set_property('background-gdk',gdkcolor)
                    key = 'background%s'%self.color_to_hex(gdkcolor)
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property('background-gdk',gdkcolor)
                        self.tagdict[self.tags[key]]={}
                        self.tagdict[self.tags[key]]['background']="#%s"%self.color_to_hex(gdkcolor)
                        tags.append(self.tags[key])
                if self.pango_translation_properties.has_key(a.type):
                    prop = self.pango_translation_properties[a.type]
                    #print 'setting property %s of %s (type: %s)'%(prop,a,a.type)
                    val = getattr(a,'value')
                    #tag.set_property(prop,val)
                    mval = val
                    if self.attval_to_markup.has_key(prop):
                        #print 'converting ',prop,' in ',val
                        if self.attval_to_markup[prop].has_key(val):
                            mval = self.attval_to_markup[prop][val]
                    key = "%s%s"%(prop,val)
                    if not self.tags.has_key(key):
                        self.tags[key] = self.buf.create_tag()
                        self.tags[key].set_property(prop,val)
                        #print "PROPIEDAD %s con valor %s"%(prop, val)
                        self.tagdict[self.tags[key]] = {}
                        self.tagdict[self.tags[key]][prop] = mval
                        #print self.tagdict#[self.tags[key]]
                        tags.append(self.tags[key])
                        #print "TAG", self.tags
                else:
					logging.info("Don't know what to do with attr %s"% str(a))
					logging.debug(str(a.type))
		return tags

    def get_tags (self):
        tagdict = {}
        for pos in range(self.buf.get_char_count()):
            iter = self.buf.get_iter_at_offset(pos)
            for tag in iter.get_tags():
                if tagdict.has_key(tag):
                    if tagdict[tag][-1][1] == pos - 1:
                        tagdict[tag][-1] = (tagdict[tag][-1][0],pos)
                    else:
                        tagdict[tag].append((pos,pos))
                else:
                    tagdict[tag]=[(pos,pos)]
        return tagdict

    def get_text (self):
        tagdict = self.get_tags()
        txt = self.buf.get_text(self.buf.get_start_iter(), self.buf.get_end_iter())
        cuts = {}
        for k, v in tagdict.items():
            stag, etag = self.tag_to_markup(k)
            for st, end in v:
				logging.debug("st %s, end de v %s" % (st, end))
                if cuts.has_key(st): cuts[st].append(stag) #add start tags second
                else: cuts[st] = [stag]
                if cuts.has_key(end+1): cuts[end+1] = [etag] + cuts[end+1] #add end tags first
                else: cuts[end+1] = [etag]
        last_pos = 0
        outbuff = ""
        cut_indices = cuts.keys()
        cut_indices.sort()
        for c in cut_indices:
			logging.debug('cut %s' % str(c))
            if not last_pos==c:
                outbuff += txt[last_pos:c]
                last_pos = c
            for tag in cuts[c]:
                outbuff += tag
			logging.debug('outbuff: %s', str(outbuff))
        outbuff += txt[last_pos:]
        return outbuff

    def tag_to_markup (self, tag):
        stag = "<span"
		gdkcolor = tag.get_property('foreground-gdk')
		if not (gdkcolor.red == 0 and gdkcolor.green == 0 and gdkcolor.blue == 0):
			logging.debug("Red: %s" % str(gdkcolor.red))
			logging.debug("Green: %s" % str(gdkcolor.green))
			logging.debug("Blue: %s" % str(gdkcolor.blue))
			hexcolor = self.color_to_hex(gdkcolor)
			loggind.debug(str(hexcolor))
			# self.tagdict[tag]['foreground']="#%s"%hexcolor
			stag += ' %s="%s"'%('foreground', hexcolor)
		else:
			for k, v in self.tagdict[tag].items():
				stag += ' %s="%s"'%(k, v)
        stag += ">"
        return stag, "</span>"

    def fontdesc_to_attrs(self, font):
        nicks = font.get_set_fields().value_nicks
        attrs = []
        for n in nicks:
            if self.desc_to_attr_table.has_key(n):
                #print 'attributeifying %s'%n
                Attr,norm = self.desc_to_attr_table[n]
                # create an attribute with our current value
                attrs.append(Attr(getattr(font,'get_%s'%n)()))
                # unset our font's value
                getattr(font,'set_%s'%n)(norm)
        return font, attrs

    def pango_color_to_gdk(self, pc):
        return gtk.gdk.Color(pc.red, pc.green, pc.blue)

    def color_to_hex(self, color):
        hexstring = ""
        for col in 'red','green','blue':
            hexfrag = hex(getattr(color,col) / (16 * 16)).split("x")[1]
            if len(hexfrag) < 2: hexfrag = "0" + hexfrag
            hexstring += hexfrag
        #print 'returning hexstring: ',hexstring
        return hexstring

    def _change_size(self, widget, tags):
        for i in range(7):
            if widget.get_active() == i:
				logging.debug("Activated option %s" % str(i))
                for t in self.tag:
                    self.remove_tag(self.tag[t])
		        self.apply_tag(self.tag[i])
				logging.debug("Text: %s" % self.get_text())
                break

    def _change_color(self, widget, tags):
        ltag = self.buf.get_iter_at_mark(self.insert).get_tags()
        for e in ltag:
            if e.get_priority() == 5:
                self.remove_tag(e)
                break
        ta = self.buf.create_tag()
        gdkcolor = widget.get_color()
        ta.set_property('foreground-gdk',gdkcolor)
        self.apply_tag(ta)

    def _toggle(self, widget, tags):
        if self.internal_toggle: return
        if widget.get_active():
		    for t in tags:
                #print t
                self.apply_tag(t)
        else:
            for t in tags:
				self.remove_tag(t)

    def _mark_set_cb(self, buffer, iter, mark, *params):
        if mark == self.insert:
            for tags, widg in self.tag_widgets.items():
                active = True
				#print "WIDGET ACTIVO:", widg
                for t in tags:
                    if not iter.has_tag(t):
                        if isinstance (widg, gtk.ComboBox):
                            active = -1
                        else:
                            active = False
				if not isinstance(widg, gtk.ComboBox):
					self.internal_toggle = True
					widg.set_active(active)
					self.internal_toggle = False

    def get_selection(self):
        bounds = self.buf.get_selection_bounds()
        if not bounds:
            iter = self.buf.get_iter_at_mark(self.insert)
            if iter.inside_word():
                start_pos = iter.get_offset()
                iter.forward_word_end()
                word_end = iter.get_offset()
                iter.backward_word_start()
                word_start = iter.get_offset()
                iter.set_offset(start_pos)
                bounds = (self.buf.get_iter_at_offset(word_start),
                          self.buf.get_iter_at_offset(word_end+1))
        return bounds

    def apply_tag(self, tag):
        selection = self.get_selection()
		if selection:
            self.buf.apply_tag(tag,*selection)

    def remove_tag (self, tag):
        selection = self.get_selection()
        if selection:
            self.buf.remove_tag(tag,*selection)


class Editor:

    buttonTags = {"toggleButtonBold": "<b>bold</b>",
		  "toggleButtonItalic": "<i>italic</i>",
		  "toggleButtonUnderline": "<u>underline</u>",
		  "comboboxSizeFont": '<span size = "6144"> xx-small </span>',
		  "buttonColorFont": '<span color = "green"> prueba </span>',
		  }

    def __init__(self, file, gaps=False):
        self.widgets = gtk.glade.XML(file)
        self.widgets.signal_autoconnect(self)
		self.buffer = self.widgets.get_widget("textView").get_buffer()

		self.ipb = InteractivePangoBuffer("", self.buffer)
		self.contents = odict.OrderedDict()
		widget = self.widgets.get_widget("scrolledwindow1")
		self.contents[widget] = self.ipb
		logging.debug("Keys: %s" % str(self.contents.keys()))
		logging.debug("Values: %s" % str(self.contents.values()))

		for n, t in Editor.buttonTags.items():
			widget = self.widgets.get_widget(n)
			self.ipb.setup_widget_from_pango(widget, t)

		if gaps:
			self.button = gtk.Button(label="<?>")
			self.widgets.get_widget("hbox2").add(self.button)
			self.button.show()
			self.button.connect("clicked", self.set_gap)

            buttonBold = self.widgets.get_widget("toggleButtonBold")
            buttonItalic = self.widgets.get_widget("toggleButtonItalic")
            buttonUnderline = self.widgets.get_widget("toggleButtonUnderline")
            comboboxSizeFont = self.widgets.get_widget("comboboxSizeFont")
            buttonColorFont = self.widgets.get_widget("buttonColorFont")

            self.ipb.setup_widget_from_pango(buttonBold, "<b>bold</b>")
            self.ipb.setup_widget_from_pango(buttonItalic, "<i>italic</i>")
            self.ipb.setup_widget_from_pango(buttonUnderline, "<u>underline</u>")
            self.ipb.setup_widget_from_pango(comboboxSizeFont, '<span size = "6144"> xx-small </span>')
            self.ipb.setup_widget_from_pango(buttonColorFont, '<span color = "green"> prueba </span>')

        button = gtk.Button(None, gtk.STOCK_JUSTIFY_CENTER, True)
		button.set_label("Texto")
		button.connect("clicked", self.insert_text)
		button.show()
		self.widgets.get_widget("hbox1").add(button)
		self.widgets.get_widget("comboboxSizeFont").set_active(3)
		self.windowEditor = self.widgets.get_widget("wEditor")
#		self.windowEditor.show_all()

# 		text = """
# 		<span font-desc="Normal 16"><span weight="700">adfasdf</span></span>

# afkjlahdaf dsahlfjkdsa <span style="italic">adfasdf</span> dfasdfasd k�jasd <span foreground="#ff0000">adsfasdfadf</span> <span underline="single">

# <span font-desc="Normal 12">adsf</span></span>
# """
# 		self.ipb.set_text(text)

	def set_gap(self, widget):
#		print self.ipb.get_text()
		for children in self.widgets.get_widget("vbox3").get_children():
			if isinstance(children, gtk.ScrolledWindow):
				for child in children.get_children():
					if isinstance(child, gtk.TextView):
# 						mark = child.get_buffer().get_mark()
						buf = child.get_buffer()
						iter = buf.get_iter_at_mark(self.get_ipb_from_buffer(buf).insert)
						anchor = buf.create_child_anchor(iter)
						#child.get_buffer().insert_child_anchor(iter, anchor)
						button = gtk.Button(label="<?>")
						child.add_child_at_anchor(button, anchor)
						child.show()
						button.show()

	def get_ipb_from_buffer(self, buf):
		for part in self.contents.values():
# 			print "CONTENIDO", self.contents[wdg]
			if isinstance(part, InteractivePangoBuffer):
				if buf == part.buf:
					return part

    def get_textview(self):
        return self.widgets.get_widget("textView")

    def get_main_vbox(self):
        return self.widgets.get_widget("vbox1")

	def get_content_vbox(self):
		return self.widgets.get_widget("vboxcontent")

    def buttonOrdList_clicked(self, widget, data=None):
        tag = self.widgets.get_widget("textView").get_buffer().create_tag()
		tag.set_property('wide_margins', left_margin=100)
		bounds = self.buffer.get_selection_bounds()
		self.buffer().apply_tag(tag, *bounds)

    def buttonList_toggled(self, widget, data=None):
	    tag = self.widgets.get_widget("textView").get_buffer().create_tag()
		tag.set_property('wide_margins', left_margin=100)
		#tag.set_property('indent',200)
		bounds = self.buffer().get_selection_bounds()
		self.buffer().apply_tag(tag, *bounds)

    def buttonImage_clicked(self, widget, data=None):
		self.widgets.get_widget("dialogImage").run()
		response = self.widgets.get_widget("dialogImage")
#		if response == 0:
		self.widgets.get_widget("dialogImage").hide()
# 	    dialog = gtk.FileChooserDialog("Abrir archivo", None,
# 									   gtk.FILE_CHOOSER_ACTION_OPEN,
# 									   (gtk.STOCK_CANCEL,
# 										gtk.RESPONSE_CANCEL,
# 										gtk.STOCK_OPEN,
# 										gtk.RESPONSE_OK))
# 		dialog.set_default_response(gtk.RESPONSE_OK)
# 		filter = gtk.FileFilter()
# 		filter.set_name("Imagen")
# 		filter.add_mime_type("image/png")
# 		filter.add_mime_type("image/jpeg")
# 		filter.add_mime_type("image/gif")
# 		filter.add_pattern("*.png")
# 		filter.add_pattern("*.jpg")
# 		dialog.add_filter(filter)
# 		response = dialog.run()
# 		if response == gtk.RESPONSE_OK:
# 		    try:
# 				anchor = self.buffer.create_child_anchor(self.buffer.get_iter_at_mark(self.buffer.get_insert()))
# 				img = gtk.image_new_from_file(dialog.get_filename())
# 				button = gtk.Button()
# 				button.set_image(img)
# 				button.connect("clicked", self.edit_image)
# 				self.widgets.get_widget("textView").add_child_at_anchor(button, anchor)
# 				button.show_all()
# 			    #pixbuf = gtk.gdk.pixbuf_new_from_file(dialog.get_filename())
# 				#anchor = self.buffer.create_child_anchor(self.buffer.get_iter_at_mark(self.buffer.get_insert()))
# 			    #self.widgets.get_widget("textView").add_child_at_anchor(child, anchor)
# 			    #self.buffer.insert_pixbuf(self.buffer.get_iter_at_mark(self.buffer.get_insert()), pixbuf)
# 			    pass
# 	        except IOError :
# 			    print "El fichero no existe."
# 		elif response == gtk.RESPONSE_CANCEL:
# 		    print "No hay elementos seleccionados"
# 		dialog.destroy()

    def insert_text(self, widget, data=None):
		swindow, textview = self.create_textpart()
		buf = textview.get_buffer()
        ipb = InteractivePangoBuffer("", buf)
		self.contents[swindow] = ipb

	def create_textpart(self):
		swindow = gtk.ScrolledWindow()
		textview = gtk.TextView()
		swindow.add(textview)
		hbox = gtk.HBox(False, 10)
		button_remove = gtk.Button(None, gtk.STOCK_REMOVE)
		button_remove.connect("clicked", self.buttonRemove_clicked)
		hbox.add(button_remove)
		hbox.set_child_packing(button_remove, False, False, 0, gtk.PACK_END)
		hbox.set_spacing(10)
# 		vbox = gtk.VBox()
# 		vbox.add(hbox)
# 		vbox.add(swindow)
# 		hbox2 = gtk.HBox()
# 		hbox2.add(button_image)
# 		hbox2.set_child_packing(button_image, 0, 0, 0, gtk.PACK_END)
# 		vbox.add(hbox2)
		self.get_content_vbox().add(hbox)
		self.get_content_vbox().add(swindow)
		self.get_content_vbox().show_all()
		return swindow, textview

#     def buttonImageCancel_clicked(self, widget, data=None):
# 		self.widgets.get_widget("dialogImage").hide()

    def buttonInsertImage_clicked(self, widget, data=None):
		button_image = self.create_imagepart()
		path = self.widgets.get_widget("entryPath").get_text()
		description = self.widgets.get_widget("entryDescription").get_text()
		scale = self.widgets.get_widget("entryScale").get_text()
		img = image.Image(path, description, float(scale))
		self.contents[button_image] = img

	def create_imagepart(self, content=None):
		button_image = gtk.Button()
		if content == None:
			path = self.widgets.get_widget("entryPath").get_text()
		else:
			path = content.path
		pb = gtk.gdk.pixbuf_new_from_file(path)
		if content:
			width = pb.get_width() * content.scale
			height = pb.get_height() * content.scale
		else:
			width = pb.get_width() * float(self.widgets.get_widget("entryScale").get_text())
			height = pb.get_height() * float(self.widgets.get_widget("entryScale").get_text())
		pb_resized = pb.scale_simple(int(width), int(height), gtk.gdk.INTERP_BILINEAR)
		image_display = gtk.image_new_from_pixbuf(pb_resized)
		#image_display = gtk.image_new_from_file(path)
		button_image.set_image(image_display)
		hbox = gtk.HBox(False, 10)
		button_remove = gtk.Button(None, gtk.STOCK_REMOVE)
		button_remove.connect("clicked", self.buttonRemove_clicked)
		hbox.add(button_remove)
		hbox.set_child_packing(button_remove, 0, 0, 0, gtk.PACK_END)
		vbox = gtk.VBox()
		vbox.add(hbox)
		vbox.add(button_image)
		self.get_content_vbox().add(vbox)
		self.get_content_vbox().show_all()
		return button_image

    def buttonTable_clicked(self, widget, data=None):
		self.widgets.get_widget("dialogTable").run()
		self.widgets.get_widget("dialogTable").hide()
# 	    dialog = gtk.FileChooserDialog("Abrir archivo", None,
# 									   gtk.FILE_CHOOSER_ACTION_OPEN,
# 									   (gtk.STOCK_CANCEL,
# 										gtk.RESPONSE_CANCEL,
# 										gtk.STOCK_OPEN,
# 										gtk.RESPONSE_OK))
# 		dialog.set_default_response(gtk.RESPONSE_OK)
# 		filter = gtk.FileFilter()
# 		filter.set_name("OOffice")
# 		filter.add_pattern("*.odt")
# 		dialog.add_filter(filter)
# 		response = dialog.run()
# 		if response == gtk.RESPONSE_OK:
# 			try:
# 				pass
# 		    except IOError :
# 				print "El fichero no existe."
# 			flectura.close()
# 	    elif response == gtk.RESPONSE_CANCEL:
# 		    print "No hay elementos seleccionados"
# 		dialog.destroy()

    def buttonTableAccept_clicked(self, widget, data=None):
		self.create_tablepart()
		description = self.widgets.get_widget("entryDescriptionTable").get_text()
		scale = self.widgets.get_widget("entryScaleTable").get_text()
		tbl = table.Table(filename, description, scale)
		self.contents[button_table] = tbl

	def create_tablepart(self, content=None):
		filename = self.widgets.get_widget("entryPathTable").get_text()
		if content == None:
			path = filename[0:filename.find(".odt")]
		# print "PATH", path
# 		retval = os.system('oo2pdf "%s"' % self.widgets.get_widget("entryPathTable").get_text())
# 		retval = os.system('convert -trim ' + path + ".pdf " + path + ".jpg")
        hbox = gtk.HBox(False, 10)
		button_remove = gtk.Button(None, gtk.STOCK_REMOVE)
		button_remove.connect("clicked", self.buttonRemove_clicked)
		hbox.add(button_remove)
		hbox.set_child_packing(button_remove, 0, 0, 0, gtk.PACK_END)
		img = gtk.image_new_from_file(path + ".jpg")
		button_table = gtk.Button()
		button_table.set_image(img)
		vbox = gtk.VBox()
		vbox.add(hbox)
		vbox.add(button_table)
		self.get_content_vbox().add(vbox)
		self.get_content_vbox().show_all()
		return button_table

	def buttonRemove_clicked(self, widget, data=None):
		grandparent = widget.get_parent().get_parent()
		for child in grandparent.get_children():
# 			if isinstance(child, gtk.ScrolledWindow):
# 				child.remove(child.get_children()[0])
			grandparent.remove(child)
			if not isinstance(child, gtk.HBox):
				try:
					self.contents.pop(child)
				except:
					pass
		greatgrandparent = grandparent.get_parent()
		for child in greatgrandparent:
			if isinstance(child, gtk.VBox) and \
				not child.get_children():
					greatgrandparent.remove(child)
					break
		loggin.debug(str(self.contents))

    def buttonChooseImage_clicked(self, widget, data=None):
		name = "Imagen"
		mime_types = ["image/png", "image/jpeg", "image/gif"]
		patterns = ["*.png", "*.jpg"]
		self.select_image(name, mime_types, patterns)

	def buttonChooseTable_clicked(self, widget, data=None):
		name = "Documento ODT"
		mime_types = []
		patterns = ["*.odt"]
		self.select_table(name, mime_types, patterns)
# 		dialog = gtk.FileChooserDialog("Abrir archivo", None,
# 									   gtk.FILE_CHOOSER_ACTION_OPEN,
# 									   (gtk.STOCK_CANCEL,
# 										gtk.RESPONSE_CANCEL,
# 										gtk.STOCK_OPEN,
# 										gtk.RESPONSE_OK))
# 		dialog.set_default_response(gtk.RESPONSE_OK)
# 		filter = gtk.FileFilter()
# 		filter.set_name("Imagen")
# 		filter.add_mime_type("image/png")
# 		filter.add_mime_type("image/jpeg")
# 		filter.add_mime_type("image/gif")
# 		filter.add_pattern("*.png")
# 		filter.add_pattern("*.jpg")
# 		dialog.add_filter(filter)
# 		response = dialog.run()
# 		if response == gtk.RESPONSE_OK:
# 		    self.widgets.get_widget("entryPath").set_text(dialog.get_filename())
# 			img = gtk.image_new_from_file(dialog.get_filename())
# 			self.widgets.get_widget("viewport").add(img)
# 			self.widgets.get_widget("scrolledwindow").show_all()
# 		elif response == gtk.RESPONSE_CANCEL:
# 		    print "No hay elementos seleccionados"
# 		dialog.destroy()

    def select_table(self, name, mime_types, patterns):
		dialog = self.create_dialog(name, mime_types, patterns)
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
		    self.widgets.get_widget("entryPathTable").set_text(dialog.get_filename())
			filename = dialog.get_filename()
			path = filename[0:filename.find(".odt")]
			retval = os.system('oo2pdf "%s"' % filename)
			retval = os.system('convert -trim ' + path + ".pdf " + path + ".jpg")
			img = gtk.image_new_from_file(path + ".jpg")
			self.widgets.get_widget("viewport1").add(img)
			self.widgets.get_widget("scrolledwindow2").show_all()
		elif response == gtk.RESPONSE_CANCEL:
			logging.debug("There are no selected elements")
		dialog.destroy()

	def select_image(self, name, mime_types, patterns):
		dialog = self.create_dialog(name, mime_types, patterns)
		response = dialog.run()
		if response == gtk.RESPONSE_OK:
		    self.widgets.get_widget("entryPath").set_text(dialog.get_filename())
			pb = gtk.gdk.pixbuf_new_from_file(dialog.get_filename())
            #self.h and self.w are the height and width of the original image loaded
            #by the inspector
			proportion = float(pb.get_width())/pb.get_height()
			rect = self.widgets.get_widget("viewport").get_allocation()
            height = 100
            width  = 60
            if width / proportion < height:
                height = int(width / proportion)
            else:
                width = int(height * proportion)
            pb_resized = pb.scale_simple(width, height, gtk.gdk.INTERP_BILINEAR)
            if pb_resized:
                img = pb_resized
            else:
                img = pb
            image = gtk.image_new_from_pixbuf(img)
# 			img = gtk.image_new_from_file(dialog.get_filename())
			self.widgets.get_widget("viewport").add(image)
			self.widgets.get_widget("scrolledwindow").show_all()
		elif response == gtk.RESPONSE_CANCEL:
			logging.debug("There are no selected elements")
		dialog.destroy()

	def create_dialog(self, name, mime_types, patterns):
		dialog = gtk.FileChooserDialog("Abrir archivo", None,
									   gtk.FILE_CHOOSER_ACTION_OPEN,
									   (gtk.STOCK_CANCEL,
										gtk.RESPONSE_CANCEL,
										gtk.STOCK_OPEN,
										gtk.RESPONSE_OK))
		dialog.set_default_response(gtk.RESPONSE_OK)
		filter = gtk.FileFilter()
		filter.set_name(name)
		for mime in mime_types:
			filter.add_mime_type(mime)
		for pattern in patterns:
			filter.add_pattern(pattern)
		dialog.add_filter(filter)
		return dialog
# 		response = dialog.run()
# 		if response == gtk.RESPONSE_OK:
# 		    self.widgets.get_widget("entryPath").set_text(dialog.get_filename())
# 			img = gtk.image_new_from_file(dialog.get_filename())
# 			self.widgets.get_widget("viewport").add(img)
# 			self.widgets.get_widget("scrolledwindow").show_all()
# 		elif response == gtk.RESPONSE_CANCEL:
#			logging.debug("There are no selected elements")
# 		dialog.destroy()


    def buttonFormula_clicked(self, widget, data=None):
		self.widgets.get_widget("dialog").run()
		filename = self.widgets.get_widget("entryPath").get_text()
		path = filename[0:filename.find(".odt")]
		retval = os.system('oo2pdf "%s"' % self.widgets.get_widget("entryPath").get_text(),
 							path + ".pdf")
		retval = os.system('convert -trim "%s %s"' % path + ".pdf", path + ".png")
		button = gtk.Button()
		img = gtk.image_new_from_file(self.widgets.get_widget("entryPath").get_text())
		button.set_image(img)
		self.get_content_vbox().add(button)

	    # dialog = gtk.FileChooserDialog("Abrir archivo", None,
# 									   gtk.FILE_CHOOSER_ACTION_OPEN,
# 									   (gtk.STOCK_CANCEL,
# 										gtk.RESPONSE_CANCEL,
# 										gtk.STOCK_OPEN,
# 										gtk.RESPONSE_OK))
# 		dialog.set_default_response(gtk.RESPONSE_OK)
# 		response = dialog.run()
# 		filter = gtk.FileFilter()
# 		filter.set_name("MathML")
# 		filter.add_pattern("*.xml")
# 		dialog.add_filter(filter)
# 		if response == gtk.RESPONSE_OK:
# 			try:
# 				pass
# 		    except IOError :
# 				logging.exception("The file does not exists")
# 		    flectura.close()
#         elif response == gtk.RESPONSE_CANCEL:
#			logging.debug("There are no selected elements")
# 		dialog.destroy()

    def wEditor_delete_event(self, widget, event, data=None):
	    gtk.main_quit()

    def print_markup(self, *args):
		logging.debug("Text: %s" %self.ipb.get_text())
		print self.buffer.get_slice(self.buffer.get_start_iter(),
		 							self.buffer.get_end_iter(), True)

    def main(self):
	    gtk.main()
		return 0

if __name__ == "__main__":
	app = Editor("editor.glade", True)
	app.main()
