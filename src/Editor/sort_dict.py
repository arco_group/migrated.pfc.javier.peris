# Las claves se almacenan en una lista para tenerlas en el mismo
# orden en que se insertaron
class SortedDict(dict):
    def __init__(self):
        self.__keylist = []

    def __getitem__(self, key):
        if not key in dict.keys(self):
            self[key] = []
        return dict.__getitem__(self, key)

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key in self.__keylist:
            self.__keylist.remove(key)
        self.__keylist.append(key)

    def keys(self):
        return self.__keylist

    def values(self):
        return [self[k] for k in self.__keylist]

    def items(self):
        return [(k, self[k]) for k in self.__keylist]

    def __iter__(self):
        return self.__keylist.__iter__()

    def clear(self):
        self.__keylist = []
        dict.clear(self)

    # Es posible que tenga que implementar el metodo pop
