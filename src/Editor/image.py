# -*- coding: iso-8859-15; tab-width:4 -*-

import sys
try:
	import pygtk
	pygtk.require('2.0')
except:
	pass
try:
	import gtk
	import gtk.glade
	import pango
	import amara
except:
	sys.exit(1)

import contenttype


class Image(contenttype.ContentType):

    def __init__(self, path, description, scale):
		self.path = path
		self.description = description
		self.scale = scale


