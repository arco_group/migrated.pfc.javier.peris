# -*- coding: iso-8859-15; tab-width:4 -*-


import contenttype


class Table(contenttype.ContentType):

    def __init__(self, path, description, scale):
		self.path = path
		self.description = description
		self.scale = scale
