README
------

QUICK INSTALL
=============

For the impatient, here is a basic outline of the
installation process, which normally takes me only
a few minutes:

1) Move the SDocente files into your Desktop directory.

2) Create a single database for SDocente to store all
   it's tables in (or choose an existing database).

   a) Connect as root:
      mysql -u root -p

   b) Execute:
      GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost' IDENTIFIED BY
      'password' WITH GRANT OPTION;


3) Create the database tables, for that purpose execute
   db.py file located in Persistencia directory.

4) Replace the exam.cls file of your LaTeX distribution (in Debian you can
find it in /usr/share/texmf-texlive/tex/latex/exam/exam.cls) by the
file provided with this application(located in LaTeX directory).

4) Execute editQuiz.py to launch authoring tool.

For more information, contact to lead developer at:

    luisj.peris@alu.uclm.es

Luis Javier Peris Morillo, Lead Developer

GU�A DE INSTALACI�N R�PIDA
==========================

Esto es una gu�a para la instalaci�n, que normalmente toma un
par de minutos:

1) Mueva los ficheros de la carpeta SDocente a su escritorio.

2) Cree un base datos para SDocente para poder almacenar sus
   tablas. Para tal prop�sito, ejecute el fichero db.py localizado
   en el directorio App.

   a) Connect as root:
      mysql -u root -p

   b) Execute:
      GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost' IDENTIFIED BY
      'password' WITH GRANT OPTION;

3) Reemplace el fichero exam.cls de su distribuci�n de LaTeX (en
Debian puede encontrarlo en
/usr/share/texmf-texlive/tex/latex/exam/exam.cls) por el fichero
facilitado por esta aplicaci�n (situado en el directorio LaTeX).

4) Ejecute editQuiz.py para cargar la herramienta de creaci�n.

Para m�s informaci�n, contacte con el desarrollor principal en:

    luisj.peris@alu.uclm.es

Luis Javier Peris Morillo, Desarollador principal

